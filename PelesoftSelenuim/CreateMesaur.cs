﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PelesoftSelenuim
{
     class CreateMesaur:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();
        SucessEndCreate sec = new SucessEndCreate();
        public CreateMesaur()
        {
        }

        public void checkElmentStep1(string typeMadd)
        {
            fp.checkCreate("מדד");
            cc.checkStepHeder(1, 4);

            checkCard("PieGraph", "pie_chart", "פאי");
            checkCard("LineGraph", "stacked_line_chart", "גרף קווי");
            checkCard("CoulmnsGraph", "assessmentt", "גרף עמודות");
            checkCard("Table", "table_chart", "טבלה");
            checkCard("TakeCareCard", "aspect_ratio", "כרטיס");
            driver.FindElement(By.XPath("//div[@class='type-payment'][text()='" +  typeMadd+ "']")).Click();
            /*string[] titles = { "מאפייני מסמך PDF" };
            fp.checkTitles(titles);*/
            //throw new NotImplementedException();
        }
        public void checkCard(string type, string nameIcon, string nameEBRUE)
        {
            fp.checkElment("check " + type, By.XPath("//div[@aria-label='divChartType" + type + "']"));
            fp.checkElment("check " + type + " img", By.XPath("//mat-icon[text()='" + nameIcon + "']"));
            fp.checkElment("check " + type + " name", By.XPath("//div[@class='type-payment'][text()='" + nameEBRUE + "']"));
        }
        public void checkElmentStep2(int numStepHeder=4)
        {
            fp.checkCreate("מדד");
            cc.checkStepHeder(2, numStepHeder);
            string[] titles = { "הגדרות כלליות" };
             fp.checkTitles(titles);
            fp.checkElmentText("title", By.Name("Title"), "כותרת");
            fp.checkElmentText("data", By.TagName("textarea"), "תאור");
            fp.checkSelectPreform("פעיל", By.TagName(""));
        }
        public void insertElmentStep2(string title= "אוטומציה")
        {
            fp.insertElment("title", By.Name("Title"),title,true);
            fp.insertElment("data", By.TagName("textarea"),automation);
            fp.selectForRequerd("פעיל",false,0);
            fp.ClickButton(" לשלב הבא ", "span");
        }
        public void checkStep3All(int numStepHeder = 4)
        {
            fp.checkCreate("מדד");
            cc.checkStepHeder(3, numStepHeder);
            string[] titles = { "הגדרות טעינה" };
            fp.checkTitles(titles);
            fp.checkElmentText("check name Table", By.XPath("//mat-select[@aria-label='slctTableId']"), "שם טבלה");
            //throw new NotImplementedException();
        }

        public void checkSailta(string typeData, string typeShilta, string equalsTo = "",int num=0,string typeSailtaMovna= "שווה ל",int equalsNum=0,int numlink=0,bool notProcess=true)
        {
            try
            {
                if (typeData != "")
                {
                    if (typeData != "false")
                    {
                        fp.selectForRequerd("בחר שדה", false, -1, "true", num);
                        fp.selectText(typeData);
                        Thread.Sleep(100);
                    }
                    if (typeSailtaMovna == "שווה ל")
                    {
                        By sailta = By.XPath("//mat-select/div/div/span/span[text()='" + typeSailtaMovna + "']");
                        fp.waitUntil(sailta);
                        try { driver.FindElements(sailta)[equalsNum].Click(); } catch { }
                        fp.selectText(typeShilta);
                    }
                    else if (typeSailtaMovna == "סוג עדכון")
                    {
                        fp.click(By.XPath("//mat-icon[text()=' drag_handle']"));
                        if (typeShilta== "low_priority")
                        fp.click(By.XPath("//mat-icon[text()='"+typeShilta+"']"));
                        else
                        fp.click(By.XPath("//mat-icon[text()='drag_handle']"));

                    }
                    else if (typeSailtaMovna == typeData)
                    {
                        if (driver.FindElements(By.XPath("//i[text()=' add ']")).Count > 0 && equalsTo == "")
                        {
                            try { fp.click(By.CssSelector("div[class='wrap-add-value ng-star-inserted']")); } catch { }
                            try { driver.FindElements(By.CssSelector("button[aria-label='menu']"))[2].SendKeys(Keys.Tab + Keys.Enter + typeShilta); } catch { }
                            fp.click(By.CssSelector("div[class='wrap-done-value ng-star-inserted']"));
                        }
                        else
                        {
                            fp.click(By.XPath("//mat-icon[text()=' drag_handle']"), equalsNum);
                            fp.click(By.XPath("//mat-icon[text()='" + typeShilta + "']"));
                        }
                    }
                    if (equalsTo != "")
                    {
                        if (equalsTo.Contains("TEXT"))
                        {
                            char[] text = { 'T', 'E', 'X', 'T' };
                            By sailta = By.XPath("//span[text()='" + typeShilta.Trim() + "']");
                            try {if (!typeShilta.Contains("שווה ל"))
                                    equalsNum = 0;
                                        driver.FindElements(sailta)[equalsNum].Click(); } catch { try { driver.FindElements(sailta)[0].Click(); } catch { } }
                            new Actions(driver).MoveToElement(driver.FindElement(By.XPath("//span[text()='" + typeShilta + "']"))).SendKeys(Keys.Tab + Keys.Enter + equalsTo.Split(text)[0]).Perform();
                        }
                        else if (typeShilta.Contains("עד לפני"))
                        {
                            fp.ClickButton(" בחר תאריך", "span");
                            fp.click(By.ClassName("mat-radio-container"), 0);
                            int numeq = fp.splitNumber(equalsTo);
                            try { driver.FindElement(By.XPath("//div[@class='mat-radio-label-content']/div/mat-form-field/div/div/div/input")).SendKeys(numeq.ToString()); } catch { }
                            driver.FindElement(By.XPath("//mat-select[contains(@class,'selectTypeTime ')]")).Click();
                            fp.selectText(equalsTo.Replace(numeq.ToString(), ""));
                            fp.ClickButton("אישור", "span");
                        }
                        else if (equalsTo.Contains("+"))
                        {
                            char[] tav = { '+' };
                            fp.insertElment("calnder start", By.Name("trip-start"), equalsTo.Split(tav)[0], false, "", 0);
                            fp.insertElment("calnder end", By.Name("trip-start"), equalsTo.Split(tav)[1], false, "", 1);
                        }
                        else
                        {
                            try
                            {
                                if (equalsTo.Contains(","))
                                {
                                    char[] tav = { ',' };
                                    string[] equalsS = equalsTo.Split(tav);
                                    int i = 0;
                                    filterSelectValue(typeSailtaMovna, typeShilta, equalsS[i], numlink, typeData);
                                    for (int j=i; j < equalsS.Length; j++)
                                    {
                                        fp.selectText(equalsS[j]);
                                    }
                                    new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
                                    Thread.Sleep(200);
                                }
                                else
                                    filterSelectValue(typeSailtaMovna, typeShilta, equalsTo, numlink, typeData);

                            }
                            catch
                            {
                                //fp.click(By.XPath("//div[@class='wrap-add-value ng-star-inserted']/i[@class='material-icons button-icon-add ng-star-inserted'][text()=' add ']"));
                                //driver.FindElement(By.CssSelector("button[aria-label='menu']")).SendKeys(Keys.Tab+Keys.Enter);
                                //try { new Actions(driver).MoveToElement(driver.FindElement(By.XPath("//div[@class='wrap-add-value ng-star-inserted']/i[@class='material-icons button-icon-add ng-star-inserted'][text()=' add ']"))).SendKeys(Keys.Tab + Keys.Enter).Perform(); } catch { }
                                try { driver.FindElement(By.XPath("//span[text()='" + typeData + "']")).SendKeys(Keys.Tab + Keys.Tab + Keys.Enter); } catch { }
                                fp.selectText(equalsTo);
                                //fp.click(By.XPath("//i[text()=' done ']"));
                            }

                        }
                    }
                    if (notProcess ==true)
                    {
                        fp.checkElmentText("check option or", By.CssSelector("div[class='OrOp ng-star-inserted']"), "אם");
                    }
                }
            }
            catch { fp.picter("checkSailta"); throw new Exception(); }
        }
/// <summary>
/// סינון על טבלת בן
/// </summary>
/// <param name="ben">מזינים את כל הפילטר</param>
/// <param name="flagCondtions">איזה סוג הפילטר על טבלת בן</param>
        public  void filterBen(string[] ben,int flagCondtions=0,int save=2,int numEquals=1)
        {
            try { fp.ClickButton("הוסף סינון על טבלת בן ", "span");
                fp.checkElmentText("select table",By.TagName("p"), "בחר טבלה");
                //fp.selectForRequerd("בחר טבלה ",false,-1,"true",1);
                int p=driver.FindElements(By.TagName("mat-select")).Count;
                driver.FindElements(By.TagName("mat-select"))[p-1].Click();
                fp.selectText(ben[0]);
                fp.ClickButton(" שמירה ", "span",save);
                fp.selectForRequerd("כאשר מתקיימים התנאים הבאים",false,0);
                if(flagCondtions!=2)
                 fp.click(By.TagName("mat-expansion-panel-header"), flagCondtions);
                else
                {
                    fp.click(By.TagName("mat-expansion-panel-header"), 0);
                    fp.click(By.TagName("mat-expansion-panel-header"), 1);
                    var element = driver.FindElements(By.XPath("//mat-label[text()='בחר שדה']"));
                    Actions actions = new Actions(driver);
                    actions.MoveToElement(element[2]);
                    try { actions.Perform(); } catch { }
                    checkSailta(ben[5],ben[6],ben[7],2,"שווה ל",numEquals);
                }
                checkSailta(ben[1], ben[2], ben[3],1,"שווה ל",numEquals);
                if(flagCondtions!=1)
                { fp.click(By.CssSelector("mat-select[aria-label='בחר פונקציה']"));
                    fp.selectText(ben[4]);
                    checkSailta("false", ben[2], ben[3],1,"שווה ל",1);
                }

            } catch { throw new NotImplementedException(); }
        }

        private void filterSelectValue(string typeSailtaMovna, string typeShilta, string equalsTo, int numlink, string typeData)
        {
            try
            {
                if (typeSailtaMovna == "שווה ל")
                {
                    new Actions(driver).MoveToElement(driver.FindElement(By.XPath("//span[text()='" + typeShilta + "']"))).SendKeys(Keys.Tab + Keys.Enter).Perform();
                    fp.selectText(equalsTo);
                }
                else
                {
                    driver.FindElements(By.CssSelector("button[aria-label='menu']"))[numlink].SendKeys(Keys.Tab + Keys.Enter);
                    fp.selectText(equalsTo);
                }
            }
            catch
            {
                try { driver.FindElement(By.XPath("//span[text()='" + typeData + "']")).SendKeys(Keys.Tab + Keys.Tab + Keys.Enter); } catch { }
                fp.selectText(equalsTo);
            }
        }
        public void checkBranchAnd(bool clickBranch = false, bool clickAnd = false, int mikom = -1)
        {
            IList<IWebElement> condition = driver.FindElements(By.TagName("app-condition"));
            int numCondition = condition.Count;
            fp.checkElment("check icon trash", By.ClassName("icon-trash"));
            fp.checkElmentsText("check option", By.TagName("option"), "וגם", 0);
            if (clickAnd == true)
            {
                IWebElement clickAndM = driver.FindElements(By.TagName("select"))[mikom];
                clickAndM.FindElement(By.TagName("option")).Click();
                clickAndM.FindElements(By.TagName("option"))[1].Click();
            }
            if (clickBranch == true)
            {
            new Actions(driver).MoveToElement(driver.FindElements(By.CssSelector("div[class='OrOp ng-star-inserted']"))[0]).Perform();

                driver.FindElements(By.ClassName("icon-trash"))[mikom].Click();
                fp.waitUntil(By.TagName("mat-dialog-container"));
                fp.checkElmentText("check title approve", By.ClassName("title"), " אשר פעולה ");
                fp.checkElmentText("check message aprrove branch", By.ClassName("wrap-message"), "האם אתה בטוח שברצונך למחוק את התנאי");
                fp.checkElmentsText("check button no", By.ClassName("mat-button-wrapper"), "לא", 0);
                fp.checkElmentsText("check button yes", By.ClassName("mat-button-wrapper"), "כן", 1);
                fp.ClickButton("כן", "span");
                if (numCondition - 1 != driver.FindElements(By.TagName("app-condition")).Count)
                {
                    Debug.WriteLine("the remove not performed");
                }
            }

        }
        public void checkStep4(int numStep = 4)
        {
            fp.checkCreate("מדד");
            cc.checkStepHeder(4, numStep);

        }

        public void stepPai(string selectTable,string typeSelect,string selectTypeM,string[] insertShilta,string typeAmount="",string addFilter="",string dateStop="")
        {
            string heder = selectTable;
            char[] g = {',' };
            fp.actionWork(" הגדרות ","מדדים");
           // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'],[text()=' יצירת מדד חדש ']")).Click();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Thread.Sleep(600);
            fp.ButtonNew("מדד");
            checkElmentStep1("פאי");
            checkElmentStep2();
            if (selectTable.Contains(","))
            {
                heder = selectTable.Split(g)[0];
                selectTable= selectTable.Split(g)[1];
            }
            insertElmentStep2(heder);
            checkStep3Pai();
            insertStep3Pai(selectTable,typeSelect,selectTypeM,insertShilta,typeAmount) ;
            fp.ClickButton(" לשלב הבא ", "span");
            checkStep4Pai();
            insertStep4Pai(addFilter,dateStop);
            sec.checkEndCreate("מדד", false);

        }
        public void stepGrafLine(string selectTable, string typeSelect, string selectTypeM, string[] insertShilta, string insertGroup, string typeScom = "",string addFilter="",string dateStop="",bool desplay=true)
        {
            fp.actionWork(" הגדרות ","מדדים");
            // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'],[text()=' יצירת מדד חדש ']")).Click();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Thread.Sleep(1800);
            fp.ButtonNew("מדד");
            checkElmentStep1("גרף קווי");
            checkElmentStep2();
            insertElmentStep2(selectTable);
            checkStep3GrafLine();
            insertStep3GrafhLine(selectTable,typeSelect,selectTypeM,insertShilta,insertGroup,typeScom);
            fp.ClickButton(" לשלב הבא ", "span");
            checkStep4Pai(desplay);
            insertStep4Pai(addFilter,dateStop);

        }
        public void stepGrafColumn(string selectTable, string typeSelect, string selectTypeM, string[] insertShilta, string insertGroup, string typeScom = "", string addFilter = "", string dateStop = "", bool desplay = true)
        {
            fp.actionWork(" הגדרות ","מדדים");
            // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'],[text()=' יצירת מדד חדש ']")).Click();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Thread.Sleep(800);
            fp.ButtonNew("מדד");
            checkElmentStep1("גרף עמודות");
            checkElmentStep2();
            insertElmentStep2(selectTable);
            checkStep3GrafLine();
            insertStep3GrafhLine(selectTable, typeSelect, selectTypeM, insertShilta, insertGroup, typeScom);
            fp.ClickButton(" לשלב הבא ", "span");
            checkStep4Pai(desplay);
            insertStep4Pai(addFilter, dateStop);

        } public void stepGrafTable(string[] insertShilta,   string addFilter = "", string dateStop = "", bool desplay = true)
        {
            fp.actionWork(" הגדרות ","מדדים");
            // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'],[text()=' יצירת מדד חדש ']")).Click();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Thread.Sleep(1300);
            fp.ButtonNew("מדד");
            checkElmentStep1("טבלה");
            checkElmentStep2(5);
            insertElmentStep2(" היסטורית פעולות ");
            checkStep3All(5);
            insertStep3Table(" היסטורית פעולות ", insertShilta);
            fp.ClickButton(" לשלב הבא ", "span");
            checkStep4Table();
            insertStep4Table();
            checkStep4Pai(desplay,5);
            insertStep4Pai(addFilter, dateStop);

        }
        public void stepCard(string selectTable, string selectTypeM, string[] insertShilta,string namePicter, string typeScom = "", string addFilter = "", string dateStop = "", bool desplay = true,string heder="",string befor="אוטו",string after="אוטו")
        {
            fp.actionWork(" הגדרות ","מדדים");
            // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'],[text()=' יצירת מדד חדש ']")).Click();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Thread.Sleep(1800);
            fp.ButtonNew("מדד");
            checkElmentStep1("כרטיס");
            checkElmentStep2(5);
            if (heder == "")
                heder = selectTable;
            insertElmentStep2(heder);
            checkStep3Card();
            insertStep3Card(selectTable, insertShilta, selectTypeM, typeScom);
            fp.ClickButton(" לשלב הבא ", "span");
            checkStep4Card();
            insertStep4Card(namePicter,befor,after);
            checkStep4Pai(desplay, 5);
            insertStep4Pai(addFilter, dateStop);
        }

        

        public  void insertStep3GrafhLine(string selectTable,string typeSelect,string selectTypeM,string []insertShilta,string insertGroup, string typeScom="")
        {
            insertStep3Pai(selectTable, typeSelect, selectTypeM,insertShilta,typeScom);
            Thread.Sleep(100);
            fp.SelectRequerdName("aria-label='slctGroupingField",insertGroup,false);
        }

        public void checkStep3GrafLine()
        {
            checkStep3All();
            fp.checkSelectPreform("עמודה לפילוח הנתונים");
            fp.checkSelectPreform("מה תרצה לחשב");
            fp.checkElmentText("fildes graph axis", By.XPath("//mat-select[@aria-label='slctGroupingField']"), "שדה לציר הגרף ");
        }
        public void checkStep3Pai()
        {
            checkStep3All();
            fp.checkSelectPreform("עמודה לפילוח הנתונים");
            fp.checkSelectPreform("מה תרצה לחשב");
        }
        public void checkStep3Card()
        {
            checkStep3All(5);
            fp.checkSelectPreform("מה תרצה לחשב");
        }

        public void insertStep3Pai(string selectTable,string typeSelect,string selectTypeM,string []inserShilta,string typeScom="")
        {

            insertStep3Table(selectTable, inserShilta);
            fp.SelectRequerdName("aria-label='slctSegmentationField", typeSelect,false);
            Thread.Sleep(100);
            fp.SelectRequerdName("aria-label='slctMeasuringType", selectTypeM, false);
            if (typeScom!="")
            {
                fp.SelectRequerdName("aria-label='slctMeasuringField", typeScom);
            }
            

        }
        //להחזיר את הסינון
        public void insertStep3Table(string selectTable,string [] inserShilta)
        {
            try
            {
                fp.SelectRequerdName("aria-label='slctTableId", selectTable, false, true);
                //fp.checkSelectPreform("בחר שדה");
                int j = 0;
                if (inserShilta[0] != "-1")
                {
                    fp.ClickButton("סינון הנתונים","span");
                    for (int i = 0; i < inserShilta.Length; i = i + 3)
                    {
                        checkSailta(inserShilta[i], inserShilta[i + 1], inserShilta[i + 2]);// );
                        if (inserShilta[i + 3] == "-1")
                            break;
                        j++;
                    }
                    //checkSailta();
                    checkBranchAnd(true, true, 0);
                    fp.ClickButton("שמירה","span");
                }
                else
                    fp.ClickButton("שמירה", "span");

            }
            catch { }
                
        }
        public void insertStep3Card(string selectTable, string[] insertShilta, string selectTypeM, string typeScom = "")
        {
            insertStep3Table(selectTable, insertShilta);
            fp.SelectRequerdName("aria-label='slctMeasuringType", selectTypeM, false);
            if (typeScom != "")
            {
                fp.SelectRequerdName("aria-label='slctMeasuringField", typeScom);
            }
        }
        public void insertStep4Pai(string addFilter,string dateStop,string typeDate= "ללא הגבלה")
        {
            Thread.Sleep(100);
            fp.SelectRequerdName("aria-label='slctInnerFilter", addFilter,false);
            Thread.Sleep(100);
            if(!dateStop.Contains("false"))
            {
                //Console.WriteLine(driver.FindElement(By.XPath("//mat-Select[@aria-label='slctFilterDateField']")).GetAttribute("aria-disabled"));
                if (driver.FindElement(By.XPath("//mat-Select[@aria-label='slctFilterDateField']")).GetAttribute("aria-disabled").Equals("false"))//.Contains("aria-disabled='false'"))
                      fp.SelectRequerdName("aria-label='slctFilterDateField", dateStop, false);
                Thread.Sleep(400);
                if (!dateStop.Contains("ללא"))
                    fp.SelectRequerdName("aria-label='slctDatesRange", typeDate, false);
                if(typeDate== " מותאם אישית ")
                {
                    fp.checkElment("from date", By.XPath("//app-long-date[@txtlabel='מתאריך']"));
                    fp.checkElment("to date", By.XPath("//app-long-date[@txtlabel='עד תאריך']"));
                    createConatrabition cc = new createConatrabition();
                    cc.calanederCheck(-1, 0, "10", -2, -1);
                    cc.calanederCheck(-1, 1, "", 2, 1);
                }
            }
           
           fp.ClickButton(" צור מדד חדש ", "span");
        }

        public void checkStep4Pai(bool desplay=true,int numStep=4)
        {
            if (numStep ==5)
                 cc.checkStepHeder(5, numStep);
            else
              checkStep4();
            string[] titles = { "סינו נוסף","הגבלות תאריך" };
            fp.checkTitles(titles);
            if(desplay==true)
            {
                fp.checkElmentText("check select date end", By.XPath("//mat-select[@aria-label='slctFilterDateField']"), "עמודת תאריך להגבלה ");
               // fp.checkElmentText("check select date range", By.XPath("//mat-select[@aria-label='slctDatesRange']"), "התקופה לפילוח הנתונים");
            }
            fp.checkElmentText("check select diname", By.XPath("//mat-select[@aria-label='slctInnerFilter']"), "עמודה לסינון דינאמי ");
           
        }
        public void checkStep4Table()
        {
            checkStep4(5);
            string[] titles = { "הגדרות נוספות" };
            fp.checkTitles(titles);
            fp.checkElmentText("check element display table", By.XPath("//mat-select[@aria-label='slctColumns']"), "שדות שיוצגו בטבלה");
            fp.checkElmentText("check sum line display", By.XPath("//input[@formcontrolname='NumberOfRows']"), "מספר השורות שיוצגו");
        }
        public void insertStep4Table()
        {
            fp.SelectRequerdName("aria-label='slctColumns", " משתמש ",false);
            fp.selectText(" תאריך ");
            fp.selectText(" הערות ");
            // driver.FindElements(By.ClassName("mat-option-text"))[1].Click();
            // driver.FindElements(By.ClassName("mat-option-text"))[1].SendKeys(Keys.Tab);
            new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[1]).SendKeys(Keys.Tab).Perform();

            fp.insertElment("insert sum line display", By.XPath("//input[@formcontrolname='NumberOfRows']"), "25",true);
            fp.ClickButton(" לשלב הבא ", "span");
        }
        public void checkStep4Card()
        {
            checkStep4(5);
            string[] titles = { "הגדרות נוספות" };
            fp.checkTitles(titles);
            fp.checkElmentText("check if dispaly sum", By.XPath("//mat-select[@aria-label='slctShowFromTotal']"), "האם להציג סכום כולל (ללא סינון הנתונים )");
            fp.checkElmentText("check cpation befor sum", By.XPath("//input[@aria-label='inputTextBeforeValue']"), "כיתוב לפני הסכום");
            fp.checkElmentText("check cpation after sum", By.XPath("//input[@aria-label='inputTextAfterValue']"), "כיתוב אחרי הסכום");
            fp.checkElmentText("check image", By.TagName("mat-panel-title"), "בחר תמונה לתצוגה");
            // throw new NotImplementedException();
        }
        public void insertStep4Card(string namePicter,string befor= "אוטו", string after= "אוטו")
        {
            fp.SelectRequerdName("aria-label='slctShowFromTotal", " כן ",false);
            fp.insertElment("check cpation befor sum", By.XPath("//input[@aria-label='inputTextBeforeValue']"),befor );
            fp.insertElment("check cpation after sum", By.XPath("//input[@aria-label='inputTextAfterValue']"), after);
            clickNamePicter(namePicter);
            // fp.SelectRequerdName("aria-label='slctImg", namePicter);
            fp.ClickButton(" לשלב הבא ", "span");
        }
        public void clickNamePicter(string namePictr)
        {
            driver.FindElement(By.TagName("mat-expansion-panel")).Click();
            IList<IWebElement> icon = driver.FindElements(By.TagName("mat-icon"));
            for (int i = 0; i < icon.Count; i++)
            {
                if (icon[i].Text.Contains(namePictr))
                {
                    icon[i].Click();
                    break;
                }
            }
        }
        public  void checkClassMadd(string nameTitle, string nameButton,string typGraph,bool miniCard=false)
        {
            fp.checkElment("check Card Madd", By.TagName("mat-grid-tile"));
            fp.checkElment("check title Madd", By.TagName("mat-card-title"));
            IList<IWebElement> card = driver.FindElements(By.TagName("mat-card"));
           // IList<IWebElement> titles = card[0].FindElements(By.TagName("h3"));
            int i=1;
           /* if (miniCard == false)
            {
                i = driver.FindElements(By.TagName("app-mini-card")).Count;
            }*/
                for (int j=0; j<card.Count;j++)
            {
                if(card[j].FindElement(By.TagName("h3")).Text.Contains(nameTitle))
                {
                    Console.WriteLine("check canvas madd title" + nameTitle);
                    i = j;
                    break;
                }
                if(j+1==card.Count)
                {
                    Debug.WriteLine("titleCard notDesplay "+nameTitle);
                    break;
                }
            }

            if (driver.FindElements(By.ClassName("link-to-table"))[i].Text.Contains(nameButton))
            {
                Console.WriteLine("the button to table desplay " + nameButton);
            }
            else
                Debug.WriteLine("the button on class madd not desplay " + nameButton);
            try
            {
                int count = driver.FindElements(By.CssSelector("div[class='no-data-msg ng-star-inserted']")).Count;
                i = card.Count - count;
                if (driver.FindElements(By.TagName("canvas"))[i].Displayed)
                    Console.WriteLine("the canvas desplay");
            }
            catch { 
                Debug.WriteLine("the canvas not desplay");
            }
            /* if (i == 0)
             {
                 if (driver.FindElement(By.XPath("//app-" + typGraph)).Displayed)
                     Console.WriteLine("the type graph good");
             }
             else*/
            if (card[i].FindElement(By.XPath("//mat-card-content/app-" + typGraph)).Displayed)
                Console.WriteLine("the type graph good");
           
            //  chart.canvas.parentNode.style.height= By.TagName("canvas"))[i].GetProperty("width");
           // Console.WriteLine(driver.FindElements(By.TagName("canvas"))[i].GetProperty("width"));
          //  Console.WriteLine(driver.FindElements(By.TagName("canvas"))[i].GetProperty("height"));
           
          //  throw new NotImplementedException();
        }
        public void stepFilterBen(string[] ben)
        {
            try
            {
                fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
                filterBen(ben, 2, 1,0);
                checkfilterben(ben[0], 1);
            }
            catch { throw new NotImplementedException(); }
        }
        public void checkfilterben(string nameHeder, int countRowBen)
        {
            try
            {
                fp.ClickButton("שמירה ", "span",2) ;
                fp.ClickButton("שמירה ", "span",2) ;
                PupilNew pn = new PupilNew();
                Thread.Sleep(750);
                pn.clickTableRandom();
                popupList pl = new popupList();
                pl.popupEdit();
                pl.clickStepHeder(-1, nameHeder);
                if (fp.splitNumber(driver.FindElement(By.CssSelector("div[class='row-number-txt ng-star-inserted']")).Text) < countRowBen)
                {
                    fp.picter("filter ben not good");
                    throw new Exception();
                }
                fp.closePoup();
            }
            catch { throw new NotImplementedException(); }
        }
        public void openTable(string nameButton,string typeGraph)
        {
            fp.ClickButton("לביטול הסינון");
            int mikom=0;
           IList< IWebElement> buGrapTable = driver.FindElements(By.ClassName("link-to-table"));
            for(int i=0;i<buGrapTable.Count;i++)
            {
                if (buGrapTable[i].Text.Contains(nameButton))
                {
                    if (driver.FindElements(By.TagName("mat-card")).Count > 1)
                    {
                        try { mikom = i -(driver.FindElements(By.TagName("app-mini-card")).Count-1); }
                        catch { mikom = i-1; }
                    }  
                    else
                        mikom = i-1;
                    try
                    {
                        if (driver.FindElements(By.TagName("mat-card"))[mikom].FindElement(By.XPath("//mat-card-content/app-" + typeGraph)).Displayed)
                        {
                            buGrapTable[i].Click();
                            break;
                        }
                    }
                    catch { }
                }
            }
            fp.waitUntil(By.TagName("mat-table"));
            fp.checkElmentText("title page table", By.ClassName("main-txt"), "ה" + nameButton + " שלי");
           // throw new NotImplementedException();
        }

        public void chekmini(string nameMIMI, string nameButton)
        {
            fp.checkElment("app mini card", By.TagName("mat-card"));
            IList<IWebElement> titles = driver.FindElements(By.TagName("h3"));
            IList<IWebElement> Butn = driver.FindElements(By.ClassName("mat-button-wrapper"));
            int countCheck=0;
            for (int i=0;i<Butn.Count;i++)
            {
                if (driver.FindElements(By.ClassName("link-to-table"))[i].Text.Contains(nameButton))
                {
                    for (int j = 0; j < titles.Count; j++)
                    {
                        if (titles[j].Text.Contains(nameMIMI))
                        {
                            countCheck = Int32.Parse(driver.FindElements(By.TagName("app-mini-card"))[j].FindElement(By.XPath("//div/span[4]/label")).Text);
                            Console.WriteLine("check title and button from mini card desplay");
                            driver.FindElements(By.ClassName("link-to-table"))[i].Click();
                            break;
                        }
                        if (titles.Count - 1 == j)
                        {
                            Debug.WriteLine("check title and button card mini card not dispaly");
                        }

                    }
                    if(countCheck!=0)
                        break;
                }
            }
            //fp.waitUntil(By.TagName("mat-table"));
            fp.checkElmentText("title page table", By.ClassName("main-txt"), "ה" + nameButton + " שלי");
            if (countCheck == 0)
            {
                if (driver.FindElement(By.CssSelector("div[class='no-data-msg ng-star-inserted']")).Text.Contains(" לא נמצאו נתונים"))
                    Console.WriteLine("the data display good");
            }
            else
            {
                if (driver.FindElements(By.TagName("mat-row")).Count != countCheck)
                { Debug.WriteLine("the count row in table (" + driver.FindElements(By.TagName("mat-row")).Count + ") not equals from dasebord(" + countCheck + ")"); }
                else
                    Console.WriteLine("the count row in table is equals from dasbord");
            }
            //  throw new NotImplementedException();
        }
    }
}