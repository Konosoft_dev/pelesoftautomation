﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class EducationalInstitution:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public EducationalInstitution()
        {
        }

        public void openPageEI()
        {
            fp.clickNameDefinitions(" מוסדות ");
            fp.clickButoonToContains("מוסדות חינוך");
        }

        public void createEI(string EducationInstitutesName = "אוטומציה", string type = "גן ילדים",string adres="אוטומציה",int num=1)
        {
            try
            {
                CreateNewDenumic cnd = new CreateNewDenumic();
                if (EducationInstitutesName != "סניף ")
                {
                    fp.ButtonNew(MosadChinuc);
                    cnd.checkClasscreate(MosadChinuc);
                }
                checkElmentPage(EducationInstitutesName);
                insertElmentPage(EducationInstitutesName,type,adres,num);
            }
            catch { throw new NotImplementedException("create EI invaled"); }
        }

        public  void insertElmentPage(string EducationInstitutesName,string type,string address="אוטומציה",int num=1)
        {
              try
             {
                if (EducationInstitutesName != "סניף ")
                {
                    fp.checkElment("name institution", By.Name("EducationInstitutesName"), false, true);
                    fp.insertElment("name institution", By.Name("EducationInstitutesName"), EducationInstitutesName);
                    //fp.checkElment("EducationInstitutesType", By.XPath("//mat-select[@ng-reflect-name='EducationInstitutesType']"), false, true);
                    fp.selectForRequerd("בחר/י סוג מוסד עבור המוסד חינוך", true, -1);//driver.FindElement(By.XPath("//mat-select[@ng-reflect-name='EducationInstitutesType']")).Click();
                    fp.selectText(type);
                    fp.insertElment("select menager", By.Name("ManagerID"),names[fp.rand(names.Length)]);

                    EducationInstitutesName = "";
                }
                else
                {
                    fp.insertElment("name branch", By.Name("EducationInstitutesBranchName"), type);
                }
                //driver.FindElement(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']")).Click();
                //fp.selectForRequerd("הזן ערך נוסף לרשימה",false,-1,automation);
                 fp.insertElment("adress", By.Name("Address"),address);
                Thread.Sleep(200);
                fp.selectForRequerd("עיר", false, num);
               // fp.selectForRequerd("עיר", false, num);
                 fp.selectForRequerd("אופן גביה שונה עבור תשלום חד\"פ", false, -1);//driver.FindElement( By.XPath("//mat-select[@ng-reflect-name='SinglePaymentsBillingSeparatel']")).Click();
                 fp.selectText("כן");
                    fp.ClickButton(" צור "+ EducationInstitutesName + "מוסד חינוך חדש ");
            }
            catch { throw new NotImplementedException("create EI invaled"); }

        }

        public  void stepCreate(string []EI,string []type)
        {
            for (int i = 0; i < EI.Length; i++)
            {
                string address = Streets[fp.rand(Streets.Length)];
                int num = fp.rand(city.Length);
                createEI(EI[i], type[i], address,num);
                 address = Streets[fp.rand(Streets.Length)];
                addBranch(address,type[i+1],num);

                //sec.checkEndCreate(p.MosadChinuc);
                //fp.closePoup();
            }
        }

        public  void addBranch(string  adress,string name,int numCity)
        {
            popupList pl = new popupList();
            driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' קח אותי לכרטיס " + MosadChinuc + " ']")).Click();
            Thread.Sleep(200);
            pl.clickStepHeder(4, "סניפי מוסדות חינוך");
            fp.ClickButton(" הוספת סניף מוסד חינוך ", "span");
            createEI("סניף ", name, adress, numCity);

        }

        public void checkElmentPage(string EducationInstitutesName="")
        {
            try
            {
                if(EducationInstitutesName!="סניף ")
                {
                    fp.checkElmentsText("title mosad", By.ClassName("title-section"), "פרטי מוסד",0);
                    fp.checkElmentText("name institution", By.Name("EducationInstitutesName"), "שם מוסד");
                   // fp.checkElmentText("select city", By.XPath("//mat-select[@ng-reflect-name='EducationInstitutesType']"), "בחר/י סוג מוסד עבור המוסד חינוך");
                    fp.checkElmentText("select menager", By.Name("ManagerID"),"מנהל");
                    fp.checkElmentsText("title mosad", By.ClassName("title-section"), "כתובת מוסד", 1);
                    //fp.checkElmentText("select city", By.XPath("//mat-select[@ng-reflect-name='City']"), "בחר/י עיר עבור המוסד חינוך");
                     fp.checkElmentsText("title mosad", By.ClassName("title-section"), "פרטים נוספים", 2);

                }
                fp.checkElmentText("adress", By.Name("Address"), " כתובת ");
                //fp.checkElmentText("select city", By.XPath("//mat-select[@ng-reflect-name='SinglePaymentsBillingSeparatel']"), "לא");
                fp.checkElmentText("button create mosad", By.ClassName("mat-button-wrapper"), " צור מוסד חינוך חדש ");
            }
            catch { throw new NotImplementedException("checkElmentPage envaled"); }
        }

        public  void clickOnTable(string nm="אוטומציה", string tm= "גן ילדים")
        {
            try
            {
                string[] listHeder = { "שם מוסד","סוג מוסד","מנהל","כתובת","עיר","קופה","-1"};
                fp.checkColumn("//", listHeder, "false","mat-header-row", "mat-header-cell");
                string[] listRow = {"",nm, "-1" };//tm,
                if(nm == "אוטומציה")
                { PupilNew pn = new PupilNew();pn.clickTableRandom(); }
                fp.checkColumn("//", listRow, "", "mat-row"/*[role='row']"*/, "mat-cell"/*[role='gridcell']"*/);
            }
             catch { throw new NotImplementedException("checkElmentPage envaled"); }
        }
    }
}