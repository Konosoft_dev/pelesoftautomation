﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
    internal class CreditClause:page
    {
        public CreditClause()
        {
        }
        FunctionPelesoft fp = new FunctionPelesoft();
        Financical fl = new Financical();
        public  void stepClauses(string model, string cardCreate, string textClauses)
        {
            fl.createNewFinancicalSection(cardCreate, model, textClauses, " זיכוי ");
            fp.closePoup();
            fp.clickNameDefinitions(model);
            fp.clickButoonToContains(cardCreate);
            EducationalInstitution ei = new EducationalInstitution();
            ei.clickOnTable("", "");
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(4, "הגדרות סעיפי זיכוי");
            fp.ClickButton(" הוספת הגדרת סעיף זיכוי ");
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.checkClasscreate("הגדרת סעיף זיכוי");
            checkElment();
            insertElment(textClauses);
        }

        public void insertElment(string nameC,string typeClause="חד פעמי")
        {
            try {
                fp.insertElment("check name Clause", By.Name("ClauseName"), nameC);
                fp.selectForRequerd(" סוג סעיף זיכוי ", false);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        private void checkElment()
        {
            try {
                fp.checkElmentsText("sub title", By.ClassName("title-section"), "פרטי סעיף זיכוי", 0);
                fp.checkElmentsText("sub title", By.ClassName("title-section"), "פרטי זיכוי", 1);
                fp.checkElmentsText("sub title", By.ClassName("title-section"), "עבור", 2);
                fp.checkElmentsText("sub title", By.ClassName("title-section"), "פרטים נוספים", 3);
                fp.checkElmentText("check name Clause", By.Name("ClauseName"), " שם סעיף זיכוי ");
                fp.checkSelectPreform(" סוג סעיף זיכוי ");
                fp.checkSelectPreform(" סעיף פיננסי ");
                fp.checkElmentText("check sum clause", By.Name("Sum"), " סכום ");
                fp.checkSelectPreform(" מטבע ");
                fp.checkSelectPreform(" תדירות זיכוי ");
            }
            catch(System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }
    }
}