﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Keys = OpenQA.Selenium.Keys;

namespace PelesoftSelenuim
{
     class process:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();
        public void cehekstep1()
        {
            try {
                fp.checkElmentsText("title section", By.ClassName("title-section"), "כללי", 0);
                cc.checkStepHeder(1, 2);
                fp.checkElmentText("check EventsOnAddOrUpdateName", By.Name("EventsOnAddOrUpdateName"), " שם התהליך ");
               // fp.checkSelectPreform
            }
            catch { }
        }

        public void insertStep1(string nameProcess, string discreption, string selectTable, string selectTime,bool action =true)
        {
            fp.insertElment("name process", By.Name("EventsOnAddOrUpdateName"), nameProcess, true);
            if(action==false)
                  fp.selectForRequerd(" פעיל ", false);
            fp.insertElment("discreption", By.TagName("textarea"), discreption);
            if (selectTable != "")
            {
                fp.selectForRequerd(" טבלה ", false, -1);
                fp.selectText(selectTable, true);
            }
            fp.selectForRequerd(" תזמון ", true, -1);
            fp.selectText(selectTime);
            fp.ClickButton(" לשלב הבא ");
        }
        /// <summary>
        /// להחזיר את כל המיורקים בפעם הבאה
        /// /// </summary>
        /// <param name="filter"></param>
        /// <param name="numFilter"></param>
        /// <param name="orAndAdd"></param>
        /// <param name="equalsNum"></param>
        /// <param name="typeButton"></param>
        public void stepFrainLast(string[] filter,int numFilter=2,bool orAndAdd=false,int equalsNum=0,string typeButton= " לשלב הבא ",bool bouttonFilter=true,bool Process=false)
        {
            int m = 0;
            CreateMesaur cm = new CreateMesaur();
            Thread.Sleep(400);
            for (int i = 0; i < filter.Length&& filter.Length>2; i += 3)
            {
                if (equalsNum > 0 || m > 0)
                { equalsNum = m; m++; }
                if(bouttonFilter==true)
                    fp.ClickButton("סינון","span");
                cm.checkSailta(filter[i], filter[i + 1], filter[i + 2],0, "שווה ל", equalsNum,0,Process);
                fp.click(By.CssSelector("button[class='simple-button mat-button mat-button-base']"));//save
                if (numFilter == 2)
                {
                    fp.ClickButton("סינון","span",2);
                    cm.checkSailta(filter[3], filter[4], filter[5], 0,"שווה ל",0,0,Process);
                    fp.ClickButton("שמירה","span");
                    break;
                }
                if (orAndAdd == true&& m>0)
                    cm.checkBranchAnd(false,true,m-1);
                
            }
            fp.ClickButton(typeButton,"span");

        }

        public  void step3(string selectAction, string v2, string v3, string v4,bool gil=false,int selectGil=-1,bool createProcess=true)
        {
            try {
                fp.ClickButton("הוספת פעולה ");
                checkElmentAction();
                insertElmentAction(selectAction,v2, v3, v4,gil, selectGil);
                //   fp.SelectRequerdName("formcontrolname='ActionID", selectAction, true, true);
                if (createProcess == true)
                {
                    driver.FindElement(By.XPath("//button[@aria-label='btnSubmitCreateItem']")).Click();
                    SucessEndCreate sec = new SucessEndCreate();
                    sec.checkEndCreate("תהליך", false);
                }
            }
            catch { fp.picter("step3"+ selectAction); throw new NotImplementedException(); }
        }

        public void insertElmentAction(string selectAction, string v2, string v3, string v4, bool gil = false, int selectGil = -1)
        {
            CreateMesaur cm = new CreateMesaur();
            try
            {
                string catgory = "";
                if (selectAction.Contains("מייל") || selectAction.Contains("שליחת דוח תקופתי"))
                    catgory = " מיילים והודעות ";
                else if (selectAction.Contains("עדכון"))
                    catgory = " שינוי ערכים ";
                else if (selectAction.Contains("חיובים") || selectAction.Contains("נכשלים"))
                    catgory = " חיובים וגביות ";
                else if (selectAction.Contains("נדרים") || selectAction.Contains("היסטוריה"))
                    catgory = " כללי ";
                else if (selectAction.Contains("שנתון"))
                    catgory = "מערכת";
                else if (selectAction.Contains("יצירת"))
                    catgory = " יצירת רשומה ";
                fp.SelectRequerdName("aria-label='selectCategory", catgory,false);
                Thread.Sleep(200);
                fp.SelectRequerdName("aria-label='selectEventAction", selectAction,false);
                switch (selectAction.Trim())
                {
                    /*לא אמור להיות case "עדכון דתות מחושב":
                         break;*/
                    case "שליחת מייל":
                        mail(v2, v3, v4,gil);
                        break;
                    case "עדכון תאריך עברי":
                        date(v2, v3, v4, gil, selectGil);
                        break;
                    case "שליחת דוח תקופתי":
                        tkofa(v2, v3, v4, gil, selectGil);
                        break;
                    case "עדכון ערכים":
                        cm.checkSailta(v2, v3, v4, 0, "סוג עדכון",0,0,false);
                        break;
                    case "מחולל חיובים וביצוע הו\"ק":
                        processGenerator(v2,v3,v4);
                        break;
                    case "שידור חוזר לנכשלים":
                        returnFail(v2,v3,v4);

                        break;
                    case "יבוא נתונים ממערכת נדרים פלוס":
                        fp.selectForRequerd("בחר ישות סליקה",false,-1);
                        fp.selectText("מסב נדרים");
                        break;
                    case "יצירת ידיד":
                        createFrind(v2, v3);

                        break;
                    case "יצירת משימה":
                        createTasx(v2,v3,v4,gil,selectGil);
                        break;
                    case "יצירת הוצאה":
                        cm.checkSailta("false", "", v2, 0, "",0,0,false);
                        cm.checkSailta("false", "", v3, 0, "",0,1,false);
                        fp.insertElment("sum",By.Name("Sum"),v4);
                        break;
                    case "יצירת שנתון":
                        createYear(v2,v3,v4);
                        break;
                        /*לא אמור להיותcase "היסטורית פעולות":
                            break;*/
                }
                fp.ClickButton(" שמירת הפעולה ");
                fp.checkElmentText("actionName-span", By.ClassName("actionName-span"), selectAction);
                fp.checkElmentText("material-icons edit", By.ClassName("material-icons"), " edit ");
                fp.checkElment("material-icons delet", By.CssSelector("button[aria-label='delete']"));

            }
            catch { fp.picter("insertElmentAction "+selectAction); throw new NotImplementedException(); }
        }

        private void createYear(string v2, string v3, string v4)
        {
            try { fp.selectForRequerd("מעבר משנת לימודים:");
                fp.selectForRequerd("מעבר לשנת לימודים:");
                fp.selectForRequerd("מוסד לימודים",true,-1);
                fp.selectText(v2);
                fp.selectForRequerd("סניף",true,-1);
                fp.selectText(v2);
                fp.click(By.TagName("mat-step-header"),5);
                string[] hedar = { "שם רמה","גיל","-1"};
                fp.checkColumn("thead[role='rowgroup']",hedar,"false","tr","th");
                string[] row = { "כיתה א", "4", "-1" };
                fp.checkColumn("[]'", row, "","tr","td",-1,1);
                fp.click(By.CssSelector("mat-icon[mattooltip='הוספת כיתה']"));
                fp.insertElment("class",By.CssSelector("input[placeholder='הקלד שם כיתה']"),"כיתה ג");
                fp.click(By.TagName("mat-step-header"),6);
                fp.ClickButton("הוסף מעבר כיתה ","span");
                fp.selectForRequerd("מעבר מכיתה",false,-1);
                fp.selectText(v3);
                fp.selectForRequerd("לכיתה",false,-1);
                fp.selectText("כיתה ג");

            }
            catch {throw new NotImplementedException(); } 
        }

        private void createTasx(string v2, string v3, string v4, bool gil, int selectGil)
        {
            try { 
                editTextLink("ידידים",null,"ניסוי שליחת מייל",0,true ,-1);
                int TasxPrivate = gil == true ? 0 : 1;
                fp.selectForRequerd("משימה פרטית",false,TasxPrivate);
                fp.selectForRequerd("סוג משימה", false,selectGil);
                string userName = driver.FindElement(By.ClassName("user-name")).Text;
                fp.selectForRequerd("אחראי",false,-1);
                char[] t = { ' '};
                string users = "";
                if (userName.Split(t).Length > 1)
                    users = userName.Split(t)[1] + " " + userName.Split(t)[0];
                else
                    users = userName;
                fp.selectText(users);
                fp.selectForRequerd("סטטוס",false,-1);
                fp.selectText(v3);
                Frind f = new Frind();
                f.calanederCheck("",1);
                fp.insertElment("tasx",By.CssSelector("input[placeholder='תיאור המשימה שלך ']"),v4);
            }
            catch { throw new NotImplementedException(); }
        }

        private void createFrind(string v2, string v3)
        {
            try
            {
                fp.click(By.CssSelector("button[aria-label='menu']"));
                try { fp.click(By.XPath("//button/mat-icon[text()='link']")); } catch { }
                try { driver.FindElement(By.CssSelector("button[aria-label='menu']")).SendKeys(Keys.Tab + Keys.Enter); } catch { }
                fp.selectText(" שם משפחה ");
                //fp.click(By.CssSelector("button[aria-label='menu']"),1);
                CreateMesaur cm = new CreateMesaur();
                cm.checkSailta(v2, v3, "", 0, v2, 2,0,false);
                cm.checkSailta(" פרטי ", "link", " שם פרטי ", 0, " פרטי ", 2,3,false);

            }
            catch { fp.picter("process create frind"); throw new NotImplementedException(); }
        }

        private void returnFail(string mosadClearing, string explain, string dateReturn)
        {
            try
            {
                new Actions(driver).MoveToElement(driver.FindElement(By.CssSelector("mat-select[aria-label='selectEventAction']"))).SendKeys(Keys.Tab+Keys.ArrowUp).Perform();
                fp.selectText(mosadClearing);
                new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab + Keys.ArrowUp).Perform();
               // fp.selectForRequerd(" בחר סיבת חזרת תשלום",false,-1);
                fp.selectText(" סירוב ");
                fp.selectText(" עסקה כפולה ");
                fp.selectText(" התקשר לחברת האשראי ");
                /*fp.selectText(" סירוב ");
                fp.selectText(" עסקה כפולה ");
                fp.selectText(" התקשר לחברת האשראי ");
                fp.selectText(explain);*/
                new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
            }
            catch { fp.picter("returnFail"); throw new NotImplementedException(); }
        }

        private void processGenerator(string mosadLearn, string finance, string Mosad)
        {
            try { checkElmentGenrator();
                fp.click(By.XPath("//mat-checkbox[@formcontrolname='GenerateCommitmentsExecution']"));
                if(driver.FindElement(By.XPath("//mat-select[@formcontrolname='EducationInstitutesID']")).GetAttribute("aria-disabled").Equals(false) && driver.FindElement(By.XPath("//mat-select[@formcontrolname='FinancialClausesID']")).GetAttribute("aria-disabled").Equals(false))
                    fp.picter("click on no generator but EducationInstitutesID and FinancialClausesID is enable");
                else
                {
                     fp.click(By.XPath("//mat-checkbox[@formcontrolname='GenerateCommitmentsExecution']"));
                    fp.SelectRequerdName("formcontrolname='EducationInstitutesID", mosadLearn);
                    Thread.Sleep(100);
                    fp.SelectRequerdName("formcontrolname='FinancialClausesID", finance, false);
                }
                    fp.click(By.XPath("//mat-checkbox[@formcontrolname='ConstantDebitsExecution']"));
                if (driver.FindElement(By.XPath("//mat-select[@formcontrolname='ClearingEntitys']")).GetAttribute("aria-disabled").Equals(false) || driver.FindElement(By.XPath("//mat-select[@formcontrolname='UsersToSendEmailList']")).GetAttribute("aria-disabled").Equals(false))
                    fp.picter("click on no ConstantDebitsExecution but ClearingEntitys and UsersToSendEmailList enabled");
                else
                {
                    fp.click(By.XPath("//mat-checkbox[@formcontrolname='ConstantDebitsExecution']"));
                    fp.SelectRequerdName("formcontrolname='ClearingEntitys", Mosad,false);
                    new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
                    fp.SelectRequerdName("formcontrolname='UsersToSendEmailList", "וויץ רבקה",false);
                    new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
                }
            } catch { fp.picter("processGenerator"); throw new NotImplementedException(); }
        }

        private void checkElmentGenrator()
        {
            try { fp.checkElmentText("title hoq",By.ClassName("main-txt"), "מחולל חיובים וביצוע הו\"ק");
                fp.checkElmentText("chebox GenerateCommitmentsExecution", By.XPath("//mat-checkbox[@formcontrolname='GenerateCommitmentsExecution']"), "האם לחולל חיובים?");
                fp.checkElmentText(" EducationInstitutesID", By.XPath("//mat-select[@formcontrolname='EducationInstitutesID']"), "מוסד לימודים");
                fp.checkElmentText(" FinancialClausesID", By.XPath("//mat-select[@formcontrolname='FinancialClausesID']"), "סעיף חיוב");
                fp.checkElmentText(" ConstantDebitsExecution", By.XPath("//mat-checkbox[@formcontrolname='ConstantDebitsExecution']"), "האם לבצע הו\"ק ? ");
                fp.checkElmentText(" ClearingEntitys", By.XPath("//mat-select[@formcontrolname='ClearingEntitys']"), "מוסד סליקה");
                fp.checkElmentText("UsersToSendEmailList", By.XPath("//mat-select[@formcontrolname='UsersToSendEmailList']"), "משתמשים לשליחת מייל בסיום הביצוע");
            } catch { throw new NotImplementedException(); }
        }

        private void checkElmentAction()
        {
            try { fp.checkElmentText("title",By.ClassName("main-txt"), "פרטי הפעולה");
                fp.checkElmentText("selectCategory", By.CssSelector("mat-select[aria-label='selectCategory']"), "קטגוריה");
                fp.checkElmentText("selectEventAction", By.CssSelector("mat-select[aria-label='selectEventAction']"), "הפעולה");
                fp.checkElmentText("button saveAction", By.CssSelector("button[aria-label='btnSaveAction']"), " שמירת הפעולה ");
            } catch { fp.picter("checkElmentAction"); throw new NotImplementedException();}
        }

        /// <summary>
        /// להחזיר בגרסה הבאה
        /// </summary>
        /// <param name="typeDoc"></param>
        /// <param name="calanderStart"></param>
        /// <param name="calanderEnd"></param>
        /// <param name="MakorMail">0=דואל,1=מייל לקבלה</param>
        /// <param name="boolSum">false=תשלומים,true=תרומות</param>
        private void tkofa(string typeDoc, string calanderStart, string calanderEnd, bool boolSum,int MakorMail)
        {
            try
            {
                clickRadio(typeDoc);
                createConatrabition cc = new createConatrabition();
                char[] tav = { '/' };
                cc.calanederCheck(1,1, calanderStart.Split(tav)[0], Int32.Parse(calanderStart.Split(tav)[1]), Int32.Parse(calanderStart.Split(tav)[2]));
                cc.calanederCheck(1, 2, calanderEnd.Split(tav)[0], int.Parse(calanderEnd.Split(tav)[1]), int.Parse(calanderEnd.Split(tav)[2]));
                fp.selectForRequerd("בחר שדה מקור לכתובת המייל", false, MakorMail);
                driver.FindElements(By.TagName("mat-checkbox"))[2].Click();
                popupAttachFile("C:\\picterToPelesoft\\z.png", "צירוף תמונת רקע");
                Thread.Sleep(350);
                driver.FindElements(By.TagName("mat-checkbox"))[3].Click();
                    popupAttachFile("C:\\picterToPelesoft\\enmatzia.gif", "צירוף לוגו");
                Thread.Sleep(800);
                fp.insertElment("insertMailForNo", By.XPath("//input[@formcontrolname='mail']"), MAIL, true, null);
                if (boolSum == true)
                    driver.FindElements(By.TagName("mat-checkbox"))[5].Click();
                Thread.Sleep(200);
            }
            catch { throw new NotImplementedException(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DateEbruLoBool">"2"=מלועזי לעבברי</param>
        /// <param name="v3"></param>
        /// <param name="v4"></param>
        /// <param name="gil"></param>
        private void date(string DateEbruLoBool, string selectSorec, string selecTarget, bool gil,int selectGil=-1)
        {
            try {
                driver.FindElement(By.XPath("//mat-radio-button[@value='" + DateEbruLoBool + "']")).Click();
                fp.selectForRequerd("בחר שדה מקור", false, -1);
                fp.selectText(selectSorec); 
                fp.selectForRequerd("בחר שדה יעד", false, -1);
                fp.selectText(selecTarget);
                if (gil == true)
                { driver.FindElement(By.TagName("mat-checkbox")).Click();
                    Thread.Sleep(100);
                    fp.selectForRequerd("בחר שדה", false, -1,"true",2);
                    driver.FindElements(By.ClassName("mat-option-text"))[selectGil].Click();
                }
            }catch{ throw new NotImplementedException(); }
        }

        private void mail(string fieldName, string emailSubject, string messageBody,bool nokisor=false)
        {
            try
            {
                Thread.Sleep(410);
                //fp.click(By.TagName("mat-expansion-panel"));
                fp.SelectRequerdName("name='fieldName", fieldName, false, true);
                checkelmentMail();
                fp.SelectRequerdName("formcontrolname='receipietsUsers", " הכל ", false);
                new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
                fp.insertElment("insert adress mail", By.Name("privateEmail"), "rvaitz22@gmail.com;sarah.konosoft@gmail.com");
                CreateNewDenumic cnd = new CreateNewDenumic();
                fp.SelectRequerdName("formcontrolname='replyToUsers", cnd.nameuser(), false);
                new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
                fp.insertElment("insetrt adres uthr mail", By.Name("privateEmail"), "rvaitz22@gmail.com;sarah.konosoft@gmail.com", false, "", 1);
                try { fp.click(By.TagName("mat-step-header"),5); } catch { try { fp.click(By.CssSelector("mat-step-header[aria-posinset='2']")); } catch { try { fp.click(By.TagName("mat-step-header"), 4); } catch { } } }
                fp.insertElment("emailSubject", By.Name("emailSubject"), emailSubject);
                if (nokisor == false)
                {   editTextLink("ידידים",null,"ניסוי שליחת מייל",2);
                  fp.checkElmentText("title mail", By.Name("emailSubject"), " {{ידידים.משפחה}} {{ידידים.פרטי}}היקר!");
                }
                try { driver.FindElement(By.Name("emailSubject")).SendKeys(Keys.Tab + messageBody); } catch { }
                tableLink(nokisor); 
                Thread.Sleep(900);
                if (nokisor == false)
                {
                    driver.FindElement(By.XPath("//button[@mattooltip='צירוף קובץ']")).Click();
                    popupAttachFile("C:\\picterToPelesoft\\waitUntil.png", "צירוף קובץ למייל");
                    driver.FindElement(By.XPath("//button[@mattooltip='צירוף קובץ']")).Click();
                    popupAttachFile("C:\\picterToPelesoft\\z.png", "צירוף קובץ למייל");
                    driver.FindElement(By.XPath("//button[@mattooltip='צירוף קובץ']")).Click();
                    popupAttachFile("C:\\picterToPelesoft\\automation.csv", "צירוף קובץ למייל");
                }
            } catch { fp.picter(fieldName); throw new NotImplementedException("mail"); }
        }

        public void tableLink(bool nokisor,string teable= "tbody")
        {
            try {
                  fp.click(By.CssSelector("button[aria-label='עוד...']"));
                fp.click(By.CssSelector("button[aria-label='טבלה']"));
                fp.click(By.ClassName("tox-collection__item-label"));

                //בחירת גודל הטבלה
                IWebElement table = driver.FindElement(By.ClassName("tox-insert-table-picker"));
                IList<IWebElement> selectTable = table.FindElements(By.CssSelector("Div[role='button']"));
                
                try {selectTable[13].Click(); }
                catch {  }
                fp.click(By.CssSelector("button[aria-label='הכנס שורה לפני']"));
                if (nokisor == false)
                //לחיצה על קוביה בטבלה ע"מ לגשת לטבלה מקושרת
                {
                    fp.click(By.XPath("//span[@class='tox-tbtn__select-label'][text()='טבלה מקושרת']"));
                    string[] heder = { "תרומה מספר", "סכום התרומה", "מייל", "מלל חופשי" };
                    string[] Ben = { "מס' תרומה", "סכום", "", "" };
                    string[] father = { "", "", "דוא\"ל", "" };
                    string[] text = { "", "", "", "תודה גדולה על התרומה הנכבדה" };
                    linkTableSetting(heder, Ben, father, text);
                }
                else
                    insertTableNoLink(teable);
            } catch(Exception ex) { throw new NotImplementedException(ex+""); }
        }

        private void insertTableNoLink(string table= "tbody")
        {
            try
            {
                try
                {
                    string[] insertValueT = { "אני","מנסה","טבלה","לא מקושרת","בכמה ","שורות","אולי","יפול","ויהיה","באג","אוטומציה","כל","המקבל","ויש","לו","רעיון ","אחר ","למלל","להודיע"};
                    int m = 0;
                    driver.SwitchTo().Frame(driver.FindElements(By.TagName("iframe"))[0]);
                    IWebElement tabl=driver.FindElement(By.TagName(table));
                    IList<IWebElement> row = tabl.FindElements(By.TagName("tr"));
                    IList<IWebElement> cell;
                    for(int i=0;i<row.Count;i++)
                    { cell = row[i].FindElements(By.TagName("td"));
                        for (int j = 0; j < cell.Count; j++)
                        {
                            cell[j].SendKeys(insertValueT[m++]+Keys.Tab);
                            if (insertValueT.Length < m)
                                break;
                        }
                        if (insertValueT.Length < m)
                            break;
                    }

                    driver.SwitchTo().DefaultContent();
                }
                catch { }
            } catch { throw new NotImplementedException(); }
        }

        //להחזיר את הסינו בפעם הבאה
        public void linkTableSetting(string []heder,string []Ben,string []father,string []text)
        {
            try { fp.checkElmentText("filter",By.ClassName("title-section"), "סינונים");
                fp.SelectRequerdName("formcontrolname='tableName", " תרומות ", false);
                CreateMesaur cm = new CreateMesaur();
                try {
                    fp.ClickButton("סינון","span");
                    cm.checkSailta("סכום", "גדול מ", "0TEXT");
                    fp.ClickButton("שמירה","span",2);
                } catch { }
                string[] hederTable = { " מספר העמודה", " שם לתצוגה ", " שדה מטבלת בן ", " שדה מטבלת האב " , "טקסט חפשי" };
                fp.checkColumn("thead[role='rowgroup']",hederTable,"false","tr","th");
                IWebElement table = driver.FindElement(By.TagName("tbody"));
                IList<IWebElement> tableToDisplay = table.FindElements(By.TagName("tr"));
                IList<IWebElement> column;
                for(int i=0;i<tableToDisplay.Count;i++)
                {
                    column = tableToDisplay[i].FindElements(By.TagName("td"));
                    column[1].FindElement(By.Name("KWh")).SendKeys(heder[i]);
                    if (Ben[i] != "")
                    {
                        column[2].FindElement(By.TagName("mat-select")).Click();
                        fp.selectText(Ben[i]);
                     }if (father[i] != "")
                    {
                        column[3].FindElement(By.TagName("mat-select")).Click();
                        fp.selectText(father[i]);
                     }
                        column[4].FindElement(By.Name("KWh")).SendKeys(text[i]);
                }
                fp.ClickButton("שמירה"); 
                try
                {
                    if (driver.FindElement(By.CssSelector("div[class='warning-text ng-star-inserted']")).Enabled == true)
                        fp.ClickButton("כן", "span");
                }
                catch { }
            } catch (Exception ex) { throw new NotImplementedException(ex+""); }
        }

        public void editTextLink(string titleLink= "ידידים",string []linkpnimi=null,string replace= "שליחת מייל ניסוי", int mikom=1,bool popup=true,int msima=0)
        {
            try {
                if (popup == true)
                {
                    fp.click(By.XPath("//i[@mattooltip='עריכה טקסט עם שדות מקושרים']"));//
                    try
                    {
                        try { fp.click(By.CssSelector("button[aria-label='עוד...']"), 1); } catch { try { fp.click(By.CssSelector("button[aria-label='עוד...']"), mikom); } catch { fp.click(By.CssSelector("button[aria-label='עוד...']"), 0); } }
                        fp.checkElment("Special character", By.XPath("//button[@class='tox-tbtn'][@title='תו מיוחד']"));//
                        fp.checkElment("Emoticons", By.XPath("//button[@class='tox-tbtn'][@title='סמלי הבעה']"));//
                fp.checkElment("spacing",By.XPath("//button[@class='tox-tbtn'][@title='רווח קשיח']"));
                        fp.checkElment("search", By.XPath("//button[@class='tox-tbtn'][@title='חיפוש והחלפה']"));
                    fp.checkElment("save", By.XPath("//button[@class='tox-tbtn'][@title='שמירה']"));//
                   fp.checkElment("close", By.XPath("//button[@class='tox-tbtn'][@title='סגור']"));//
                    }
                    catch { }
                } 
                        
                    fp.checkElment("popup edit",By.TagName("mat-dialog-container"));
                fp.checkElmentText("link",By.ClassName("tox-tbtn__select-label"), "שדה מקושר");
               // fp.checkElment("void", By.XPath("//button[@class='tox-tbtn'][@title='בטל']"));
                fp.checkElment("return", By.XPath("//button[@class='tox-tbtn tox-tbtn--disabled'][@title='בצע שוב']"));
                string[] linkPnimi = {"משפחה" , "פרטי" };
                if (linkpnimi == null)
                    linkpnimi =linkPnimi;
                int ms = replace.Contains("מסמך") ? mikom + 1 : mikom + 1 + msima;
                for (int i=0;i<linkpnimi.Length;i++)
                { 
                    try { driver.FindElements(By.CssSelector("button[class='tox-tbtn tox-tbtn--select']"))[ms].Click(); } catch {try { driver.FindElements(By.CssSelector("button[class='tox-tbtn tox-tbtn--select']"))[0].Click(); } catch { } }
                    
                    fp.checkElmentText("title link", By.ClassName("tox-collection__item-label"), titleLink);
                    try { fp.click(By.ClassName("tox-collection__item-label")); } catch { }
                    fp.click(By.XPath("//div[@class='tox-collection__item-label'][text()='"+linkpnimi[i]+"']"));
                    if(popup==true)
                        try { fp.click(By.CssSelector("button[aria-label='רווח קשיח']"),1+msima); } catch { }
                }
                    if(popup==true)
                    fp.click(By.CssSelector("button[aria-label='רווח קשיח']"),1+msima);

                Search(replace,"היקר!",mikom+msima);
              if (popup == true)
                    {   try {
                   
                        driver.SwitchTo().Frame(driver.FindElements(By.TagName("iframe"))[1]);
                        fp.checkElmentText("mesage mkosar", By.TagName("p"), "היקר!");
                        driver.SwitchTo().DefaultContent();
                   
                } catch { }
                try { fp.click(By.XPath("//button[@title='שמירה']"));
                   // fp.click(By.XPath("//button[@title='שמור']"));
                //    driver.FindElement(By.CssSelector("button[title='שמור']")).Click();
                } catch { } }
            }
            catch(Exception ex)
            {
                fp.picter("editTextLink");
                throw new NotImplementedException(ex+"");
            }
        }

        private void Search(string search,string replace,int mikom=1)
        {
            try {
                int m = search.Contains("מייל") ? mikom - 1 : mikom;
                try { fp.click(By.CssSelector("button[aria-label='חיפוש והחלפה']"), mikom + 1); } catch { try { fp.click(By.CssSelector("button[aria-label='חיפוש והחלפה']"), mikom ); } catch { try { fp.click(By.CssSelector("button[aria-label='חיפוש והחלפה']"), mikom - 1); } catch { } } }
                fp.checkElment("title search", By.ClassName("tox-dialog__title"));
                fp.checkElment("button previsous",By.CssSelector("button[title='הקודם']"));
                fp.checkElment("button previsous",By.CssSelector("button[title='הבא']"));
                fp.checkElment("replace all",By.CssSelector("input[placeholder='החלף ב']"));
                fp.checkElmentText("search",By.CssSelector("button[title='חפש']"), "חפש");
                fp.checkElmentText("replace",By.CssSelector("button[title='החלף']"), "החלף");
                fp.checkElmentText("replace",By.CssSelector("button[title='החלף הכל']"), "החלף הכל");
                fp.insertElment("insert search", By.ClassName("tox-textfield"),search);
                fp.insertElment("insert replace to", By.CssSelector("input[placeholder='החלף ב']"), replace);
                
                fp.click(By.CssSelector("button[title='חפש']"));
                try {
                fp.click(By.CssSelector("button[title='החלף']"));
                }
                catch { 
                fp.click(By.CssSelector("button[title='אישור']"));
                }
                try { fp.click(By.CssSelector("button[aria-label='סגור']"), mikom + 1); } catch { try { fp.click(By.CssSelector("button[aria-label='סגור']"), mikom - 1); } catch { } }
            } 
            catch { throw new NotImplementedException(); }
        }

        private void checkelmentMail()
        {
            try { fp.checkElmentsText("title man",By.ClassName("title-section"), "מען",0);
                fp.checkElmentsText("frind",By.TagName("p"), "ידיד  ",0);
                fp.checkElmentsText("frind",By.TagName("p"), "משתמשים",1);
                fp.checkElmentsText("outher", By.TagName("p"), "אחר", 2);
                fp.checkElmentsText("title nman", By.ClassName("title-section"), " מען לתשובה", 1);
                fp.checkElmentsText("users",By.TagName("p"), "משתמשים",3);
                fp.checkElmentsText("outher", By.TagName("p"), "אחר", 4);
            }
            catch { throw new NotImplementedException(); }
        }

        public void popupAttachFile(string nitovAttach,string nameTitle,string main="-main")
        {
            try
            {
                if (nameTitle != "")
                {
                    fp.checkElmentText("check popup attach file", By.ClassName("ng-star-inserted"), nameTitle);
                    Thread.Sleep(800);
                    driver.FindElement(By.ClassName("icon-attached-file"+main)).Click();
                }
                Thread.Sleep(800);
                SendKeys.SendWait(nitovAttach);
                SendKeys.SendWait("{Enter}");
                fp.ClickButton("שמירה ");
            }
            catch { fp.picter("popupAttachFile "+nitovAttach); throw new NotImplementedException(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeProcessA"></param>
        /// <param name="startDateProcess"></param>
        /// <param name="typeCurennt"></param>
        /// <param name="TakesAll"></param>
        /// <param name="dayMont">if(typeCurennt="שבועי")7=כל השבוע
        /// יותר מיום אחד יכתב לדוגמא כך123=ימים א ב ג</param>
        public void Automation(string typeProcessA, DateTime startDateProcess,string typeCurennt="",int TakesAll=-1,int dayMont=-1)
        {
            try {
                Thread.Sleep(200);
                clickRadio(typeProcessA);
                Frind f = new Frind();
                if (typeProcessA != "שוטף")
                {
                    try { f.calanederCheck(typeCurennt); } catch { }
                }
                else
                {
                    fp.checkElmentsText("title tadirot", By.CssSelector("div[class='title-section title-section-readonly ng-star-inserted']"), "תדירות", 1);
                    fp.checkElmentsText("title time hour", By.CssSelector("div[class='title-section title-section-readonly ng-star-inserted']"), "תזמון השעה", 2);
                    fp.SelectRequerdName("formcontrolname='frequencyOcurrs", typeCurennt);
                    fp.insertElment("TakesAll", By.XPath("//input[@formcontrolname='frequencyRequresEvery']"), TakesAll.ToString());
                    switch (typeCurennt)
                    {
                        case "יומית":
                            fp.checkElmentText("value TakesAll", By.CssSelector("p[class='frequency-requers-occurs-p ng-star-inserted']"), "ימים");
                            break;
                        case "חודשית":
                            fp.checkElmentText("day in month", By.XPath("//input[@formcontrolname='frequencyOcurrsMonthlyDayDate']"), " בחודש ");
                            fp.checkElmentText("value time mont", By.Id("monthly-p"), "בכל: ");
                            fp.checkElmentText("value TakesAll", By.CssSelector("p[class='frequency-requers-occurs-p ng-star-inserted']"), "חודשים");
                            fp.insertElment("day in month", By.CssSelector("input[formcontrolname='frequencyRequresEvery']"), dayMont.ToString());
                            break;
                        case "שבועי":
                            fp.checkElmentText("value TakesAll", By.CssSelector("p[class='frequency-requers-occurs-p ng-star-inserted']"), "שבועות");
                            fp.checkElmentText("section day in work", By.CssSelector("section[class='example-section ng-star-inserted']"), "בימי:");
                            fp.checkElmentText("not select day in work", By.Id("wrongMessage"), "לא נבחר יום");
                            selectDayWork(dayMont);
                            break;

                    }

                }
               
            }
            catch { throw new NotImplementedException(); }
        }

        private void selectDayWork(int dayMont)
        {
            try {if (dayMont == 7)
                    fp.click(By.CssSelector("mat-checkbox[class='mat-checkbox example-margin mat-accent']"));
                  var digits = dayMont.ToString().Select(x => int.Parse(x.ToString()));
                for(int i=0;i<digits.Count();i++)
                {
                    //Console.WriteLine(digits.ElementAt(i));
                    driver.FindElements(By.TagName("li"))[digits.ElementAt(i)].Click();
                    Thread.Sleep(100);
                }
            }
            catch{ fp.picter("selectDayWork "+dayMont); throw new NotImplementedException(); }
        }

        public void clickRadio(string valueClick)
        {
            fp.clickRadio(valueClick);
        }

        public  void timeHour(string timing, string hourStart, int takesPlaceAll=-1, string hourOrMminut="", string hourEnd="")
        {
            try
            {
                string nameStarthour = "יתרחש בשעה:";
                if (timing.Trim()== "מס' פעמים")
                {
                    clickRadio(timing.Trim());
                    clickRadio(timing.Trim());
                    fp.insertElment("takesPlaceAll", By.XPath("//input[@formcontrolname='dailyFrequencyOcurrsEvery']"), takesPlaceAll.ToString(), true);
                    fp.SelectRequerdName("formcontrolname='dailyFrequencyOcurrsEveryPart", hourOrMminut, false);
                    hour(hourEnd, "עד שעה:", 1);
                    nameStarthour = "משעה:";
                    hour(hourStart, nameStarthour, 0);
                }
                hour(hourStart, nameStarthour, -1);

            }
            catch { fp.picter("time hour"); throw new NotImplementedException(); }
        }

        private void hour(string hour,string nameHour,int mikom)
        {
            try {
                if (mikom == -1)
                    fp.checkElmentText("hour",By.CssSelector("div[class='input-time ng-star-inserted']mat-label"),nameHour);
                else
                     fp.checkElmentsText("hour", By.XPath("//div[@class='frequency']/mat-label"), nameHour, mikom);
                char[] tav = { ':' };
                driver.FindElement(By.XPath("//input[@placeholder='דקות']")).Clear();
                fp.insertElment("minut", By.XPath("//input[@placeholder='דקות']"), hour.Split(tav)[1], false, "", mikom);
                fp.insertElment("hour", By.XPath("//input[@placeholder='שעות']"), hour.Split(tav)[0], false, "", mikom);
                fp.checkElment("icon time", By.CssSelector("span[class='material-icons clock']"));
            } catch { throw new NotImplementedException(); }
        }

        

        public  void openProcess()
        {
            CreateNewDenumic cnd = new CreateNewDenumic();
            fp.actionWork("הגדרות", "תהליכים");
            fp.ButtonNew("תהליך");
            cnd.checkClasscreate("תהליך");
        }

        public  void checkSucess(string dateTimeH)
        {
            try {
                int waitTime = Int32.Parse(dateTimeH.Replace(":","")) - Int32.Parse(DateTime.Now.ToString("HHmm"));
                if (waitTime > 0)
                {
                    if (waitTime > 1)
                        throw new NotImplementedException();
                    else
                     Thread.Sleep(waitTime * 1000);
                }
                 fp.actionWork("הגדרות", "תהליכים");
                //driver.Navigate().Refresh();
                string[] tableRun = { "","","","","כן","",DateTime.Today.ToString("dd/MM/yyyy-")+dateTimeH,"", " עבר בהצלחה " ,"-1"};
                fp.checkColumn("mat-table[role='grid']", tableRun, "", "mat-row", "mat-cell");
                    popupList pl = new popupList();
                    pl.popupEdit();
                   // new Actions(driver).MoveToElement(driver.FindElement(By.Name("EventsOnAddOrUpdateName"))).SendKeys(Keys.Tab).Perform();
                    fp.selectForRequerd("פעיל",false);
                    fp.ClickButton(" שמירה ", "span");
                
            } catch { fp.picter("checkSucess"); throw new NotImplementedException(); }
        }

        internal void checkProcesTasx()
        {
            try {
                string userName = driver.FindElement(By.ClassName("user-name")).Text;
                char[] t = { ' ' };
                string users = (userName.Split(t).Length > 1) ? userName.Split(t)[1] + " " + userName.Split(t)[0] : userName;
            string[] checkRow = { "", "", DateTime.Today.ToString("dd/MM/yy"), "גבייה", "",users,"לביצוע","", users,"ידידים", "-1" };
                fp.checkColumn("//",checkRow,"false", "mat-row", "mat-cell");
                fp.checkElment("num tasx",By.CssSelector("span[class='mat-badge-content mat-badge-active']"));
                fp.checkElmentText("checlk post",By.TagName("i"), " post_add ");
                fp.checkElment("heartbit point",By.ClassName("heartbit"));
                //לא תקין
                /* fp.click(By.ClassName("heartbit"));
                checkTasxPreform();*/
            } catch { throw new NotImplementedException(); }
        }

        private void checkTasxPreform()
        {
            try { fp.checkElmentText("title",By.ClassName("main-txt"), "משימות לביצוע");
                fp.checkElmentText("count tasx",By.ClassName("row-number-txt"), "משימות");
                fp.ClickButton(" הוספת משימה ","span");
                string[] rowAddTasx = { "גבייה","","לביצוע","",DateTime.Today.ToString("dd/MM/yyyy"),"-1"};
                fp.checkColumn("//",rowAddTasx,"false", "mat-row", "mat-cell");
            } catch { throw new NotImplementedException(); }
        }

        public  void checkChange()
        {
            try {
                Thread.Sleep(500); string name = driver.FindElement(By.ClassName("name-text")).Text;
               if(!name.Contains( driver.FindElement(By.TagName("textarea")).GetAttribute("value")))
                { fp.picter("check change process edit");throw new NotImplementedException(); }
            } catch { throw new NotImplementedException(); }
        }
    }
}