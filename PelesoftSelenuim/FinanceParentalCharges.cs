﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class FinanceParentalCharges:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void stepFinanceParentalCharges(string nameParent,string nameMosad = "אוטומציה",string filter="",string num="")
        {
            try { 
                checkElmentS();
                string dateMont = DateTime.Today.ToString("MM/yyyy");
                if(Int16.Parse( DateTime.Today.ToString("MM"))<9)
                    dateMont = DateTime.Today.ToString("M/yyyy");
               string  nameParents=insertElment(nameMosad,dateMont ,"",filter, "DebitMonth");
                nameParent = nameParent != "" ? nameParent : nameParents;
                Frind f = new Frind();
                f.openParent();
                fp.insertElment("filter", By.CssSelector("input[placeholder='מה תרצה לחפש?']"),nameParent);
                char[] spl = { ' '};
                string[] friend = {"", nameParent.Split(spl)[0], "-1" };
                Thread.Sleep(550);
                fp.checkColumn("//", friend, "", "mat-row", "mat-cell");
                popupList pl = new popupList();
                pl.expansionPopUP();
                pl.clickStepHeder(7, "חיובים");
                string[] Charges = {"","",num,"שקל","-1" };
                fp.checkColumn("//", Charges, "false", "mat-row", "mat-cell", -1, 1);
            } catch { fp.picter("stepFinanceParentalCharges"); throw new NotImplementedException(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InstitutionEducation">מוסד לימודים</param>
        /// <param name="DebitMonth">dateTime.today.toString("MM/yyyy")</param>
        /// <param name="FinancialClausesID">סעיף חיוב</param>
        public string insertElment(string InstitutionEducation,string DebitMonth="",string FinancialClausesID="הכל",string filter="",string debitMonth= "CreditMonth")
        {
            string nameFamily = "";
            try {
                fp.SelectRequerdName("formcontrolname='InstitutionEducation",InstitutionEducation);
                fp.SelectRequerdName("formcontrolname='"+debitMonth+"", DebitMonth);
                Thread.Sleep(100);
                fp.SelectRequerdName("formcontrolname='FinancialClausesID", FinancialClausesID,false);
                fp.ClickButton("חולל והצג חיובים לתלמידים ");
                string[] checkHederTable = { "מזהה ", "שם משפחה ", "שם פרטי ", "כתובת ","טלפון ", "סעיף חיוב ", "פרטים ", "סכום ","-1" };
                fp.checkColumn("//", checkHederTable, "false", "mat-header-row", "mat-header-cell");
                if (filter != "")
                    fp.insertElment("filter",By.CssSelector("input[placeholder='חיפוש']"),filter);
                 nameFamily = driver.FindElements(By.TagName("mat-cell"))[1].Text;
                }
            catch { throw new NotImplementedException(); }
            return nameFamily;
        }

        private void checkElmentS()
        {
            try { fp.checkElmentText("check title",By.ClassName("main-txt"), "מחולל חיובים להורים");
                fp.checkElmentText("check InstitutionEducation", By.CssSelector("mat-select[formcontrolname='InstitutionEducation']"), "מוסד לימודים");
                fp.checkElmentText("check DebitMonth", By.CssSelector("mat-select[formcontrolname='DebitMonth']"), "בחר חודש חיוב");
                fp.checkElmentText("check FinancialClausesID", By.CssSelector("mat-select[formcontrolname='FinancialClausesID']"), "סעיף חיוב");
                fp.checkElmentText("check button", By.ClassName("mat-button-wrapper"), "חולל והצג חיובים לתלמידים ");
            } catch { throw new NotImplementedException(); }
        }

        public  void offset(string ofenOffset,int iofenOffset,bool exap=true)
        {
            try
            {
                popupList pl = new popupList();
                if (exap==true)
                   pl.expansionPopUP();
                Thread.Sleep(300);
                pl.clickStepHeder(iofenOffset, ofenOffset);
                string[] offseting = { "", "", "", "", "-1" };
                int mikom = 8;
                if (ofenOffset == "תשלומים")
                    mikom = 10;
                Thread.Sleep(200);
                fp.checkColumn("//",offseting, "//button[@aria-label='display']", "mat-row", "mat-cell",mikom,1);
                fp.click(By.CssSelector("button[aria-label='btnOpenCommitmentOffsetting']"));
                Laiblites l = new Laiblites();
                if(ofenOffset=="תשלומים")
                    l.checkPageOffsetting("חיובים",1,0);
                else
                     l.checkPageOffsetting("חיובים");
                if(ofenOffset!="תשלומים")
                l.offse(exap);
            }
            catch { fp.picter("קיזוז");
                throw new NotImplementedException(); }
        }
    }
}