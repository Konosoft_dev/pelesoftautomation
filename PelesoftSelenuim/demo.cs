﻿using Microsoft.TeamFoundation.TestImpact.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
    [TestClass]
    public class demo
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        PupilNew pn = new PupilNew();
        ClearingEntity ce = new ClearingEntity();
        createConatrabition cc = new createConatrabition();
        page p = new page();

        [TestMethod,Order(1)]
        public void Test01Login()
        {
            login ln = new login();
            ln.openChrome_2();
            ln.UserNamee("ציבי");
            ln.Password("ציבי1234!");
            ln.cLickLogIn();
        }
        string[] EI = { "תלמוד תורה", "גן בנים", "ישיבה" };
        string[] type = { "תלמוד תורה", "גן ילדים", "ישיבה קטנה" };
        string[] grade = { "א", "ב", "ג", "ד", "ה", "ו" };

        [TestMethod,Order(2)]
        public void CreateEducationalInstitution()
        {
            EducationalInstitution ei = new EducationalInstitution();
            Test01Login();
            ei.openPageEI();
            for (int i = 0; i < EI.Length; i++)
            {
                ei.createEI(EI[i], type[i]);
                //sec.checkEndCreate(p.MosadChinuc);
                fp.closePoup();
            }

            fp.closeChrome();
        }
        [TestMethod,Order(3)]
        public void CreateNewFrined()
        {
            Frind f = new Frind();
            fp.rand(p.names.Length);
            Test01Login();
            f.openFrind();
            int j = 0;
            for (int i = 0; i < 100; i++)
            {
                j++;
                if (j == p.names.Length)
                    j = 0;
                f.newFrind(p.names[j]);
                pn.goToCard(p.nameFrind);
                string sum = "400";
                pn.methodPaymens(p.ofen[fp.rand(p.ofen.Length)], sum, false);
                fp.closePoup(1);
                fp.closePoup();
            }
            fp.closeChrome();
        }
            
        [TestMethod,Order(4)]
        public void GradeLavel()
        {
            
            string[] ageMin = { "5", "6", "7", "8", "9", "10", "11", "12", "13" };
            string[] ageBig = { "14", "15", "16", "17", "18", "19" };
            string[] minAge = { "3", "4" };
            Test01Login();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepgradeLavel(EI,type,grade,ageMin,ageBig,ageBig,minAge); 
           
            //sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            fp.closeChrome();
        }
        [TestMethod,Order(5)]
        public void CreateGrade()
        {
            Test01Login();
            Grade g = new Grade();
            for (int i = 0; i < EI.Length; i++)
                for (int j = 0; j < grade.Length; j++)
                {
                    if (EI[j] == "גן בנים")
                        if (i > 1)
                            break;
                    g.stepCreateGard(EI[i], type[i], grade[j] + "-" + EI[i]);
                    fp.closepopup();
                    Thread.Sleep(300);
                    g.Close();
                    fp.closePoup();
                }
            fp.closeChrome();
        }


        /* [TestMethod]
         public void SettingFildesTable()
        // AddTable at = new AddTable();
         {
        // AddTable at = new AddTable();
             Test01Login();
             at.openTable(" תלמידים ", " תלמידים ");
             at.OpenSettings();
             at.OpenSettingsTable();
             string[] addFildes = { "ClassID", "ClassLevelID" };
             at.addFildes(addFildes);
             at.save();
             string[] remove = { " Neighborhood ", " DatotCode ", " CalculatedDatotValue ", " LearningPathId " };
             at.removeFildes(remove);
             at.save();
             Thread.Sleep(500);
             fp.closeChrome();
         }*/
        
        [TestMethod,Order(6)]
        public void CreatePupil()
        {
            Financical fl = new Financical();
            PupilNew pn = new PupilNew();
            Test01Login();
            string[] mosad = EI;
           
            pn.openPupil();
            int f = 0;
            int o = 0;
            for (int j = 0; j < mosad.Length; j++)
                for (int m = 0; m < grade.Length; m++)
                {
                    if (mosad[j] == "גן בנים")
                        if (m > 1)
                            break;
                    for (int i = 0; i < 9; i++)
                    {
                        string pupil = fl.tzPupil();
                        pn.stepCreatePupil(pupil,mosad[j]);
                        string[] namePupil;
                        if (mosad[j] == "בית ספר")
                            namePupil = p.mather;
                        else
                            namePupil = p.names;
                        pn.stepCreatePUPIL(namePupil[f], mosad[j], f, grade[m] + "-" + mosad[j]);
                        // sec.checkEndCreate("תלמיד", false, "", p.nameFrind);
                        /* pn.goToCard();
                         pn.insertDate(mosad[j], m);
                         Thread.Sleep(100);
                         string sum = "400";
                         if (mosad[j].Contains("ישיבה"))
                             sum = "1400";
                         string[] ofen = p.ofenM;
                         pn.prentalData(ofen[fp.rand(ofen.Length)], sum, false, 1);
                         fp.ClickButton(" שמירה ");
                         Thread.Sleep(100);*/
                        fp.closePoup();
                        if (p.names.Length == f + 1)
                            f = 0;
                        if (o + 1 == p.pupilID.Length)
                            break;
                        f++; o++;
                    }
                }
            fp.closeChrome();
        }
       /* [TestMethod]
        public void createParentalData()
        {
            Test01Login();
            fp.ClickList("תלמידים");
            fp.clickListinListEqualName("תלמידים");
            pn.openPrenatalData("false");
        }[Test]
        public void createMethodPaymens()
        {
            Test01Login();
            fp.ClickList(p.nameFrind + "ים");
            fp.clickListinListEqualName(p.nameFrind + "ים");
            pn.openPrenatalData();
            fp.closeChrome();
        }*/
       [TestMethod,Order(7)]
        public void CreateNewClearingEntity()
        {
            string[] cE = { "אשראי ", "אשראי הו\"ק", "מסב" };
            Test01Login();
            ce.openClearingEninty();
            for (int i = 0; i < cE.Length; i++)
            {
                ce.stepClaeringEninety(cE[i]);
                fp.closePoup();
            }
            fp.closeChrome();

        }
        [TestMethod,Order(8)]
        public void CreateNewAccountBank()
        { 
         CreateBank cb = new CreateBank();
            Test01Login();
            cb.CreateNewAccountBank();
            fp.closeChrome();
        }
        [TestMethod,Order(9)]
        public void CreateNewConatrabitionCase()
        {

            Test01Login();
            cc.conatrabitionList();
            for (int i = 0; i < 18; i++)
            {
                cc.createNew("מזומן","Cash", true,i);
                fp.closePoup();
            }
            cc.checknumConatarbition();
        }
        [TestMethod,Order(9)]
        public void CreateNewConatrabitionBag()
        {

            Test01Login();
            cc.conatrabitionList();
            for (int i = 0; i < 16; i++)
            {
                if (i == 2)
                    Thread.Sleep(200);
                cc.createNew("צק", "Check", true,i+8);
                fp.closePoup();
            }
            cc.checknumConatarbition();
            fp.closeChrome();
        }
        [TestMethod,Order(9)]
        public void CreateDipositCreadetCard()
        {
            Test01Login();
            cc.conatrabitionList();
            for (int i = 0; i < 16; i++)
            {
                if (i == 2)
                    Thread.Sleep(200);
                cc.createNew("כרטיס אשראי", "Credit", true,i+15);
              //  sec.checkEndCreate("תרומה", false, "ה", "ידיד");
                fp.closePoup();
            }
            fp.closeChrome();
        }
     /*   [Test]
        public void CreatePDF()
        {
            receStrct rs = new receStrct();
            Test01Login();
            rs.stepPDF();
            fp.closeChrome();
        }
        [Test]
        public void ReceptionStructures()
        {
            receStrct rs = new receStrct();
            Test01Login();
            rs.stepRec();
            fp.closeChrome();
            //sec.pageCard("מבנה קבלה");
        }
        [Test]
        public void CreateNewFinancicalSection()
        {
            Financical fl = new Financical();

            Test01Login();
            string[] finances = { " מוסדות חינוך ", "תלמידים", "הסעות", " חיוב ", " מוסדות חינוך ", "תלמידים", "טיול שנתי", " חיוב ", " מוסדות חינוך ", "תלמידים", "שכר לימוד", " חיוב ", " מוסדות חינוך ", "תלמידים", "מורה מתקנת", " חיוב " };
            for (int i = 0; i < finances.Length; i = i + 4)
            {
                fl.createNewFinancicalSection(finances[i], finances[i + 1], finances[i + 2], finances[i + 3]);
                fp.closePoup();
            }
            fp.closeChrome();
        }
        //[Test]//נא לא להריץ
        public void createBilingClauses()
        {
            Test01Login();
            pn.openPupil();
            pn.openBilingClauses("1", "", "שכר לימוד", "50", "תשלומים,1/1/0,1/12/0", "העברה בנקאית", "שוטף");
            // pn.BilingClauses();
        }
*/
       /* [Test]
        public void createBilingClausesAll()
        {

            string[] EI = { "תלמוד תורה", "גן בנים", "ישיבה" };
            Test01Login();
            fp.clickNameDefinitions(" ניהול כספים ");
            fp.clickNameDefinitions("הגדרת סעיפי חיוב");
            string amount = "50";
            BilingClauses bc = new BilingClauses();
            for (int i = 0; i < EI.Length; i++)
            {
                if (EI[i].Contains("ישיבה"))
                    amount = "200";
                bc.stepBilingClauses(i.ToString(), "שכר לימוד", "שכר לימוד", amount, "תשלומים,1/1/0,1/12/0", "שוטף");

            }

        }*/
        // [Test]
      /*  public void CreateMaddGrafLine()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] inShilta = { "-1" };
            cm.stepGrafLine(" אמצעי תשלום ", " סוג אמצעי תשלום ", "כמות", inShilta, " ישות סליקה ", "", "סטטוס", " ExpiryDate ");
            //sec.closePage();
            fp.closeChrome();
        }*/
        [TestMethod,Order(10)]
        public void MosadDatot()
        {
            Test01Login();
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" מוסדות לפי דתות ");
            religiousInstitutions rn = new religiousInstitutions();
            string[] mosad = { "ישיבה בית ישראל", "215", "ישיבת נזר ישראל", "893", "כולל ערד", "999" };
            for (int i = 0; i < mosad.Length; i = i + 2)
            {
                rn.stepNewMosad(mosad[i], mosad[i + 1]);
                //sec.checkEndCreate("מוסד לפי דתות", false);
                fp.closepopup();
            }

            fp.closeChrome();
        }
        // [Test]
      /*  public void CreateMaddGrapColumn()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] inShilta = { " תאריך יצירה ", " אינו ריק ", "", " מעדכן הרשומה ", " שונה מ ", "0text", "-1" };
            cm.stepGrafColumn(" מסמכים ", " מעדכן הרשומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
            //sec.closePage();
            fp.closeChrome();
        }
        // [Test]
        public void CreateMaddTable()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחtext", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            //sec.closePage();
            fp.closeChrome();
        }
        [Test]
        public void CreateMaddCardMini()
        {

            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] inShilta = { "-1" };
            cm.stepCard(" תלמידים ", "כמות", inShilta, "people_alt", "", " מוסד לימודים ", " תאריך כניסה ", true, "רישום תשפ\"א", "הרישום מתמלא", "ילדים נרשמו בהצלחה");
            Thread.Sleep(200);
            //sec.closePage();
            fp.closeChrome();
        }
        [Test]
        public void CreateMaddPaiPupil()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] insertShilta = { "-1" };// [{ },{ }];
            cm.stepPai("תלמידים לפי מוסדות,תלמידים", "מוסד לימודים", " כמות ", insertShilta, "", "רמת כיתה", "false");
           // sec.closePage();
            fp.closeChrome();
        }
        [Test]
        public void CreateMaddPai()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] insertShilta = { "עיר", "אינו ריק", "", "עיר", "אינו ריק", "", "-1" };// [{ },{ }];
            cm.stepPai("ידידים", "ערים", " כמות ", insertShilta, "סכום", "יוצר הרשומה", "תאריך יצירה");
            //sec.closePage();
            fp.closeChrome();
        }
        [Test]
        public void zpcreateMaddGrafLineNew()
        {
            CreateMesaur cm = new CreateMesaur();
            Test01Login();
            string[] inShilta = { " יוצר הרשומה ", " אינו ריק ", "", " הערות ", " שווה ל ", p.automation + "text", "-1" };
            cm.stepGrafLine(" תרומות ", " מטבע ", "סכום", inShilta, " אופן תרומה ", " סכום ", " איש קשר ", " תאריך יצירה ");

            fp.closePage();// sec.pageNewCreate("מדד נוסף");
                           // CheckMadd("תרומות", "תרומות", "line");
        }*/
        [TestMethod,Order(2)]
        public void InsertList()
        {
            Test01Login();
            fp.actionWork(" הגדרות ","ניהול רשימות");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            for (int i = 0; i < p.nmeList.Length; i++)
            {
                nl.clickTable(p.nmeList[i]);
                Thread.Sleep(200);
                nl.saveEditData(p.newList[i]);
                Thread.Sleep(400);
            }
            fp.closeChrome();
        }

        /*[Test]
        public void tackingFrind()
        {
            Frind f = new Frind();

            Test01Login();
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" מעקב ידידים ");
            int count = p.TackingFrind.Length;
            for (int i = 4; i < count; i++)
            {
                fp.ButtonNew("מעקב ידיד");
                f.stepTackingFrind(p.TackingFrind[i], p.typeTackingFrind[i], i + 1);
                fp.closePoup();
            }
        }*/
    }
}
