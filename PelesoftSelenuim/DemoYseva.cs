﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
    [TestClass]
    public class DemoYseva
    {
        page p = new page();
        FunctionPelesoft fp = new FunctionPelesoft();
        
        [TestMethod,Order(1)]
        public void Login()
        {
            login ln = new login();
            ln.openChromeYseva();
            ln.UserNamee("admin");
            ln.Password("admin1234");
            ln.cLickLogIn();
            //עד שתהיה סביבה רק הכנה
           // Institution i = new Institution();
           // i.changeChaseBox(p.automation);
        }
        [TestMethod,Order(2)]
        public void InsertList()
        {
            Login();
            fp.actionWork(" הגדרות ","ניהול רשימות");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            for (int i = 0; i < p.nmeList.Length; i++)
            {
                nl.clickTable(p.nmeList[i]);
                Thread.Sleep(200);
                nl.saveEditData(p.newList[i]);
                Thread.Sleep(400);
            }
            fp.closeChrome();
        }
        [TestMethod,Order(3)]
        public void CreateNewClearingEntity()
        {
            ClearingEntity ce = new ClearingEntity();
            string[] cE = { "אשראי ", "אשראי הו\"ק", "מסב" };
            Login();
            ce.openClearingEninty();
            for (int i = 0; i < cE.Length; i++)
            {
                ce.stepClaeringEninety(cE[i]);
                fp.closePoup();
            }
            fp.closeChrome();

        }

        [TestMethod,Order(4)]
        public void CreateNewFrined()
        {
            Frind f = new Frind();
            PupilNew pn = new PupilNew();
            fp.rand(p.names.Length);
            Login();
            f.openFrind();
            int j = 0;
            for (int i = 0; i < 100; i++)
            {
                j++;
                if (j == p.names.Length)
                    j = 0;
                f.newFrind(p.names[j]);
                string sum = "1400";
                pn.goToCard(p.nameFrind);
                pn.methodPaymens(p.ofen[fp.rand(p.ofen.Length)], sum,false);
                fp.closePoup(1);
                fp.closePoup();
            }
            fp.closeChrome();
        }   
        string[] EI = { "ישיבה" };
        string[] type = { "ישיבה קטנה", "ישיבה גדולה" };
        string[] grade = { "א", "ב", "ג", "ד", "ה", "ו" };

        [TestMethod,Order(5)]
        public void CreateEducationalInstitution()
        {
            EducationalInstitution ei = new EducationalInstitution();

            Login();
            ei.openPageEI();
            ei.stepCreate(EI,type);
            fp.closeChrome();
        }
        [TestMethod,Order(6)]
        public void GradeLavel()
        {
            string[] ageMin = { "5", "6", "7", "8", "9", "10", "11", "12", "13" };
            string[] ageBig = { "14", "15", "16", "17"};
            string[] ageBigBig = {  "18", "19" };
            string[] minAge = { "3", "4" };
            Login();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepgradeLavel(EI,type,grade,ageMin,ageBig,ageBigBig,minAge);
            //sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            fp.closeChrome();
        }
        [TestMethod,Order(7)]
        public void CreateGrade()
        {
            int returnInser = 0;
            Login();
            Grade g = new Grade();
            for (int i = 0; i < EI.Length; i++)
            {
                g.openStepGrade(EI[i],type[i]);
                for (int m = 0; m < type.Length; m++)
                {
                    g.addGreadeStep(m);
                    for (int j = 0; j < grade.Length; j++)
                    {
                        if (EI[i] == "גן בנים")
                            if (j > 1)
                                break;
                        g.clickGrade(m);
                        returnInser= g.insertData(grade[j] + "-" + type[m]);
                        if (returnInser == 1)
                        {
                            fp.closePoup(2);
                            break;
                        }
                        fp.closepopup();
                        Thread.Sleep(300);
                        
                    }
                    fp.closePoup();
                }
                fp.closePoup();
            }
            fp.closeChrome();
        }
        [TestMethod,Order(8)]
        public void MosadDatot()
        {
            Login();
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" מוסדות לפי דתות ");
            religiousInstitutions rn = new religiousInstitutions();
            string[] mosad = { "ישיבה ", "215", "ישיבת ישראל", "893" };
            for (int i = 0; i < mosad.Length; i = i + 2)
            {
                rn.stepNewMosad(mosad[i], mosad[i + 1]);
                //sec.checkEndCreate("מוסד לפי דתות", false);
                fp.closepopup();
            }

            fp.closeChrome();
        }
        [TestMethod,Order(9)]
        public void CreatePupil()
        {
            PupilNew pn = new PupilNew();
            Login();
            string[] mosad = type;
            pn.createPupils(mosad, EI);
        }
        createConatrabition cc = new createConatrabition();
         [TestMethod,Order(10)]
        public void CreateNewConatrabitionCase()
        {

            Login();
            cc.conatrabitionList();
            for (int i = 0; i < 8; i++)
            {
                cc.createNew("מזומן", "Cash", true,i+4);
                fp.closePoup();
            }
            fp.closeChrome();

        }
        [TestMethod,Order(10)]
        public void CreateNewConatrabitionBag()
        {

            Login();
            cc.conatrabitionList();
            for (int i = 0; i < 6; i++)
            {
                if (i == 2)
                    Thread.Sleep(200);
                cc.createNew("צק", "Check", true,i);
                fp.closePoup();
            }
            cc.checknumConatarbition();
            fp.closeChrome();
        }
        [TestMethod,Order(10)]
        public void CreateDipositCreadetCard()
        {
            Login();
            cc.conatrabitionList();
            for (int i = 0; i < 6; i++)
            {
                if (i == 2)
                    Thread.Sleep(200);
                cc.createNew("כרטיס אשראי", "Credit", true,i+8);
                //  sec.checkEndCreate("תרומה", false, "ה", "ידיד");
                fp.closePoup();
            }
            fp.closeChrome();
        }
        [TestMethod,Order(11)]
        public void CreateNewAccountBank()
        {
            CreateBank cb = new CreateBank();
            Login();
            cb.CreateNewAccountBank();
            fp.closeChrome();
        }
        [TestMethod,Order(12)]
        public void CreateDipositBankTransfer()
        {
            createConatrabition cc = new createConatrabition();
            Login();
            cc.conatrabitionList();
            for (int i = 0; i < 9; i++)
            {
                cc.createNew("העברה בנקאית", "BankDeposit", true,i+20);
                fp.closePoup();
            }
            fp.closeChrome();
        }
    }
}
