﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;

namespace PelesoftSelenuim
{
     class Promotional:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void stepPromotional(string typePromotional, string frequencyP, string monthSelect, string ClauseName = "כולל", string finance = " אוטומציה (זיכוי) ", int sum = 10)
        {
            try {
                fp.actionWork("ניהול כספים", "הגדרת סעיפי זיכוי");
                fp.ButtonNew("הגדרת סעיף זיכוי");
                CreateNewDenumic cnd = new CreateNewDenumic();
                cnd.checkClasscreate("הגדרת סעיף זיכוי", false);
                createConatrabition cc = new createConatrabition();
                cc.clickFrind(MosadChinuc,"כולל");
                step2(typePromotional, frequencyP, monthSelect, ClauseName, finance, sum); 
            } catch { throw new NotImplementedException(); }
        }

        private void step2(string typePromotional, string frequencyP,string monthSelect, string ClauseName= "כולל", string finance = " אוטומציה (זיכוי) ", int sum = 10)
        {
            try { checkElment();
                insertElment(typePromotional,frequencyP, monthSelect, ClauseName,finance,sum);
                fp.ClickButton(" צור הגדרת סעיף זיכוי חדש ");
                Thread.Sleep(250);
            } catch { fp.picter("step2זיכוי"); throw new NotImplementedException(); }
        }
        /// <summary>
        /// הזנת תוכן סעיףזיכוי
        /// </summary>
        /// <param name="typePromotional">שוטף\חד פעמי</param>
        /// <param name="frequencyP">חודש בודד/טווח חודשים/תשלומים/קבוע</param>
        /// <param name="montSelect">"10/12/21-10/12/22"</param>
        /// <param name="ClauseName"></param>
        /// <param name="finance"></param>
        /// <param name="sum"></param>
        private void insertElment(string typePromotional,string frequencyP,string montSelect,string ClauseName="כולל",string finance= " אוטומציה (זיכוי) ",int sum=10)
        {
            try {
                fp.insertElment("ClauseName", By.Name("ClauseName"), ClauseName);
                fp.selectForRequerd(" סוג סעיף זיכוי ",false,-1);
                fp.selectText(typePromotional);
                fp.selectForRequerd(" סעיף פיננסי ",true,-1);
                fp.selectText(finance);
                fp.insertElment("Sum", By.Name("Sum"), sum.ToString(),true);
                fp.selectForRequerd(" מטבע ");
                fp.selectForRequerd(" תדירות זיכוי ", true,-1);
                fp.selectText(frequencyP);
                char[] split = { '-','/' };
                createConatrabition cc = new createConatrabition();
                cc.calnderMont(0,Int16.Parse( montSelect.Split(split)[0]),Int16.Parse( montSelect.Split(split)[1]));
                //fp.selectForRequerd("בחר/י מחודש", false, -1, montSelect.Split(split)[0]);
                //fp.selectForRequerd("בחר/י מחודש", false, -1, montSelect.Split(split)[0]);
                if (frequencyP != " חודש בודד ")
                {
                    cc.calnderMont(1, Int16.Parse(montSelect.Split(split)[1]), Int16.Parse(montSelect.Split(split)[1]));
                    //fp.selectForRequerd("בחר/י  עד חודש", false, -1, montSelect.Split(split)[1]);
                    //fp.selectForRequerd("בחר/י  עד חודש", false, -1, montSelect.Split(split)[1]);
                }
                fp.selectForRequerd(" שנה\"ל ",false,-1);
                fp.selectText(yearHebru);
              //  fp.selectForRequerd(" סניף ", false, -1);
                //fp.selectText( ClauseName);
                fp.checkSelectPreform(" מגמה ");
                fp.insertElment("Remarks", By.Name("Remarks"), automation );
            }
            catch { throw new NotImplementedException(); }
        }

        private void checkElment()
        {
            try { fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "פרטי סעיף זיכוי", 0);
                fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "פרטי זיכוי", 1);
                fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "עבור", 2);
                fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "פרטים נוספים", 3);
                fp.checkElmentText("ClauseName", By.Name("ClauseName"), " שם סעיף זיכוי ");
                fp.checkSelectPreform(" סוג סעיף זיכוי ");
                fp.checkSelectPreform(" סעיף פיננסי ");
                fp.checkElmentText("Sum", By.Name("Sum"), " סכום ");
                fp.checkSelectPreform(" מטבע ");
                fp.checkSelectPreform(" תדירות זיכוי ");
                fp.checkSelectPreform(" שנה\"ל ");
                fp.checkSelectPreform(" סניף ");
                fp.checkSelectPreform(" מגמה ");
                fp.checkElmentText("Remarks", By.Name("Remarks"), " הערות ");
            } catch { throw new NotImplementedException(); }
        }

        public  void stepCreditGenerator(string mosad="כולל", string promotional="הכל")
        {
            try {
                checkElmentGenerator();
                insertElmentGenerator(mosad,promotional);
            } catch { fp.picter("stepCreditGenerator"); throw new NotImplementedException(); }
        }

        private void insertElmentGenerator(string mosad,string promotional)
        {
            try
            {
                fp.SelectRequerdName("formcontrolname='InstitutionEducation",mosad);
                if(Int16.Parse( DateTime.Today.ToString("MM"))<10)
                    fp.SelectRequerdName("formcontrolname='CreditMonth", DateTime.Today.ToString("M/yyyy"));
                else
                    fp.SelectRequerdName("formcontrolname='CreditMonth", DateTime.Today.ToString("MM/yyyy"));
                Thread.Sleep(100);
                fp.SelectRequerdName("formcontrolname='FinancialClausesID", promotional,false);
                fp.ClickButton("הצג אברכים לזיכוי ");
                Thread.Sleep(200);
                fp.ClickButton("חולל זיכויים ");
                Thread.Sleep(350);
            } catch { throw new NotImplementedException(); }

        }

        private void checkElmentGenerator()
        {
            try { fp.checkElmentText("title",By.ClassName("main-txt"), "מחולל זיכויים"); 
                fp.checkElmentText("title",By.CssSelector("mat-select[formcontrolname='InstitutionEducation']"), "מוסד לימודים"); 
                fp.checkElmentText("title",By.CssSelector("mat-select[formcontrolname='CreditMonth']"), "בחר חודש זיכוי"); 
                fp.checkElmentText("title",By.CssSelector("mat-select[formcontrolname='FinancialClausesID']"), "סעיף זיכוי"); 
            } catch { throw new NotImplementedException(); }
        }

        public void stepCraditPayments()
        {
            try { checkElmentCreaditPayment();
                insertElmentCreaditPayment();
                Thread.Sleep(500);
            } catch { fp.picter("stepCraditPayments"); throw new NotImplementedException(); }
        }

        private void insertElmentCreaditPayment(string nameStudent="אוטומציה אוטומציה")
        {
            try {
                driver.FindElements(By.TagName("mat-select"))[2].Click();
                fp.selectText(" לגבייה ");
                int numrow = countNumRowPayment();
                fp.click(By.XPath("//div[@mattooltip='הדפסת המחאות']"));
                popupPrint(nameStudent,numrow);
            } catch { throw new NotImplementedException(); }
        }
        public int countNumRowPayment()
        {
            int numrow= fp.splitnumber(driver.FindElement(By.ClassName("mat-paginator-range-label")).Text);
                string numrowS = numrow.ToString().Substring(3);
                return numrow = numrowS != "" ? Int16.Parse(numrowS) : 1;

        }
        private void popupPrint(string nameStudent,int countRow)
        {
            try {
                
                checkElmentPrint(nameStudent,countRow);
                insertElmentPrint(nameStudent,countRow); } catch { fp.picter("popupPrint"); throw new NotImplementedException(); }
        }

        public void insertElmentPrint(string nameStudent,int countRow)
        {
            try { 
                fp.insertElment("generalRemark",By.CssSelector("textarea[formcontrolname='generalRemark']"));
                createConatrabition cc = new createConatrabition();
                cc.calanederCheck(1, 0);
                fp.SelectRequerdName("formcontrolname='accountID", " "+ automation + " 7-"+numberAmountBank+" ");
                fp.insertElment("checkNumber1", By.CssSelector("input[formcontrolname='checkNumber1']"), "145");
                //fp.insertElment("checkNumber2", By.CssSelector("input[formcontrolname='checkNumber2']"), "200000");
                string[] tablestuedentCheck = { "", /*nameStudent*/"", "", "","", "", "", "-1" };
                fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck, "mat-checkbox", "tr", "td", 0, countRow);
                fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck, "mat-checkbox", "tr", "td", 0,countRow);
                fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck, "הרב החשוב הצדיק STRING", "tr", "td", 2, countRow );
                fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck, "12/12/2023STRINGNO", "tr", "td",4, countRow );
                try { fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck, "תשלום גם על חודשים קודמים STRING", "tr", "td", 6, countRow); } catch { }
                fp.ClickButton("הדפסת המחאות ");
                popupApprovePrint("ביטול");
                fp.ClickButton("הדפסת המחאות ");
                popupApprovePrint("אישור");
                Thread.Sleep(1800);
                Thread.Sleep(250);
                CreateNewDenumic cnd = new CreateNewDenumic();
                if (cnd.CheckFileDownloaded("Downloads\\סיכום המחאות") == false)
                    throw new NotImplementedException("המחאות לא הודפס");
            }
                 
            catch { throw new NotImplementedException(); }
        }

        private void popupApprovePrint(string stringApp)
        {
            try { fp.checkElmentText("title",By.ClassName("title"), " הדפסת המחאות ");
                // fp.checkElmentText("message",By.ClassName("ng-star-inserted"),);
                fp.checkElmentText("message ", By.CssSelector("div[class='warning-text ng-star-inserted']"), "לאחר אישור, לא יתאפשר להשתמש שוב במספרי ההמחאות שנבחרו"); fp.ClickButton(stringApp);
            } catch { throw new NotImplementedException(); }
        }

        private void checkElmentPrint(string nameStudent,int countRow)
        {
            try { fp.checkElmentText("title print",By.ClassName("main-title"), "נתונים להדפסת המחאות לחודש "+DateTime.Today.ToString("MM-yyyy"));
                fp.checkElmentText("generalRemark",By.CssSelector("textarea[formcontrolname='generalRemark']"), "הערה כללית");
                fp.checkSelectPreform("תאריך להדפסה על ההמחאות");
                fp.checkElmentText("checkNumber1", By.CssSelector("input[formcontrolname='checkNumber1']"), "ממספר המחאה");
                fp.checkElmentText("accountID", By.CssSelector("mat-select[formcontrolname='accountID']"), "חשבון בנק לחיוב");
                fp.checkElmentText("checkNumber2", By.CssSelector("input[formcontrolname='checkNumber2']"), "עד מספר המחאה");
                fp.checkElment("shearch",By.CssSelector("input[placeholder='חיפוש']"));
                fp.checkElmentText("display data printed", By.ClassName("mat-checkbox-label"), "הצג נתונים עבור המחאות שהודפסו ");
                string[] heder = {"", "שם האברך", "לפקודת", "סכום", "תאריך ההמחאה", "מספר ההמחאה", "הערה אישית","-1" };
                fp.checkColumn("thead[role='rowgroup']", heder,"false","tr","th",-1,countRow);
                string[] tablestuedentCheck = {"", /*nameStudent*/"","","","","","","-1" };
                fp.checkColumn("tbody[role='rowgroup']", tablestuedentCheck,"false","tr","td",-1,countRow);
            }
            catch { throw new NotImplementedException(); }
        }

        private int checkTable(string nameStudent)
        {
            string[] heder = { "", "זיכויים", "תשלומים", "-1" };
            fp.checkColumn("thead[role='rowgroup']", heder, "false", "tr", "th");
            string[] heder2 = { "", "שם האברך", "חודש זיכוי", "סכום הזיכויים", "סכום הבונוסים", "סכום הקיזוזים", "סך הכל", "סכום התשלומים", "סטטוס התשלום", "-1" };
            fp.checkColumn("thead[role='rowgroup']", heder2,"false", "tr", "th");
            string[] student = { "", /*nameStudent*/"", DateTime.Today.ToString("MM-yyyy"), "", "", "", "", "", "לגבייה" ,"-1"};
         return fp.checkColumn("tbody[role='rowgroup']", student, "false");
        }

        private void checkElmentCreaditPayment()
        {
            try { fp.checkElmentText("title",By.XPath("//div[contins(@class,'main-txt title')]"), "זיכויים ותשלומים");
                fp.checkSelectPreform("סטטוס");
                fp.checkElment("mont payment",By.XPath("//input[@placeholder='חודש']"));
                fp.checkElment("icon filter",By.XPath("//div[@mattooltip='סינון לפי פרטי האברך']"));
                fp.checkElment("icon PDF",By.XPath("//div[@mattooltip='יצא לPDF']"));
                fp.checkElment("icon Excel",By.XPath("//div[@mattooltip='יצא לאקסל']"));
                fp.checkElment("icon filter",By.XPath("//div[@mattooltip='הדפסת המחאות']"));
                fp.checkElment("icon filter",By.XPath("//div[@mattooltip='ביטול המחאה']"));
                fp.checkElment("icon filter",By.XPath("//div[@mattooltip='הוספת זיכוי לאברך']"));
            } catch { throw new NotImplementedException(); }
        }

        internal void clickCancel()
        {
           // driver.SwitchTo().Window("print-preview-app");
            driver.FindElement(By.ClassName("cancel-button")).Click();
           // driver.SwitchTo().DefaultContent();
            //throw new NotImplementedException();
        }

        internal void cenelloctionCheck(int numCheck)
        {
            try {
                checkTableAfterPrint();
                fp.click(By.XPath("//div[@mattooltip='ביטול המחאה']"));
               checkElmentPopupCheckCancel();
               insertElmentPopupCheckCancel(numCheck);
            }
            catch { fp.picter("cenelloctionCheck"); throw new NotImplementedException(); }
        }

        private void checkTableAfterPrint()
        {
            try { driver.FindElements(By.TagName("mat-select"))[2].Click();
                fp.selectText(" בוצע "); string[] rowTa = {"","","","","","","","", " בוצע","-1" }; fp.checkColumn("tbody[role='rowgroup']", rowTa, "false");
            } catch { throw new NotImplementedException(); }
        }

        private void checkElmentPopupCheckCancel()
        {
            try { fp.checkElmentText("title popupcaneloocation check",By.ClassName("main-title"), "ביטול המחאה");
                fp.checkElmentText("Bank account",By.CssSelector("mat-select[formcontrolname='accountID']"), "חשבון בנק");
                fp.checkElmentText("checkNumber", By.CssSelector("input[formcontrolname='checkNumber']"), "מספר ההמחאה");
            } catch { throw new NotImplementedException(); }
        }

        private void insertElmentPopupCheckCancel(int checkNumber, string acountId= " אוטומציה 7-123456 ")
        {
            try {
                     fp.SelectRequerdName("formcontrolname='accountID", acountId);
                    fp.insertElment("checkNumber", By.CssSelector("input[formcontrolname='checkNumber']"), checkNumber.ToString());
                fp.ClickButton("אישור ");
                } catch { throw new NotImplementedException(); }
        }
        public void addPromtionalPaymenMethod(string[] student,string paymentMethod="")
        {
                fp.checkColumn("tbody[role='rowgroup']",student, "i","tr","td",0);
                fp.checkElment("popup student",By.CssSelector("div[aria-label='divOpenParentDialog']"));
                fp.checkElmentText("name student",By.ClassName("name-text"),automation+" "+automation);
            if(paymentMethod!="")
            { fp.SelectRequerdName("aria-label='בחר אופן קבלת תשלום מועדף",paymentMethod,false);
                if (paymentMethod == "העברה בנקאית")
                {
                    fp.click(By.XPath("//span/i[@class='material-icons'][text()=' settings_ethernet ']"));
                    fp.click(By.CssSelector("i[class='material-icons button-icon-add']"));
                    createConatrabition cc = new createConatrabition();
                    cc.checkPoupBank("-1");
                    fp.SelectRequerdName("formcontrolname='ClearingEntityID", "מסב זיכויים", false);
                    fp.ClickButton("שמירה", "span",2);
                }
                fp.ClickButton("שמירה","span");
            }
                fp.closePoup();
                fp.checkColumn("tbody[role='rowgroup']", student,"");
                fp.checkColumn("tbody[role='rowgroup']", student, "i[mattooltip='הוסף זיכוי']", "tr", "td",0);
                popupAddPromtional();
                Thread.Sleep(500);
        }
        public  void clickOnTableP()
        {
            try {
                string[] clickStudent = {"",""/*automation+" "+automation*/,"-1"  };
                string[] changeStatus = { "","","","2","","","","","לגבייה","-1"};
                addPromtionalPaymenMethod(clickStudent);
                
                fp.checkColumn("tbody[role='rowgroup']", changeStatus, "false");
                fp.checkColumn("tbody[role='rowgroup']", clickStudent, "i[mattooltip='הוסף בונוס']", "tr", "td",0);
                popupBonos();
                Thread.Sleep(500);
                 changeStatus[4] =  "1";
                changeStatus[3] = "";
                fp.checkColumn("tbody[role='rowgroup']", changeStatus, "false");
                fp.checkColumn("tbody[role='rowgroup']", clickStudent, "i[mattooltip='הוסף קיזוז']", "tr", "td", 0);
                popupOffset();
            }
            catch { fp.picter("לחיצה על טבלאות בזיכויים ותשלומים"); throw new NotImplementedException(); }
        }

        private void popupOffset()
        {
            try { checkElmentBonos("קיזוז"); insertElmentBonos("קיזוז"); } catch { throw new NotImplementedException(); }
        }

        private void popupBonos()
        {
            try { checkElmentBonos("בונוס");insertElmentBonos("בונוס"); } catch { throw new NotImplementedException(); }
        }

        private void insertElmentBonos(string typeAction)
        {
            try {
                fp.SelectRequerdName("formcontrolname='FinancialClausesID", typeAction);
                if (typeAction == "בונוס")
                {
                    driver.FindElement(By.CssSelector("input[formcontrolname='Count']")).Clear();
                    fp.insertElment("Count", By.CssSelector("input[formcontrolname='Count']"), "1");
                }
                else
                    fp.insertElment("Sum", By.CssSelector("input[formcontrolname='Sum']"), "1");
                fp.SelectRequerdName("formcontrolname='CurrencyID", "שקל",false);
                createConatrabition cc = new createConatrabition();
                cc.calanederCheckOld(1,1);
                fp.insertElment("Details", By.CssSelector("input[formcontrolname='Details']"));
                fp.ClickButton("שמירה");
            }
            catch { throw new NotImplementedException(); }
        }

        private void checkElmentBonos(string typeAction)
        {
            try { fp.checkElmentText("title",By.ClassName("main-title"), "הוספת "+typeAction);
                fp.checkElmentText("FinancialClausesID",By.CssSelector("mat-select[formcontrolname='FinancialClausesID']"), "סעיף פיננסי");
               // if(typeAction=="בונוס")
                 //   fp.checkElmentText("Count", By.CssSelector("input[formcontrolname='Count']"), "כמות");
                fp.checkElmentText("sum", By.CssSelector("input[formcontrolname='Sum']"), "סכום");
                fp.checkElmentText("CurrencyID", By.CssSelector("mat-select[formcontrolname='CurrencyID']"), "מטבע");
                fp.checkSelectPreform("תאריך זיכוי");
                fp.checkElmentText("Details", By.CssSelector("input[formcontrolname='Details']"), "הערות");
            }
            catch { throw new NotImplementedException(); }
        }

        private void popupAddPromtional()
        {
            try { 
                checckElmentPromtional();
                insertElmentPromtional();
            } catch { throw new NotImplementedException(); }
        }

        public  void insertElmentPromtional(string finance="כולל")
        {
            try {
                fp.SelectRequerdName("formcontrolname='FinancialClausesID",finance);
                fp.insertElment("sum", By.CssSelector("input[formcontrolname='Sum']"), "1");
                fp.SelectRequerdName("formcontrolname='CurrencyID", " שקל ",false);
                createConatrabition cc = new createConatrabition();
                cc.calanederCheckOld(1,1);
                fp.insertElment("Details", By.CssSelector("input[formcontrolname='Details']"));
                fp.ClickButton("שמירה ");
            }
            catch { throw new NotImplementedException(); }
        }

        private void checckElmentPromtional()
        {
            try { fp.checkElmentText("title",By.ClassName("main-title"), "הוספת זיכוי");
                fp.checkElmentText("FinancialClausesID",By.CssSelector("mat-select[formcontrolname='FinancialClausesID']"), "סעיף פיננסי");
                fp.checkElmentText("sum",By.CssSelector("input[formcontrolname='Sum']"), "סכום");
                fp.checkElmentText("CurrencyID", By.CssSelector("mat-select[formcontrolname='CurrencyID']"), "מטבע");
                fp.checkSelectPreform("תאריך זיכוי");
                fp.checkElmentText("Details", By.CssSelector("input[formcontrolname='Details']"), "הערות");
            }
            catch { throw new NotImplementedException(); }
        }

        public  void stepCreaditStudent(string typeCredit="בונוס",string nameAvrec="אברך")
        {
            try {
                Thread.Sleep(950);
                fp.click(By.XPath("//div[@mattooltip='הוספת זיכוי לאברך']"));
                fp.SelectRequerdName("formcontrolname='CreditTypeID",typeCredit,false);
                createConatrabition cc = new createConatrabition();
                cc.clickFrind(nameAvrec,1.ToString(),"אברך כולל");
                if (typeCredit == "בונוס")
                    insertElmentBonos("בונוס");
                else
                    insertElmentPromtional();
            } catch { fp.picter("stepCreaditStudent"); throw new NotImplementedException(); }
        }

        public  void checkTable()
        {
            try {
                Thread.Sleep(500);
                string numExpanses = driver.FindElement(By.CssSelector("div[class='row-number-txt']")).Text;
                int n = 1;
                if (numExpanses.Contains("הוצאה"))
                    n = 2;
                string[] val = { "יש לך", "הוצאות","הוצאה" };
                string[] num = numExpanses.Split(val, StringSplitOptions.None);
                if (Int16.Parse(num[n].Trim()) == 0)
                    fp.picter("זיכוי לאברך לא נשמר בטבלת הוצאות");
                else
                {
                    string[] listStuden = { "", "", "", "", "", "שולם", "", DateTime.Today.ToString("dd/MM/yyyy"), "צ'ק", "", "-1" };
                    fp.checkColumn("//", listStuden, "false", "mat-row", "mat-cell");
                }
            } catch { throw new NotImplementedException(); }
        }

        public  void insertDataExcel()
        {
            try { ExcelApiTest eat = new ExcelApiTest("ע", "קובץ", "C:\\Users\\konosoftuser\\Downloads\\קובץ זיכוים למילוי.csv");
                string sum = "100";
                string[] insertSum = {"100","30","50","60" };
                eat.insertExcelColl("קובץ זיכוים למילוי",insertSum , @"C:\Users\konosoftuser\Downloads\קובץ זיכוים למילוי.csv");
                fp.click(By.XPath("//div[@mattooltip='העלאת קובץ זיכויים']/i[text()='publish']"));
                   process ps = new process();
                 ps.popupAttachFile("C:\\Users\\konosoftuser\\Downloads\\קובץ זיכוים למילוי.csv.xlsx", "");
            } catch { throw new NotImplementedException(); }
        }

        public  void checkSave()
        {
            try {if (!driver.FindElement(By.XPath("//div[text()='נוספו בהצלחה']")).Displayed)
                { fp.picter("check save upload zicoim not upload"); } } catch { throw new NotImplementedException(); }
        }

        public  void cholarships(string yesPopupAmla="")
        {
            try {
                string[] clickTab = { "מזומן", "העברה בנקאית ", "-1" };
                for (int i = 0; i < clickTab.Length-1; i++)
                {
                    //driver.FindElements(By.ClassName("mat-tab-label-content"))[i].Click();
                    fp.click(By.XPath("//div[@class='mat-tab-label-content'][text()='" + clickTab[i] + "']"));
                    string[] checkHeder = { "", "שם", "סעיף פיננסי", "סכום", "מטבע", "תאריך התשלום", "סטטוס התשלום", "אופן התשלום","-1" };
                    string[] tableRow = { "","","","","שקל",DateTime.Today.ToString("dd/MM/yy"),"לגבייה",clickTab[i],"-1"};
                    if (clickTab[i] == "צ'ק")
                    { string[] heder = { "","שם","לפקודת","סכום","תאריך ההמחאה","מספר ההמחאה","הערה אישית","אופן תשלום","-1"};
                        checkHeder = heder;
                        string[] row = { "","","","",DateTime.Today.ToString("dd/MM/yyyy"),"","","צ'ק","-1"};
                        tableRow = row;
                    }
                    fp.checkColumn("thead[role='rowgroup']", checkHeder,"false","tr", "mat-header-cell",-1,1);
                    fp.checkColumn("tbody[role='rowgroup']", tableRow,"false","tr", "mat-cell",-1,1);
                    if (clickTab[i] == "העברה בנקאית ")
                    {
                        fp.SelectRequerdName("aria-label='מוסד סליקה", "מסב זיכוים", false);
                    }
                    if(clickTab[i]!="צ'ק")
                        fp.ClickButton("ביצוע תשלומים ","span");
                    if (yesPopupAmla == "כן" && clickTab[i]== "העברה בנקאית ")
                    {
                        popupAmla(yesPopupAmla);
                        TelephoneVerification();
                    }

                } } catch { throw new NotImplementedException(); }
        }

        public void TelephoneVerification()
        {
            try { fp.checkElmentText("title TelephoneVerification", By.CssSelector("main-txt ng-star-inserted"), "הנך עומד/ת לבצע ");
                fp.checkElmentsText("title",By.ClassName("main-txt"), "בסך של ",1);
                fp.SelectRequerdName("aria-label='בחר מספר טלפון לקבלת קוד",phonME,false);
                fp.checkElmentText("type send",By.ClassName("send"), " שלח קוד אימות ");
                fp.checkElmentText("voiceMail",By.ClassName("hodaha"), "בהודעה קולית");
                fp.checkElmentText("insert kode send you",By.CssSelector("input[placeholder='הזן את הקוד שנשלח אליך']"), "הזן את הקוד שנשלח אליך");
                fp.checkElment("button check kode",By.XPath("//span[@class='mat-button-wrapper'][text()='בדיקת קוד אימות']"));
                fp.checkElment("button gvia",By.XPath("//span[@class='mat-button-wrapper'][text()='בצע גבייה']"));
                fp.ClickButton("בצינתוק","span");
                Thread.Sleep(9000);
                try
                {
                    if (driver.FindElement(By.CssSelector("input[placeholder='הזן את הקוד שנשלח אליך']")).GetAttribute("value") != "")
                    {
                        fp.ClickButton("בדיקת קוד אימות", "span");
                        fp.ClickButton("בצע גבייה", "span");
                        CreateNewDenumic cnd = new CreateNewDenumic();
                        if (cnd.CheckFileDownloaded("\\Downloads\\MSV_0962210") == false)
                            throw new System.Exception("לא ירד");
                    }
                    else
                        fp.click(By.XPath("span/i[@class='material-icons close-btn'][text()=' close ']"));
                            }
                catch {
                        fp.click(By.XPath("span/i[@class='material-icons close-btn'][text()=' close ']"));
                }
            } catch { throw new NotImplementedException(); }
        }

        private void popupAmla(string yesPopupAmla)
        {
            try { fp.checkElmentText("title",By.ClassName("title"), " אשר פעולה ");
                fp.checkElmentText("mesage",By.ClassName("wrap-message"), "נא לאשר זיכוי עסקה");
                fp.checkElmentText("message",By.CssSelector("div[class='warning-text ng-star-inserted']"), "פעולה זו כרוכה בעמלה");
                fp.ClickButton(yesPopupAmla, "span");
            } catch { throw new NotImplementedException(); }
        }
    }
}