﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class HebDates
    {
        private static CultureInfo he;
        static HebDates()
        {
            he = CultureInfo.CreateSpecificCulture("he-IL");
            he.DateTimeFormat.Calendar = new HebrewCalendar();
        }
        /// <summary>
        /// מקבל תאריך עברי - מחזיר תאריך לועזי 
        /// </summary>
        /// <param name="sHebDate">תאריך עברי</param>
        /// <param name="tryFix">נסה לתקן</param>
        /// <returns></returns>
        public static DateTime? FromHeb(string sHebDate, bool tryFix = true)
        {
            DateTime dt;
            if (DateTime.TryParse(sHebDate, he, DateTimeStyles.None, out dt))
                return dt;
            if (tryFix)
            {
                string[] arr = sHebDate.Split(new char[] { ' ' });
                if (arr.Length < 3)
                {
                    return null;
                }
                string day = arr[0];
                string month = arr[1];
                string year = arr[arr.Length - 1];
                if (arr.Length == 4)
                {
                    month = month + " " + arr[2];
                }
                if (day.Length == 2)
                {
                    day = day.Substring(0, 1) + "\"" + day.Substring(1, 1);
                }
                else if (day.Length == 1)
                {
                    day = day + "'";
                }
                if (year.Length >= 3 && !year.Contains("\""))
                {
                    year = year.Substring(0, year.Length - 1) + "\"" + year.Substring(year.Length - 1, 1);
                }
                //if (month.StartsWith("אדר א"))
                //{
                //    month = "אדר";                    
                //}
                return FromHeb($"{day} {month} {year}", false);
            }
            return null;
        }
        /// <summary>
        /// מקבל תאריך לועזי - מחזיר תאריך עברי מלא
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public  string FullDate(DateTime d)
        {
            return d.ToString("dd MMMM yyyy", he);
        }
    }
}
