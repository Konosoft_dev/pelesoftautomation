﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class Bulding:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public Bulding()
        {
        }

        public  void openNewB()
        {
            try {
                openNew(" בניינים ");
            } catch { throw new NotImplementedException(); }
        }
        public void openNew(string nameNew)
        {
            open(nameNew);
            fp.ButtonNew("");
        }
        public void open(string name)
        { 
            fp.clickNameDefinitions(" " + niulBuldin + " ");
            fp.clickNameDefinitions(name);
        }
        public void insertB()
        {
            try { fp.selectForRequerd("עיר");
                fp.insertElment("",By.Name("Street"),Streets[fp.rand(Streets.Length)]);
                fp.insertElment("",By.Name("HouseNumber"),fp.rand(25).ToString());
                fp.insertElment("Floors", By.Name("Floors"),fp.rand(5).ToString());
                fp.insertElment("",By.Name("Enterance"),"א");
                fp.selectForRequerd("מספר בנק", false);
                fp.selectForRequerd("מספר סניף", false);
                fp.insertElment("AccountNumber", By.Name("AccountNumber"), "123456");
                fp.insertElment("HouseManagmentEmail", By.Name("HouseManagmentEmail"), MAIL);
                fp.insertElment("MonthlyManagementFee", By.Name("MonthlyManagementFee"), "180");
                fp.insertElment("HouseCommitteeFees", By.Name("HouseCommitteeFees"), "180");
                fp.ClickButton(" לשלב הבא ", "span");
                fp.insertElment("",By.CssSelector("input[aria-label='InputStartApartment']"),"1");
                fp.insertElment("",By.CssSelector("input[aria-label='InputEndApartment']"),"90");
                fp.ClickButton(" צור בנין חדש ","span");
            } catch { throw new NotImplementedException(); }
        }

        public void openHome()
        {
            try { openNew(" דירות "); } catch { throw new NotImplementedException(); }
        }

        public  void insertHome(string homeCut="לא",string mony="לא")
        {
            try { createConatrabition cc = new createConatrabition();
                cc.clickFrind("בנין","1");
                fp.insertElment("", By.Name("Number"), fp.rand(50).ToString());
                fp.insertElment("", By.Name("Subnumber"), "א");
                fp.insertElment("Floor", By.Name("Floor"), fp.rand(8).ToString());
                fp.selectForRequerd("סוג נכס",false,-1);
                fp.selectText("דירה");
                fp.selectForRequerd("דירה מפוצלת?",false,-1);
                fp.selectText(homeCut);
                fp.selectForRequerd("תשלום נפרד?", false,-1);
                fp.selectText(mony);
                fp.insertElment("HouseCommitteeFees", By.Name("HouseCommitteeFees"),"180");
                fp.insertElment("textarea",By.TagName("textarea"));
                fp.insertElment("newElectCounter", By.Name("newElectCounter"),"15");
                fp.ClickButton(" צור דירה חדשה ","span");
            } catch { throw new NotImplementedException(); }
        }

        public  void insertTenant(string v="לא",string famli="",string zika= " בעל הדירה ")
        {
            try { createConatrabition cc = new createConatrabition();
                cc.clickFrind("בנין","1");
                Financical fl = new Financical();
                fp.insertElment("Identity",By.Name("Identity"), fl.tzPupil());
                famli = famli == "" ? familys[fp.rand(familys.Length)] : famli;
                fp.insertElment("LastName", By.Name("LastName"), famli);
                fp.insertElment("FirstName", By.Name("FirstName"), names[fp.rand(names.Length)]);
                fp.insertElment("PhoneNumber", By.Name("PhoneNumber"), poneHome);
                fp.insertElment("WorkPhone", By.Name("WorkPhone"), poneHome);
                fp.insertElment("Fax", By.Name("Fax"), poneHome);
                fp.insertElment("CellPhone", By.Name("CellPhone"), phonME);
                fp.insertElment("email", By.Name("email"), MAIL);
                fp.insertElment("WhatsApp", By.Name("WhatsApp"), MAIL);
                fp.insertElment("spouseName", By.Name("spouseName"), mather[fp.rand(mather.Length)]);
                fp.insertElment("spousePhoneNumber", By.Name("spousePhoneNumber"), poneHome);
                fp.insertElment("spouseCellPhone", By.Name("spouseCellPhone"), phonME);
                fp.insertElment("spouseEmail", By.Name("spouseEmail"), MAIL);
                fp.insertElment("lawyerName", By.Name("lawyerName"), names[fp.rand(names.Length)]);
                fp.insertElment("lawyerPhoneNumber", By.Name("lawyerPhoneNumber"), poneHome);
                fp.insertElment("lawyerCellPhone", By.Name("lawyerCellPhone"), phonME);
                fp.insertElment("lawyerEmail", By.Name("lawyerEmail"), MAIL);
                fp.selectForRequerd("שם אחר לקבלה", false,-1);
                fp.selectText(v);
                fp.selectForRequerd("אופן משלוח קבלה", false,-1);
                fp.selectText(" מייל + דואר ");
                fp.insertElment("ReceiptAddress", By.Name("ReceiptAddress"),Streets[fp.rand(Streets.Length)]);
                fp.insertElment("ReceiptMail", By.Name("ReceiptMail"),MAIL);
                fp.selectForRequerd("דירה",false);
                cc.calanederCheckOld(1,0);
                cc.calanederCheckOld(1,1,"",5,1);
                fp.selectForRequerd("זיקה",false,-1);
                fp.selectText(zika);
                fp.selectForRequerd("איש קשר", false);
                fp.insertElment("textarea",By.TagName("textarea"));
                fp.ClickButton(" צור דייר לדירה חדש ","span");
            } catch { throw new NotImplementedException(); }
        }
    }
}