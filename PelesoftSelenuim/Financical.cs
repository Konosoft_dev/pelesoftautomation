﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{

    class Financical :page
    {
      //static IWebDriver driver;// = new ChromeDriver();

        FunctionPelesoft fp = new FunctionPelesoft();
        SucessEndCreate sec = new SucessEndCreate();
        CreateNewDenumic cnd = new CreateNewDenumic();
        public void createNewFinancicalSection(string TypeTable = " תרומות ",string model = "",string name="אוטומציה",string type=" חיוב ",string clearingE="aaa")
        {
            if (model == "")
                model = nameFrind + "ים";
           // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            fp.actionWork(" ניהול כספים ","ניהול סעיפים פיננסיים");
            fp.ButtonNew(FinancicalSection);
            cnd.checkClasscreate(FinancicalSection, false);
            checkPageFinancical(TypeTable, model,name,type,clearingE);
            sec.checkEndCreate(FinancicalSection);
        }
        public string  tzPupil()
        {
            string tz = fp.rand(99999999).ToString();
            int calc = 0;
            int toza = 0;
            for(int i=0;i<tz.Length;i++)
            {
                //calc = tz[i];
                if (i % 2 == 0)
                    calc = Int16.Parse(tz[i].ToString()) * 1;
                else
                    calc = Int16.Parse(tz[i].ToString()) * 2;
                if (calc > 9)
                    calc = calc/10 + calc%10;
                toza += calc;
                
            }
            if (toza % 10 > 0)
                calc = 10 - toza % 10;
            string ret=tz+calc.ToString();
            return ret;
        }
        public void checkPageFinancical(string TypeTable,string Model,string nameFinance,string typeFinance,string ddClearingE="aaa")
        {
            /* try
             {*/

            fp.checkElment("title details", By.XPath("//div[@class='title-section'][text()='פרטי " + FinancicalSection + "']"));
            fp.checkElment("description", By.XPath("//input[@formcontrolname='ClauseDescription']"), false,true);
            fp.insertElment("description", By.XPath("//input[@formcontrolname='ClauseDescription']"), nameFinance);
            SelectRequerdName("ModuleID",Model);//, 3, By.XPath("//input[@ng-reflect-name='ClauseDescription']"));
          //  driver.FindElement(By.XPath("//input[@ng-reflect-name='ClauseDescription']")).Click();
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='TableID']")).Click();
            driver.FindElement(By.XPath("//span[@class='mat-option-text'][text()='"+ TypeTable + "']")).Click();
            fp.SelectRequerdName("formcontrolname='FinancialClauseTypeID", typeFinance);
            if (typeFinance == "בונוס")
                fp.insertElment("sum", By.CssSelector("input[formcontrolname='Sum']"), "1");
            if(ddClearingE.Contains("אשראי"))
                fp.SelectRequerdName("formcontrolname='CreditClearingEntities", ddClearingE, false);
             else if(ddClearingE!="")
                fp.SelectRequerdName("formcontrolname='DirectDebitClearingEntities", ddClearingE, false);
                //driver.FindElement(By.Id("mat-option-74")).Click();
                fp.ClickButton(" צור " + FinancicalSection + " חדש ");
            /*}
            catch { throw new NotImplementedException("check page financi not good"); }*/
        }
        /*public void SelectRequerdNameSelect(string nameSelect, string ValueSelect)//, int numErr, By idFromCheck)
        {

            fp.checkElment("model", By.XPath("//mat-select[@formcontrolname='" + nameSelect + "']"));
            /*driver.FindElement(By.XPath("//mat-select[@formcontrolname='"+ nameSelect + "']")).Click();
             driver.FindElement(idFromCheck).Click();

             funError(numErr);
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='" + nameSelect + "']")).Click();
            fp.selectText(ValueSelect);
        }*/
        public void SelectRequerdName(string name,string val)
        {
            fp.checkElment("model", By.XPath("//mat-select[@formcontrolname='" + name + "']"));
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='" + name + "']")).Click();
            fp.selectText(val);
        }
        public  void closePage()
        {
            driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();

        }

    }
}
