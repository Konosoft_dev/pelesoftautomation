﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;

namespace PelesoftSelenuim
{
     class receStrct:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        SucessEndCreate sec = new SucessEndCreate();

        public void stepRec()
        {
            fp.actionWork(" הגדרות ","מבני קבלות");
            fp.ButtonNew("מבנה קבלה");
            checkElment();
            insertElment();
            sec.checkEndCreate("מבנה קבלה");
        }
        public  void checkElment()
        {
            fp.checkCreate("מבנה קבלה");
            string[] titles = {"מאפייני קבלה","מספר קבלה","כותרות מסמך","מאפייני דוא\"ל בשליחת קבלה" };
            fp.checkTitles(titles);
            fp.checkSelectPreform("בחר/י סוג קבלה עבור המבנה קבלה", By.XPath(""));
            fp.checkElmentText("select pdf", By.Name("PDFDocumentID_Display"), "בחר/י מסמך PDF");
            fp.checkSelectPreform("בחר/י תאריך יצירה אחרון",By.Name(""));
            //fp.checkElmentText("start number receipt", By.Name("Prefix"), "תחילית מספרי קבלה");
            fp.checkElmentText("Leading Zeros", By.Name("LeadingZeros"), "אפסים מובילים");
            fp.checkElmentText("Start Num", By.Name("StartNum"), "החל ממספר");
            fp.checkElmentText("Institution Name", By.Name("InstitutionName"), "שם מוסד");
            fp.checkElmentText("Institution ENG Name", By.Name("InstitutionEngName"), "שם מוסד באנגלית");
            fp.checkElmentText("Secondary Title", By.Name("SecondaryTitle"), "כותרת משנה");
            fp.checkElmentText("Footer Title", By.Name("FooterTitle"), "כותרת תחתונה");
            fp.checkElmentText("TaxNum", By.Name("TaxNum"), "מספר אישור מס הכנסה");
            fp.checkElment("data mail", By.TagName("textarea"));

           // throw new NotImplementedException();
        }

        public  void insertElment()
        {
            fp.selectForRequerd("בחר/י סוג קבלה עבור המבנה קבלה");
            PDF();
            //fp.checkSelectPreform("בחר/י תאריך יצירה אחרון", By.Name(""));
           // fp.waitUntil(By.Name("Prefix"));
           // fp.insertElment("start number receipt", By.Name("Prefix"),"555");
            fp.insertElment("Leading Zeros", By.Name("LeadingZeros"), "5");
            fp.insertElment("Start Num", By.Name("StartNum"), "5");
            fp.insertElment("Institution Name", By.Name("InstitutionName"), automation);
            fp.insertElment("Institution ENG Name", By.Name("InstitutionEngName"), "automation");
            fp.insertElment("Secondary Title", By.Name("SecondaryTitle"), "אוטו");
            fp.insertElment("Footer Title", By.Name("FooterTitle"), "או");
            fp.insertElment("TaxNum", By.Name("TaxNum"), "123456789");
            fp.insertElment("data mail", By.TagName("textarea"),automation);
            fp.ClickButton(" צור מבנה קבלה חדש ", "span");
        }

        public void PDF()
        {
            driver.FindElement(By.XPath("//i[text()=' edit ']")).Click();
            fp.selectForRequerd("הקלד/י את שם המסמך PDF או מספר מזהה",false,-1);//).SendKeys(Keys.Enter);
            fp.selectForRequerd("הקלד/י את שם המסמך PDF או מספר מזהה",false,-1,Keys.Enter);//).SendKeys(Keys.Enter);
            new Actions(driver).DoubleClick(driver.FindElements(By.CssSelector("div[class='item-data ng-star-inserted']"))[0]).Perform();
        }

        public void stepPDF()
        {
            fp.actionWork(" הגדרות ","מסמכי PDF");
            fp.ButtonNew("מסמך PDF");
            checkElmentPDF();
            insertElmentPDF();

            fp.ClickButton(" צור מסמך PDF חדש ","span");
            sec.checkEndCreate("מסמך PDF",false,"", " התחל עיצוב מסמך PDF ");
        }

        public  void insertElmentPDF()
        {
            fp.insertElment("nameDisplay", By.Name("DisplayName"), automation,true);
            fp.selectForRequerd("בחר/י מודול עבור המסמך PDF",false);
            fp.insertElment("DataObjectType", By.Name("DataObjectType"), "Pelesoft.Server.PdfTypes.PDFReceipt", true);
            fp.selectForRequerd(" TableID ", false);
            CreateMesaur cm = new CreateMesaur();
            cm.clickNamePicter("picture_as_pdf");
            fp.insertElment("functiionName", By.Name("PDFFunctionName"),"jhj");

        }

        public void checkElmentPDF()
        {
            fp.checkCreate("מסמך PDF");
            string[] titles = { "מאפייני מסמך PDF" };
            fp.checkTitles(titles);
            fp.checkElmentText("nameDisplay", By.Name("DisplayName"), "שם לתצוגה");
            fp.checkSelectPreform("בחר/י מודול עבור המסמך PDF", By.Name("DisplayName"));
            fp.checkElmentText("DataObjectType", By.Name("DataObjectType"), "טיפוס נתונים");
            // throw new NotImplementedException();
        }
    }
}