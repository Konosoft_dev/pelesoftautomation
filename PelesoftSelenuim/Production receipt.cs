﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class Production_receipt:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        private ReadOnlyCollection<IWebElement> listTable;
        HandlingHQReceipts hhqr = new HandlingHQReceipts();

        public void ProductionReceipt()
        {
            //fp.ClickList(nameFrind + "ים");
            Thread.Sleep(350);

            fp.actionWork(niaolCase, "טיפול בקבלות");
            Thread.Sleep(150);
        }
        public void checkPageProductionReceipt(string hq="",string typeselect="",string nameFrind="איש קשר")
        {
            fp.checkElmentText("title receipt",By.ClassName("main-txt"), "הפקת קבלות"+hq);
            fp.checkElmentText("select frind", By.ClassName("wrap-field"), "בחר/י " + nameFrind);
            fp.checkElment("button data-display", By.XPath("//span[@class='mat-button-wrapper'][text()=' הצג נתונים ']"));
            fp.checkElment("button data-display", By.XPath("//span[@class='mat-button-wrapper'][text()=' הפק קבלות ']"));
            ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
            string family=eat.createExcel("nameFamily","nameFamily",false);
           // clickNameFrind(family,nameFrind);
            if (hq != "")
            {
                hhqr.checkSlika(typeselect);
            }
                
            fp.ClickButton(" הצג נתונים ");
        }

     

        public void clickNameFrind(string nameFamily="",string nameFrind="ידיד")
        {
            try
            {
                driver.FindElement(By.XPath("//i[text()=' edit ']")).Click();
                createConatrabition cc = new createConatrabition();
                cc.clickFrind("",nameFamily,nameFrind);
            }
            catch { throw new NotImplementedException("click name frind"); }
        }

        public void tableProductionReceipt(string []listText,int all=0)//IList<string> listText)
        {
            
             fp.checkHeder(  "מקור מזהה שם אופן תשלום פרטאי א. תשלום תאריך סה\"כ סכום סה\"כ פריטים");
            fp.checkColumn("tbody[role='rowgroup']", listText, "mat-checkbox","tr","td",0);
            if (all == 1)
                fp.click(By.TagName("mat-checkbox"));
              fp.ClickButton(" הפק קבלות ");
            try
            {
              /*  try
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    fp.click( By.ClassName("cancel-button"));
                    driver.SwitchTo().DefaultContent();
                }
                catch { }*/
                if (driver.FindElement(By.CssSelector("div[class='msg-content ng-star-inserted']")).Displayed)
                {
                    driver.FindElement(By.CssSelector("button[class='close-btn mat-icon-button mat-button-base']")).Click();
                }
              else  fp.popupMessage("ProductionReceipt "+listText[4]);
                
                //fp.waitUntil(By.XPath("//div[text()='הפעולה בוצעה בהצלחה']"));
            }
            catch
            {//mat-dialog-content msg-content ng-star-inserted
               // if (driver.FindElement(By.XPath("//div[text()='ניכשל ביצירת הקבלה עבור "+automation+" "+automation+"']")).Displayed)
                     //throw new NotImplementedException("ProductionReceipt not done"); 
            }

        }

        public  void printCancel()
        {
            try {
                fp.closeChrome();
                //driver.FindElement(By.XPath("//cr-button[@class='cancel-button']")).Click();
            } catch { throw new NotImplementedException(""); }
        }
    }
}
