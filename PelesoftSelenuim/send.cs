﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    
    class send:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void checkDataInset(string nameTypeDataInsert)
        {
           // fp.selectTextNF(nameTypeDataInsert);
            switch (nameTypeDataInsert)
            {
                case "null":
                    fp.checkDataNot("ReceiptAddress");
                    fp.checkDataNot("ReceiptMail");

                    break;
                //breack;
                case " דואר ":
                   // fp.checkDataNot("ReceiptMail");
                    inserDoar();
                    break;
                case "מייל":
                    fp.checkDataNot("ReceiptAddress");
                    //checkDataNot("ReceiptMail");
                    insertMail();
                    break;
                case " מייל + דואר ":
                    insertMail();
                    inserDoar();
                    break;
            }
        }
        private void inserDoar()
        {
            try
            {
                IWebElement iPost = driver.FindElement(By.Name("ReceiptAddress"));
                fp.checkElment("post", By.Name("ReceiptAddress"), true);
            }
            catch
            {
                throw new NotImplementedException("insert post");
            }
        }
        public void insertMail()
        {
            mailInvalid("jfhd");
            mailInvalid("ff@ff");
            driver.FindElement(By.Name("ReceiptMail")).SendKeys(MAIL);
        }

        public void mailInvalid(string valueMail)
        {
            IWebElement iMail = driver.FindElement(By.Name("ReceiptMail"));
            iMail.SendKeys(valueMail);
            driver.FindElement(By.ClassName("title-section")).Click();
            fp.wait();
            fp.funError(1, "כתובת דוא\"ל לא תקינה");
            if (driver.FindElement(By.XPath("//button[contains(@type,'submit')]")).Enabled == true)
                Debug.WriteLine("the button create enabled");
            iMail.Clear();
        }
        public void checkDataAdmission()
        {
            try
            {
                fp.checkElmentsText("title data", By.ClassName("title-section"), "נתוני קבלה", 1);
                fp.checkSelectPreform(" אופן משלוח קבלה ");
                fp.selectForRequerd("משלוח קבלה", false, 3);
                fp.checkElmentText("check ReceiptMail", By.Name("ReceiptMail"), " מייל לקבלה ");
                fp.checkSelectPreform(" שם אחר לקבלה ");
                fp.checkElmentText("check ReceiptAddress",By.Name("ReceiptAddress"), " כתובת לקבלה ");
            }
            catch { throw new NotImplementedException(); }
        }
    }
}
