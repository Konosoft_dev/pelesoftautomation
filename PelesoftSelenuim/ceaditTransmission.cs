﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class ceaditTransmission:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public ceaditTransmission()
        {
        }

        public void checkClearing(string clickTable="true",string mosad="אוטומציה",int closepop=1,string name="",string Finance="תרומה")
        {
            string[] listHeder = { "","שם", "כתובת", "מזהה", "סעיף פיננסי", "סכום חיוב", "מטבע", "מספר תשלומים", "סטטוס" };
            string[] listRow = { "",name,"","", Finance,amount, "שקל", "2", "לגבייה","-1"};
            Thread.Sleep(400);
            fp.selectForRequerd("מוסד לסליקה", false, -1);// driver.FindElement(By.XPath("//mat-select[@aria-label='clearing']")).Click();
            fp.selectText(mosad, true);
            fp.checkTableHeder(listHeder, "//thead[@role='rowgroup']");
            if (clickTable == "true")
            {
                //fp.checkColumn("table[role='grid']", listRow, "mat-checkbox", "tr", "td", 0);//"//mat-checkbox[@ng-reflect-aria-label='select row  "+family+"1']"
                if (closepop != 1)
                    fp.checkColumn("tbody[role='rowgroup']", listRow, "mat-checkbox", "tr", "td", 0);//"//mat-checkbox[@ng-reflect-aria-label='select row  "+family+"1']"
            }
            else
                fp.checkColumn("table[role='grid']", listRow, "false");

            fp.ClickButton("שדר חיובים ");
          /*  if (driver.FindElement(By.XPath("//div[text()='הפעולה בוצע בהצלחה']")).Displayed)
                Console.WriteLine("the action bordacuest charges approve");*/
           //הפעולה בוצע בהצלחה fp.spinner();
           // if (!driver.FindElement(By.XPath("//span[[@class='mat-button-wrapper'][text()='הצג קובץ שידור']]")).Enabled)
                fp.wait();
           if(closepop==1)
                fp.closePoup();
            Console.WriteLine("שידור אשראי לא יכול להתבצע עד הסוף עקב מגבלת ביצוע שיחת טלפון דרך האוטומציה");
            fp.ClickButton("הצג קובץ שידור ","span");
           //throw new NotImplementedException();
        }

        public void checkElment()
        {
            try
            {
                fp.checkElment("title", By.XPath("//div[text()=' שידורי חיוב אשראי ']"));
                fp.checkElmentText("mosad clearing", By.XPath("//mat-select[@role='listbox']"), "מוסד לסליקה");
                fp.checkElmentText("button Broadcast charges", By.XPath("//span[@class='mat-button-wrapper']"), "שדר חיובים ");
                fp.checkElmentText("button Desplay Broadcast file", By.XPath("//span[@class='mat-button-wrapper']"), "הצג קובץ שידור ");
            }
            catch
                {
                 throw new NotImplementedException("invaled checkElment");
            }
        }
        public void viewingHandalingPayments(string exportInPage,string name="אוטומציה אוטומציה")
        {
            string[] listRow = {"",name,adress,"אשראי",DateTime.Today.ToString("dd/MM/yyyy"),amount+" ש\"ח","בוצע"," עסקה תקינה. ","-1" };
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.checkElmentVHP(exportInPage);
            vhp.checkTable(listRow);
        }
    }
}