﻿using com.sun.org.apache.xml.@internal.resolver.helpers;
using com.sun.tools.javac.util;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class CreateBank:page
    {
        //static IWebDriver driver;//= new ChromeDriver();

        FunctionPelesoft fp = new FunctionPelesoft();
        SucessEndCreate sec = new SucessEndCreate();
        public void checkCreateBank()
        {
            CreateNewDenumic cnd = new CreateNewDenumic();
            fp.checkElment("page create bank account", By.ClassName("quick-object-creation"));
            cnd.checkClasscreate(amuonBank);
            fp.checkElment("section bank", By.ClassName("wrap-section"));
            fp.checkElment("title form", By.XPath("//div[contains(@class,'title-section')][contains(text(),'פרטי בנק')]"));
            fp.selectForRequerd("בנק עבור החשבון בנק");
            // selectRquired(0, By.XPath("//mat-select[@ng-reflect-name='BankID']"), "nameBank");
            fp.selectForRequerd("סניף");
           // selectRquired(1, By.XPath("//mat-select[@ng-reflect-name='BankBranchID']"), "BankBranchID");
            fp.checkElment("form details", By.ClassName("information-unit"));
            fp.checkElment("title details", By.XPath("//div[@class='title-section'][text()='פרטי חשבון']"));
            fp.checkElment("Account Name", By.Name("AccountName"), true, true);
            fp.checkElment("Account Number", By.Name("AccountNumber"));
            driver.FindElement(By.Name("AccountNumber")).Click();
            driver.FindElement(By.ClassName("title-section")).Click();
            fp.funError(3);
            fp.insertElment("AccountNumber", By.Name("AccountNumber"), "123456");
            fp.checkElment("IBAN", By.Name("IBAN"), true);
            //  fp.checkElment("select type account", By.XPath("//mat-select[@ng-reflect-name='AccountType']"));
            fp.selectForRequerd(" סוג חשבון ", false);
            //selectRquired(0, By.XPath("//mat-select[@ng-reflect-name='AccountType']"), "AccountType");

            // driver.FindElement(By.Id("mat-select-8")).Click();
            //selectText("עסקי");
            // fp.checkElment("english name", By.Name("ENAccountName"));
            //insertElment("english nsme", By.Name("ENAccountName"), "automation");
            fp.ClickButton(" צור חשבון בנק חדש ");
        }
        
        public void selectRquired(int idE, By @by, string selectValue)
        {
            //IWebDriver driver;//= IWebDriver;
            fp.wait();
            /* this.SelectValue = selectValue;
             this.iDe = idE;
             this.by1 = @by;*/
            fp.checkElment("select " + selectValue, @by);
            driver.FindElement(@by).Click();
            driver.FindElements(By.ClassName("mat-option-text"))[1].Click();
            //selectText(" ללא ");
            //driver.FindElement(@by).Click();
           // fp.funError(idE);
          //  driver.FindElements(By.ClassName("mat-option-text"))[1].Click();
        }
        public void CreateNewAccountBank()
        {
         //   driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(1);
            fp.actionWork(" הגדרות ","חשבונות בנק");
            fp.ButtonNew(amuonBank);
            checkCreateBank();
            sec.checkEndCreate("חשבון בנק");
        }
    }
}

