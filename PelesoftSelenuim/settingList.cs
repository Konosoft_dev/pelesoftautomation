﻿using com.sun.org.apache.xalan.@internal.templates;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class settingList:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public settingList()
        {
        }

        public void checkElment()
        {
            fp.checkElmentText("title", By.ClassName("main-txt"), "הגדרות רשימות ");
            fp.checkElment("add list", By.CssSelector("button[class='button-Create mat-button mat-button-base']"));
            fp.checkElment("table list", By.TagName("table"));
           // throw new NotImplementedException();
        }

        public void checkTable()
        {
            //throw new NotImplementedException();
        }

        public void clickAdd()
        {
            fp.ClickButton(" הוסף רשימה ");
            //driver.FindElement(By.XPath("//button[@mattooltip='הוסף רשימה']")).Click();
            //throw new NotImplementedException();
        }
        public void addList(string nameList,string[] newValue)
        {
            checkElmentPUPList();
            fp.ClickButton("אפס שינויים", "span");
            driver.FindElement(By.CssSelector("mat-checkbox[formcontrolname='AutoNumbering']")).Click();
            fp.checkElment("number messa", By.CssSelector("input[formcontrolname='AddValueId']"));
            driver.FindElement(By.CssSelector("mat-checkbox[formcontrolname='AutoNumbering']")).Click();
            //driver.FindElement(By.XPath("//div[@class='mat-form-field-infix\u00a0']")).Clear();
           // driver.FindElements(By.TagName("mat-label"))[2].SendKeys("jkjk");
            new Actions(driver).MoveToElement(driver.FindElements(By.TagName("mat-label"))[2]).Click().Build().Perform();
            //new Actions(driver).MoveToElement(driver.FindElements(By.TagName("mat-label"))[2]).SendKeys(Keys.clear);
            fp.selectForRequerd("שם רשימה", false, -1, nameList);
            saveEditData(newValue);
           
            string[] hederTable = { "שם רשימה", "מיספור אוטומטי", "פעולות" };
            fp.checkTableHeder(hederTable, "thead[role='rowgroup']", "tr", "th[role='columnheader']");
            string[] tablecheck= {"רשימה חדשה 1"+nameList,"", " edit " };
             fp.checkColumn("tbody[role='rowgroup']", tablecheck, "");
           
        }

        public  void saveEditData(string[] newValue)
        {
            insertValue(newValue);
            fp.ClickButton("שמירה","span");
            Thread.Sleep(700);
            driver.FindElement(By.XPath("//button[@aria-label='edit']")).Click();
            try {if(!driver.FindElement(By.XPath("//div[@class='mat-dialog-title title'][text()=' שמירת הגדרות רשימות ']")).Displayed) 
                    driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text='כן']")).Click(); } catch { try{driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text='כן']")).Click(); }
            catch { }
        }
        }

        public void insertValue(string[] newValue)
        {
            int m = 0;
            for (int i = 0; i < newValue.Length; i++)
            {
                if(m==0&& i!=0)
                {
                    i = 0;
                    m = 1;
                }
                driver.FindElement(By.CssSelector("input[formcontrolname='AddValue']")).Clear();
                fp.insertElment("add value", By.CssSelector("input[formcontrolname='AddValue']"), newValue[i]);
                Thread.Sleep(1300);
                fp.ClickButton(" הוסף ", "span");
                fp.checkElmentsText("addValueIntable", By.TagName("td"), newValue[i], i);
                

            }
            fp.checkHeder("ערך");
           
            //throw new NotImplementedException();
        }

        public void checkElmentPUPList()
        {
            fp.checkElmentText("title poup", By.TagName("H2"), " רשימה חדשה 1");
            fp.checkSelectPreform("שם רשימה", By.TagName("mat-label"));
            fp.checkSelectPreform("מיספור אוטומטי", By.TagName("mat-label"));
            fp.checkElmentText("newValue", By.XPath("//input[@formcontrolname='AddValue']"), " הזן ערך חדש");
            fp.checkElmentsText("button add", By.ClassName("mat-button-wrapper"), " הוסף ",2);
            fp.checkElmentsText("button remove change", By.ClassName("mat-button-wrapper"), "אפס שינויים ", 0);
            fp.checkElmentsText("button save", By.ClassName("mat-button-wrapper"), "שמירה ", 1);
         }

        public  void clickTable(string nameValueEdit)
        {
            string[] tablecheck = { nameValueEdit, "", " edit " };
            fp.checkColumn("tbody[role='rowgroup']", tablecheck, "//i[@class='material-icons']YEC","tr", "td",2);
        }
    }
}