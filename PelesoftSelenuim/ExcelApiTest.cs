﻿using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VisioForge.Shared.DirectShowLib;
using static java.awt.font.NumericShaper;
using Point = System.Drawing.Point;
using Rectangle = System.Drawing.Rectangle;
using xl = Microsoft.Office.Interop.Excel;


namespace PelesoftSelenuim
{
    class ExcelApiTest : page
    {
        xl.Application xlApp = null;
        xl.Workbooks workbooks = null;
        xl.Workbook Workbook = null;
        Hashtable sheets;
        public string xlFilePath;

        public ExcelApiTest(string xlFilePath = "", string nameTest = "", string pathFile = "")
        {
            if (xlFilePath != "" || nameTest != "")
            {
                if (xlFilePath == "")
                {
                    if (!Directory.Exists(@"C:\" + DateTime.Today.ToString("MM/dd")))
                    {
                        Directory.CreateDirectory(@"C:\" + DateTime.Today.ToString("MM/dd"));
                    }
                    if (!Directory.Exists(@"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv"))
                    {
                        try
                        {
                            File.AppendAllText(@"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv", "pelesoft" + Environment.NewLine);
                        }
                        catch { }
                    }

                }
                if (pathFile != "")
                    xlFilePath = pathFile.Trim();
                else
                    xlFilePath = @"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv";
                this.xlFilePath = xlFilePath;
            }
        }
        public void ExcelApiTest2(string xlFilePath, string nameTest)
        {
            if (xlFilePath == "")
            {
                if (!Directory.Exists(@"C:\" + DateTime.Today.ToString("MM/dd")))
                {
                    Directory.CreateDirectory(@"C:\" + DateTime.Today.ToString("MM/dd"));
                }
                if (!Directory.Exists(@"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv"))
                {
                    try
                    {
                        File.AppendAllText(@"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv", "pelesoft" + Environment.NewLine);
                    }
                    catch { }
                }

            }
            xlFilePath = @"C:\" + DateTime.Today.ToString("MM/dd") + "\\" + nameTest + ".csv";
            this.xlFilePath = xlFilePath;
        }

        public void OpenExcel(string xlFilePat = "")
        {
            if (xlFilePath == null)
                xlFilePath = xlFilePat;
            xlApp = new xl.Application();
            workbooks = xlApp.Workbooks;
            Workbook = workbooks.Open(xlFilePath);
            sheets = new Hashtable();
            int count = 1;
            // Storing worksheet names in Hashtable.
            foreach (xl.Worksheet sheet in Workbook.Sheets)
            {
                sheets[count] = sheet.Name;
                count++;
            }
        }

        public void CloseExcel()
        {
            try
            {
                Workbook.Close(false, xlFilePath, null); // Close the connection to workbook
                Marshal.FinalReleaseComObject(Workbook); // Release unmanaged object references.
                Workbook = null;

                workbooks.Close();
                Marshal.FinalReleaseComObject(workbooks);
                workbooks = null;

                xlApp.Quit();
                Marshal.FinalReleaseComObject(xlApp);
                xlApp = null;
            }
            catch { }
        }
        /// <summary>
        /// bugReturn
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="status">passed/warning/Failed</param>
        /// <param name="nameBug"></param>
        /// <param name="valueBug"></param>
        /// <param name="picter"></param>

        public void bugReturn(string sheetName, string status, string nameBug, string valueBug = "", bool picter = false, int del = 1)
        {
            OpenExcel();

            int sheetValue = 0;
            int colNumber = 0;
            int rowNumber = 0;
            try
            {
                if (sheets.ContainsValue(sheetName))
                {
                    foreach (DictionaryEntry sheet in sheets)
                    {
                        if (sheet.Value.Equals(sheetName))
                        {
                            sheetValue = (int)sheet.Key;
                        }
                    }

                    xl.Worksheet worksheet = null;
                    worksheet = Workbook.Worksheets[sheetValue] as xl.Worksheet;
                    xl.Range range = worksheet.UsedRange;

                    for (int i = 1; i <= range.Rows.Count + 1; i++)
                    {
                        string colNameValue = Convert.ToString((range.Cells[i + 1, 1] as xl.Range).Value2);
                        if (colNameValue == null)
                        {
                            rowNumber = i;
                            break;
                        }
                    }

                    string[] colName = { nameBug, status, valueBug, "picter" };
                    xl.Range rng;
                    for (int i = 1; i <= 4; i++)
                    {
                        //הכנסת value לשורה
                        //צביעת השורה לפי משמעות הבג
                        switch (status)
                        {
                            case "passed":
                                rng = range.Cells[rowNumber, i];
                                rng.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green); ;
                                break;
                            case "warning":
                                rng = range.Cells[rowNumber, i];
                                rng.Font.Bold = XlRgbColor.rgbYellow;
                                break;
                            case "Failed":
                                //rng = range.Cells[rowNumber, i];
                                range.Cells[rowNumber, i].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                                break;
                            case "":
                                break;
                        }
                        range.Cells[rowNumber, i + 1] = colName[i - 1];

                        if (colName[i] == "picter")
                        {
                            if (picter == true)
                            {
                                FunctionPelesoft fp = new FunctionPelesoft();
                                range.Cells[rowNumber, i + 1] = fp.picter(nameBug);
                            }
                            break;
                        }

                    }

                    /* if (del == 0)
                     {
                         range.Rows[rowNumber-1].Delete();
                     }*/
                    Workbook.Save();
                    Marshal.FinalReleaseComObject(worksheet);
                    worksheet = null;

                    CloseExcel();
                }
                else
                    CloseExcel();
            }
            catch (Exception ex)
            {
                CloseExcel();
                throw new Exception();
            }
        }
        public void insertExcelColl(string sheetName, string[] colName, string xlPath)
        {
            OpenExcel();

            int sheetValue = 0;
            int colNumber = 0;
            int rowNumber = 0;
            try
            {
                if (sheets.ContainsValue(sheetName))
                {
                    foreach (DictionaryEntry sheet in sheets)
                    {
                        if (sheet.Value.Equals(sheetName))
                        {
                            sheetValue = (int)sheet.Key;
                        }
                    }

                    xl.Worksheet worksheet = null;
                    worksheet = Workbook.Worksheets[sheetValue] as xl.Worksheet;
                    xl.Range range = worksheet.UsedRange;

                    for (int i = 1; i <= range.Rows.Count + 1; i++)
                    {
                        string colNameValue = Convert.ToString((range.Cells[i + 1, 6] as xl.Range).Value2);
                        if (colNameValue == null)
                        {
                            rowNumber = i;
                            break;
                        }
                    }

                    xl.Range rng;
                    for (int i = 0; i <= colName.Length - 1; i++)
                        range.Cells[rowNumber + 1, i + 6] = colName[i];

                    Workbook.Save();
                    try { Workbook.SaveAs(xlPath + ".XLSx", XlFileFormat.xlOpenXMLWorkbook); } catch { }
                    Marshal.FinalReleaseComObject(worksheet);
                    worksheet = null;

                    CloseExcel();
                }
                CloseExcel();

                // FileInfo f = new FileInfo(xlPath);
                //f.MoveTo(Path.ChangeExtension(xlPath, ".xlsx"));
                // var Newfile = @"C:\Users\KonosoftUser\Downloads\קובץ זיכוים למילוי.xlsx";
            }
            catch (Exception ex)
            {
                CloseExcel();
                throw new Exception();
            }
        }
        public string createExcel(string sheetName, string nameValue, bool dele = true, int j = 4, int numRow = -1, bool cell2equalscell = false)
        {


            try
            {
                string valueExcel = "";
                OpenExcel();

                int sheetValue = 0;
                int colNumber = 0;
                int rowNumber = 0;
                if (sheets.ContainsValue(sheetName))
                {
                    foreach (DictionaryEntry sheet in sheets)
                    {
                        if (sheet.Value.Equals(sheetName))
                        {
                            sheetValue = (int)sheet.Key;
                        }
                    }

                    xl.Worksheet worksheet = null;
                    worksheet = Workbook.Worksheets[sheetValue] as xl.Worksheet;
                    xl.Range range = worksheet.UsedRange;
                    if (numRow == -2)
                    {
                        rowNumber = range.Columns.Count;
                    }
                    if (numRow == -1)
                    {
                        for (int i = 1; i <= range.Columns.Count + 1; i++)
                        {
                            string colNameValue = Convert.ToString((range.Cells[i, 2] as xl.Range).Value2);
                            if (colNameValue == nameValue)
                            {
                                rowNumber = i;
                                break;
                            }
                        }
                    }
                    else
                    {
                        rowNumber = numRow;
                        numRow = range.Columns.Count;
                    }
                    valueExcel = Convert.ToString((range.Cells[rowNumber, j] as xl.Range).Value2);
                    if (cell2equalscell == true && rowNumber > 2)
                    {
                        if (Convert.ToString((range.Cells[rowNumber + 1, 2] as xl.Range).Value2) != Convert.ToString((range.Cells[rowNumber, 2] as xl.Range).Value2))
                            valueExcel = valueExcel + ",-1";
                    }
                    if (dele == true)
                    {
                        range.Rows[rowNumber].Delete();
                    }
                    Workbook.Save();
                    Marshal.FinalReleaseComObject(worksheet);
                    worksheet = null;

                    CloseExcel();

                }
                if (cell2equalscell != true && numRow != -1)
                {
                    return valueExcel + "," + numRow;
                }
                else
                    return valueExcel;
            }
            catch { return "false"; }

        }
        public void saveExcel(string sheetName, string[] valueExcel, int numberRow)
        {
            OpenExcel();

            int sheetValue = 0;
            int colNumber = 0;
            int rowNumber = 0;
            //  try
            //  {
            if (sheets.ContainsValue(sheetName))
            {
                foreach (DictionaryEntry sheet in sheets)
                {
                    if (sheet.Value.Equals(sheetName))
                    {
                        sheetValue = (int)sheet.Key;
                    }
                }

                xl.Worksheet worksheet = null;
                worksheet = Workbook.Worksheets[sheetValue] as xl.Worksheet;
                xl.Range range = worksheet.UsedRange;

                for (int i = 1; i <= range.Rows.Count + 1; i++)
                {
                    string colNameValue = Convert.ToString((range.Rows[i] as xl.Range).Value);
                    if (colNameValue == null)
                    {
                        rowNumber = i + 1;
                        break;
                    }
                }


                xl.Range rng;
                for (int i = 1; i <= valueExcel.Length - 1; i++)
                {
                    //הכנסת value לשורה
                    //צביעת השורה לפי משמעות הבג

                    range.Cells[numberRow, i + 1] = valueExcel[i];

                }


                Workbook.Save();
                Marshal.FinalReleaseComObject(worksheet);
                worksheet = null;

                CloseExcel();
                // }

            }
            // catch (Exception ex) { throw; }
        }
        public string savePicter(string namePageandnituv)
        {
            //login l = new login();
            FunctionPelesoft fp = new FunctionPelesoft();
            fp.waitUntil(By.ClassName("mat-button-wrapper"));
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            ss.SaveAsFile("C:\\" + namePageandnituv + ".png", ScreenshotImageFormat.Png);
            return @"C:\" + namePageandnituv + ".jpg";

        }

        public void checkDownloadFromAbove(string[] allText, int countRow, string sheetName)
        {
            try
            {
                OpenExcel();
                int sheetValue = 0;
                try
                {
                    if (sheets.ContainsValue(sheetName.Trim()))
                    {
                        foreach (DictionaryEntry sheet in sheets)
                        {
                            if (sheet.Value.Equals(sheetName))
                            {
                                sheetValue = (int)sheet.Key;
                            }
                        }

                        xl.Worksheet worksheet = null;
                        worksheet = Workbook.Worksheets[sheetValue] as xl.Worksheet;
                        xl.Range range = worksheet.UsedRange;
                        int j = 1;
                        for (int i = 1; i <= range.Columns.Count; i++)
                        {
                            string colNameValue = Convert.ToString((range.Cells[1,j] as xl.Range).Value2);
                            while (allText[i] == "")
                                i++;
                            if (colNameValue!=allText[i])
                                throw new NotImplementedException();
                            j++;
                        }
                        for (int i = 1; i <= range.Rows.Count; i++)
                        {
                            string colNameValue = Convert.ToString((range.Cells[i,1] as xl.Range).Value2);
                            if (colNameValue ==""&& i<countRow )
                            { throw new NotImplementedException(); }
                                }
                    }
                    CloseExcel();

                }
                catch {
                    CloseExcel();
                    throw new NotImplementedException("the excel not equals to table"+sheetName); }
            }
            catch { throw new NotImplementedException(); }
        }
    }
}
