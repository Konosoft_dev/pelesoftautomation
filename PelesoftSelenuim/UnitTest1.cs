﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using com.sun.org.apache.bcel.@internal.generic;
using com.sun.xml.@internal.bind.v2.runtime;
using java.lang;
using java.time;
using Microsoft.TeamFoundation.WorkItemTracking.Client.Wiql;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Assert = NUnit.Framework.Assert;

namespace PelesoftSelenuim
{
    //ckckckckckckckc
    [TestClass]
    public class UnitTest1
    {
        //static IWebDriver driver;//= new ChromeDriver();

        page p = new page();
        FunctionPelesoft fp = new FunctionPelesoft();
        //public IWebDriver driver = null;
        SucessEndCreate sec = new SucessEndCreate();
        CreateNewDenumic cnd = new CreateNewDenumic();
        Financical fl = new Financical();
        Frind f = new Frind();
        AddTable at = new AddTable();
        receStrct rs = new receStrct();
        CreateMesaur cm = new CreateMesaur();

        //public TimeSpan timeSpan;
        //[SetUp]
        ClearingEntity ce = new ClearingEntity();
        // ExcelApiTest eat = new ExcelApiTest();
        //@Test(Priority=1)
         // [TestMethod]
        public void Test01Login(string nameTasret = "")
        {
            if (nameTasret != "")
                fp.OpenExcel(nameTasret, "failed");
            login ln = new login();
            ln.openChrome();
           ln.UserNamee("ציבי");
             // ln.UserNamee("צביה");
            //  ln.UserNamee("aaa");
           // ln.Password("צביה1234!");
             ln.Password("ציבי1234!");//לשני לשנות לציבי
            //  ln.Password("aaa");
            ln.cLickLogIn();
            Institution i = new Institution();
           // i.changeChaseBox(" ");

        }
        [TestMethod, Category("2st")]
        public void Test02InsertList()
        {
            Test01Login("Test02InsertList");
            fp.actionWork(" הגדרות ", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.sleep(200);
            for (int i = 0; i < p.nmeList.Length; i++)
            {
                nl.clickTable(p.nmeList[i]);
                Thread.sleep(200);
                nl.saveEditData(p.newList[i]);
                Thread.sleep(1900);
            }
            fp.closeChrome();
            fp.OpenExcel("Test02InsertList", "passed");
        }
        //יצירת ידיד//רץ תקין יש שינויים
        [TestMethod, Order(2)]
        public void Test03CreateNewFrinedClose()
        {
            Test01Login("Test03CreateNewFrinedClose");
            f.createNewFrined();
            sec.closePage();
            fp.OpenExcel("Test03CreateNewFrinedClose", "passed");

        }

        [TestMethod, Order(21)]
        public void Test22CreateNewFrinedGTCardFrind()
        {
            //לא רץ תקין
            Test01Login("Test03CreateNewFrinedGTCardFrind");
            f.createNewFrined();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test03CreateNewFrinedGTCardFrind", "passed");

        }
        [TestMethod, Order(22)]
        public void Test03CreateNewFrinedGTCNFrind()
        {
            Test01Login("Test03CreateNewFrinedGTCNFrind");
            f.createNewFrined();
            f.clickCNfrind();
            fp.OpenExcel("Test03CreateNewFrinedGTCNFrind", "passed");

        }
        //ישויות סליקה
        [TestMethod, Order(3)]
        public void Test02CreateNewClearingEntityclose()
        {
            Test01Login("Test02CreateNewClearingEntityclose");
            ce.CreateNewClearingEntity();
            sec.closePage();
            fp.OpenExcel("Test02CreateNewClearingEntityclose", "Passed");
        }
        [TestMethod, Order(22)]//ישויות סליקה
        public void Test04CreateNewClearingEntityCard()
        {
            Test01Login("Test04CreateNewClearingEntityCard");
            ce.CreateNewClearingEntity(" אשראי הו\"ק ");
            sec.pageCard("ישות סליקה");
            fp.OpenExcel("Test04CreateNewClearingEntityCard", "Passed");
        }

        [TestMethod, Order(22)]
        public void Test04CreateNewClearingEntityCreateNew()
        {
            Test01Login("Test04CreateNewClearingEntityCreateNew");
            ce.CreateNewClearingEntity("מסב");
            sec.pageNewCreate("ישות סליקה נוספת");
            fp.OpenExcel("Test04CreateNewClearingEntityCreateNew", "Passed");
        }

        [TestMethod, Order(22)]//ישויות סליקה
        public void Test04CreateNewClearingEntityCardMosed()
        {
            Test01Login("Test04CreateNewClearingEntityCardMosed");
            ce.CreateNewClearingEntity("מסב נדרים");
            sec.pageCard("מוסד");
            fp.OpenExcel("Test04CreateNewClearingEntityCardMosed", "Passed");
        }
        //חשבון בנק//נפל בסגירה
        CreateBank cb = new CreateBank();

        [TestMethod, Order(4)]
        public void Test05CreateNewAccountBankClose()
        {
            Test01Login("Test05CreateNewAccountBankClose");
            cb.CreateNewAccountBank();
            sec.closePage();
            fp.OpenExcel("Test05CreateNewAccountBankClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCMosed()
        {
            Test01Login("Test05CreateNewAccountBankGTCMosed");
            cb.CreateNewAccountBank();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test05CreateNewAccountBankGTCMosed", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCard()
        {
            Test01Login("Test05CreateNewAccountBankGTCard");
            cb.CreateNewAccountBank();
            sec.pageCard("חשבון בנק");
            fp.OpenExcel("Test05CreateNewAccountBankGTCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCNew()
        {
            Test01Login("Test05CreateNewAccountBankGTCNew");
            cb.CreateNewAccountBank();
            sec.pageNewCreate("חשבון בנק נוסף");
            fp.OpenExcel("Test05CreateNewAccountBankGTCNew", "Passed");
        }

        //סעיפים פיננסים
        [TestMethod, Order(5)]//נוסף שדה חובה //לטפל בה בריצה הבאה
        public void Test06CreateNewFinancicalSectionClose()
        {
            Test01Login("Test06CreateNewFinancicalSectionClose");
            fl.createNewFinancicalSection();
            sec.closePage();
            fp.OpenExcel("Test06CreateNewFinancicalSectionClose", "Passed");
        }
        [TestMethod, Order(22)]//נוסף שדה חובה
        public void Test06CreateNewFinancicalSectionCardFinanci()
        {
            Test01Login("Test06CreateNewFinancicalSectionCardFinanci");
            fl.createNewFinancicalSection(" הוראות קבע ראשי ", "", "הוראות קבע ראשי","חיוב","מסב");
            sec.pageCard(p.FinancicalSection);
            fp.OpenExcel("Test06CreateNewFinancicalSectionCardFinanci", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionCreatNew()
        { Test01Login("Test06CreateNewFinancicalSectionCreatNew");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "כולל", "זיכוי","");
            sec.pageNewCreate(p.FinancicalSection + " נוסף");
            fp.OpenExcel("Test06CreateNewFinancicalSectionCreatNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionMosad()
        {
            Test01Login("Test06CreateNewFinancicalSectionMosad");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "בונוס", "בונוס","");
            sec.pageCard("מוסד");
            fp.OpenExcel("Test06CreateNewFinancicalSectionMosad", "Passed");
        }

        /// <summary>
        /// תרומות -תרומה במזומן
        /// </summary>//run 
        [TestMethod, Order(6)]
        public void Test07CreateNewConatrabitionCase()//נופל על יצירת קבלה
        {
            string idConatarbition = "78";
            createConatrabition cc = new createConatrabition();
            Test01Login("Test07CreateNewConatrabitionCase");
            cc.conatrabitionList();
            cc.createNew("מזומן", "Cash");
            cc.checknumConatarbition();
            createDepositcase();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            idConatarbition = eat.createExcel("conatarbition", "mesaa");
            CreateProductionReceipt("מזומן", idConatarbition);//נפל פה
            fp.OpenExcel("Test07CreateNewConatrabitionCase", "Passed");
        }
        //  [TestMethod]
        public void createDepositcase()
        {
            Deposit d = new Deposit();
            Test01Login();
            Thread.sleep(100);
            d.dipositType("מזומן ");
            d.diposetCash("מזומן");
            fp.closeChrome();
        }
        //טיפול בקבלות
        public void CreateProductionReceipt(string card, string idConatarbition = "", int all = 0)
        {
            if (idConatarbition == "")
                idConatarbition = p.mesaa;
            //idConatarbition = "4";
            Test01Login();
            Production_receipt pr = new Production_receipt();
            Thread.sleep(900);
            pr.ProductionReceipt();
            pr.checkPageProductionReceipt();
            string[] listColumn = { "", "תרומות", "", "", card, "",  DateTime.Today.ToString("dd/MM/yyyy") ,"10", "1", "-1" };
            pr.tableProductionReceipt(listColumn, all);
            fp.spinner();
            pr.printCancel();
        }
        //תרומה בצק
        [TestMethod]
        public void Test07CreateNewConatrabitionBag()//טיפול בצקים 
        {
            Test01Login("Test07CreateNewConatrabitionBag");
            string idConatarbition = 64.ToString();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("צק", "Check");
            Thread.sleep(200);
            cc.checknumConatarbition();
            hcreateDepositCheeck();
            ihandlingCheck();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            idConatarbition = eat.createExcel("conatarbition", "mesaa");
            CreateProductionReceipt("צ\'ק", idConatarbition);//נופל כאן
            fp.OpenExcel("Test07CreateNewConatrabitionBag", "Passed");
        }
        //הפקדת צקים
        public void hcreateDepositCheeck()
        {
            Deposit d = new Deposit();
            fp.refres();
            d.dipositType("צ");
            // d.diposetCash("צ\'קים", p.numberCheck + " " + p.nameBank + " " + p.numBranch + " " + p.numberAmountBank + "30/09/2020" /*DateTime.Today.ToString("dd/MM/yyyy") */+ " 10 שקל לגבייה תרומה");
            //"12345678 04 007 123456 30/09/2020 00:00:00 10 שקל לגבייה תרומה"
            //p.numberCheck+" "+p.nameBank+" "+p.numBranch+" "+ p.numberAmountBank +DateTime.Today.ToString("dd/MM/yyyy")+ " 10 שקל לגבייה תרומה",
            string[] row = { "", "", p.numberCheck, p.nameBankNO0, p.numBranch, p.numberAmountBank, DateTime.Today.ToString("dd/MM/yyyy"), "10", "שקל", "תרומה", "לגבייה", "-1" };
            d.tableclickCheck(row);
            Thread.sleep(250);
            fp.ClickButton("הפקדה ");
            Thread.sleep(400);
            //fp.checkElment("open pdf", By.TagName("embed"));
            fp.refres();
        }
        //טיפול בצקים
        public void ihandlingCheck()
        {
            HandingCheeck hc = new HandingCheeck();
            fp.actionWork("ניהול כספים", "טיפול בצ");
            //click button filter
            hc.checkPagehandingCheeck();
            fp.closeChrome();
        }
        //תרומה באשראי
        [TestMethod, Order(6)]
        public void Test07CreateDipositCreadetCard()//מאוד איטי 
        {
            Test01Login("Test07CreateDipositCreadetCard");
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("כרטיס אשראי", "Credit");
            sec.checkEndCreate("תרומה", false, "ה", "ידיד");
            sec.closePage();
            fp.OpenExcel("Test07CreateDipositCreadetCard", "Passed");
        }
        //שידור אשראי דרך הכרטיס
        [TestMethod, Order(7)]//מוסד סליקה
        public void Test08CreaditCardConaatarbition()
        {
            ///מתבצע בדיקה שהכל תקין אך לא מתבצע באמת שידור עקב כך שבאוטומציה אי אפשר לדמות שיחת טלפון
            Test07CreateDipositCreadetCard();
            createConatrabition cc = new createConatrabition();
            Test01Login("Test08CreaditCardConaatarbition");
            cc.conatrabitionList();
            string[] listRow = { "", "", "", "", "", "", DateTime.Today.ToString("dd/MM/yy"), "שקל", "1", "", "", "", "אשראי" };
            cc.checkTabl(listRow);
            popupList ppl = new popupList();
            ppl.checkPage();
            ppl.checkElment();
            ppl.chekName(p.family);
            ppl.popupEdit();
            ppl.clickPaymentTransmission();
            fp.closeChrome();
            fp.OpenExcel("Test08CreaditCardConaatarbition", "Passed");
        }
        //שידור אשראי דרך ניהול כספים
        [TestMethod, Order(7)]
        public void Test08CreaditTransmissionCT()
        {
            Test07CreateDipositCreadetCard();
            ceaditTransmission ct = new ceaditTransmission();
            Test01Login("Test08CreaditTransmissionCT");
            fp.actionWork(" ניהול כספים ", "שידורי אשראי");
            ct.checkElment();
            ct.checkClearing("true", "אשראי", 0);
            ct.viewingHandalingPayments("true","");
            fp.closeChrome();
            fp.OpenExcel("Test08CreaditTransmissionCT", "Passed");
        }

        //טיפול בתשלומי אשראי
        [TestMethod, Order(8)]//מוסד סליקה
        public void Test09ReturnBroadcast()
        {
            Test01Login("Test09ReturnBroadcast");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.returnBroadcast("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test09ReturnBroadcast", "Passed");
        }
        [TestMethod, Order(9)]//מוסד סליקה
        public void Test10BroadcastSdreenViewingPayments()
        {
            Test01Login("Test10BroadcastSdreenViewingPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.BroadcastSdreenViewingPayments("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test10BroadcastSdreenViewingPayments", "passed");
        }
        /// <summary>
        /// לא הורץ
        /// </summary>טיפול בקבלת אשראי
        [TestMethod, Order(10)]
        public void Test11HandingViewPayments()
        {
            Test08CreaditCardConaatarbition();
            Test01Login("Test11HandingViewPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            //vhp.handingViewPayments("אשראי");//לבדוק עם ריקי
            vhp.checkchange();
            fp.OpenExcel("Test11HandingViewPayments", "Passed");
        }
        [TestMethod, Order(11)]
        public void Test12productionReceipt()//הפקת קבלות
        {

            CreateProductionReceipt("אשראי", "10");
            Production_receipt pr = new Production_receipt();
            Test01Login("Test12productionReceipt");
            pr.ProductionReceipt();//לא נבדק שלא מופיע
            fp.OpenExcel("Test12productionReceipt", "passed");
        }
        //העברה בנקאית
        [TestMethod, Order(6)]//הוספת פרטי בנק לא הוצג מיד
        public void Test07CreateDipositBankTransfer()
        {
            Test01Login("Test07CreateDipositBankTransfer");
            string idConatarbition = "8";
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("העברה בנקאית", "BankDeposit");
            cc.checknumConatarbition();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            idConatarbition = eat.createExcel("conatarbition", "mesaa");
            fp.closeChrome();
            CreateProductionReceipt("העברה בנקאית", idConatarbition);
            fp.OpenExcel("Test07CreateDipositBankTransfer", "Passed");
        }
        /// <summary>
        /// הו"ק אשראי
        /// </summary>

        [TestMethod, Order(6)]//איטיות
        public void Test07CreateDirectDebitClose()
        {
            Test01Login("Test07CreateDirectDebitClose");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.closePage();
            fp.OpenExcel("Test07CreateDirectDebitClose", "Passed");
        }
        //הוראת קבע
        [TestMethod, Order(22)]//איטיות
        public void Test07CreateDirectDebitFrind()
        {
            Test01Login("Test07CreateDirectDebitFrind");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test07CreateDirectDebitFrind", "Passed");
        }
        [TestMethod, Order(22)]//איטיות
        public void Test07CreateDirectDebitCardDD()
        {
            Test01Login("Test07CreateDirectDebitCardDD");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageCard("הו\"ק");
            fp.OpenExcel("Test07CreateDirectDebitCardDD", "Passed");
        }
        [TestMethod, Order(22)]//איטיות
        public void Test07CreateDirectDebitAddCard()
        {
            Test01Login("Test07CreateDirectDebitAddCard");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageNewCreate("הו\"ק נוספת");
            fp.OpenExcel("Test07CreateDirectDebitAddCard", "passed");
        }
        //ביצוע הו"ק
        [TestMethod, Order(12)]
        public void Test13ExecutionHQ()
        {
            Test01Login("Test13ExecutionHQ");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            string faml = "";
             /*ehq.insertExecutionHQ();
             ehq.checkelmentExecutionHQ("אשראי הו\"ק", m);
             ehq.clickGvia();
             fp.spinner();
            Thread.sleep(250);*/
            faml = vhp.stepHQ();
            fp.refres();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.asraiHok, m);
            if (faml == "false")
                faml = "";
            string[] listRow = { "", faml + " ", p.adress, "", p.hoqRase, p.asraiHok, p.amount, "שקל", "1", "לגבייה", "-1" };
            ehq.checkTable(listRow);
            fp.closeChrome();
            fp.OpenExcel("Test13ExecutionHQ", "Passed");
        }
        //טיפול בקבלות עבור הו"ק
        [TestMethod, Order(13)]
        public void Test14HandlingReceiptsHQ()
        {
            Test01Login("Test14HandlingReceiptsHQ");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
            string faml = eat.createExcel("nameFamily", "nameFamily", false);
            if (faml == "false")
                faml = "";
            string[] listRow = { "", "טבלת הוראות קבע", "", faml, "מסב", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow, 0);
            fp.ClickButton(" הפק קבלות ");
            fp.closeChrome();
            fp.OpenExcel("Test14HandlingReceiptsHQ", "Passed");
        }
        //יצירת מס"ב
        /// fl.createNewFinancicalSection(" הוראות קבע ראשי ");

        [TestMethod, Order(6)]
        public void Test07HQMSABClose()
        {
            Test01Login("Test07HQMSABClose");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.closePage();
            fp.OpenExcel("Test07HQMSABClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABAddCard()
        {
            Test01Login("Test07HQMSABAddCard");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת סכום ");
            sec.pageNewCreate("הו\"ק נוספת");
            fp.OpenExcel("Test07HQMSABAddCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABCardHQ()
        {
            Test01Login("Test07HQMSABCardHQ");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת חיובים ");
            sec.pageCard("הו\"ק");
            fp.OpenExcel("Test07HQMSABCardHQ", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22HQMSABCardFrind()
        {
            Test01Login("Test22HQMSABCardFrind");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.pageCard(p.parent);
            fp.OpenExcel("Test22HQMSABCardFrind", "Passed");
        }

        [TestMethod, Order(14)]
        public void Test15ExecutionHQMsab()
        {
            Test01Login("Test15ExecutionHQMsab");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            fp.spinner();
            ehq.clickGvia();
            fp.spinner();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false", "מסב");
            string[] listHQ = {"", "", p.adress, "מסב", "", "", "בוצע", "עסקה תקינה. ", "-1" };
            vhp.checkTable(listHQ);
            vhp.returnBroadcast("מסב", "מסב");
            vhp.handingViewPayments("מסב", "מסב");
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
            string faml = eat.createExcel("nameFamily", "nameFamily", false);
            if (faml == "false")
                faml = "";
            string[] listRow = { faml, p.adress, "", p.hoqRase, "מסב", p.amount, "שקל", "1", "לגבייה", "-1" };
            ehq.checkTable(listRow);
            fp.closeChrome();
            fp.OpenExcel("Test15ExecutionHQMsab", "Passed");
        }
        //הפקת קבלות למס"ב
        [TestMethod, Order(15)]//רץ תקין
        public void Test16HandlingReceiptsHQMsab()
        {
            Test01Login("Test16HandlingReceiptsHQMsab");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            string[] listRow = { "", "טבלת הוראות קבע", "", "", "מסב", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow);
            fp.ClickButton(" הפק קבלות ");
            fp.picter("הפק קבלות");
            fp.closeChrome();
            fp.OpenExcel("Test16HandlingReceiptsHQMsab", "Passed");
        }

        [TestMethod, Order(16)]//באג/בא
        public void Test17ReturenMsab()
        {
            Test01Login("Test17ReturenMsab");
            returnMSAB rm = new returnMSAB();
            rm.openMsabPage();
            rm.checkPageReturnMsab();
            rm.insertData();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.filterMsab();
            fp.closeChrome();
            fp.OpenExcel("Test17ReturenMsab", "Passed");
        }
        ///יצירת הורה
        [TestMethod]
        public void Test06createParents()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents(p.names[fp.rand(p.names.Length)]);
            sec.checkEndCreate("הורה", false);
            sec.closePage();
        }
        [TestMethod]
        public void Test06createParentsCard()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("חנוך");
            sec.checkEndCreate("הורה", false);
            f.clickCardFrind();
            fp.closeChrome();
        }[TestMethod]
        public void Test06createParentsNew()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("רוזנברגר");
            sec.checkEndCreate("הורה", false);
            sec.pageNewCreate("הורה");
        }
        /// <summary>
        /// יצירת תלמיד
        /// </summary>
        /// 

        ///מוסד חנוך
        [TestMethod, Order(5)]
        public void Test06CreateEducationalInstitutionClose()
        {
            Test01Login("Test6CreateEducationalInstitutionClose");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI("בית אברהם","תלמוד תורה");
            sec.checkEndCreate(p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test6CreateEducationalInstitutionClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateEducationalInstitutionCard()
        {
            Test01Login("Test06CreateEducationalInstitutionCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI(" כולל ", "כולל", "באר שבע", 3);
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test06CreateEducationalInstitutionCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateEducationalInstitutionAddNew()
        {
            Test01Login("Test06CreateEducationalInstitutionAddNew");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI("גן עירוני");
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageNewCreate(p.MosadChinuc + " נוסף");
            fp.OpenExcel("Test06CreateEducationalInstitutionAddNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateEducationalInstitutionCardMosad()
        {
            Test01Login("Test22CreateEducationalInstitutionCardMosad");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI("בית ספר","בית ספר לבנות");
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22CreateEducationalInstitutionCardMosad", "Passed");
        }

        /// <summary>
        /// רמת כיתה
        /// </summary>
        [TestMethod, Order(6)]
        public void Test07GradeLavelClose()
        {
            Test01Login("Test07GradeLavelClose");
            createGradeLavel cgl = new createGradeLavel();
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            cgl.stepLavel("בית אברהם");
            cgl.GradeLavelCreate("כיתה א");
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.closePage(1);
            fp.OpenExcel("Test07GradeLavelClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07GradeLavelCard()
        {
            Test01Login("Test07GradeLavelCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel("בית אברהם");
            cgl.GradeLavelCreate("כיתה ב");
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageCard(p.gradeLavel);
            fp.OpenExcel("Test07GradeLavelCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22GradeLavelnewCard()
        {
            Test01Login("Test07GradeLavelnewCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel("גן עירוני");
            cgl.GradeLavelCreate("גן ראשון");
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageNewCreate(p.gradeLavel + " נוספת", 1);
            fp.OpenExcel("Test07GradeLavelnewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22GradeLavelMosadChinuch()
        {
            Test01Login("Test22GradeLavelMosadChinuch");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel("גן עירוני");
            cgl.GradeLavelCreate("גן שני");
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22GradeLavelMosadChinuch", "Passed");
        }
        /// <summary>
        /// יצירת כיתה
        /// </summary>
        [TestMethod, Order(7)]
        public void Test08CreateGradeClose()
        {
            Test01Login("Test08CreateGradeClose");
            Grade g = new Grade();
            g.stepCreateGard("בית אברהם","תלמוד תורה","כיתה א");
            sec.closePage(2);
            fp.OpenExcel("Test08CreateGradeClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test08CreateGradecardGrade()
        {
            Test01Login("Test08CreateGradecardGrade");
            Grade g = new Grade();
            g.stepCreateGard("גן עירוני","גן ילדים","גן ראשון");
            sec.pageCard("כיתה");
            fp.OpenExcel("Test08CreateGradecardGrade", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateGradeAddNew()
        {
            Test01Login("Test22CreateGradeAddNew");
            Grade g = new Grade();
            g.stepCreateGard("בית אברהם","תלמוד תורה","כיתה ב",1);
            sec.pageNewCreate("כיתה נוספת", 2);
            fp.OpenExcel("Test22CreateGradeAddNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateGradeCardMosadChinuc()
        {
            Test01Login("Test22CreateGradeCardMosadChinuc");
            Grade g = new Grade();
            g.stepCreateGard();
            sec.pageCard("סניף " + p.MosadChinuc + " ");
            fp.OpenExcel("Test22CreateGradeCardMosadChinuc", "Passed");
        }
        /// <summary>
        /// יצירת תלמיד
        /// </summary>
        [TestMethod, Order(8)]
        public void Test09CreatePupilClose()
        {
            Test01Login("Test09CreatePupilClose");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL("רייכמן", "בית אברהם",-1,"כיתה א");
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test09CreatePupilClose", "Passed");
        }
        [TestMethod, Order(22)]//בגרסה הבאה להחזיר את כיתה וסניף pupil->114/124,127//נפל על הורים
        public void Test09CreatePupilCard()
        {
            Test01Login("Test09CreatePupilCard");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL();
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            fp.click(By.CssSelector("button[aria-label='btnGoToObjectCreated']"));
            pn.insertDate("ישיבה", 1);
            string sum = "1400";
            string[] ofen = p.ofenM;
            pn.prentalDataiInset(ofen[fp.rand(ofen.Length)], "", false, 1);
            pn.addDataWork(3, "2000");
            fp.closePoup(1);
            fp.closeChrome();
            fp.OpenExcel("Test09CreatePupilCard", "Passed");
        }

        [TestMethod, Order(22)]
        public void Test22CreatePupilNewCard()
        {
            Test01Login("Test22CreatePupilNewCard");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL(p.familys[fp.rand(p.familys.Length)], "בית אברהם");
            sec.checkEndCreate("תלמיד", false, "", "הורה");
            pn.pageNewCreate("תלמיד נוסף");
            fp.OpenExcel("Test22CreatePupilNewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePupilCardFrind()
        {
            Test01Login("Test22CreatePupilCardFrind");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil("229931464", "בית אברהם");
            pn.stepCreatePUPIL(p.familys[fp.rand(p.familys.Length)], "בית אברהם");
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            sec.pageCard(p.parent);
            fp.OpenExcel("Test22CreatePupilCardFrind", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkCard()
        {
            Test01Login("Test22CreateAddWorkCard");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.sleep(300);
            pn.clickTableRandom();
            pn.stepWork(2, "8000", "אם");
            sec.pageCard(" קח אותי לכרטיס נתון הכנסה להורה ");
            fp.OpenExcel("Test22CreateAddWorkCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkNew()
        {
            Test01Login("Test22CreateAddWorkNew");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.sleep(290);
            pn.clickTableRandom();
            pn.stepWork(1, "5000", "אפוטרופוס");
            sec.pageNewCreate("נתון הכנסה להורה נוסף", 1);
            fp.OpenExcel("Test22CreateAddWorkNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkSnatonPrenatal()
        {
            Test01Login("Test18CreateAddWorkSnatonPrenatal");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.sleep(450);
            pn.clickTableRandom();
            pn.stepWork(3, "1400", "אב");
            sec.pageCard("שנתון להורים");
            fp.OpenExcel("Test18CreateAddWorkSnatonPrenatal", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22StudenTestExtension()//P_lastName
        {
            Test01Login("Test22StudenTestExtension");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.sleep(400);
            pn.clickTableRandom();
            pn.checkFrind();
            Thread.sleep(100);
            pn.learn();
            fp.closeChrome();
            fp.OpenExcel("Test22StudenTestExtension", "Passed");
        }
        /// <summary>
        /// מחולל חיובי הורים
        /// </summary>
        /// 
        [TestMethod, Order(9)]
        public void Test10FinanceParentalCharges()
        {
            Test01Login("Test10FinanceParentalCharges");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים");
            sec.closePage();
            fp.OpenExcel("Test10FinanceParentalCharges", "Paassed");
        }

        /// <summary>
        /// יצירת הגדרת סעיף חיוב חד פעמי
        /// </summary>
        /// 
        [TestMethod, Order(10)]
        public void Test11CreateCurrentBillingOneTimeClose()
        {
            Test01Login("Test11CreateCurrentBillingOneTimeClose");

            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test11CreateCurrentBillingOneTimeClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCard()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardNew()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardMC()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardMC", "Passed");
        }
        /// <summary>
        /// יצירת הגדרת סעיף חיוב שוטף
        /// </summary>
        /// 
        [TestMethod, Order(11)]
        public void Test12CreateCurrentBillingClauseClose()
        {
            Test01Login("Test12CreateCurrentBillingClauseClose");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test12CreateCurrentBillingClauseClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCard()
        {
            Test01Login("Test12CreateCurrentBillingClauseCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test12CreateCurrentBillingClauseCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardNew()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף", "בית אברהם");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardMC()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף", "3");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardMC", "Passed");
        }
        [TestMethod]
        public void Test13ParentalCharges()
        {
            Test01Login("Test13ParentalCharges");
            fp.actionWork(" ניהול כספים ", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges("", "בית אברהם");
            fp.closeChrome();
            fp.OpenExcel("Test13ParentalCharges", "Passed");
        }
        [TestMethod]
        public void Test14CreatePayment()//closeCardAsraiOne
        {
            Test01Login("Test14CreatePayment");
            fl.createNewFinancicalSection(" תשלומים ");
            fp.closepopup();

            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", p.idIdid, p.automation, "", "Credit", "שנור");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test14CreatePayment", "passed");
        }[TestMethod]
        public void Test22CreatePaymentCardCaseOne()
        {
            Test01Login("Test22CreatePaymentCardCaseOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "מזומן", p.idIdid, "אוט", "", "Cash", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard("תשלום", "btnGoToObjectCreated");
            fp.OpenExcel("Test22CreatePaymentCardCaseOne", "passed");
        }[TestMethod]
        public void Test22CreatePaymentNewCheckOne()
        {
            Test01Login("Test22CreatePaymentNewCheckOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "צק", p.idIdid, p.automation, "", "Check", "אוטומציה");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageNewCreate("תשלום נוסף");
            fp.OpenExcel("Test22CreatePaymentNewCheckOne", "passed");
        }[TestMethod]
        public void Test22CreatePaymentFrindBankTransferOne()
        {
            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", p.idIdid, p.automation, "", "BankDeposit", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard(p.nameFrind, "btnGoToParent");
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }[TestMethod]//באג
        public void Test22CreatePaymentCreadetCardHok()
        {

            Test01Login("Test22CreatePaymentCreadetCardHok");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "כללי", "", "divPaymentMethodTypesCredit", "שושן", "false","");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentCreadetCardHok");
        }[TestMethod]//באג
        public void Test22CreatePaymentMsabHok()
        {

            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום בהו\"ק", "מס\"ב", p.idIdid, p.automation, "", "divPaymentMethodTypesMasav", "רוזנברגר", "false");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }
        [TestMethod]//באג בתשלומים
        public void Test14CreatePaymentParentsCase()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.sleep(400);
            pn.clickTable("לוי");
            string ofen = "מזומן";
            pn.prentalDataPayment(ofen, "", true, 1);
          
            fp.ClickButton("שמירה");
            Thread.sleep(600);
            fp.popupMessage(" שמירת התלמיד ");
            fp.closePoup();
            pn.clickTable("לוי", 1);
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.offset("חיובים", 7, false);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }[TestMethod]//באג בתשלומים
        public void Test14CreatePaymentParentsCaseT()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTable("לוי");
            string ofen = "מזומן";
            pn.prentalDataPayment(ofen, "", true, 1);
            fp.ClickButton("שמירה");
            Thread.sleep(1550);
            fp.closePoup();
            pn.clickTable("לוי", 1);
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.offset("תשלומים", 3, false);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }
        // [TestMethod]//לבדוק שרץ תשלום במזומן כללי
        public void Test14CheckOfsset()
        {
            Test01Login("Test14CheckOfsset");

        }
        [TestMethod]
        public void Test14CreatePaymentParents()//לא נימצאו חיובים
        {
            Test01Login("Test14CreatePaymentParents");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.stepPaymentParent();
            fp.OpenExcel("Test14CreatePaymentParents");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
      /*  [TestMethod]
        public void TableCreate()
        {
            creatAddTable();
            CreateFildes();
            SettingFildesTable();
            createSectionsTable();
            createNewrowInNewTable();}*/
        ///הוספת טבלה
        [TestMethod, Order(6)]
        public void Test07CreatAddTable()
        {
            Test01Login("Test07CreatAddTable");
            fp.actionWork("הגדרות", "הוספת טבלה ");
            at.insertdataExcel();
            at.checkElment();
            at.insertElment();
            fp.closeChrome();
            fp.OpenExcel("Test07CreatAddTable", "Passed");
        }
        /// <summary>
        /// הגדרות טבלה הוספת שדות
        /// </summary>
        /// 
        [TestMethod, Order(7)]//לאוסיף X על popup//להריץ שוב
        public void Test08CreateFildes()//רץ תקין
        {
            Test01Login("Test08CreateFildes");
            at.openTable();
            at.OpenSettings();
            at.checkESettingTable();
            at.OpenSettingsTable();
            at.clickCreateFilds();
            at.addFildesTable();
            at.save();
            at.close();
            fp.closeChrome();
            fp.OpenExcel("Test08CreateFildes", "Passed");
        }
        [TestMethod, Order(8)]
        public void Test09SettingFildesTable()
        {
            Test01Login("Test09SettingFildesTable");
            at.openTable();
            at.OpenSettings();
            at.OpenSettingsTable();
            at.addFildes();
            at.checkAddFildesActive();
            at.save();
            fp.closeChrome();
            fp.OpenExcel("Test09SettingFildesTable", "Passed");
        }
        [TestMethod, Order(9)]
        public void Test10CreateSectionsTable()//רץ טוב
        {
            Test01Login("Test10CreateSectionsTable");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.clickaddSections();
            fp.closeChrome();
            fp.OpenExcel("Test10CreateSectionsTable", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test11CreateSectionsTableApprove()
        {
            Test01Login("Test11CreateSectionsTableApprove");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.approveDisplay();
            Thread.sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test11CreateSectionsTableApprove", "Passed");
        }
        [TestMethod, Order(11)]
        public void Test12CreateNewrowInNewTable()
        {
            Test01Login("Test12CreateNewrowInNewTable");
            at.openTable();
            fp.ButtonNew("ccc");
            at.checkPagecreateNewInTable(p.automation);
            sec.checkEndCreate("ccc", false);
            sec.closePage();
            fp.OpenExcel("Test12CreateNewrowInNewTable", "Passed");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
        [TestMethod, Order(5)]
        public void Test16MangerList()
        {
            Test01Login("Test16MangerList");
            fp.actionWork("הגדרות", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.sleep(200);
            nl.clickAdd();
            string[] newValue = { "שעון", "מחשב", "מצלמה", "פלאפון" };
            nl.addList(p.automation, newValue);
            fp.closeChrome();
            // nl.checkTable();
            fp.OpenExcel("Test16MangerList", "Passed");
        }
        /// <summary>
        /// הוספת מוסד
        /// </summary>//באג בזכר נקווה
        [TestMethod, Order(17)]
        public void Test18AddInstitutionClose()
        {
            Test01Login("Test18AddInstitutionClose");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment("בית ספר חסידי");
            sec.checkEndCreate("קופה", false,"ה");
            sec.closePage();
            fp.OpenExcel("Test18AddInstitutionClose", "Passed");
        }
        [TestMethod, Order(22)]//באג זכר ונקבה
        public void Test22AddInstitutionNewCard()
        {
            Test01Login("Test22AddInstitutionNewCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment("חיידר גור");
            sec.checkEndCreate("קופה", false,"ה");
            sec.pageNewCreate("קופה נוספת");
            fp.OpenExcel("Test22AddInstitutionNewCard", "Passed");
        }
        [TestMethod, Order(22)]//באג
        public void Test22AddInstitutionCard()
        {
            Test01Login("Test22AddInstitutionCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment("ישיבה");
            sec.checkEndCreate("קופה", false);
            sec.pageCard("קופה");
            fp.OpenExcel("Test22AddInstitutionCard", "Passed");
        }
        [TestMethod, Order(17)]
        public void Test22CreatePDFClose()
        {
            Test01Login("Test22CreatePDFClose");
            rs.stepPDF();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFCard()
        {
            Test01Login("Test22CreatePDFCard");
            rs.stepPDF();
            sec.pageCard("מסמך PDF");
            fp.OpenExcel("Test22CreatePDFCard", "Passed");
        }
        [TestMethod, Order(22)]//להריץ בדיבג
        public void Test22CreatePDFStartDesign()
        {
            Test01Login("Test22CreatePDFStartDesign");
            rs.stepPDF();
            sec.startDesign();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFStartDesign", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFNew()
        {
            Test01Login("Test22CreatePDFNew");
            rs.stepPDF();
            sec.pageNewCreate("מסמך PDF נוסף");
            fp.OpenExcel("Test22CreatePDFNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFMosad()
        {
            Test01Login("Test22CreatePDFMosad");
            rs.stepPDF();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22CreatePDFMosad", "Passed");
        }
        //  [TestMethod, Order(18)]
        public void Test22ReceptionStructuresClose()
        {
            Test01Login("Test22ReceptionStructuresClose");
            rs.stepRec();
            sec.closePage();
            fp.OpenExcel("Test22ReceptionStructuresClose", "Passed");
        }
        //   [TestMethod, Order(22)]
        public void Test22ReceptionStructuresGoCard()
        {
            Test01Login("Test22ReceptionStructuresGoCard");
            rs.stepRec();
            sec.pageCard("מבנה קבלה");
            fp.OpenExcel("Test22ReceptionStructuresGoCard", "Passed");
        }
        // [TestMethod, Order(22)]
        public void Test22ReceptionStructuresNew()
        {
            Test01Login("Test22ReceptionStructuresNew");
            rs.stepRec();
            sec.pageNewCreate("מבנה קבלה נוסף");
            fp.OpenExcel("Test22ReceptionStructuresNew", "Passed");
        }
        // [TestMethod, Order(22)]
        public void Test22ReceptionStructuresMosad()
        {
            Test01Login("Test22ReceptionStructuresMosad");
            rs.stepRec();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22ReceptionStructuresMosad");
        }


        [TestMethod, Order(10)]//נופל בבדיקה
        public void Test19CreateMaddPaiClose()
        {
            Test01Login("Test19CreateMaddPaiClose");
            string[] insertShilta = { "מטבע", "אינו ריק", "", "אופן תרומה", "שווה ל", "אשראי", "-1" };// [{ },{ }];
            cm.stepPai("תרומות", "אופן תרומה", " סכום ", insertShilta, "סכום", "יוצר הרשומה", "תאריך יצירה");
            sec.closePage();
            CheckMadd("תרומות", "תרומות", "pie");//נפל פה
            fp.OpenExcel("Test19CreateMaddPaiClose");
        }
        public void CheckMadd(string nameTitle, string nameButton, string typeGraph)
        {
            Test01Login();

            cm.checkClassMadd(nameTitle, nameButton, typeGraph);
            cm.openTable(nameButton, typeGraph);
            fp.closeChrome();
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddPaiCard()
        {
            Test01Login("Test23CreateMaddPaiCard");
            string[] inShilta = { "כיתה וסניף", "אינו ריק", "", "מוסד לימודים", "אינו ריק", "", "-1" };
            cm.stepPai("תלמידים", "רמת כיתה", "כמות ", inShilta, "", " רמת כיתה ", " תאריך כניסה ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddPaiCard");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddPaiNew()
        {
            Test01Login("Test23CreateMaddPaiNew");
            string[] inShilta = { "ידידים: רחוב ", "ריק", "", "שם הילד", "אינו ריק", "", "-1" };
            cm.stepPai("ילדים", "מגדר", "כמות", inShilta, "", " שייכות ", " תאריך לידה לועזי ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddPaiNew", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test19CreateMaddGrafLineClose()
        {
            Test01Login("Test19CreateMaddGrafLineClose");
            string[] inShilta = { "-1" };
            cm.stepGrafLine(" אמצעי תשלום ", " סוג אמצעי תשלום ", "כמות", inShilta, " ישות סליקה ", "", "סטטוס", " ExpiryDate ");
            sec.closePage();
            fp.OpenExcel("Test19CreateMaddGrafLineClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrafLineCard()
        {
            Test01Login("Test23CreateMaddGrafLineCard");
            string[] inShilta = { " שנה\"ל ", "אינו ריק", "", " רמת כיתה ", " שווה ל ", p.automation, "-1" };
            cm.stepGrafLine(" כיתות ", " רמת כיתה ", "כמות", inShilta, " סניף מוסד חינוך ", "", " רמת כיתה ", "false", false);
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddGrafLineCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrafLineNew()//נופל בבדיקה
        {
            Test01Login("Test23CreateMaddGrafLineNew");
            Thread.sleep(500);
           string[] inShilta = { " יוצר הרשומה ", " אינו ריק ", "", " הערות ", " שווה ל ", p.automation + "TEXT", "-1" };
            cm.stepGrafLine(" תרומות ", " מטבע ", "סכום", inShilta, " אופן תרומה ", " סכום ", " איש קשר ", " תאריך יצירה ");
            sec.pageNewCreate("מדד נוסף");
            CheckMadd("תרומות", "תרומות", "line");
            fp.OpenExcel("Test23CreateMaddGrafLineNew", "Passed");
        }

        [TestMethod, Order(10)]
        public void Test19CreateMaddGrapColumnClose()
        {
            Test01Login("Test19CreateMaddGrapColumnClose");
            string[] inShilta = { "-1" };
            cm.stepGrafColumn(" מסמכים ", " מעדכן הרשומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
            sec.closePage();
            fp.OpenExcel("Test19CreateMaddGrapColumnClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrapColumnCard()
        {
            Test01Login("Test23CreateMaddGrapColumnCard");
            string[] inShilta = { " תאריך יצירה ", " אינו ריק ", "", " מעדכן הרשומה ", " שונה מ ", " מערכת מנהל ", "-1" };
            cm.stepGrafColumn(" תרומות ", " אופן תרומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddGrapColumnCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrapColumnNew()
        {
            Test01Login("Test23CreateMaddGrapColumnNew");
            string[] inShilta = { " תאריך יצירה ", " אינו ריק ", "", " מעדכן הרשומה ", "שונה מ", cnd.nameuser(), "-1" };
            cm.stepGrafColumn(" ידידים ", " עיר ", "כמות", inShilta, " תואר ", "", " ללא ", " תאריך יצירה ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddGrapColumnNew", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test23CreateMaddTableClose()
        {
            Test01Login("Test23CreateMaddTableClose");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            Thread.sleep(250);
            sec.closePage();
            fp.OpenExcel("Test23CreateMaddTableClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddTableCard()
        {
            Test01Login("Test23CreateMaddTableCard");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddTableCard", "Passed");
        }
        [TestMethod, Order(22)]//פה נגמרה ההרצה
        public void Test23CreateMaddTableNew()
        {
            Test01Login("Test23CreateMaddTableNew");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddTableNew", "Passed");
        }
        [TestMethod, Order(10)]//להחזיר את הסינון
        public void Test19CreateMaddCardClose()
        {
            Test01Login("Test19CreateMaddCardClose");
            string[] inShilta = { " כונן ", " גדול מ ", "0TEXT", " כסאות ", " אינו ריק ", "", "-1" };
            cm.stepCard("kckc", "כמות", inShilta, "assignment", "", " ללא ", " ללא ");
            Thread.sleep(200);
            sec.closePage();
            chckDasMini("kckc", "kckc");
            fp.OpenExcel("Test19CreateMaddCardClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddCardCard()
        {
            Test01Login("Test23CreateMaddCardCard");
            string[] inShilta = { " הערות ", " שווה ל ", p.automation + "TEXT", " שם פרטי ", " מתחיל ב ", "אTEXT", "-1" };
            cm.stepCard(" תלמידים ", "כמות", inShilta, "apartment", "", " ללא ", " תאריך ");
            sec.pageCard("מדד");
            chckDasMini("תלמידים", "תלמידים");
            fp.OpenExcel("Test23CreateMaddCardCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void TestCreateMaddCardNew()
        {
            Test01Login("TestCreateMaddCardNew");
            string[] inShilta = { " אופן תרומה ", " אינו ריק ", "", " הערות ", " שווה ל ", p.automation + "TEXT", "-1" };
            cm.stepCard(" תרומות ", "סכום", inShilta, "attach_money", " סכום ", " ללא ", " תאריך ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("TestCreateMaddCardNew", "Passed");
        }
        //
        public void chckDasMini(string nameMIMI, string nameButton)
        {
            Test01Login();
            cm.chekmini(nameMIMI, nameButton);
            fp.closeChrome();
        }
        /// <summary>
        /// ניהול מערכת
        /// </summary>
        [TestMethod, Order(1)]//לא רץ תקין
        public void Test03CreateNameUser()
        {
            Test01Login("Test03CreateNameUser");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.openCreate();
            nu.checkCreate();
            nu.insertCreate();
            Thread.sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test03CreateNameUser", "Passed");
        }
        [TestMethod, Order(2)]
        public void Test05CheckNewUser()
        {
            ExcelApiTest eat = new ExcelApiTest("", "tasret");
            eat.bugReturn("tasret", "Failed", "Test05CheckNewUser");
            login ln = new login();
            ln.openChrome();
            ln.UserNamee(p.automation + "משתמש");
            ln.Password(p.automation + "משתמש1234");
            ln.cLickLogIn();
            ln.newPassword();
            fp.closeChrome();
            fp.OpenExcel("Test05CheckNewUser", "Passed");
        }
        [TestMethod, Order(20)]//רץ מאוד לאט נא ליבדוק
        public void Test22ChangeChaseBox()
        {
            Institution i = new Institution();
            Test01Login("Test22ChangeChaseBox");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.clickEditUser();
            nu.addMosad(p.automation);
            fp.refres();
            i.changeChaseBox(p.automation);
            i.checkChange(" ידידים ", " תרומות ");
            i.checkChange("", " תרומות בהו\"ק ");
            i.checkChange(p.student, " הוראות קבע ");
            i.checkChange("", " קמפיינים ");
            i.checkChange(" הוצאות והכנסות ", " הוצאות ");
            i.checkChange("", " תלמידים ");
            i.checkChange(" מוסדות ", " מוסדות חינוך ");
            i.checkChange(" ניהול כספים ", "הפקדת מזומן ", "", true);
            // i.checkChange(" ניהול כספים ", "הפקדת צ\'קים ", "", true);
            //  i.checkChange(" ניהול כספים ", "טיפול בצ\'קים ", "", true);
            i.checkChange(" ניהול כספים ", "ביצוע הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "דיווח חזרות מסב", "", true);
            i.checkChange(" ניהול כספים ", "שידורי אשראי ", "", true);
            i.checkChange(" ניהול כספים ", "צפייה וטיפול בתשלומים", "", true);
            i.checkChange(" ניהול כספים ", "ניהול סעיפים פיננסיים");
            i.checkChange(" ניהול כספים ", "טיפול בקבלות", " הצג נתונים ", true);
            i.checkChange(" ניהול כספים ", "טיפול בקבלות עבור הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "צפיה בקבלות");
            i.checkChange(" הגדרות ", "ישויות סליקה");
            i.checkChange(" הגדרות ", "חשבונות בנק");
            i.checkChange(" הגדרות ", "מבני קבלות");
            i.checkChange(" הגדרות ", "מסמכי PDF");
            i.cheackNotChange(" הגדרות ", "קופות");
            i.cheackNotChange(" הגדרות ", "מדדים");
          //  i.cheackNotChange(" הגדרות ", "ניהול רשימות ", "tr");
            i.cheackNotChange("", " ידידים ");
            i.cheackNotChange(" ניהול כספים ", "אמצעי תשלום");
            fp.closeChrome();
            fp.OpenExcel("Test22ChangeChaseBox", "Passed");
        }
        [TestMethod, Order(3)]
        public void Test04CreateRolse()
        {
            Test01Login("Test04CreateRolse");
            fp.actionWork("ניהול מערכת", "תפקידים ");//fp.clickNameDefinitions();
            Thread.sleep(300);
            fp.ClickButton("הוסף תפקיד ");
            newUser nu = new newUser();
            nu.addRolse(true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse(false, true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse();
            fp.closeChrome();
            fp.OpenExcel("Test04CreateRolse", "Passed");
        }
        [TestMethod, Order(4)]//באג
        public void Test05CheackAddRolse()
        {
            Test01Login("Test05CheackAddRolse");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickListinListEqualName(p.nameFrind + "ים");
            at.OpenSettings();
            at.openSettingTest(p.nameFrind + "ים");
            at.ceackAddSection(p.automation);
            fp.closeChrome();
            fp.OpenExcel("Test05CheackAddRolse", "Passed");
        }
        [TestMethod, Order(9)]
        public void Test10MosadDatotClose()
        {
            Test01Login("Test10MosadDatotClose");

            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.closePage();
            fp.OpenExcel("Test10MosadDatotClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22MosadDatotCard()
        {
            Test01Login("Test22MosadDatotCard");

            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.pageCard("מוסד לפי דתות");
            fp.OpenExcel("Test22MosadDatotCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22MosadDatotNew()
        {
            Test01Login("Test22MosadDatotNew");
            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.pageNewCreate("מוסד לפי דתות נוסף");
            fp.OpenExcel("Test22MosadDatotNew", "Passed");
        }
        //יצירת תרומה בהו"ק
        [TestMethod, Order(6)]
        public void Test07CreateConatarbitionHokClose()
        {
            Test01Login("Test07CreateConatarbitionHokClose");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK();
            sec.closePage();
            fp.OpenExcel("Test07CreateConatarbitionHokClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07CreateConatarbitionHokAsraiCard()
        {
            Test01Login("Test07CreateConatarbitionHokAsraiCard");

            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK();
            sec.pageCard("תרומה בהו\"ק");
            fp.OpenExcel("Test07CreateConatarbitionHokAsraiCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokAsraiFrind()
        {
            Test01Login("Test22CreateConatarbitionHokAsraiFrind");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateConatarbitionHokAsraiFrind", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokAsraiNew()
        {
            Test01Login("Test22CreateConatarbitionHokAsraiNew");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK();
            sec.pageNewCreate("תרומה בהו\"ק נוספת");
            fp.OpenExcel("Test22CreateConatarbitionHokAsraiNew", "Passed");
        }
        //תרומות הו"ק מס"ב
        [TestMethod, Order(6)]//דפקתי אותו
        public void Test07CreateConatarbitionHokMsadClose()
        {
            Test01Login("Test07CreateConatarbitionHokMsadClose");
            Thread.sleep(400);
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.closePage();
            fp.OpenExcel("Test07CreateConatarbitionHokMsadClose");
        }
        [TestMethod, Order(22)]//יצירת מסב נופל על
        public void Test22CreateConatarbitionHokMsadCard()//לשנות את הסעיף לתרומה
        {
            Test01Login("Test22CreateConatarbitionHokMsadCard");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.pageCard("תרומה בהו\"ק");
            fp.OpenExcel("Test22CreateConatarbitionHokMsadCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokMsadFrind()
        {
            Test01Login("Test22CreateConatarbitionHokMsadFrind");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav", "תרומה");
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateConatarbitionHokMsadFrind");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokMsadNew()
        {
            Test01Login("Test22CreateConatarbitionHokMsadNew");
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav", "תרומה");
            sec.pageNewCreate("תרומה בהו\"ק נוספת");
            fp.OpenExcel("Test22CreateConatarbitionHokMsadNew", "Passed");
        }
        //התחיבויות
        [TestMethod, Order(9)]
        public void Test10CreateNewLaiblitesClose()
        {
            Test01Login("Test10CreateNewLaiblitesClose");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "שטרן");
            sec.closePage();
            fp.OpenExcel("Test10CreateNewLaiblitesClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesCard()
        {
            Test01Login("Test22CreateNewLaiblitesCard");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "רוזנברגר");
            sec.pageCard("התחייבות");
            fp.OpenExcel("Test22CreateNewLaiblitesCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesNew()
        {
            Test01Login("Test22CreateNewLaiblitesNew");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "רוזנברגר");
            sec.pageNewCreate("התחייבות נוספת");
            fp.OpenExcel("Test22CreateNewLaiblitesNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test10CreateNewLaiblitesFrind()
        {
            Test01Login("Test22CreateNewLaiblitesFrind");
            fp.ClickList(p.nameFrind + "ים");
            Laiblites l = new Laiblites();
            fp.clickNameDefinitions(" תרומות ");
            string name = l.shearchNameRequerd();
            fp.clickNameDefinitions(" התחייבויות ");
            l.stepNewLaiblites("", name);
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateNewLaiblitesFrind", "Passed");
        }
        //[TestMethod, Order(10)]//לא צריך יותר
        public void Test11OffsettingLaiblites()//לא מוצג חיובים
        {
            Test01Login("Test11OffsettingLaiblites");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.clickTableUser();
            l.offsetting();
            fp.closeChrome();
            fp.OpenExcel("Test11OffsettingLaiblites", "Passed");
        }
        [TestMethod]//כולל בדיקה של סמיילי
        public void Test19OffsettingLiabilitiesFrind()
        {
            Test01Login("Test11CreatCehildrenClose");
            Frind f = new Frind();
            int numHora = f.stepLiabilities();
            fp.closePoup(1);
            f.checkChargesTitle(numHora);
            f.checkSmaile(numHora);
            fp.closeChrome();
            fp.OpenExcel("Test11CreatCehildrenClose");
        }
        [TestMethod, Order(10)]
        public void Test11CreatCehildrenClose()
        {
            Test01Login("Test11CreatCehildrenClose");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.closePage();
            fp.OpenExcel("Test11CreatCehildrenClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenCard()
        {
            Test01Login("Test22CreatCehildrenCard");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageCard("ילד");
            fp.OpenExcel("Test22CreatCehildrenCard");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenNew()
        {
            Test01Login("Test22CreatCehildrenNew");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageNewCreate("ילד נוסף");
            fp.OpenExcel("Test22CreatCehildrenNew");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenFrind()
        {
            Test01Login("Test22CreatCehildrenFrind");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreatCehildrenFrind", "Passed");
        }
        [TestMethod, Order(18)]
        public void Test19CreateTackingFrindClose()
        {
            Frind f = new Frind();
            Test01Login("Test19CreateTackingFrindClose");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.closePage();
            fp.OpenExcel("Test19CreateTackingFrindClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindCard()
        {
            Frind f = new Frind();
            Test01Login("Test22CreateTackingFrindCard");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageCard("מעקב ידיד");
            fp.OpenExcel("Test22CreateTackingFrindCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindNew()
        {
            Frind f = new Frind();
            Test01Login("Test22CreateTackingFrindNew");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageNewCreate("מעקב ידיד נוסף ");
            fp.OpenExcel("Test22CreateTackingFrindNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindGFrind()
        {
            Test01Login("Test22CreateTackingFrindGFrind");
            Frind f = new Frind();
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateTackingFrindGFrind", "Passed");
        }
        [TestMethod, Order(20)]
        public void Test20CreateTaskClose()
        {
            Test01Login("Test20CreateTaskClose");
            Thread.sleep(200);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " כיתות ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("כיתה", "א", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("יש צורך לשנות את שם הכיתה");
            sec.closePage();
            fp.OpenExcel("Test20CreateTaskClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskCard()//לטפל
        {
            Test01Login("Test22CreateTaskCard");
            Thread.sleep(200);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " תלמידים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("תלמיד", "א", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("לתלמיד מגיע מתנה על היתנהגותו הנהותה");
            sec.pageCard("משימה");
            fp.OpenExcel("Test22CreateTaskCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskNew()//רץ תקין
        {
           Test01Login("Test22CreateTaskNew");
            Thread.sleep(600);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " ידידים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("ידיד", "100005", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("הידיד עבר דירה יש להתעדכן ולעדכן את הכתובת החדשה");
            sec.closePage();
            checkTeask("ידידים", "ידידים", "100005");
            fp.OpenExcel("Test22CreateTaskNew", "Passed");
        }
        public void checkTeask(string nitov, string Tatnitov, string name)
        {
            Test01Login();
            fp.ClickList(nitov);
            if (nitov != Tatnitov)
                fp.clickNameDefinitions(Tatnitov);
            else
                fp.clickListinListEqualName(Tatnitov);
            string[] listc = { "", "", "", "-1" };
            if (name.Contains("1"))
                listc[1] = name;
            else
                listc[2] = name;
            fp.checkColumn("//", listc, "", "mat-row", "mat-cell");
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(16, "משימות");
            string[] checktaskl = { "", "", "לביצוע", "", " pageview ", " post_add ", " delete ", "-1" };
            fp.checkColumn("//", checktaskl, "false", "mat-row", "mat-cell", -1, 1);
            fp.closeChrome();
        }
        [TestMethod]//שדה מקושר ומעבר לטאב השני של המייל
        public void Test18CreateProcesseSendMailAfterIdceonClose()
        {
            Test01Login("Test18CreateProcesseSendMailAfterIdceonClose");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה", "ידידים", "אחרי עדכון");
            string[] filter = { "דוא\"ל", "שווה ל", p.MAIL + "TEXT", "מייל לקבלה", "שווה ל", p.MAIL + "TEXT", "-1" };
            ps.stepFrainLast(filter, 2, false, 1);
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
        }
        //לבדוק חיובים לשלחית מייל בכל תחילת חודש
        [TestMethod]
        public void Test19CheckSendMailProcess()
        {
            Test01Login("Test19CheckSendMailProcess");
            f.openFrind();
            PupilNew pn = new PupilNew();
            Thread.sleep(350);
            pn.clickTableRandom();
            f.changeW();
            fp.closeChrome();
            Console.WriteLine("יש לבצע בדיקה שהכן נשלח מייל");
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
            // login l = new login();
            // l.openMail();
        }

        [TestMethod]
        public void Test18CreateProcesseUpdateValueBeforIdceonCard()//נופל בגלל ה==
        {
            Test01Login("Test18CreateProcesseUpdateValueBeforIdceonCard");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה לפני עדכון", "קופות", "לפני עדכון");
            string[] filter = { "קוד מוסד", "אינו ריק", "", "עיר", "שווה ל", "ערד", "-1" };
            ps.stepFrainLast(filter);
            ps.step3(" עדכון ערכים ", "עיר", "עיר", "ערד");
            sec.pageCard("תהליך");
            fp.OpenExcel("Test18CreateProcesseUpdateValueBeforIdceonCard", "Passed");
        }
        //עדכון תאריך
        [TestMethod]
        public void Test18CreateProcesseUpdateDateEbruBeforAddNew()
        {
            Test01Login("Test18CreateProcesseUpdateDateEbruBeforAddNew");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה הוספה", "נסיון ריצה ע\"י אוטומציה לפני הוספה", " תלמידים ", " לפני הוספה ");
            string[] filter = { " ת. לידה עברי ", " ריק ", "", " ת. לידה לועזי ", " אינו ריק ", "" };
            ps.stepFrainLast(filter, 1);
            ps.step3("עדכון תאריך עברי", "2", "ידידים ת. לידה לועזי ", "ידידים ת. לידה עברי ", true, 2);
            sec.pageNewCreate("תהליך נוסף");
            fp.OpenExcel("Test18CreateProcesseUpdateDateEbruBeforAddNew", "Passed");
        }
        [TestMethod]//תהליך קבלה מסוכמת//באג בצרוף תמונה
        public void Test18CreateProcesseDochTkofatAutomationOneTime()
        {
            Test01Login("Test18CreateProcesseDochTkofatAutomation");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("דוח תקופתי אוטומטי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " ידידים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("חד פעמי", DateTime.Today);
            fp.ClickButton(" לשלב הבא ", "span");
            ps.step3("שליחת דוח תקופתי", "תקופתי", "1/08/-1", "01/0/0", true, 1);
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");

        }

        [TestMethod]//באג

        public void Test18CreateProcesseDochTkofatAutomationCurrent()
        {
            Test01Login("Test18CreateProcesseDochTkofatAutomation");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("דוח תקופתי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " ידידים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 3, 0235);
            ps.timeHour("מס' פעמים", "8:18", 10, "שעות", "20:18");
            // ps.numberCheckStatusAction("3 ימים, 0235, 10, "8:18", "20:18");
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");
        }
        [TestMethod]
        public void Test18ProcessGenratorAndHQ()
        {
            string dateTimeH = "";
            Test01Login("Test18ProcessGenratorAndHQ");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("מחולל ביצוע הוק", " אוטומציה", " הורים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1, false, 0, " לשלב הבא ", true, true);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 2, 1);
            dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            ps.timeHour("חד פעמי", dateTimeH);
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" מחולל חיובים וביצוע הו\"ק ", "בית אברהם", "כללי", "אשראי");
            fp.closePoup();
            fp.clickNameDefinitions(" הוצאות והכנסות ");
            fp.clickNameDefinitions(" תקבולים ");
            PupilNew pn = new PupilNew();
            pn.checkTable(DateTime.Today.ToString("dd/MM/yyyy-") + dateTimeH);
            fp.closeChrome();
            fp.OpenExcel("Test18ProcessGenratorAndHQ", "Passed");
        }
        [TestMethod]//רץ
        public void Test18ProcessreturenFail()//נבדק רק מקקומית ולא בטבלאות הקשורות
        {
            string dateTimeH = "12:45";
            Test01Login("Test18ProcessreturenFail");
            process ps = new process();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            string[] name = { "", "", "" };
           /* name = vhp.changestatues();
            ps.openProcess();
            ps.insertStep1("החזרת נכשלים", " אוטומציה", "", " אחרי הוספה ");
            //מיותר
             //dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            // ps.Automation("חד פעמי", DateTime.Today, dateTimeH);
            //fp.ClickButton(" לשלב הבא ", "span");
            ps.step3(" שידור חוזר לנכשלים ", "אשראי הו\"ק", "סירוב", "5");
            fp.closePoup();*/
            // ps.checkSucess(dateTimeH);
            vhp.checkstatusProcess(name);
            fp.closeChrome();
            fp.OpenExcel("Test18ProcessreturenFail");
        }
        [TestMethod]
        public void Test18createProcessTasx()
        {
            Test01Login("Test18createProcessTasx");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("יצירת משימה", "תהליך יצירת משימה", " ידידים ", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            ps.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            ps.step3(" יצירת משימה ", " תהליך משימה ע\"י אוטומציה", " לביצוע ", " נסיון אוטומציההרבה הצלחה", true, 3);
            fp.refres();
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            ps.checkProcesTasx();
            fp.closeChrome();
            fp.OpenExcel("Test18createProcessTasx");
        }

        [TestMethod]
        public void Test24ProcessCreateFrind()
        {
            Test01Login("Test24ProcessCreateFrind");
             process ps = new process();
            ps.openProcess();
             ps.insertStep1("יצירת ידיד", " יצירת ידיד אוטומציה", " תלמידים ", " לפני עדכון ");
             string[] filter = { "", "", "", " סיבת עזיבה", " שווה ל ", " חתונה " };
             ps.stepFrainLast(filter, 2);
             ps.step3(" יצירת ידיד ", " סוג תורם ", " בוגר ", "", false, -1, false);
             ps.step3(" עדכון ערכים ", " סטטוס ", " סטטוס ", "עזב");
             fp.refres();
             PupilNew pn = new PupilNew();
             pn.openPupil();
            string nameStudent  = pn.changeStudentToLeave();
            f.openFrind();
            pn.clickTable(nameStudent);
            fp.closeChrome();
            fp.OpenExcel("Test24ProcessCreateFrind");
        }
        [TestMethod]
        public void Test33ProcessExpenditure()
        {
            Test01Login("Test33ProcessExpenditure");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("יצירת הוצאה", "תהליך יצירת הוצאה", " ידידים ", " אחרי עדכון ");
            string[] filter = { "עיר", "שונה מ", "ירושלים", "מקום עבודה", "שווה ל", "conosoftTEXT", "-1" };
            pr.stepFrainLast(filter, 2, false, 0);
            pr.step3("יצירת הוצאה", "גרסה", "קבלת שירות", "4");
            fp.refres();
            f.openFrind();
            Thread.sleep(500);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            string nameFrind = f.changeW();
            fp.refres();
            fp.ClickList("הוצאות והכנסות");
            fp.clickListinListEqualName("הוצאות");
            string[] rowExp = { "", nameFrind, "", "קבלת שירות", "4", "-1" };
            fp.checkColumn("//", rowExp, "false", "mat-row", "mat-cell");
            fp.closeChrome();
            fp.OpenExcel("Test33ProcessExpenditure");
        }
        [TestMethod]
        public void Test21ProcessUpdateValueTableFather()
        {
            Test01Login("Test21ProcessUpdateValueTableFather");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("עדכון מטבלת אב", "עדכון ערכים מטבלת אב", " תרומות בהו\"ק ", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(1).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            pr.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            pr.step3("עדכון ערכים", "הערות", "low_priority", "משפחה");
            fp.refres();
            createConatrabition cc = new createConatrabition();
            cc.openPageCDDebit();
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            popupList pl = new popupList();
            pl.clickEdit();
            pr.checkChange();
            fp.closeChrome();
            fp.OpenExcel("Test21ProcessUpdateValueTableFather");
        }
        [TestMethod]
        public void Test36ProcessTableNoConnected()
        {
            Test01Login("Test36ProcessTableNoConnected");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("טבלה לא מקושרת", "שליחת מייל עם טבלה לא מקושרת", "", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(1).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            pr.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            pr.step3("שליחת מייל", "", " מייל ללא טבלה מקושרת", "אין פה טבלה מקושרת", true);
            fp.closeChrome();
            fp.OpenExcel("Test36ProcessTableNoConnected");
        }
        [TestMethod]
        public void Test35ProceseCreateYear()
        {
            Test01Login("Test35ProceseCreateYear");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("יצירת שנתון", "יצירת שנתון לשנה חדשה"," רמות כיתה ","אוטומטי");
            string dt = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            string[] filter = { "מוסד", "שווה ל", "בית אברהם" };
            pr.stepFrainLast(filter, 1, false, 0);
            pr.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            pr.step3( "יצירת שנתון", "בית אברהם", "כיתה א","כיתה ב");
            fp.refres();
            fp.selectForRequerd("שנת לימודים", false, -1);
            fp.selectText(p.yearAdd);
            PupilNew pn = new PupilNew();
            pn.openPupil();
            fp.checkInRow("כיתה ג");
            fp.closeChrome();
            fp.OpenExcel("Test35ProceseCreateYear");
        }
        [TestMethod]
        public void Test20GradeLavelCOLL()
        {
            Test01Login("Test20GradeLavelCOLL");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.clickOnTable("כולל", "כולל");
            popupList pl = new popupList();
            pl.clickLavel();
            createGradeLavel cgl = new createGradeLavel();
            cgl.GradeLavelCreate("סדר א", "כולל", "כולל", "22");
            Thread.sleep(200);
            fp.closeChrome();
            fp.OpenExcel("Test20GradeLavelCOLL", "Passed");
        }
        [TestMethod, Order(7)]
        public void Test21CreateGradeCOLL()
        {

            Test01Login("Test21CreateGradeCOLL");
            Grade g = new Grade();
            g.stepCreateGard("כולל", "כולל", "סדר א");
            fp.closeChrome();
            fp.OpenExcel("Test21CreateGradeCOLL", "Passed");
        }

        [TestMethod]
        public void Test23CreatePupilCOLL()//רץ תקין הופסק ידנית
        {
            Test01Login("Test23CreatePupilCOLL");
            PupilNew pn = new PupilNew();
            pn.createPupilColl();
            fp.OpenExcel("Test23CreatePupilCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalCurentOneCloseCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentOneCloseCOLL");

            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " חודש בודד ", "00/0000" + "-", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalCurentOneCloseCOLL");
        }[TestMethod]
        public void Test24CreatePromotionalCurentRangeCardCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentRangeCardCOLL");
            //fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " טווח חודשים ", "0/0-0/1", "כולל", " כולל (זיכוי) ");
            sec.pageCard("הגדרת סעיף זיכוי");
            fp.OpenExcel("Test24CreatePromotionalCurentRangeCardCOLL");
        }[TestMethod]
        public void Test24CreatePromotionalCurentPaymentAddCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentPaymentAddCOLL");
            //  fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " תשלומים ", "0/0-5/0", "כולל", " כולל (זיכוי) ");
            sec.pageNewCreate("הגדרת סעיף זיכוי נוסף");
            fp.OpenExcel("Test24CreatePromotionalCurentPaymentAddCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimeMosadOne()
        {
            Test01Login("Test24CreatePromotionalOneTimeMosadOne");
            // fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים",p.automation,"זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " חודש בודד ", "0/0-", p.automation, " כולל (זיכוי) ");
            sec.pageCard("מוסד חינוך");
            fp.OpenExcel("Test24CreatePromotionalOneTimeMosadOne");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimeRange()
        {
            Test01Login("Test24CreatePromotionalOneTimeRange");
            //fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " טווח חודשים ", "0/0-0/1", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalOneTimeRange");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimetPayment()
        {
            Test01Login("Test24CreatePromotionalOneTimetPayment");
           //  fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " תשלומים ", "0/0-5/0", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalOneTimetPayment");
        }
        [TestMethod]
        public void Test25CreditGenerator()
        {
            Test01Login("Test25CreditGenerator");
            fp.actionWork("ניהול כספים", "מחולל זיכויים");
            Promotional pr = new Promotional();
            pr.stepCreditGenerator();
            fp.closeChrome();
            fp.OpenExcel("Test25CreditGenerator");
        }
        [TestMethod]
        public void Test26PrintCheck()
        {
            Test01Login("Test26PrintCheck");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCraditPayments();
            // fp.closeChrome();
            fp.OpenExcel("Test26PrintCheck");
        }

        [TestMethod]
        public void Test27cancelCheck()
        {
            Test01Login("Test27cancelCheck");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            int numCheck = 20000;
            pr.cenelloctionCheck(numCheck);
            fp.picter("print cancel check");
            fp.closeChrome();
            fp.OpenExcel("Test27cancelCheck");
        }
        [TestMethod]
        public void Test27CheckPromotional()
        {
            Test01Login("Test27CheckPromotional");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "קיזוז", "קיזוז");
            fp.closepopup();
           fp.refres();
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.clickOnTableP();
            fp.closeChrome();
            fp.OpenExcel("Test27CheckPromotional");
        }
        [TestMethod]//לא סיימתי
        public void Test28ScholarshipsCaseCheckHQ()
        {
            Test01Login("Test28ScholarshipsCaseCheckHQ");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            string[] nameAvrec = { "", "", "-1" };
            pr.addPromtionalPaymenMethod(nameAvrec, "מזומן");
            pr.addPromtionalPaymenMethod(nameAvrec, "צ'ק");
            pr.addPromtionalPaymenMethod(nameAvrec, "העברה בנקאית");
            dobut d = new dobut();
            d.checkMakePayment();
            pr.cholarships();
            fp.closeChrome();
            fp.OpenExcel("Test28ScholarshipsCaseCheckHQ");
        }
        [TestMethod]
        public void Test10createContacts()
        {
            Test01Login("Test10createContacts");
            f.createContacts("חנוך");
            sec.closePage();
            fp.OpenExcel("Test10createContacts");

        }
        [TestMethod]
        public void Test25createContactsCard()
        {
            Test01Login("Test10createContacts");
            f.createContacts("משה");
            sec.pageCard("איש קשר");
            fp.OpenExcel("Test25createContactsCard");
        }
        [TestMethod]
        public void Test25createContactsNew()
        {
            Test01Login("Test10createContacts");
            f.createContacts("אברהם");
            sec.pageNewCreate("איש קשר נוסף");
            fp.OpenExcel("Test25createContactsNew");
        }
        [TestMethod]
        public void Test28CreateCreaditStudentBonos()
        {
            Test01Login("Test28CreateCreaditStudentBonos");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCreaditStudent();
            fp.closeChrome();
            fp.OpenExcel("Test28CreateCreaditStudentBonos");
        }[TestMethod]
        public void Test28CreateCreaditStudent()
        {
            Test01Login("Test28CreateCreaditStudent");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCreaditStudent("זיכוי");
            fp.closeChrome();
            fp.OpenExcel("Test28CreateCreaditStudent");
        }
        /// <summary>
        /// שמירה פרטית
        /// </summary>
        // [TestMethod, Order(7)]//לא תקין לבדוק בהרצה הבאה
        /*  public void Test29savePraivet()
          {
              Test01Login("Test29savePraivet");
              at.stepSavePrivate();
              fp.OpenExcel("Test29savePraivet");
          }*/
        [TestMethod]
        public void Test29checkCreditTableExpanses()
        {
            Test01Login("Test29checkCreditTableExpanses");
            at.openTable(" הוצאות והכנסות ", "הוצאות", 1);
            Promotional pr = new Promotional();
            pr.checkTable();
            fp.closeChrome();
            fp.OpenExcel("Test29checkCreditTableExpanses");
        }
        /// <summary>
        /// פרופיל אישי
        /// </summary>
        [TestMethod]
        public void Test27cheangePrivateProfil()//רץ כמעט תקין
        {
            Test01Login("Test27cheangePrivateProfil");
            PrivateProfil PP = new PrivateProfil();
            Thread.sleep(750);
            PP.iconUser();
            string name = p.names[fp.rand(p.names.Length)]+" "+p.familys[fp.rand(p.familys.Length)];
            PP.changeName(name, "0556771165", "rvaitz22@gmail.com");
            string color = PP.changeColor("yellow_blue");
            string typeFont = PP.changeWrote(" מודגש ");
            PP.changeDisplayTable(true, true);
            fp.refres();
            char[] fon = { '-' };
            PP.checkChange(name , color, typeFont.Split(fon)[1]);
            PP.changeInTable(true, true, color);
            fp.closeChrome();
            fp.OpenExcel("Test27cheangePrivateProfil");
        }
        /// <summary>
        /// החזרת אשראי(לחיצה על הכפתור הקטן של העט)
        /// </summary>
        [TestMethod]
        public void Test20ReturnAsrai()
        {
            Test08CreaditTransmissionCT();
            Test01Login("Test20ReturnAsrai");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.returnAsrai();
            fp.closeChrome();
            fp.OpenExcel("Test20ReturnAsrai");
        }
        /// <summary>
        ///בדיקת פתיחת רשומה ושינויים נתונים
        /// </summary>
        [TestMethod]
        public void Test28checkOpenListAndChange()
        {
            Test01Login("Test28checkOpenListAndChange");
            popupList pl = new popupList();
            pl.checkOpen();
            fp.closeChrome();
            fp.OpenExcel("Test28checkOpenListAndChange");
        }
        /// <summary>
        /// יצירת דוח
        /// </summary>
        [TestMethod]
        public void Test28CreateDoc()
        {
            Test01Login("Test28CreateDoc");
            PupilNew pn = new PupilNew();
           pn.openPupil();
            string[] listSelect = { "מוסד לימודים", "שם משפחה", "שם פרטי" };
            string[] listSelect2 = { "מוסד לימודים" };
            cnd.createDoc(true, true, "תלמיידדד",listSelect,listSelect2);
            fp.refres();
            pn.openPupil();
            cnd.clickTab("תלמיידד", 4);
            fp.closeChrome();
            fp.OpenExcel("Test28CreateDoc");
        }
        /// <summary>
        /// יצירת סינון מהיר
        /// </summary>
        [TestMethod]//לא רץ טוב
        public void Test28filterQuickly()
        {
            Test01Login("Test28filterQuickly");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            cnd.filterQuickly();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
            cnd.filter("תאריך", "תאריך שנה", " תאריך ; שנה זו ;", true, 0, "", 0, 0, "", "yyDATE");
            cnd.filter("edit", "תאריך שבוע", " תאריך ; שבוע זה ;", true, 1, "תאריך", 0, 0, "", "שבוע זה");
            cnd.filter("editbin", "תאריך בין", " תאריך ;בין;" + DateTime.Today.AddDays(-7).ToString("dd/MM/yyyy") + "+" + DateTime.Today.ToString("dd/MM/yyyy"), true, 1, "תאריך", 0, 0, "", "בין");
            cnd.filter("editbin", "תאריךעדהיום", " תאריך ;עד היום;", true, 1, "תאריך", 0, 0, "", "עד היום");
            cnd.filter("editbin", "תאריךהיום", " תאריך ;היום;", true, 1, "תאריך", 0, 0, "", "היום");
            fp.actionWork("הגדרות", "תהליכים");
            fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
            cnd.filter("תאריך עתידי", "תאריךשנה+", " זמן ריצה הבא ;שנה הבאה;", true, 1, "תאריך", 0, 0, "", "year+");
            cnd.filter("editbin", "תאריךחודש+", " זמן ריצה הבא ;חודש הבא;", true, 1, "תאריך", 0, 0, "", "mont+");
            cnd.filter("editbin", "תאריךשבוע+", " זמן ריצה הבא ;שבוע הבא;", true, 1, "תאריך", 0, 0, "", "week+");
            cnd.filter("editbin", "תאריךיום+", " זמן ריצה הבא ;מהיום;", true, 1, "תאריך", 0, 0, "", "DAY+");
            fp.closeChrome();
            fp.OpenExcel("Test28filterQuickly");
        }
        [TestMethod]
        public void Test23Nedarim()
        {
            Test01Login("Test23Nedarim");
            Nedarim np = new Nedarim();
            np.stepProcess();
            fp.refres();
            Thread.sleep(1500);
            fp.actionWork(" ניהול כספים ", "טיפול בנתונים מנדרים פלוס");
            np.actionRowNdarim();
            fp.closeChrome();
            fp.OpenExcel("Test23Nedarim");
        }
        [TestMethod]
        public void Test30ImageLOGO()
        {
            Test01Login("Test30ImageLOGO");
            cnd.imageLogo();
            cnd.chancgeSizeOrImage(1);
            cnd.chancgeSizeOrImage(0);
            fp.closeChrome();
            fp.OpenExcel("Test30ImageLOGO");
        }
        [TestMethod]//בשביל שהתסריט ירוץצריך לשנות לKONOSOFTUSER
        public void Test30ExportImportCheckColl()
        {
            Test01Login("Test30ExportImportCheckColl");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Thread.sleep(780);
            fp.click(By.XPath("//div[@mattooltip='הורדת קובץ זיכויים למילוי']/i[text()='download']"));
            Promotional pr = new Promotional();
             pr.insertDataExcel();
            pr.checkSave();
            fp.closeChrome();
        }
        [TestMethod]//יש באג//באג בשם
        public void Test31CheckStutusCharges()
        {
            int numparents;
            Test01Login("Test31CheckStutusCharges");
            f.openParent();
            PupilNew pn = new PupilNew();
            Thread.sleep(290);
            numparents = pn.clickTableRandom();
            popupList pl = new popupList();
            pl.clickEdit();
            f.charges();
            f.parentsHok();
            f.payments(numparents);
            string m = fp.dateD();
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", m);
            ehq.clickGvia();//לא סימתי יש באג עם התאריך
            Thread.sleep(300);
            fp.closeChrome();
        }

        //קופה קטנה
        [TestMethod]
        public void Test31Mateers()
        {
            Test01Login("Test31Mateers");
            Materrs ms = new Materrs();
            ms.openMetters();
            ms.stepMeterss(true, "20", "בגין קניית כוסות");
            fp.closepopup();
            ms.stepMeterss(false, "80", "החזרת הלוואה לתלמיד");
            sec.pageCard(" טופס ", "btnGoToObjectCreated");
            Test01Login("Test31Mateers");
            ms.openMetters();
            ms.stepMeterss(true, "20", "חלב");
            sec.pageNewCreate("טופס נוסף ");
            fp.OpenExcel("Test31Mateers");
        }
        [TestMethod]
        public void Test31FinanceClearingMeanesPayment()
        {

            Test01Login("Test31FinanceClearingMeanesPayment");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "הוק תלמידים", "חיוב או זיכוי", " אשראי הו\"ק");
           fp.closePoup();
            PupilNew pn = new PupilNew();
           Thread.sleep(100);
            pn.openPupil();
            Thread.sleep(350);
            pn.clickTableRandom();
            popupList pl = new popupList();
            pn.prentalDataPayment(" הו\"ק ", "", true, 1, " אשראי ");
            fp.ClickButton("שמירה", "span");
            Thread.sleep(800);
            fp.closePoup();
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי", "בית אברהם", "תלמידים הוק", "הוק תלמידים");
            fp.closePoup();
            fp.actionWork(" ניהול כספים ", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.insertElment("בית אברהם", DateTime.Today.ToString("M/yyyy"), "הוק תלמידים","", "DebitMonth");
            fp.closeChrome();
        }
        [TestMethod]
        public void Test31Histori()
        {
            Test01Login("Test31Histori");
            Thread.sleep(350);
           PupilNew pn = new PupilNew();
            string user = cnd.nameuser();
            string[] rowAction = { user, "הורים", DateTime.Today.ToString("dd/MM/yyyy-"), "עדכון", "-1" };
           string[] listAdd = { "", "סטטוס", "", "פעיל", "-1" };
               fp.clickNameDefinitions(" תלמידים ");
             fp.clickNameDefinitions(" הורים ");
            Thread.sleep(350);
             int row = pn.clickTableRandom();
             string nameM = cnd.change();
             char[] spli = { ';' };
             string[] listAction = { fp.splitnumber(nameM).ToString(), "שם פרטי", nameM.Split(spli)[0], nameM.Split(spli)[0] + "אאא", "-1" };
             Thread.sleep(200);
             pn.clickTableRandom(row);
             cnd.histori(rowAction, listAction);
             cnd.historiAdd();
             fp.clickNameDefinitions(" תלמידים ",1);
            Thread.sleep(350);
               row = pn.clickTableRandom();
            rowAction[1] = "תלמידים";
            rowAction[3] = "הוספה";
            
              cnd.histori(rowAction, listAdd);
            fp.refres();
            pn.openStudentColl();
            Thread.sleep(250);
            pn.clickTableRandom(0);
            rowAction[1] = p.avracim;
            cnd.histori(rowAction, listAdd);
            fp.closeChrome();
            fp.OpenExcel("Test31Histori");
        }
        [TestMethod]
        public void Test31SuppliersClose()
        {

            Test01Login("Test31SuppliersClose");
            dobut d = new dobut();
            d.createnewDoubt(p.familys[fp.rand(p.familys.Length)], p.names[fp.rand(p.names.Length)]);
            sec.closePage();
            fp.OpenExcel("Test31SuppliersClose");
        }[TestMethod]
        public void Test31Supplierscard()
        {
            Test01Login("Test31Supplierscard");
            dobut d = new dobut();
            d.createnewDoubt(p.familys[fp.rand(p.familys.Length)], p.names[fp.rand(p.names.Length)]);
            sec.pageCard("ספק", "btnGoToObjectCreated");
            fp.OpenExcel("Test31Supplierscard");
        }[TestMethod]
        public void Test31SupplierdNew()
        {
            Test01Login("Test31SupplierdNew");
            dobut d = new dobut();
            d.createnewDoubt(p.familys[fp.rand(p.familys.Length)], p.names[fp.rand(p.names.Length)]);
            sec.pageNewCreate("ספק נוסף");
            fp.OpenExcel("Test31SupplierdNew");
        }
        [TestMethod]
        public void Test32ServiceCardClose()
        {
            Test01Login("Test32ServiceCardClose");
            dobut d = new dobut();
            d.ServiceReceipts();
            sec.closePage();
            fp.OpenExcel("Test32ServiceCardClose");
        }[TestMethod]
        public void Test32ServiceCardCard()
        {
            Test01Login("Test32ServiceCardCard");
            fl.createNewFinancicalSection(" הוראות קבע ראשי ", "ספקים", "ספקים", "תשלום לספק");
            fp.refres();
            dobut d = new dobut();
            d.ServiceReceipts();
            sec.pageCard("קבלת שרות", "btnGoToObjectCreated");
            fp.OpenExcel("Test32ServiceCardCard");
        }[TestMethod]
        public void Test32ServiceCardnew()
        {
            Test01Login("Test32ServiceCardnew");
            dobut d = new dobut();
            d.ServiceReceipts(false);
            sec.pageNewCreate("קבלת שירות נוסף ");
            fp.OpenExcel("Test32ServiceCardnew");
        }
        [TestMethod]
        public void Test32ServicePayment()
        {

            Test01Login("Test32ServicePayment");
            ce.CreateNewClearingEntity("מסב זיכוים", "מסב");
            fp.refres();
            dobut d = new dobut();
            d.ServiceReceipts();
            fp.ClickButton(" צור תשלום עבור הקבלת שירות ", "span");
            d.addPayment(true);
            fp.closeChrome();
            fp.OpenExcel("Test32ServicePayment");
        }[TestMethod]
        public void Test32ServiceCardDobt()
        {
            Test01Login("Test32ServiceCardnew");
            dobut d = new dobut();
            d.ServiceReceipts();
            sec.pageCard("ספק");
            fp.OpenExcel("Test32ServiceCardnew");
        }
        [TestMethod]
        public void Test33PaymentsSupplier()
        {
            Test01Login("Test33PaymentsSupplier");
            dobut d = new dobut();
            d.openDoubt();
            d.clickTab(1, "תשלומים לספקים");
            d.checkTablePaymen();
            d.makePayment();
            fp.closeChrome();
            fp.OpenExcel("Test33PaymentsSupplier");
        }
        [TestMethod]//באג בהדפסת צקים
        public void Test34typePaymentDoubt()
        {
            Test01Login("Test34typePaymentDoubt");
            dobut d = new dobut();
           d. openDoubt();
            string cell = d.ServiceReceipts();
            for (int i = 0; i < 2; i++)
            {
                fp.ClickButton(" ליצור קבלת שירות נוספת ", "span");
                d.newRespit(cell, false);
            }
            fp.closepopup();
            d.clickTab(1, "תשלומים לספקים");
            d.paymentsDoubt();
            fp.closeChrome();
            fp.OpenExcel("Test34typePaymentDoubt");
        }
        //שינוי CVV
        [TestMethod]
        public void Test33Cheange3numberAsrai()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            string[] rowCard = { "", "", "", "", "", "", "", "", "", "", "", "", "אשראי", "-1" };
            int row = fp.checkColumn("//", rowCard, "", "mat-row", "mat-cell", 0);
            string row3 = fp.rand(999).ToString();
            cc.checkChangeCVV(row, "");
            row = fp.checkColumn("//", rowCard, "", "mat-row", "mat-cell", 0);
            string name = cc.checkChangeCVV(row, row3);
            fp.actionWork("ניהול כספים", "אמצעי תשלום");
            cc.checkValidity(name, row3);
            fp.closeChrome();
        }
        //החלפת סוג תשלום(לאשראי אחר(
        [TestMethod]
        public void Test33ChangeMethodPayments()
        {
            Test01Login("Test33ChangeMethodPayments");
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("כרטיס אשראי", "Credit", false, -1, "", 2);
           fp.refres();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.cheangeMethodPayments();
            fp.closeChrome();
            fp.OpenExcel("Test33ChangeMethodPayments");
        }
        //שינוי תוקף כרטיס
        [TestMethod]
        public void Test33CheangeValidityAsrai()
        {
            Test01Login("Test33CheangeValidityAsrai");
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            string name = cc.changeValidity();
            fp.actionWork("ניהול כספים", "אמצעי תשלום");
            cc.checkValidity(name, DateTime.Today.AddYears(4).ToString("MM/yy"));
            fp.closeChrome();
            fp.OpenExcel("Test33CheangeValidityAsrai");
        }

        [TestMethod]
        public void Test34frindsTrackingesClose()
        {
            Test01Login("Test34frindsTrackingesClose");
            int numCreate = 25;
            f.openMakavFrind();
           
            fp.ButtonNew("מעקב ידיד", "", 0);
            cm.checkSailta("דוא\"ל", "שווה ל", p.MAIL + "TEXT");
            fp.click(By.XPath("//button/span/i[@class='icon-filter']"));
            Thread.sleep(500);
            f.insertElmentTacking(" שי שבועות ", "שי לשבועות מרוכז לכל התורמים", 0);
            numCreate = f.checkEndCreateTracking();
            fp.closePoup();
            f.checkTablenumberRow(numCreate-1, " שי שבועות ");
            fp.closeChrome();
            fp.OpenExcel("Test34frindsTrackingesClose");
        }[TestMethod]
        public void Test34frindsTrackingesNew()
        {
            Test01Login("Test34frindsTrackingesClose");
            f.openMakavFrind();
            fp.ButtonNew("מעקב ידיד", "", 0);
            cm.checkSailta("דוא\"ל", "שווה ל", p.MAIL + "TEXT");
            fp.click(By.XPath("//button/span/i[@class='icon-filter']"));
            Thread.sleep(500);
            f.insertElmentTacking("שי פורים", "מתנת פורים מרוכזת לכל התורמים", 0);
            f.checkEndCreateTracking();
            sec.pageNewCreate("ליצור מעקב ידיד נוסף");
            fp.OpenExcel("Test34frindsTrackingesClose");
        }[TestMethod]
        public void Test34frindsTrackingesNews()
        {
            Test01Login("Test34frindsTrackingesClose");
            f.openMakavFrind();
            fp.ButtonNew("מעקב ידיד", "", 0);
            cm.checkSailta("דוא\"ל", "שווה ל", p.MAIL + "TEXT");
            fp.click(By.XPath("//button/span/i[@class='icon-filter']"));
            Thread.sleep(500);
            f.insertElmentTacking("שי פורים", "מתנת פורים מרוכזת לכל התורמים", 0);
            f.checkEndCreateTracking();
            sec.pageNewCreate("מעקב ידיד נוספים");
            fp.OpenExcel("Test34frindsTrackingesClose");
        }
        [TestMethod]//Kא ניתן להרצה במחשבת כתיבת הקוד
        public void Test35DownloadExcel()
        {
            Test01Login("Test35downloadExcel");
           cnd.allDownloads();
            fp.closeChrome();
            fp.OpenExcel("Test35downloadExcel");
        }
        [TestMethod]
        public void Test35DownloadPDF()
        {
            Test01Login("Test35DownloadPDF");
            Thread.sleep(350);
            cnd.allDownloads(false);
            fp.closeChrome();
            fp.OpenExcel("Test35DownloadPDF");
        }
        [TestMethod]
        public void Test35DownloadMSB()
        { Test01Login("Test35DownloadMSB");
            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("מסב", m);
            ehq.clickGvia();
            Thread.sleep(800);
            CreateNewDenumic cnd = new CreateNewDenumic();
            if (cnd.CheckFileDownloaded("\\Downloads\\MSV_0962210") == false)
                throw new System.Exception("לא ירד");
            fp.closeChrome();
            fp.OpenExcel("Test35DownloadMSB");
        }
        [TestMethod]
        public void Test35Discounts()
        {
            Test01Login("Test35Discounts");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "אחים", "הנחה", "");
            fp.refres();
            fp.actionWork("ניהול כספים", "הגדרות הנחות וזיכויים");
            fp.ButtonNew("");
            Discounts d = new Discounts();
            d.createNewDiscount("הנחת תלמידים", " אחים (הנחה) ", "בית אברהם", " אוטומציה (חיוב) ");
            string[] fil = { "דוא\"ל", "שווה ל", "rvaitz22@gmail.comTEXT" };
            string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "שווה ל", "2TEXT", "scatter_plot", " שם פרטי ", "מתחיל ב", "אTEXT" };
            d.filter(fil, ben, 2);
            fp.ClickButton(" צור הגדרת הנחה חדשה ", "span");
            sec.checkEndCreate("הגדרת הנחה", false, "ה");
            sec.closePage();
            fp.OpenExcel("Test35Discounts");
        }[TestMethod]
        public void Test35DiscountsCard()
        {
            Test01Login("Test35Discounts");
            fl.createNewFinancicalSection(" מוסדות חינוך ","תלמידים","אחים","הנחה","");
            fp.refres();
            fp.actionWork("ניהול כספים", "הגדרות הנחות");
            fp.ButtonNew("");
            Discounts d = new Discounts();
            d.createNewDiscount("2הנחת תלמידים", " אחים (הנחה) ", "בית אברהם", " אוטומציה (חיוב) ");
            string[] fil = { "דוא\"ל", "שווה ל", "rvaitz22@gmail.comTEXT" };
            string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "שווה ל", "2TEXT", "scatter_plot", " שם פרטי ", "מתחיל ב", "אTEXT" };
            d.filter(fil, ben, 2);
            fp.ClickButton("צור הגדרת הנחה", "span");
            sec.checkEndCreate("הגדרת הנחה", false, "ה");
            sec.pageCard("הגדרת הנחה", "btnGoToObjectCreated");
            fp.OpenExcel("Test35Discounts");
        }[TestMethod]
        public void Test35DiscountsNew()
        {
            Test01Login("Test35Discounts");
           
            fp.actionWork("ניהול כספים", "הגדרות הנחות");
            fp.ButtonNew("");
            Discounts d = new Discounts();
            d.createNewDiscount("הנחת תלמידים1", " אחים (הנחה) ", "בית אברהם", " אוטומציה (חיוב) ");
            string[] fil = { "דוא\"ל", "שווה ל", "rvaitz22@gmail.comTEXT" };
            string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "שווה ל", "2TEXT", "scatter_plot", " שם פרטי ", "מתחיל ב", "אTEXT" };
            d.filter(fil, ben, 2);
            fp.ClickButton("צור הגדרת הנחה חדשה", "span");
            sec.checkEndCreate("הגדרת הנחה", false, "ה");
            sec.pageNewCreate(" הגדרת הנחה נוספת ");
            fp.OpenExcel("Test35Discounts");
        }
        [TestMethod]
        public void Test36DiscountParent()
        {
            Test01Login("Test36DiscountParent");
            fp.actionWork("ניהול כספים", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges("", "בית אברהם", "אחים", "-3");
            fp.closeChrome();
            fp.OpenExcel("Test36DiscountParent");
        }
        [TestMethod]//הכן יש באג
        public void Test36changeSumHoq()
        {
            Test01Login("Test36changeSumHoq");
            ExecutionHQ ehq = new ExecutionHQ();
           ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", DateTime.Today.ToString("dd"));
            string nameOfChange = ehq.editSum();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false");
          //  string nameOfChange = "ברים גד";
            vhp.checkChangeSum(nameOfChange);
            fp.OpenExcel("Test36changeSumHoq");
        }
        [TestMethod]//הולך להשתנות
        public void Test35MargeContacts()
        {
            Test01Login("Test35MargeContacts");
            //תלמידים
            PupilNew pn = new PupilNew();
            pn.openPupil();
            string IDname = pn.nameForMarge(0);
            int i = 0; string IDname2 = "";
            for (i = 0; i < 5; i++)
            { if (IDname == IDname2 || IDname2 == "") IDname2 = pn.nameForMarge(i);
                else
                    break;
            }
            fp.actionWork("אנשי קשר", "");
            fp.click(By.XPath("//i[text()='people_outline']"));
            f.Misug(IDname, IDname2, true);
            try { fp.click(By.CssSelector("button[aria-label='close']")); } catch { }
            Thread.sleep(500);
            fp.refres();
            pn.openPupil();
            if (IDname != pn.nameForMarge(0) || IDname2 == pn.nameForMarge(i))
            {
                throw new System.Exception("not marge good");
            }
            fp.closeChrome();
            fp.OpenExcel("Test35MargeContacts");
        }
        [TestMethod]
        public void Test36filterCharges()
        {
            Test01Login("Test36filterCharges");
            int numH = f.stepLiabilities();
            fp.ClickButton(" ליצור חיוב נוסף ", "span");
            f.insertCharges(DateTime.Today.ToString("yyyyMM"), DateTime.Today.AddMonths(-1).ToString("dd/MM/yyyy"));
            fp.ClickButton(" ליצור חיוב נוסף ", "span");
            f.insertCharges(DateTime.Today.ToString("yyyyMM"), DateTime.Today.AddMonths(1).ToString("dd/MM/yyyy"));
            fp.ClickButton(" ליצור חיוב נוסף ", "span");
            f.insertCharges(DateTime.Today.ToString("yyyyMM"), DateTime.Today.AddYears(-1).ToString("dd/MM/yyyy"));
            fp.closePoup(1);
            fp.refres();
            f.openParent();
            Thread.sleep(200);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom(numH);
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(10, "חיובים");
            f.checkFilter();
            fp.closeChrome();
            fp.OpenExcel("Test36filterCharges");
        }
        [TestMethod]
        public void Test36createDiscountParents()
        {
            Test01Login("Test36createDiscountParents");
            PupilNew pn = new PupilNew();
             pn.openPupil();
            Thread.sleep(350);
             pn.clickTableRandom(-1, 1);
             popupList pl = new popupList();
             pl.clickStepHeder(6, "הגדרות הנחות");
             fp.ClickButton("הוספת הגדרת הנחה");
             Discounts d = new Discounts();
             d.createNewDiscount("הנחת תלמידים הורים", " אחים (הנחה) ", "בית אברהם", " אוטומציה (חיוב) ");
             string[] fil = { "דוא\"ל", "שווה ל", "rvaitz22@gmail.comTEXT" };
             string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "שווה ל", "2TEXT", "scatter_plot" };
             d.filter(fil, ben, 0, 3);
             fp.ClickButton("צור הגדרת הנחה", "span");
             string name = f.nameRow();
             fp.refres();
            fp.actionWork("ניהול כספים", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges(name, "בית אברהם", "", "-1");
            fp.closeChrome();
            fp.OpenExcel("Test36createDiscountParents");
        }
        [TestMethod]
        public void Test36CheckGoParent()
        {
            Test01Login("Test36CheckGoParent");
            ExecutionHQ ehq = new ExecutionHQ();
            //מיותר
           /* string m = fp.dateD();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", m);
            cnd.namePopupParent("mat-row", "mat-cell");
            fp.actionWork("ניהול כספים", "צפייה וטיפול בתשלומים");
            fp.selectForRequerd("סטטוס", false, 0);
            cnd.namePopupParent("tr", "td", 0);*/
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות ");
            cnd.namePopupParent("mat-row", "mat-cell", 34, 0);
            fp.closeChrome();
            fp.OpenExcel("Test36CheckGoParent");
        }
        [TestMethod]//אני באמצע
        public void Test37documentDesigned()
        {
            Test01Login("Test37documentDesigned");
            fp.actionWork("מסמכים","");
            DocumentDesigned dd = new DocumentDesigned();
            dd.newDocumint("ספקים", "grading");
            fp.refres();
            dobut d = new dobut();
            d.openDoubt();
            Thread.sleep(250);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            popupList pl = new popupList();
            pl.popupEdit();
            pl.clickStepHeder(4,"אישורים");
            dd.checkKiomDocuments("ספקים", "grading");
            fp.closeChrome();
            fp.OpenExcel("Test37documentDesigned");
        }
        [TestMethod]//באג קריטי
        public void Test37FilterBen()
        {
            Test01Login("Test37FilterBen");
             f.openParent();
             string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "גדול מ", "1TEXT", "scatter_plot", " שם פרטי ", "מתחיל ב", "אTEXT" };
             cm.stepFilterBen(ben);
             f.openFrind();
             string[] benFrind = { " תרומות ", "מזהה תורם", "גדול מ", "0TEXT", "functions", " סכום ", "גדול מ", "1TEXT" };
             cm.stepFilterBen(benFrind);
             fp.ClickButton("לא");
                fp.actionWork("אנשי קשר", "");
                string[] benContacts = {" הוצאות ","סכום","שווה ל","5TEXT", "arrow_downward", "אסמכתא","אינו מכיל את","יTEXT" };
            cm.stepFilterBen(benContacts);
            fp.actionWork("הגדרות", "מוסדות");
            string[] benInstitutes = { " מוסדות ","קוד מוסד","גדול או שווה ל","1TEXT", "scatter_plot","שם מוסד","שווה ל","בית אברהםTEXT" };
            cm.stepFilterBen(benInstitutes);
            dobut d = new dobut();
            d.openDoubt();
            string[] benDoubt = { " קבלות שירות ","מזהה","גדול מ","1TEXT", "scatter_plot", "סכום לתשלום","קטן או שווה ל","70TEXT"};
            cm.stepFilterBen(benDoubt);
            fp.closeChrome();
            fp.OpenExcel("Test37FilterBen");
        }
        [TestMethod]
        public void test38checkPeleCard()
        {
            Test01Login("test38checkPeleCard");
            ce.openClearingEninty();
            //("אשראי", " פלאקארד ", );
            string[] asrai = { "","","אשראי","-1"};
            fp.checkColumn("//",asrai,"","mat-row","mat-cell",-1,0,true);
             popupList pl = new popupList();
            pl.clickEdit();  
            fp.insertElment("SenderCode", By.Name("SenderCode"), "TEST PS");
                fp.insertElment("name", By.Name("Name"),  "לבדיקה");
            fp.ClickButton("שמירה","span");
            fp.closepopup();
            
            fp.refres();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("כרטיס אשראי", "Credit",false,-1,"",1,"אשראילבדיקה");
            fp.ClickButton("קח אותי לכרטיס תרומה","span");
            Thread.sleep(450);
            fp.ClickButton("שדר תשלום");
            Thread.sleep(250);
            fp.closeChrome();
            Pelecard pd = new Pelecard();
            pd.login();
            fp.closeChrome();
            fp.OpenExcel("test38checkPeleCard");
        }

        [TestMethod]
        public void Test39OffsetOtumation()
        {
            Test01Login("Test39OffsetOtumation");
            Frind f = new Frind();
            int numHora = f.stepLiabilities(1);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom(numHora,1);
            popupList pl = new popupList();
            pl.clickStepHeder(2, "תשלומים");
            fp.ClickButton(" הוספת תשלום ", "span");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", "false", "הוראות קבע ראשי","","","","Remarks","5","כן");
            fp.refres();
            pn.openPupil();
            pn.clickTableRandom(numHora,1);
            pl.clickStepHeder(2, "תשלומים");
            fp.click(By.XPath("//i[text()=' pageview ']"));
            fp.ClickButton("שדר תשלום ", "span");
                fp.refres();
                fp.actionWork(" ניהול כספים ", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges("", "בית אברהם");
            fp.refres();
            pn.openPupil();
            pn.clickTableRandom(numHora,1);
            pl.clickStepHeder(3, "חיובים");
            string[] offOC = { "","","1","שקל","","1",DateTime.Today.ToString("dd/MM/yyyy"),"הושלם","פעיל","-1"};
            fp.checkColumn("//",offOC,"false", "mat-row", "mat-cell");
            fp.closeChrome();
            fp.OpenExcel("Test39OffsetOtumation");
        }
        [TestMethod]
        public void Test39sendMailContacts()
        {
            Test01Login("Test39sendMailContacts");
            Frind f = new Frind();
            f.MailContacts();
            Thread.sleep(500);
            fp.closeChrome();
            fp.OpenExcel("Test39sendMailContacts");
        }
        [TestMethod]
        public void Test91LoadsConatarbition()
        {
            Test01Login("Test91LoadsConatarbition");

            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
           for (int i = 0; i < 24; i++)
            {
                cc.createNew("מזומן", "Cash");
                Thread.sleep(250);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }for (int i = 0; i < 24; i++)
            {
                cc.createNew("כרטיס אשראי", "Credit");
                Thread.sleep(350);
                    fp.ClickButton("ליצור תרומה נוספת", "span");
            }for (int i = 0; i < 24; i++)
            {
                cc.createNew("צק", "Check");
                Thread.sleep(250);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }for (int i = 0; i < 24; i++)
            {
                cc.createNew("העברה בנקאית", "BankDeposit");
                Thread.sleep(250);
                    fp.ClickButton("ליצור תרומה נוספת", "span");
            }
            fp.closeChrome();
            fp.OpenExcel("Test91LoadsConatarbition");
        }
        [TestMethod]
        public void Test91Loadspayments()
        {
            Test01Login("Test91Loadspayments");

            
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            for (int i = 0; i < 24; i++)
             {
                 cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", "", "כללי", "", "Credit");
                 Thread.sleep(200);
                 fp.ClickButton(" ליצור תשלום נוסף ", "span");
             }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "מזומן", "", "כללי", "", "Cash", "רוזנברגר");
                Thread.sleep(300);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "צק", "", "כללי", "", "Check", "אוטומציה");
                Thread.sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", "", "כללי", "", "BankDeposit");
                Thread.sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "כללי", "", "divPaymentMethodTypesCredit", "אוטומציה", "false","");
                Thread.sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "מס\"ב", "", "כללי", "", "divPaymentMethodTypesMasav", "", "false","");
                Thread.sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadPrintKabla()
        { 
            CreateProductionReceipt("אשראי", " ",1);
            
        }
        [TestMethod]
        public void Test91LoadDDebit()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openDebit();
            for (int i = 0; i < 50; i++)
            {
                cc.stepDebit();
                Thread.sleep(250);
                fp.ClickButton("ליצור הו\"ק נוספת", "span");
            }
            for(int i=0;i<50;i++)
            {
                cc.stepDMsab();
                Thread.sleep(250);
                fp.ClickButton("ליצור הו\"ק נוספת", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadConatarbitionDDebit()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            for (int i = 0; i < 50; i++)
            {
                Thread.sleep(250);
                cc.stepConatarbitionHOK("כרטיס אשראי","divPaymentMethodTypesCredit","אוטומציה", "Remarks");
                Thread.sleep(250);
                Console.WriteLine(i);
                fp.ClickButton("ליצור תרומה בהו\"ק נוספת", "span");
            }
            for (int i = 0; i < 50; i++)
            {
                cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav", "אוטומציה", "Remarks");
                Thread.sleep(250);
                fp.ClickButton("ליצור תרומה בהו\"ק נוספת", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]//באג
        public void Test91LoadDebitPayments()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            for (int i = 0; i < 25; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "אוטומציה", "", "divPaymentMethodTypesCredit", "אוטומציה", "false","");
                Thread.sleep(250);
                fp.ClickButton("ליצור תשלום נוסף", "span");
            }
            for (int i = 0; i < 25; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "מס\"ב", p.idIdid, p.automation, "", "divPaymentMethodTypesMasav", "", "false");
                Thread.sleep(250);
                fp.ClickButton("ליצור תשלום נוסף", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadExecutionHQ()
        {
            Test01Login("Test13ExecutionHQ");
           
            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", m);
            ehq.clickGvia();
            fp.spinner();
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadPrintRecipet()
        {
            Test01Login("Test91LoadPrintRecipet");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            fp.click(By.TagName("mat-checkbox"));
            fp.ClickButton(" הפק קבלות ");
            fp.refres();
            hhrq.insertpageHhrq();
            hhrq.checkPage("אשראי");
            fp.click(By.TagName("mat-checkbox"));
            fp.ClickButton(" הפק קבלות ");

        }
        // [TestMethod]
        public void check()
        { }
        //public void Test5Create
        // [TestMethod]
        public void TestcreatePromotional()
        {
            Test01Login();
           
            CreditClause cc = new CreditClause();
            cc.stepClauses(" תלמידים ", " מוסדות חינוך ", "זיכוי לטיול שבוטל");
        }
     //   [TestMethod]
        public void TestCheck()
        {
          
            // Test01Login();
            //Console.WriteLine(p.creaditcard.Substring(p.creaditcard.Length - 4,4 ));
            // fp.picter("bla");
            /*  int year = -5;
              year += -3;*/
            int input = 123845;
            var digits = input.ToString().Select(x => int.Parse(x.ToString()));
            //Console.WriteLine(digits.FirstOrDefault());
          //  Console.WriteLine(digits.ElementAt(2));
            // i = i. j;
             HebDates hd = new HebDates();
             DateTime d = System.Convert.ToDateTime("05/09/2021");// driver.FindElement(By.XPath("//input[contains(text(),'/')]")).Text);
             string dateEbrue = hd.FullDate(d);
             Console.WriteLine(dateEbrue);
            // string[] nm = { "kl", "jkh" ,"lklk"};


            //char[] tav = { '0' };
            //Console.WriteLine(Int16.Parse(DateTime.Today.AddMonths(1).ToString("MM"))>10);
            // login ln = new login();
            //   ln.openChrome();
            /* ExcelApiTest eat = new ExcelApiTest("", "automation");
             string[] valueExcel0 = { "", "name", "typeFiled", "permision", "DesplaySectionEdit", "DesplayCreateSection", "type", "insertChangeTable" };
             eat.saveExcel("automation", valueExcel0, 1);
             string[] valueExcel = { "", "אביזרי מחשב", "עכבר", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "mouse,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel, 2);
             Thread.sleep(300);
             string[] valueExcel1 = { "", "אביזרי מחשב", "מקלדת", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "keyBord,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel1, 3);
             Thread.sleep(300);
             string[] valueExcel2 = { "", "אביזרי מחשב", "מסך", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "computers,תיבת סימון,false,false,false", "true,,,,,," };
             eat.saveExcel("automation", valueExcel2, 4);
             Thread.sleep(300);
             string[] valueExcel3 = { "", "אביזרי מחשב", "כונן", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "conan,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel3, 5);
             Thread.sleep(300);
             string[] valueExcel4 = { "", "איבזור כיתה", "כסאות", "", "true", "true", "chairs,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel4, 6);
             Thread.sleep(300);
             string[] valueExcel5 = { "", "איבזור כיתה", "שולחנות", "", "true", "true", "tabels,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel5, 7);
             string[] valueExcel6 = { "", "איבזור כיתה", "ארונות", "", "true", "true", "loker,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel6, 8);
             Thread.sleep(300);
             string[] valueExcel7 = { "", "איבזור כיתה", "לוח", "", "true", "true", "board,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel7, 9);*/
            // Console.WriteLine(eat.createExcel("automation", "", false, 5, i););


            // ExcelApiTest el = new ExcelApiTest("", "nisoy");
            //el.bugReturn("nisoy", "warning", "nisyonot", "אני עכשיו מבצעת ניסוי",true);
            // el.bugReturn("nisoy", "Failed", "nisyonot", "אני עכשיו מבצעת ניסוי",false);
            // el.bugReturn("nisoy", "warning", "nisyonot", "אני עכשיו מבצעת ניסוי",true);

           // DateTime moment = DateTime.Today.AddDays(1);
            //Console.WriteLine(moment.ToString("dd-MM-yyyy"));
            //  Console.WriteLine(DateTime.Today.ToString("dd-MM-yyyy"));
            // Console.WriteLine(moment.ToString("MM/yy"));
            //Console.WriteLine(DateTimeOffset.Now);
            //Console.WriteLine(DateTimePickerFormat.Custom);

            // Console.WriteLine(moment.Day+"/"+("0"+moment.Month)+"/"+moment.Year);
        }



    }
}
