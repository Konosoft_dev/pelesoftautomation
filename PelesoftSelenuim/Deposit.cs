﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class Deposit:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();

        public void dipositType(string typeDeposit)
        {
            fp.actionWork(niaolCase, "הפקדת " + typeDeposit);
        }
        public void diposetCash(string namyTypeDiposit,string stringcheck=null)
        {
            int balancint=0;
            fp.checkElmentText("diposet Cash", By.ClassName("main-txt"), "הפקדת "+ namyTypeDiposit);
            if (namyTypeDiposit == "מזומן")
            {
                string balanc = driver.FindElement(By.ClassName("balance")).Text;
                 balancint = fp.splitNumber(balanc);
                Console.WriteLine(balanc + "" + balancint);
               
            }
            By typeCash = By.ClassName("deposit-cash");
            fp.checkElment("typeCash", typeCash);
            // List<> listTCash = listTCash["שקל","דולר"]
            driver.FindElement(typeCash).Click();
            fp.selectText("שקל");
            IWebElement sumInsert = driver.FindElement(By.XPath("//input[@formcontrolname='sum']"));
            sumInsert.SendKeys("0.5");
            // Console.WriteLine(sumInsert.Text);
             double sumDeposit = 0.5;// Int32.Parse(sumInsert.Text);
            Console.WriteLine(sumDeposit);
            //fp.checkElmentText("account", By.XPath("//mat-select[@formcontrolname='account']"), "חשבון:");
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='account']")).Click();
            fp.waitUntil(By.ClassName("mat-option-text"));
            Thread.Sleep(200);
            driver.FindElements(By.ClassName("mat-option-text"))[0].Click();
            /*if (namyTypeDiposit=="צ\'קים")
                tableclickCheck(stringcheck, "checks-table-wrapper ng-star-inserted");*/
            fp.ClickButton("הפקדה ");
            if (namyTypeDiposit == "מזומן")
            { checkBalanc(balancint, sumDeposit); }
           // fp.closeChrome();
        }

        public void tableclickCheck(string []stringcheck)
        {
            // try
            // {
            fp.checkElmentText("account", By.XPath("//mat-select[@formcontrolname='account']"), "חשבון:");
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='account']")).Click();
            driver.FindElements(By.ClassName("mat-option-text"))[0].Click();
            string[] heder = { "סמן","מספר צ\'ק ","מספר בנק"," מספר סניף","מספר חשבון ","תאריך","סכום צ\'ק","מטבע","סטטוס","מקור צ\'ק" };
            fp.checkTableHeder(heder, "thead[role='rowgroup']", "tr", "th");
            fp.checkColumn("tbody[role='rowgroup']", stringcheck, "mat-checkbox","tr[role='row']", "td[role='gridcell']",0);
            //fp.checkTable(stringcheck, "סמן מספר צ\'ק מספר בנק מספר סניף מספר חשבון תאריך סכום צ\'ק מטבע סטטוס מקור צ\'ק ",byClass);
          //  }
         //   catch { throw new NotImplementedException("click on table check deposit invaled"); }
        }

        public void checkBalanc(double balanc,double sumDeposit)
        {
            try
            {
                string blanceNew= driver.FindElement(By.ClassName("balance")).Text;
                double blanceNewInt = fp.splitNumber(blanceNew);
                if (blanceNewInt != (balanc + sumDeposit))
                    Debug.WriteLine("the blance invalid , the balanc:" + blanceNewInt+"should be" +balanc + sumDeposit);
            }
            catch { throw new NotImplementedException(); }
        }
    }
}
