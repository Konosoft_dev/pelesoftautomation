﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class ExecutionHQ:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void insertExecutionHQ()
        {
            fp.actionWork(" ניהול כספים ","ביצוע הו\"ק");
        }
        public void checkelmentExecutionHQ(string mosad,string dayDebit= "2")
        {
            if (driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()='תצוגת נתונים לגביה ']")).Enabled)
                Debug.WriteLine("the button enabled"); 
            if (driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()='שליחה לגביה ']")).Enabled)
                Debug.WriteLine("the button enabled");
            fp.checkElment("page ExecutionHQ", By.TagName("app-constant-debits-execution"));
            fp.mosadSlika(mosad);
            MonthObligation();
            dayObligation(dayDebit);
            //startEnd(dayDebit);
            fp.ClickButton("תצוגת נתונים לגביה ", "span");
        }

        private void startEnd(string end)
        {
            try { fp.SelectRequerdName("formcontrolname='FromDebitDay","1",false);
                fp.click(By.CssSelector("mat-select[formcontrolname='DebitDay']"));
               // int coun = driver.FindElements(By.TagName("mat-option")).Count -1;
                fp.selectText(end);
            } catch { throw new Exception(); }
        }

        public void clickGvia()
        {
            fp.ClickButton("שליחה לגביה ", "span");
            //הפעולה בוצע בהצלחה
        }
        public void dayObligation(string dayDebit)
        {
            try
            {
                fp.checkElment("dateobligation", By.XPath("//mat-select[@formcontrolname='DebitDay']"));
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='DebitDay']")).Click();
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='DebitDay']")).SendKeys(Keys.Tab);
               // fp.funError(3);
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='DebitDay']")).Click();
                int cou = driver.FindElements(By.ClassName("mat-option-text")).Count;
                fp.selectText(dayDebit,false,cou-1);
                //driver.FindElements(By.ClassName("mat-option-text"))[0].Click();
            }
            catch { throw new NotImplementedException("dayObligation invaled"); }
            
        }

        public void MonthObligation()
        {
            try
            {
                string d1;
                fp.checkElment("dateobligation", By.XPath("//mat-select[@formcontrolname='DebitMonth']"));
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='DebitMonth']")).Click();
                DateTime dd = DateTime.Today;
               if(Int16.Parse( dd.ToString("MM"))<10)
                 d1 = dd.ToString("M/yyyy");
               else
                    d1 = dd.ToString("MM/yyyy");

                fp.selectTextNF(" "+d1+" ");

                //driver.FindElements(By.ClassName("mat-option-text"))[0].Click();

            }
            catch { throw new NotImplementedException("dateobligation invaled"); }
        }

        public void checkTable(string[] listRow)
        {
            try
            {
                string[] listHeder = {"שם","כתובת","תאריך לגביה","סעיף פיננסי","מוסד סליקה","סכום חיוב","מטבע","מספר תשלומים","סטטוס" };
                fp.checkTableHeder(listHeder,"//thead[@role='rowgroup']","mat-header-row", "mat-header-cell");
                ViewingHandalingPayment vhp = new ViewingHandalingPayment();
                    vhp.clickTable( listRow, "", "mat-row", "mat-cell");
            }
            catch { throw new NotImplementedException("check table execution HQ invaled"); }
        }

        public  string editSum()
        {
            string nameOfChange = "";
            try { nameOfChange = driver.FindElements(By.TagName("mat-cell"))[1].Text;
                try { driver.FindElements(By.TagName("mat-cell"))[10].FindElement(By.TagName("button")).Click(); } catch { driver.FindElements(By.TagName("mat-cell"))[9].FindElement(By.TagName("button")).Click(); }
                fp.checkElmentText("title popup",By.ClassName("title"), "עדכון סכום לגביה");
                int cou = driver.FindElements(By.TagName("input")).Count - 1;
                driver.FindElements(By.TagName("input"))[cou].Clear();
                driver.FindElements(By.TagName("input"))[cou].SendKeys("18");

                fp.selectForRequerd(" סכום גביה ",false,-1,"18");
                fp.ClickButton("שמירה","span");
                Thread.Sleep(550);
                if (!driver.FindElements(By.TagName("mat-cell"))[6].Text.Contains("18")&& !driver.FindElements(By.TagName("mat-cell"))[5].Text.Contains("18"))
                        throw new Exception();
                fp.ClickButton("שליחה לגביה ", "span");
            } catch(Exception ex) { fp.picter("edit sum"); throw new NotImplementedException(""+ex); }
            return nameOfChange;
        }
    }
}
