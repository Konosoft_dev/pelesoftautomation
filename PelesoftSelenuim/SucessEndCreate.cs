﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class SucessEndCreate:page
    {
        //static IWebDriver driver;// = new ChromeDriver();

        FunctionPelesoft fp = new FunctionPelesoft();
        CreateNewDenumic cnd = new CreateNewDenumic();
        public void checkEndCreate(string nameCreate, bool Mosad = true,string typeMain="",string addcard="")
        {
            try
            {
                fp.checkElment("page create frind", By.TagName("mat-dialog-container"));
               fp.checkElment("logo sucess", By.ClassName("creation-image"));//"התרומה בהו\"ק נוצרה בהצלחה!"
                if ( fp.checkElmentText("title create sucess", By.CssSelector("div[class='main-title ng-star-inserted']"),"ה" + nameCreate + " נוצר"+typeMain+" בהצלחה!")==1)
                    throw new NotImplementedException(" page sucess create not good");

                fp.checkElment("sub title sucess", By.XPath("//div[@class='sub-title'][text()=' מה תרצ/י לעשות הלאה']"));
                fp.checkElment("close", By.XPath("//button[@aria-label='edit']"));
                chekcElmentDisplay(" קח אותי לכרטיס " + nameCreate );
                if (typeMain!="")
                    chekcElmentDisplay(" ליצור " + nameCreate + " נוספת ");
                else
                    chekcElmentDisplay(" ליצור " + nameCreate + " נוסף ");
                if (Mosad == true)
                    chekcElmentDisplay("קח אותי לכרטיס קופה ");
                  if (addcard != "")
                {
                    if(addcard.Contains("PDF"))
                        chekcElmentDisplay(addcard );
                   else
                        chekcElmentDisplay(" קח אותי לכרטיס " + addcard );

                }

            }
            catch
            {
                throw new NotImplementedException(" page sucess create not good");
            }
        }
        public void goToCard(string nameCard)
        {
            By @by = By.ClassName("mat-button-wrapper");
            //nameCard =  + nameCard+ " ";
            for (int i=0;i<driver.FindElements(@by).Count;i++)
            {
                if(driver.FindElements(@by)[i].Text.Trim().Contains(" קח אותי לכרטיס " + nameCard))
                {
                    driver.FindElements(@by)[i].Click();
                    break;
                }
            }
           // driver.FindElement(By.XPath("//span[@class=''][text()=' קח אותי לכרטיס  " + nameCard + " ']")).Click();

        } 
        public void chekcElmentDisplay(string nameCard)
        {
            By @by = By.ClassName("mat-button-wrapper");
            for (int i=0;i<driver.FindElements(@by).Count;i++)
            {
                if(driver.FindElements(@by)[i].Text.Trim().Contains(nameCard.Trim()))
                    break;
                if(i+1== driver.FindElements(@by).Count)
                    throw new NotImplementedException("the button not display");

            }
            // driver.FindElement(By.XPath("//span[@class=''][text()=' קח אותי לכרטיס  " + nameCard + " ']")).Click();

        }
        public void pageCard(string NameCard,string ariaCard= "btnGoToParent")
        {
            try
            {
                if(driver.FindElement(By.CssSelector("div[class='main-title ng-star-inserted']")).Text.Contains(NameCard))
                    fp.click(By.CssSelector("button[aria-label='btnGoToObjectCreated']"));
                else if(ariaCard!="")
                    fp.click(By.CssSelector("button[aria-label='"+ariaCard+"']"));
                else
                    goToCard(NameCard);
                fp.checkElment("page card Entity", By.ClassName("app-dynamic-form"));
                fp.checkElment("button close", By.XPath("//button[@aria-label='edit']"));//XPath("//i[contains(text(),' close ')]"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                // driver.FindElement(By.XPath("//button[@aria-label='edit']")).Click();
                fp.closeChrome();
            }
            catch
            {
                throw new NotImplementedException("page card not good");
            }
        }

        public void pageNewCreate(string NameNew,int popup=-1)
        {
            try
            {
                fp.ClickButton(" ליצור "+NameNew);
               // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' ליצור " + NameNew + " ']")).Click();
                cnd.onInit();
                Thread.Sleep(100);
                fp.closePoup(popup);
                // driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();
                fp.closeChrome();
            }
            catch { throw new NotImplementedException("page new create"); }
        }
        public void closePage(int popup=-1)
        {
            Thread.Sleep(300);
            fp.closePoup(popup);
            //driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();
            driver.Close();
        }

        public  void startDesign()
        {
            fp.ClickButton(" התחל עיצוב מסמך PDF ","span");
            fp.waitUntil(By.TagName("app-pdf-tool-box"));
            driver.FindElement(By.XPath("//app-wrapper-dialog/div/button[@aria-label='edit']")).Click();
            //closePage();
           // throw new NotImplementedException();
        }
    }
}
