﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace PelesoftSelenuim
{
     class HandlingHQReceipts:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();

        public void insertpageHhrq()
        {
            try
            {
                fp.actionWork(" ניהול כספים ","טיפול בקבלות עבור הו\"ק");
                java.lang.Thread.sleep(150);
            }
            catch { throw new NotImplementedException("insert page Hhrq invaled"); }
        }

        public void checkPage(string typeSelect)
        {
            try
            {
                Production_receipt pr = new Production_receipt();

                pr.checkPageProductionReceipt("עבור הו\"ק", typeSelect);
            }
            catch { throw new NotImplementedException("check Page invaled"); }
        }
        public void checkSlika(string typeSelect)
        {
            try
            {
                fp.selectForRequerd("מוסד לסליקה", false, -1);
               /* fp.checkElmentText("mosad slika", By.XPath("//mat-select[@aria-label='clearing']"), "מוסד לסליקה");
                driver.FindElement(By.XPath("//mat-select[@aria-label='clearing']")).Click();
               */ fp.selectText(typeSelect,true);
                fp.selectForRequerd("בחר שנה", false, -1);
                /*
                fp.checkElmentText("selectYear", By.XPath("//mat-select[@aria-label='selectYear']"), );
                driver.FindElement(By.XPath("//mat-select[@aria-label='selectYear']")).Click();
                */
                string yearselect = DateTime.Today.ToString("yyyy");
                fp.selectText(""+yearselect+"");

            }
            catch { throw new NotImplementedException("check slika invaed"); }
        }
        public void checkTable(string []listRow,int m=1)
        {
            string[] listHeder = { "","סוג","מזהה","שם","סוג א.תשלום","פרטי א.תשלום","מתאריך","עד תאריך","סה\"כ סכום","סה\"כ חיובים"};
            fp.checkTableHeder(listHeder, "//thead[@role='rowgroup']");
            if (listRow[3].Contains("false"))
                listRow[3] = "";
            IWebElement tableElement = driver.FindElement(By.XPath("//tbody[@role='rowgroup']"));
            //IWebElement tableElement = driver.FindElement(By.TagName("tbody"));

            //IList<IWebElement> tableRow = tableElement.FindElements(By.XPath("//tr[@role='row']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.CssSelector("tr[role='row']"));
            //IList<IWebElement> rowTD;
            for (int i=m; i < tableRow.Count; i++)
            {
                Console.WriteLine(tableRow[i].Text);
                IList<IWebElement> rowTD = tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"));
                for (int j = 1; j <= rowTD.Count - 1; j++)
                {

                    // Console.WriteLine(rowTD.Count);
                    //  Console.WriteLine(listRow[j]);
                    if (/*j + 1 == rowTD.Count - 2*/ listRow[j+1] == "-1")
                    {
                        if (m == 1)
                            tableRow[i].FindElement(By.TagName("mat-checkbox")).Click();
                        else
                            tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[0].Click();
                             //tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[0].FindElement(By.XPath("//input[@aria-label='select row NaN']")).Click();
                            // tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[j].FindElement(By.TagName("mat-checkbox")).Click();

                        f = i;
                        i = -1;
                        ///  Console.WriteLine(tableRow[i].Text);
                        break;
                    }
                    if (listRow[j] != "")
                        if(tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[j].Text.Trim() != listRow[j].Trim()  && listRow[j] != "-1")
                    {
                        Console.WriteLine(tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[j].Text);
                        break;

                    }
                    /*  if (rowTD[j].Text == listRow[j])
                          Console.WriteLine(rowTD[j].Text, listRow[j]);*/
                }
                if (i == -1)
                    break;
                if (i + 1 == tableRow.Count - 1)
                    throw new NotImplementedException("the row not display :" + listRow[0]);
            

            }
            //ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            //fp.checkColumn("tbody[role='rowgroup']",listRow, "//mat-checkbox[@ng-reflect-aria-label='select row NaN']","tr[role='row']", "td[role='gridcell']");
        }
        
        
    }
}