﻿using com.sun.xml.@internal.ws.server;
using Microsoft.TeamFoundation.WorkItemTracking.Process.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class AddTable : page
    {

        FunctionPelesoft fp = new FunctionPelesoft();
        public void insertdataExcel()
        {
            ExcelApiTest eat = new ExcelApiTest("", "automation");
             string[] valueExcel0 = { "", "name", "typeFiled", "permision", "DesplaySectionEdit", "DesplayCreateSection", "type", "insertChangeTable" };
             eat.saveExcel("automation", valueExcel0, 1);
             string[] valueExcel = { "", "אביזרי מחשב", "עכבר", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "mouse,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel, 2);
             Thread.Sleep(300);
             string[] valueExcel1 = { "", "אביזרי מחשב", "מקלדת", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "keyBord,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel1, 3);
             Thread.Sleep(300);
             string[] valueExcel2 = { "", "אביזרי מחשב", "מסך", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "computers,תיבת סימון,false,false,false", "true,,,,,," };
             eat.saveExcel("automation", valueExcel2, 4);
             Thread.Sleep(300);
             string[] valueExcel3 = { "", "אביזרי מחשב", "כונן", "מנהל מערכת,false,מנהל חינוכי,false", "false", "true", "conan,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel3, 5);
             Thread.Sleep(300);
             string[] valueExcel4 = { "", "איבזור כיתה", "כסאות", "", "true", "true", "chairs,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel4, 6);
             Thread.Sleep(300);
             string[] valueExcel5 = { "", "איבזור כיתה", "שולחנות", "", "true", "true", "tabels,מספר,מספר שלם,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel5, 7);
             string[] valueExcel6 = { "", "איבזור כיתה", "ארונות", "", "true", "true", "loker,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel6, 8);
             Thread.Sleep(300);
             string[] valueExcel7 = { "", "איבזור כיתה", "לוח", "", "true", "true", "board,תיבת סימון,false,false,false", ",,,,,," };
             eat.saveExcel("automation", valueExcel7, 9);
        }
        public void checkElment()
        {
            try
            {
                fp.checkElmentText("check title", By.ClassName("main-txt"), "הוספת טבלה");
                fp.checkElmentText("check displayName", By.Name("displayName"), "שם לטבלה");
                fp.checkElmentText("check name singale", By.Name("singleName"), "שם לפריט בטבלה (לש'יחיד)");
                fp.checkElment("check title radio button", By.XPath("//div/mat-label[text()='המין לפריט בטבלה:']"));
                fp.checkElmentsText("check radio button", By.TagName("mat-radio-button"), "זכר", 0);
                fp.checkElmentsText("check radio button", By.TagName("mat-radio-button"), "נקבה", 1);
                fp.checkElmentText("check TableName", By.Name("TableName"), "שם לטבלה בבסיס הנתונים");
                fp.checkElmentText("check table parent", By.XPath("//mat-select[@formcontrolname='ParentTableId']"), "טבלת אב");
                fp.checkElmentText("check Model", By.XPath("//mat-select[@formcontrolname='ModuleID']"), "מודול");
                fp.checkElmentText("check button save", By.ClassName("mat-button-wrapper"), "שמירה");
            }
            catch { throw new NotImplementedException("invaled check elment"); }
        }
        public void insertElment()
        {
            fp.insertElment("check displayName", By.Name("displayName"), "kckc");
            IWebElement elem1 = driver.FindElement(By.CssSelector("input:required"));
            fp.insertElment("check name singale", By.Name("singleName"), "ccc");
            driver.FindElements(By.TagName("mat-radio-button"))[0].Click();
            fp.insertElment("check TableName", By.Name("TableName"), "automann");
            //fp.SelectRequerdName("formcontrolname='ParentTableId", nameFrind+"ים",false);
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='ModuleID']")).Click();
            fp.wait();
            driver.FindElement(By.XPath("//span[text()=' תלמידים ']")).Click();
            Thread.Sleep(300);

            fp.ClickButton("שמירה");
            Thread.Sleep(1300);
            // errorSave();
        }

        public void errorSave()
        {


            try
            {
                while (driver.FindElement(By.XPath("//div[@class='main-txt'][text()='הוספת טבלה']")).Displayed)
                {

                    if (driver.FindElement(By.XPath("//div[@class='wrap-action-name'][text()='הוספת טבלה']")).Displayed == true)
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(25));
                        wait.Until(drv => drv.FindElement(By.XPath("//div[@class='wrap-action-name'][text()='הוספת טבלה']")).Displayed);
                        fp.ClickButton("שמירה");
                    }

                }
            }
            catch { }
            //throw new NotImplementedException();
        }

        public void openTable(string nameTable= " תלמידים ", string nameTTTable= "kckc",int num=0)
        {
            fp.clickNameDefinitions(nameTable);
            fp.clickButoonToContains(nameTTTable,num);
        }
        public void OpenSettings()
        {
            WebDriverWait customWait = new WebDriverWait(driver, TimeSpan.FromSeconds(2005));
            customWait.Until(driver => driver.FindElement(By.ClassName("button-settings")));
            Thread.Sleep(2080);
            driver.FindElement(By.ClassName("icon-settings")).Click();
            fp.checkElment("page popup setting", By.TagName("mat-dialog-container"));
        }

        public void checkESettingTable()
        {
            fp.checkElmentsText("check setting table create", By.ClassName("mat-tab-label-content"), "הגדרות טבלת " + automation + "", 0);
            fp.checkElmentsText("check setting table create", By.ClassName("mat-tab-label-content"), "הגדרות טופס " + automation + "", 0);
            fp.checkElment("check logo setting", By.ClassName("wrap-icon"));
            fp.checkElmentText("check logo setting", By.ClassName("select-settings-title"), "מה תרצה להגדיר?");
            fp.checkElmentsText("check type setting", By.ClassName("select-settings-action"), "הגדרות טבלת " + automation, 0);
            fp.checkElmentsText("check type setting", By.ClassName("select-settings-action"), "הגדרות טופס " + automation, 1);
            fp.checkElment("check close page", By.XPath("//button[@aria-label='edit']"));
            //throw new NotImplementedException();
        }

        public void OpenSettingsTable()
        {
            try
            {
                fp.wait();
                Thread.Sleep(200);
                driver.FindElements(By.ClassName("select-settings-action"))[0].Click();
                checkElmentSettingTable();
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void removeFildes(string[] remove)
        {
            driver.FindElements(By.XPath("//div[@role='tab']"))[3].Click();
            for (int i = 0; i < remove.Length; i++)
                insertChangeTable(remove[i]);
        }

        public void clickCreateFilds()
        {
            try
            {
                fp.ClickButton(" יצירת שדה חדש ");
                checkPoUpSaveData();
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void checkPoUpSaveData()
        {
            fp.checkElmentText("check title popup save data", By.ClassName("title"), " שמירה ");
            fp.checkElment("check mesege popup save data", By.ClassName("wrap-message"));
            fp.checkElmentsText("check button aprrove popup", By.ClassName("mat-button-wrapper"), "לא", 0);
            fp.checkElmentsText("check button aprrove popup", By.ClassName("mat-button-wrapper"), "כן", 1);
            fp.ClickButton("כן");

            //throw new NotImplementedException();
        }

        public void checkElmentSettingTable()
        {
            try
            {
                if (!driver.FindElement(By.ClassName("wrap-icon")).Displayed)
                {
                    driver.FindElements(By.XPath("//div[@role='tab']"))[1].Click();
                    driver.FindElements(By.XPath("//div[@role='tab']"))[0].Click();
                }
                fp.checkElmentText("check title", By.ClassName("txt-title"), "שם טבלה לתצוגה");
                fp.checkElmentText("check title", By.XPath("//input[@formcontrolname='tableDN']"), automation);
                fp.checkElment("icon setting", By.ClassName("wrap-icon"));
                fp.checkElmentText("check name", By.XPath("//input[@formcontrolname='tableSN']"), automation);
                fp.checkElment("check zachrNekva", By.ClassName("tbl-settings"));
                fp.checkElmentText("check search", By.XPath("//input[@formcontrolname='searchHeader']"), "כותרת לחיפוש מהיר");
                fp.checkElmentsText("check button clearing change", By.ClassName("mat-button-wrapper"), "אפס שינויים ", 1);
                fp.checkElmentsText("check button save", By.ClassName("mat-button-wrapper"), " שמירה ", 2);
                fp.checkElmentsText("check button create new fildes", By.ClassName("mat-button-wrapper"), " יצירת שדה חדש ", 3, true);
                fp.checkElmentsText("check tab active fileds", By.XPath("//div[@role='tab']"), "שדות פעילים", 0);
                fp.checkElmentsText("check tab add fileds", By.XPath("//div[@role='tab']"), "שדות נוספים", 1);
                fp.checkElment("check table", By.TagName("mat-header-row"));
                fp.checkElment("check close popup setting", By.XPath("//BUTTON[@aria-label='edit']"));
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void addFildesTable()
        {
            int m = 3;
            string valueType, valueName;
            char[] delimiterChars = { ',', '.', ':', '\t' };
            ExcelApiTest eat = new ExcelApiTest("", "automation");
            checkElmentAddFildes();

            for (int i = 2; i < m; i++)
            {
                valueName = eat.createExcel("automation", "", false, 3, i);
                m = Int32.Parse(valueName.Split(delimiterChars)[1]);
                valueType = eat.createExcel("automation", "", false, 7, i);
                insertElmentAddFildes(valueName.Split(delimiterChars)[0], valueType.Split(delimiterChars)[0], valueType.Split(delimiterChars)[1], valueType.Split(delimiterChars)[2], valueType.Split(delimiterChars)[3], valueType.Split(delimiterChars)[4]);
                //popupSave();
                if (i + 1 != m)
                    clickCreateFilds();

            }

            /* insertElmentAddFildes("מחשב", "computer", "תיבת סימון");
                     clickCreateFilds();
                 insertElmentAddFildes("שם עיר", "nameCity", "תיבת בחירה", "טקסט", "9", "ערים");
                 fp.wait();
                 clickCreateFilds();
                 insertElmentAddFildes("תאריך", "dateDiff", "תאריך");*/

            //throw new NotImplementedException();
        }

        private void popupSave()
        {
            if (fp.checkElmentText("msg-title", By.ClassName("msg-title"), "שים לב!") == 0)
            {
                fp.checkElmentText("mell", By.CssSelector("div[class='msg-content ng-star-inserted']"), "הנתונים שישמרו בשמירה הכללית הם השינויים שנעשו מהשמירה הקודמת ");//throw new NotImplementedException();
                fp.click(By.XPath("//button[@class='close-btn mat-icon-button mat-button-base']"));
            }
        }

        private void checkElmentAddFildes()
        {
            fp.checkElmentText("check title", By.ClassName("title"), " הוספת שדה לטבלת אוטומציה" + automation);
            fp.checkElmentText("check name fildes", By.XPath("//input[@formcontrolname='DisplayName']"), "שם שדה");
            fp.checkElmentText("check name fildesin Englise", By.XPath("//input[@formcontrolname='FieldName']"), "שם שדה לשמירה בבסיס הנתונים");
            fp.checkElmentText("check name display", By.XPath("//mat-select[@formcontrolname='DisplayTypeID']"), "אופן תצוגה");
            fp.checkElmentsText("check button save", By.ClassName("mat-button-wrapper"), "שמירה ", 0);
            fp.checkElment("check button close", By.XPath("//i[text()='close'"));
            //throw new NotImplementedException();
        }

        private void insertElmentAddFildes(string nameFildes, string nameFildesinEnglise, string typeDisplay, string typeFildes = "false", string longFildes = "false", string selectListMakor = "false")
        {
            fp.insertElment("check name fildes", By.XPath("//input[@formcontrolname='DisplayName']"), nameFildes, true);
            fp.insertElment("check name fildesin Englise", By.XPath("//input[@formcontrolname='FieldName']"), nameFildesinEnglise);
            fp.SelectRequerdName("formcontrolname='DisplayTypeID", typeDisplay);
            if (selectListMakor != "false")
                fp.SelectRequerdName("formcontrolname='ListID", selectListMakor);
            if (typeFildes != "false")
                fp.SelectRequerdName("formcontrolname='FieldTypeId", typeFildes, true, true);
            if (longFildes != "false")
                fp.insertElment("longFildes", By.XPath("//input[@formcontrolname='FieldLength']"), longFildes);
            fp.ClickButton("שמירה ", "span",2);
            fp.wait();
            try
            {
                if (driver.FindElement(By.XPath("//div[text()='השדה נוסף לטבלה בהצלחה']")).Displayed)
                    Console.WriteLine(nameFildes+ "בהצלחה נוסף");

            }
          catch {
               // if (driver.FindElement(By.XPath("//div[text()=' שדות " + nameFildesinEnglise + "  ו  " + nameFildes + " כבר קיימים']")).Displayed)
                    driver.FindElement(By.CssSelector("button[class='close-btn close mat-icon-button mat-button-base']")).Click();
              //  else
                     //  if (driver.FindElement(By.XPath("//h2[text()=' הוספת שדה לטבלת kckc']")).Displayed)
                  //  driver.FindElement(By.CssSelector("button[class='close-btn close mat-icon-button mat-button-base']")).Click();
            }
        }

        public void save()
        {
            fp.ClickButton("שמירה ");
            //popupSave();
            //throw new NotImplementedException();
        }

        public void close()
        {
            //driver.FindElement(By.CssSelector("button[aria-label='edit']")).Click();
            fp.closePoup();
            //throw new NotImplementedException();
        }

        public void addFildes(string []addFildes=null)
        {
            fp.checkElmentsText("check tab add fileds", By.XPath("//div[@role='tab']"), "שדות נוספים", 1);
            driver.FindElements(By.XPath("//div[@role='tab']"))[4].Click();
            checkHederTableFildesSeting();
            int m = 3;
            string valueType, valueName;
            char[] delimiterChars = { ',', '.', ':', '\t' };
            if (addFildes == null)
            {
                ExcelApiTest eat = new ExcelApiTest("", "automation");
                for (int i = 2; i < m; i++)
                {
                    valueName = eat.createExcel("automation", "", false, 3, i);
                    m = Int32.Parse(valueName.Split(delimiterChars)[1]);
                    valueType = eat.createExcel("automation", "", false, 8, i);
                    string[] sValueType = valueType.Split(delimiterChars);
                    insertChangeTable(valueName.Split(delimiterChars)[0], sValueType[0], sValueType[1], sValueType[2], sValueType[3], sValueType[4], sValueType[5], sValueType[6]);

                }
            }
            else {
                for (int i = 0; i < addFildes.Length; i++)
                    insertChangeTable(addFildes[i]);
                 //   insertChangeTable(valueName.Split(delimiterChars)[0], sValueType[0], sValueType[1], sValueType[2], sValueType[3], sValueType[4], sValueType[5], sValueType[6]);
            }

            /* insertChangeTable("שם עיר", "true", "עיר");
             insertChangeTable("מחשב");
             insertChangeTable("תאריך");*/

            //throw new NotImplementedException();
        }
        public void checkAddFildesActive(string []addFildes=null)
        {
            driver.FindElements(By.XPath("//div[@role='tab']"))[3].Click();
            checkHederTableFildesSeting(true);
            int m = 3;
            string valueType1, valueType, valueName;
            char[] delimiterChars = { ',', '.', ':', '\t' };
            if (addFildes == null)
            {
                ExcelApiTest eat = new ExcelApiTest("", "automation");
                for (int i = 2; i < m; i++)
                {
                    valueName = eat.createExcel("automation", "", false, 3, i);
                    m = Int32.Parse(valueName.Split(delimiterChars)[1]);
                    valueType = eat.createExcel("automation", "", false, 8, i);
                    valueType1 = eat.createExcel("automation", "", false, 7, i);
                    string[] sValueType = valueType.Split(delimiterChars);
                    if (sValueType[1] != "")
                        sValueType[1] = valueName.Split(delimiterChars)[0];
                    if (sValueType[0] == "true")
                        sValueType[0] = "*";

                    string[] listRow = { "", valueName.Split(delimiterChars)[0], sValueType[1], valueType1.Split(delimiterChars)[1], sValueType[2], sValueType[3], sValueType[4], sValueType[5], sValueType[6], sValueType[0], "-1" };
                    fp.checkColumn("mat-table[role='grid']", listRow, "false", "mat-row", "mat-cell");

                }
            }
            else
            {

            }
            //string[] listRow = { "", "שם עיר", "עיר", "תיבת בחירה", "ערים", "", "", "", "*", "-1" };
            /*string[] listRow1 = { "", "מחשב", "מחשב", "תיבת סימון", "", "", "", "", "", "-1" };
                fp.checkColumn("mat-table[role='grid']", listRow1, "false", "mat-row", "mat-cell");
                string[] listRow2 = { "", "תאריך", "תאריך", "", "", "", "", "", "", "-1" };
                fp.checkColumn("mat-table[role='grid']", listRow2, "false", "mat-row", "mat-cell");
            */
        }
        public void insertChangeTable(string checkName, string requerd = "false", string nameDisplay = "", string typeDisplay = "", string listMakor = "", string valed = "", string messageValed = "", string valedSet = "",int l=0)
        {
            try
            {
                IWebElement nameTable = driver.FindElements(By.CssSelector("mat-table[role='grid']"))[l];
                IList<IWebElement> tableRow = nameTable.FindElements(By.TagName("mat-row"));
                IList<IWebElement> rowTD;
                int j=0, m=8;
                for (int i = 0; i < tableRow.Count - 1; i++)
                {
                    if (tableRow[0].Text.Contains("1"))
                    {
                        j = 1;
                        m = 9;
                    }
                    rowTD = tableRow[i].FindElements(By.TagName("mat-cell"));
                    if (rowTD[j].Text.Contains(checkName.Trim()))
                    {
                        if (nameDisplay != "")
                        {
                            rowTD[1].FindElement(By.TagName("input")).Clear();
                            rowTD[1].FindElement(By.TagName("input")).SendKeys(nameDisplay);

                        }
                        if (typeDisplay != "")
                            fp.SelectRequerdName("ng-reflect-name='" + i + "DT", typeDisplay, false, true);
                        if (listMakor != "")
                            fp.SelectRequerdName("ng-reflect-name='" + i + "SL", listMakor, false, true);
                        if (valed != "")
                            driver.FindElement(By.XPath("//input[@ng-reflect-name='" + i + "VR']")).SendKeys(valed);
                        if (messageValed != "")
                            driver.FindElement(By.XPath("//input[@ng-reflect-name='" + i + "VM']")).SendKeys(messageValed);
                        if (valedSet != "")
                            fp.SelectRequerdName("ng-reflect-name='" + i + "VID", valedSet, false, true);
                        if (requerd == "true")
                            rowTD[7].Click();
                        rowTD[m].Click();
                        break;

                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            //throw new NotImplementedException();
        }

        public void checkHederTableFildesSeting(bool active = false)
        {
            string[] listHeder = { "שם שדה", "שם לתצוגה", "אופן תצוגה", "רשימת מקור", "אימות", "הודעת אימות", "אימות מוגדר", "נדרש", "הצג", "-1" };
            string[] listHeder1 = { "סדר תצוגה", "שם שדה", "שם לתצוגה", "אופן תצוגה", "רשימת מקור", "אימות", "הודעת אימות", "אימות מוגדר", "נדרש", "הצג", "-1" };
            if (active == true)
                listHeder = listHeder1;
            fp.checkTableHeder(listHeder, "mat-table[role='grid']", "mat-header-row", "mat-header-cell");

        }
        public void openSettingTest(string nameTofs="")
        {
            if (nameTofs == "")
                nameTofs = "kckc";
            Thread.Sleep(200);
            WebDriverWait customWait = new WebDriverWait(driver, TimeSpan.FromSeconds(3055));
            customWait.Until(driver => driver.FindElements(By.ClassName("icon-table-properties"))[1]);
            //driver.FindElements(By.ClassName("icon-table-properties"))[1].Click();
            driver.FindElement(By.XPath("//div[@class='select-settings-action']/div[@class='action-title'][text()='הגדרות טופס " + nameTofs + "']")).Click();

            checkElmentTofes();
        }

        public void checkElmentTofes()
        {
            fp.click(By.ClassName("mat-list-text"),0);
            fp.checkElmentText("check title test setting", By.CssSelector("div[class='wrap-financial-title']"), "טאבים ומקטעים " + automation);
            fp.checkElmentsText("check button add section", By.ClassName("mat-button-wrapper"), "הוסף מקטע ", 2);
            fp.checkElmentsText("check button add section", By.ClassName("mat-button-wrapper"), "איפוס ", 3);
            //throw new NotImplementedException();
        }

        public void clickaddSections()
        {
            try
            {

                Thread.Sleep(100);
                fp.waitUntil(By.XPath("//span[@class='mat-button-wrapper'][text()='הוסף מקטע ']"));
                fp.ClickButton("הוסף מקטע", "span");
                checkSections();
                List<Section> sections = new List<Section>();
                int m = 3;
                string valueType1, valueType, valueName;
                char[] dC = { ',', '.', ':', '\t' };
                ExcelApiTest eat = new ExcelApiTest("", "automation");
                for (int p = 2; p < m; p++)
                {
                    valueName = eat.createExcel("automation", "", false, 2, p);
                    m = Int32.Parse(valueName.Split(dC)[1]);
                    valueType1 = "";
                    for (int f = p; f < m + 1; f++)
                    {
                        valueType1 = valueType1 + eat.createExcel("automation", "", false, 3, f, true);
                        eat.CloseExcel();
                        if (valueType1.Contains("-1"))
                        {
                            p = f;
                            break;
                        }
                        valueType1 = valueType1 + ",";
                    }
                    string[] sValueType = valueType1.Split(dC);
                    string[] permisions;
                    Thread.Sleep(200);
                    string valueType2 = eat.createExcel("automation", "", false, 4, p - 1, true);
                    if (valueType2 != null)
                        permisions = valueType2.Split(dC);
                    else
                        permisions = null;
                    string dSEdit = eat.createExcel("automation", "", false, 5, p - 2, true);
                    sections.Add(new Section(valueName.Split(dC)[0], sValueType, permisions, dSEdit, eat.createExcel("automation", "", false, 6, p - 1)));
                }
                // sections.Add(new Section("מחשב", new List<string>() { "מחשב", "-1" }, null, true, true));
                //  sections.Add(new Section("תאריך", new List<string>() { "תאריך", "-1" }, new List<string>() { "מזכיר", "false", "-1" }, true, false));


                int i = 0;

                foreach (Section sec in sections)
                {
                    i = i + 1;
                    insertDataSections(sec.NameSection, sec.SelectSection, sec.Permissions);
                    if (sections.Count == i)
                        break;
                    fp.ClickButton("הוסף מקטע", "span");

                }
                string[] listHeder = { "שם מקטע", "הרשאות", "סדר תצוגה", "האם להציג מקטע בעריכה", "האם להציג מקטע ביצירה", "פעולות", "-1" };
                fp.checkTableHeder(listHeder, "mat-table[role='grid']", "mat-header-row", "mat-header-cell");
                foreach (Section sec in sections)
                {
                    displaySection(sec.NameSection, sec.DesplaySectionEdit, sec.DesplayCreateSection);
                }
                fp.ClickButton("שמירה", "span");
                Thread.Sleep(100);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            //throw new NotImplementedException();
        }

        public void displaySection(string nameSection, string desplaySectionEdit, string desplayCreateSection)
        {
            try
            {
                IWebElement nameTable = driver.FindElement(By.CssSelector("mat-table[role='grid']"));
                IList<IWebElement> tableRow = driver.FindElements(By.CssSelector("mat-row[role='row']"));
                IList<IWebElement> rowTD;
                Thread.Sleep(300);
                for (int i = 0; i < tableRow.Count; i++)
                {
                    Thread.Sleep(100);
                    rowTD = tableRow[i].FindElements(By.CssSelector("mat-cell[role='gridcell']"));
                    if (rowTD[0].Text.Contains(nameSection))
                    {
                        if (desplaySectionEdit.Contains("true"))
                            if (!tableRow[i].FindElements(By.CssSelector("mat-cell[role='gridcell']"))[3].FindElement(By.TagName("mat-checkbox")).GetAttribute("className").Contains("mat-checkbox-checked"))
                                tableRow[i].FindElements(By.CssSelector("mat-cell[role='gridcell']"))[3].FindElement(By.TagName("mat-checkbox")).Click();
                        // driver.FindElement(By.XPath("//mat-checkbox[@ng-reflect-name='" +( i-1) + "UDIS']")).Click();
                        if (desplayCreateSection.Contains("True"))
                            if (!tableRow[i].FindElements(By.CssSelector("mat-cell[role='gridcell']"))[4].FindElement(By.TagName("mat-checkbox")).GetAttribute("className").Contains("mat-checkbox-checked"))
                                tableRow[i].FindElements(By.CssSelector("mat-cell[role='gridcell']"))[4].FindElement(By.TagName("mat-checkbox")).Click();

                        //driver.FindElement(By.XPath("//mat-checkbox[@ng-reflect-name='" +( i-1 )+ "CDIS']")).Click();
                        break;
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void insertDataSections(string nameSections, string[] selectFildes, string[] premsionFalse = null)
        {
            try
            {
                fp.waitUntil(By.XPath("//input[@formcontrolname='sectionDN']"));
                driver.FindElement(By.XPath("//input[@formcontrolname='sectionDN']")).Clear();
                fp.insertElment("new section name", By.XPath("//input[@formcontrolname='sectionDN']"), nameSections);
                selectfildes(selectFildes);
                selectPermsion(premsionFalse);
                driver.FindElement(By.CssSelector("button[class='btn-table save-btn mat-raised-button mat-button-base mat-primary']")).Click();
                // fp.ClickButton("שמור");
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            // throw new NotImplementedException();
        }

        public void selectPermsion(string[] premsionFalse)
        {
            try
            {

                int j = 0;
                IList<IWebElement> per = driver.FindElements(By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"));
                IList<IWebElement> editing = driver.FindElements(By.TagName("mat-slide-toggle"));
                for (int i = 0; i < per.Count - 1; i++)
                {
                    if (premsionFalse == null)
                        break;
                    if (per[i].Text.Contains(premsionFalse[j]))
                    {
                        if (premsionFalse[j + 1] == "true")
                            per[i].Click();
                        else
                            editing[i].Click();
                        j = j + 2;
                    }
                    if (premsionFalse.Length <= j || premsionFalse[j] == "-1")
                        break;
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }//throw new NotImplementedException();
        }

        public void selectfildes(string[] selectFildes)
        {
            try
            {
                int j = 0;
                IList<IWebElement> fildes = driver.FindElements(By.TagName("option"));
                for (int i = 0; i < fildes.Count - 1; i++)
                {
                    if (fildes[i].Text.Trim().Contains(selectFildes[j].Trim()))
                    {
                        j = j + 1;
                        new Actions(driver).DoubleClick(fildes[i]).Perform();
                        Thread.Sleep(200);
                    }
                    if (selectFildes[j] == "-1")
                        break;
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            //throw new NotImplementedException();
        }

        public void checkSections()
        {
            try
            {
                fp.checkElment("check button zero changes", By.XPath("//span[@class='mat-button-wrapper'][text()='איפוס ']"));
                fp.checkElment("check button save", By.XPath("//span[@class='mat-button-wrapper'][text()='שמירה ']"));
                fp.checkElmentText("check title", By.ClassName("title"), "עריכת מקטע מקטע חדש 1");
                fp.checkElmentText("chevk title name sections", By.ClassName("dn-section"), " שם מקטע לתצוגה:   ");
                fp.checkElmentText("check title chenge", By.XPath("//input[@formcontrolname='sectionDN']"), "מקטע חדש 1");
                fp.checkElmentText("check select all", By.XPath("//mat-select[@formcontrolname='tab']"), "בחר טאב");
                fp.checkElmentText("check premsion", By.ClassName("prms-title"), "הרשאות ");
                fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "הכל", 0);
                fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "מנהל", 1);
                fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "מנהל חינוכי", 2);
                fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "מזכיר", 3);
                fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "הנהלת חשבונות", 4);
                //fp.checkElmentsText("check premsion all", By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"), "מתכנת", 5);
                fp.checkElmentText("ceck edit fildes", By.TagName("h3"), "עריכת שדות");
                fp.checkElmentText("check search", By.XPath("//DIV/input"), "חיפוש");
                fp.checkElmentsText("select fildes", By.ClassName("list-title"), "שדות לבחירה", 0);
                fp.checkElmentsText("select fildes", By.ClassName("list-title"), "שדות שנבחרו", 1);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            // throw new NotImplementedException();
        }

        public void checkPagecreateNewInTable(string NameNewAdd)
        {
            fp.checkCreate(NameNewAdd);
            int m = 3;
            string valueType1, valueType, valueName;
            char[] dC = { ',', '.', ':', '\t' };
            ExcelApiTest eat = new ExcelApiTest("", "automation");
            string nameTitle = "";
            int j = 0;
            for (int p = 2; p < m; p++)
            {
                valueName = eat.createExcel("automation", "", false, 2, p);
                m = Int32.Parse(valueName.Split(dC)[1]);
                if(valueName.Split(dC)[0]!=nameTitle)
                {
                    nameTitle = valueName.Split(dC)[0];
                    fp.checkElmentsText("subTitle", By.ClassName("title-section"), valueName.Split(dC)[0], j);
                        j++;
                }
                valueName = eat.createExcel("automation", "", false, 3, p, true);
                valueName = valueName.Split(dC)[0];
                fp.checkSelectPreform(valueName, By.XPath("//div/span/label/mat-lebel"));
                valueType= eat.createExcel("automation", "", false, 7, p, true);
                switch (valueType.Split(dC)[1])
                {
                    case "תיבת סימון":
                        fp.selectForRequerd(valueName, false,0);
                        break;
                    case "מספר":
                        fp.selectForRequerd(valueName, false, -1, "8");
                        break;
                    case "טקסט":
                        fp.selectForRequerd(valueName, false, -1, automation);
                        break;
                    case "תאריך":
                        createConatrabition cc = new createConatrabition();
                        cc.calanederCheck();
                        break;
                    case "שעה":
                        fp.checkElment("age", By.Name(valueName));
                        fp.insertElment("age", By.Name(valueName), "1221");
                        break;
                    case "תיבת בחירה":
                        select(valueName);
                       break;
                    case "בחירה מרובה":
                        select(valueName);
                       break;
                    case "צירוף קובץ":
                        fp.checkElment("attached-file", By.ClassName("icon-attached-file"));
                        break;
                }
                //throw new NotImplementedException();
            }
            fp.ClickButton("צור " + "ccc" + " חדש", "span");

        }

        public  void approveDisplay()
        {

           // IWebElement table = driver.FindElement(By.TagName("mat-table"));
            IList<IWebElement> rows = driver.FindElements(By.TagName("mat-row"));
            IList<IWebElement> cells;
            for (int i=1;i<rows.Count;i++)
            {
                cells = rows[i].FindElements(By.TagName("mat-cell"));
                cells[3].FindElement(By.TagName("mat-checkbox")).Click();
                cells[4].FindElement(By.TagName("mat-checkbox")).Click();
            }
            fp.ClickButton("שמירה", "span");
           // driver.FindElement(By.XPath("//span[@class='mat-button-wrapper']/i[text()='çlose']")).Click();
            //  throw new NotImplementedException();
        }

        public  void select(string valueName)
        {
            driver.FindElement(By.XPath("//i[text()='add']")).Click();
            fp.selectForRequerd("הזן ערך נוסף לרשימה", false, -1, automation);
            fp.selectForRequerd(valueName, false, 1);
        }

        public void ceackAddSection(string valueCheck)
        {
            Thread.Sleep(100);
                fp.waitUntil(By.XPath("//span[@class='mat-button-wrapper'][text()='הוסף מקטע ']"));
                fp.ClickButton("הוסף מקטע", "span");
            IList<IWebElement> approve = driver.FindElements(By.XPath("//p/mat-checkbox/label/span[@class='mat-checkbox-label']"));
            if (approve[approve.Count - 1].Text.Contains(valueCheck))
                Console.WriteLine("the roles add");
            else
                Debug.WriteLine("the roles not add");
        }

        public void changePlace()
        {
            try { //test();
                string[] removeDesplayLastName = { "", " LastName ", "-1" };
                fp.checkColumn("mat-table[role='grid']",removeDesplayLastName, "//div[@class='wrap-icon-in-table active-icon']/div[@class='display-icon']", "mat-row", "mat-cell",9,1);
                fp.ClickButton(" שמירה פרטית ","span");
                fp.checkElmentText("msgPopup",By.ClassName("msg-title"), "שים לב!");
                fp.checkElmentText("msgPopup",By.XPath("//div[@class='msg-content ng-star-inserted']/p"), "הנתונים שישמרו בשמירה הפרטית הם השינויים שנעשו מהשמירה הקודמת ");
                fp.click(By.CssSelector("button[class='close-btn mat-icon-button mat-button-base']"));
            } catch { fp.picter("changePlace"); throw new NotImplementedException(); }
        }
        public static void test()
        {
            By test1 = By.XPath("//mat-row/mat-cell/div[@class='cdk-drag-handle display-order']/div[text()='1']");
            By test2 = By.CssSelector("div[class='cdk-drag-handle display-order]");

            Actions builder1 = new Actions(driver);
            //IAction dragAndDrop1 = builder1.ClickAndHold(driver.FindElement( test1)).MoveToElement(driver.FindElements(test2).Release(driver.FindElement(test1));
            //dragAndDrop1.Perform();
        }

        public  void checkTableAfterChange(bool onLastName)
        {
            try { string[] hederNo = { "","שם משפחה","שם פרטי","-1"};
                int numret = fp.checkColumn("//", hederNo, "false", "mat-header-row", "mat-header-cell");
                if (( numret== -1 && onLastName==false)|| (numret != -1 && onLastName == true))
                    Console.WriteLine("the heder cell in save private is very good");
                else
                {
                    Debug.WriteLine("the hede cell family is desplay in save private");
                    fp.picter("checkTableAfterChange");
                }
            } catch { throw new NotImplementedException(); }
        }

        public void openUserChange()
        {
            try
            {
                login ln = new login();
                ln.openChrome();
                 ln.UserNamee("אוטומציה משתמש");
                ln.Password("אוטומציה1!");
                ln.cLickLogIn();
            } catch { throw new NotImplementedException(); }
        }

        public  void stepSavePrivate()
        {
            try
            {
                openTable(student, "הורים");
                OpenSettings();
                checkESettingTable();
                OpenSettingsTable();
                changePlace();
                fp.closePoup();
                Thread.Sleep(650);
                checkTableAfterChange(false);
                fp.closeChrome();
                openUserChange();
                openTable(student, "הורים");
                checkTableAfterChange(true);
                fp.closeChrome();
            }
            catch { throw new NotImplementedException(); }
        }
        /*  public static class WebDriverExtensions
{
public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
{
if (timeoutInSeconds > 0)
{
var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
return wait.Until(drv => drv.FindElement(by));
}
return driver.FindElement(by);
}
}*/
    }

}




