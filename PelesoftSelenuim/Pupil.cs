﻿using com.sun.tools.javac.util;
using java.awt;
using java.awt.datatransfer;
using java.awt.@event;
using java.lang;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Thread = System.Threading.Thread;

namespace PelesoftSelenuim
{
    class PupilNew : page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();
        Financical fl = new Financical();
        CreateNewDenumic cnd = new CreateNewDenumic();
        popupList pl = new popupList();

        public PupilNew()
        {
        }
        public void openPupil()
        {
            fp.clickNameDefinitions(" תלמידים ");
            fp.clickListinListEqualName("תלמידים");
        }
        public void openCreatePupil(string IdentityNumber,string nameMosad)
        {
            openPupil();
            stepCreatePupil(IdentityNumber,nameMosad);
           // fp.waitUntil(By.CssSelector("button[class='button-Create mat-button mat-button-base ng-star-inserted']"));
           // driver.FindElement(By.CssSelector("button[class='button-Create mat-button mat-button-base ng-star-inserted']")).Click();
            
        }
        public void stepCreatePupil(string IdentityNumber,string nameMosad)
        {
            fp.ButtonNew("תלמיד");
            Thread.Sleep(100);
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='SchoolYear']")).Click();
            fp.selectText(yearHebru);
            try
            {
                Thread.Sleep(200);
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='EducationInstitutesID']")).Click();
                fp.selectText(nameMosad);
            }
            catch { }
            //fp.SelectRequerdName("formcontrolname='SchoolYear", "תשפ\"א");
            fp.wait();
            insertTZ(IdentityNumber);
            fp.ClickButton("איתור");
        }
        public void insertTZ(string IdentityNumber)
        {
            fp.insertElment("numberTZ", By.Name("IdentityNumber"), IdentityNumber);
            try
            {
                Console.WriteLine(driver.FindElement(By.TagName("button")).GetAttribute("disabled"));
                if (driver.FindElement(By.TagName("button")).GetAttribute("disabled") == "true")
                    while (driver.FindElement(By.TagName("button")).GetAttribute("disabled") == "true")
                    {
                        driver.FindElement(By.XPath("//input[@formcontrolname='IdentityNumber']")).Clear();
                        IdentityNumber = fl.tzPupil();
                        fp.insertElment("numberTZ", By.XPath("//input[@formcontrolname='IdentityNumber']"), IdentityNumber);
                    }
            }
            catch { }
        }
        public int stepCreatePUPIL(string lastName="", string mosad="",int frind=-1,string grade="")
        {
            cnd.checkClasscreate("תלמיד", false);
            cc.checkElmentPageStart(2, "תלמיד");
             if(frind>1000)
                cc.clickFrindNew("",nameFrind, frind.ToString(),frind.ToString());
            else if(frind!=-1)
            cc.clickFrindNew("",nameFrind, "1000",frind.ToString());
             else if(lastName!="")
                cc.clickFrindNew(lastName);

            else
                cc.clickFrindNisoyAddFrind("הורה");

            cc.checkStepHeder(2, 2);
            string firstName,fatheName;
            char[] sp = { ' '};
            firstName = names[fp.rand(names.Length)]; 
                lastName = driver.FindElement(By.CssSelector("[class='sub-title ng-star-inserted']")).Text.Split(sp)[0];//familys[fp.rand(familys.Length)];
                fatheName = driver.FindElement(By.CssSelector("[class='sub-title ng-star-inserted']")).Text.Split(sp)[1];//familys[fp.rand(familys.Length)];
                //fatheName = fatheName + " "+driver.FindElement(By.CssSelector("[class='sub-title ng-star-inserted']")).Text.Split(sp)[2];//familys[fp.rand(familys.Length)];

            return insertDataPupil( lastName,  firstName, mosad,grade,fatheName);
        }

        public int insertDataPupil(string lastName, string firstName,string mosad,string grade,string fatheName="")
        {
            checkElmentPupil();
           return insertElment( lastName,  firstName,mosad,grade,fatheName);
        }
        public int insertPratim(string lastName,string firstName,string mosad,string grade,string typeAtion= "התלמיד")
        {
            int slct = 0;
           // fp.selectForRequerd("סטטוס", false, 1);
          
           // new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementExists(By.Name("LastName"))).SendKeys(lastName);
            //fp.selectForRequerd("שם משפחה", false, -1, lastName);
           // fp.insertElment("",By.TagName("input"),lastName);
            //  fp.insertElment("", By.Name("LastName"), lastName);
            //  fp.insertElment("Last Name", By.CssSelector("input[name='LastName']"),firstName);
            //driver.FindElement(By.Name("IdentityNumber")).SendKeys(Keys.Tab+Keys.Tab);
            int co = driver.FindElements(By.TagName("input")).Count-6;
            fp.insertElment("", By.TagName("input"), lastName,false,"",co);
            fp.insertElment("", By.TagName("input"), firstName,false,"",co+1);
           // fp.insertElment("First Name", By.CssSelector("input[class='mat-input-element mat-form-field-autofill-control text-overflow cdk-text-field-autofill-monitored ng-pristine ng-valid ng-star-inserted ng-touched'][name='FirstName']"),firstName,false,"",0);
           // fp.insertElment("Last Name", By.CssSelector("input[class='mat-input-element mat-form-field-autofill-control text-overflow cdk-text-field-autofill-monitored ng-pristine ng-valid ng-star-inserted ng-touched'][name='LastName']"),lastName);
           // fp.insertElment("First Name", By.CssSelector("input[class='mat-input-element mat-form-field-autofill-control text-overflow cdk-text-field-autofill-monitored ng-pristine ng-valid ng-star-inserted ng-touched']"), firstName, false, "", 1); // driver.FindElement(By.Name("IdentityNumber")).SendKeys(Keys.Tab + Keys.Tab+firstName);
            //try { driver.FindElement(By.Name("FirstName")).SendKeys(firstName); } catch { }
            //try { driver.FindElement(By.XPath("//div/div/input[@name='FirstName']")).SendKeys(firstName); } catch { }
            fp.selectForRequerd("סטטוס", false, 1);
            //fp.selectForRequerd("מוסד לימודים ", true, -1);
            //fp.SelectRequerdName("ng-reflect-name='EducationInstitutesID",automation);
          //המוסד מוזן אוטומטית 
          /* if (mosad == "")
                mosad = automation;
            fp.selectText(mosad);*/
            if (grade == "")
            {
                fp.selectForRequerd("רמת כיתה");
                  fp.selectForRequerd("כיתה וסניף", false);// fp.SelectRequerdName("ng-reflect-name='ClassID", automation + " - " + automation, false);
            }
            else
            {
                fp.selectForRequerd("רמת כיתה", false, -1);//fp.SelectRequerdName("ng-reflect-name='ClassLevelID", automation);
                if (fp.selectText(grade) == 1)
                    slct = 1;
                else
                {
                    Thread.Sleep(100);
                    //    fp.selectForRequerd("כיתה וסניף", false, -1);// fp.SelectRequerdName("ng-reflect-name='ClassID", automation + " - " + automation, false);
                    Thread.Sleep(250);
                    //   fp.selectText(mosad);
                }

            }
            cc.calanederCheck(1, 2, "1", 1, 0);//"1",DateTime.DaysInMonth.-2);
            return slct;
        }
        public  int insertElment(string lastName, string firstName, string mosad,string grade,string fatheName)
        {
            int slct = 0;
           slct= insertPratim(lastName,firstName,mosad,grade);
            //, "ng-reflect-name='Status", "נקלט",false);
            //
            //driver.FindElements(By.CssSelector("div[class='wrap-add-value ng-star-inserted']"))[2].Click();
            // fp.addValueList("הורה",0,By.Name("CalculatedDatotValue"));
            //fp.selectForRequerd("הזן ערך נוסף לרשימה", false, -1, automation + " תלמיד");
            //fp.insertElment("add ", By.XPath("//mat-form-field[@aria-label='addToList']/div/div/div/div/div/input"),automation+"תלמיד");
            //insertDataParent("Father", "אב", 0,lastName,fatheName);
            //insertDataParent("Mother", "אם", 1);
            
            

            cc.calanederCheck(1, 1, DateTime.Today.ToString("dd"),12,2);
           driver.FindElement( By.XPath("//button[@aria-label='btnSubmitCreateItem']")).Click();
            return slct;
        }
        public void openStudentColl()
        {
            try
            {
                fp.ClickList(pupilsYesva);
                fp.clickButoonToContains(avracim);
            }
            catch { fp.picter("openStudentColl"); throw new NotImplementedException(); }
        }
        public void stepCreateStuedent(string nameAvrec,string Mosad,int mikom, string pupil)
        {
            try
            {
                stepCreatePupil(fl.tzPupil(),Mosad);
                 createConatrabition cc = new createConatrabition();
                string family = familys[fp.rand(familys.Length)];
                cc.clickFrindNisoyAddFrind("אברך",family, nameAvrec );
                
                insertPratim(family, nameAvrec,Mosad,"סדר א", "האברך כולל");
                fp.click(By.XPath("//button[@aria-label='btnSubmitCreateItem']"));

            }
            catch { }
        }
        public void createPupilColl()
        {
            int n =  100045;
            int k = 0;
            openStudentColl();
            for (int m = 0; m < 358; m++)
            {
                string pupil = fl.tzPupil();
                stepCreateStuedent(names[fp.rand(names.Length)], "כולל", n, pupil);
                n++;k++;
                if(k==10)
                {
                    k = 0;
                    Thread.Sleep(600);
                }
                Thread.Sleep(650);
                fp.closePoup();
            }
        }
        internal void createPupils(string[] mosad, string[] EI)
        {
            int numStudent = 9;
            openPupil();
            int o = 0;
            int n = 0;
            for (int j = 0; j < EI.Length; j++)
                for (int k = 0; k < mosad.Length; k++)
                    for (int m = 0; m < grade.Length; m++)
                    {
                        n = names.Length;
                        if (EI[j].Contains("ישיבה"))
                            numStudent = 15;
                        if (EI[j] == "גן בנים")
                        {
                            numStudent = 13;
                            if (m > 1)
                                break;
                        }
                        for (int i = 0; i < numStudent; i++)
                        {
                            string pupil = fl.tzPupil();
                            stepCreatePupil(pupil,mosad[j]);
                            string[] namePupil;
                            if (mosad[j] == "בית ספר")
                                namePupil = mather;
                            else
                                namePupil = names;
                            if (stepCreatePUPIL(namePupil[fp.rand(namePupil.Length)], EI[j],n, grade[m] + "-" + mosad[k]) == 1)
                            {
                                fp.closePoup();
                                break;
                            }// sec.checkEndCreate("תלמיד", false, "", p.nameFrind);
                            goToCard();
                            insertDate(mosad[j], m);
                            string sum = "1400";
                            string[] ofen = ofenM;
                            prentalData(ofen[fp.rand(ofen.Length)], sum, false,1);

                            Thread.Sleep(100);

                          
                            if (o + 1 == pupilID.Length)
                                break;
                            f++; o++;n--;
                            if (n < 0)
                                n = names.Length;
                        }
                    }
            fp.closeChrome();
        
    }

        public  void insertDataParent(string typeParent, string typeParentH, int numCalnder,string lastName="",string fatheName="")
        {
            DateTime birtDate = DateTime.Today.AddYears(-20);
            string firstName;
            if (typeParent.Contains("M"))
                firstName = mather[fp.rand(mather.Length)];
            else
            {
                if (fatheName == "")
                    firstName = names[fp.rand(names.Length)];
                else
                    firstName = fatheName;
            }

            if (lastName == "")
                lastName = familys[fp.rand(familys.Length)];
            fp.insertElment(typeParent + " First Name ", By.Name(typeParent+"FirstName"),firstName);
            fp.insertElment(typeParent + " Identity", By.Name(typeParent+"Identity"), fl.tzPupil());
            try
            { string colorVa = driver.FindElement(By.XPath("//span/label/mat-label[text()=' ת.ז "+typeParentH+" ' ]")).GetCssValue("border-color");
                if (colorVa.Contains("rgb(244, 67, 54)"))//(By.TagName("mat-error")).Displayed)
                {
                    driver.FindElement(By.Name(typeParent + "Identity")).Clear();
                    fp.insertElment(typeParent + " Identity", By.Name(typeParent + "Identity"), fl.tzPupil());
                }
            }
            catch { }
            fp.insertElment(typeParent + " Last Name ", By.Name(typeParent+"LastName"),lastName );
            cc.calanederCheck(1, numCalnder,birtDate.ToString("dd"),Int32.Parse(birtDate.ToString("MM")),-20);
        }

        public void goToCard(string card= "תלמיד")
        {
                driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' קח אותי לכרטיס "+card+" ']")).Click();

        }

        //private StringBuilder verify;
        public void insertDate(string mosad,int grade)
        {
            int year=-2;
            if (mosad.Contains("גן"))
                year = -4;
            if (mosad.Contains("ישיבה"))
                year = -13;
            if (mosad.Contains("תלמוד תורה"))
                year = -5;
            year += -grade;


            string day = fp.rand(30,1).ToString();
            int month = fp.rand(12,1);
            Thread.Sleep(600);
             cc.calanederCheck(-1, -1, day,month,year);
            HebDates hd = new HebDates();
            DateTime d = System.Convert.ToDateTime(day + "/" + month + "/" + DateTime.Today.AddYears(year).ToString("yyyy"));// driver.FindElement(By.XPath("//input[contains(text(),'/')]")).Text);
            string dateEbrue= hd.FullDate(d);
            fp.insertElment("date ebrue", By.Name("HebrewDate"), dateEbrue);
            if(mosad.Contains("ישיבה"))
            {
                yshevaStudent();
            }
        }

        private void yshevaStudent()
        {
            int idDatot = fp.rand(IDDatot.Length);
            int mgmaR = fp.rand(mgma.Length);
            driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[0].Click();
           // fp.addValueList(IDDatot[idDatot], 0, By.TagName("textarea"));
            fp.addValueList(IDDatot[idDatot], 0, By.TagName("textarea"), 1);

            Thread.Sleep(100);
            fp.selectForRequerd("קוד דתות",false,-1);
            fp.selectText(IDDatot[idDatot]);
            Thread.Sleep(1900);
            driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[1].Click();
            fp.addValueList(mgma[mgmaR], 0, By.TagName("textarea"),2);
            Thread.Sleep(100);
            fp.selectForRequerd("מגמה", false, -1);
            fp.selectText(mgma[mgmaR]);

            /* string calcDatot = "";
             fp.insertElment("datot calc", By.Name("CalculatedDatotValue"),calcDatot);*/
        }

        public void checkElmentPupil()
        {
            fp.checkElmentText("title insert Data", By.ClassName("sub-title"), "אנא מלא/י את הפרטים הבאים");
            fp.checkElmentsText("title data pupil", By.ClassName("title-section"), "פרטי תלמיד", 0);
            fp.checkElmentText("number Tz", By.Name("IdentityNumber"), TZ);
            fp.checkElmentText("Last Name", By.Name("LastName"), "שם משפחה");
            fp.checkElmentText("First Name", By.Name("FirstName"), "שם פרטי");
            fp.checkSelectPreform("בחר/י סטטוס עבור התלמיד", By.TagName("mat-select"));
            //fp.checkElmentText("select stutes pupil", By.XPath("//mat-select[@ng-reflect-name='Status']"), "בחר/י סטטוס עבור התלמיד");
            fp.checkElmentsText("title data pupil", By.ClassName("title-section"), "קשר ידיד לתלמיד", 3);
            fp.checkSelectPreform("בחר/י קשר ידיד לתלמיד עבור התלמיד", By.TagName("mat-select"));//[@ng-reflect-name='PersonRealationId']"), );
            /*fp.checkElmentsText("title data pupil", By.ClassName("title-section"), "פרטי אב", 4);
            fp.checkElmentText("Father First Name ", By.Name("FatherFirstName"), "שם פרטי אב");
            fp.checkElmentText("Father Identity", By.Name("FatherIdentity"), " ת.ז אב ");
            fp.checkElmentText("Father Last Name ", By.Name("FatherLastName"), "שם משפחה אב");
            fp.checkElment("selectBirthDateFather", By.XPath("//button[@aria-label='Open calendar']"));
            fp.checkElmentsText("title data pupil", By.ClassName("title-section"), "פרטי אם", 5);
            fp.checkElmentText("Mother First Name ", By.Name("MotherFirstName"), "שם פרטי אם");
            fp.checkElmentText("Mother Identity", By.Name("MotherIdentity"), " ת.ז אם ");
            fp.checkElmentText("Mother Last Name ", By.Name("MotherLastName"), "שם משפחה אם");
            fp.checkElment("selectBirthDateMother", By.XPath("//button[@aria-label='Open calendar']"));*/
            fp.checkElmentsText("title data pupil", By.ClassName("title-section"), "מקום לימודים", 4);
            fp.checkSelectPreform("שנת לימודים", By.TagName("mat-select"));// "[@ng-reflect-name='SchoolYearID']"), yearHebru);
            fp.checkSelectPreform("בחר/י מוסד לימודים עבור התלמיד", By.TagName("mat-select")); //XPath("//mat-select[@ng-reflect-name='EducationInstitutesID']"),);
            fp.checkSelectPreform("בחר/י רמת כיתה עבור התלמיד", By.TagName("mat-select")); //"select class lavel", By.XPath("//mat-select[@ng-reflect-name='ClassLevelID']"), );
            fp.checkSelectPreform("בחר/י כיתה וסניף עבור התלמיד",By.TagName("mat-select")); //"select class lavel", By.XPath("//mat-select[@ng-reflect-name='ClassID']"), );
            //fp.checkElmentsText("select date start", By.CssSelector("app-long-date[classcss='generic-field']"), "בחר/י  תאריך התחלה", 0);
            //fp.checkElmentsText("select date end", By.CssSelector("app-long-date[classcss='generic-field']"), "בחר/י  תאריך סיום", 1);
            fp.checkElmentText("button create new pupil", By.XPath("//button[@aria-label='btnSubmitCreateItem']"), " צור תלמיד חדש ");

        }
        public void openPrenatalData(string method="")
        {
            IWebElement tableName = driver.FindElement(By.TagName("mat-table"));
            IList<IWebElement> tableRow = tableName.FindElements(By.TagName("mat-row"));
            for (int i = 0; i < tableRow.Count; i++)
            {
                tableName = driver.FindElement(By.TagName("mat-table"));
                tableRow = tableName.FindElements(By.TagName("mat-row"));
                string sum = "400";
                if (method != "")
                {
                    ofen = ofenM;
                    if (tableRow[i].Text.Contains("ישיבה"))
                     sum = (Int16.Parse(sum) * 12).ToString();
                }
                
                tableRow[i].Click();
                if (i == 14)
                    Thread.Sleep(200);
                if (method == "")
                    methodPaymens(ofen[fp.rand(ofen.Length)], sum);
                else
                 prentalData(ofen[fp.rand(ofen.Length)], sum);
                Thread.Sleep(400);
                fp.closePoup(1);
                fp.closePoup(0);
                if (i + 1 == tableRow.Count)
                    if (fp.clickLeft() == true)
                    {
                        i = 0;
                        Thread.Sleep(1300);
                    }
                if (i + 1 == tableRow.Count)
                    break;
            }
        }

        /// <summary>
        /// קשור להורים
        /// </summary>
        /// <param name="ofen"></param>
        /// <param name="sum"></param>
        /// <param name="edit"></param>
        public void methodPaymens(string ofen, string sum,bool edit=true)
        {
            if (edit == true)
                pl.expansionPopUP();
            pl.clickStepHeder(2, "אמצעי תשלום");
            Thread.Sleep(800);
            fp.ClickButton(" הוספת אמצעי תשלום ", "span");
            Thread.Sleep(400);
            ofenT(ofen,sum);
            
            
        }

        public  void ofenT(string ofen,string sum)
        {
            fp.SelectRequerdName("aria-label='בחר אופן תשלום", ofen, false);
            switch (ofen)
            {
                case " אשראי ":
                    asrai("", -1);
                    break;
                case " הו\"ק ":
                    hok(sum, -1);
                    break;
            }
        }

        public void prentalData(string ofen, string sum, bool edit = true, int closeCatch = 2)
        {
            prentalDataiInset(ofen, sum, edit, closeCatch);
            Thread.Sleep(450);
            fp.closePoup(0);
        }
        public void prentalDataiInset(string ofen, string sum, bool edit = true, int closeCatch = 2)
        {
            prentalDataPayment( ofen,  sum,  edit,  closeCatch);
            isuk("אב");
            isuk("אם");
            fp.ClickButton("שמירה");

        }

        /// <summary>
        /// ללא שמירה
        /// </summary>
        /// <param name="ofen"></param>
        /// <param name="sum"></param>
        /// <param name="edit"></param>
        /// <param name="closeCatch"></param>
        public string  prentalDataPayment(string ofen, string sum, bool edit = true, int closeCatch = 2,string ofenhok="")
            { 
            string TZ = "";
            if (edit == true)
                pl.expansionPopUP();
            TZ =driver.FindElement(By.Name("IdentityNumber")).Text;
                pl.clickStepHeder(5, "נתוני הורים לשנה\"ל");
            Thread.Sleep(800);
            fp.SelectRequerdName("aria-label='בחר אופן תשלום", ofen, false);
            switch (ofen)
            {
                case " אשראי ":
                    asrai("",1, closeCatch);
                    break;
                case " הו\"ק ":
                    hok(sum,1, closeCatch,ofenhok);
                    break;
             }
            if (sum != "")
            {
                if (driver.FindElement(By.Name("TuitionFeeSumLimit")).Text != "")
                    driver.FindElement(By.Name("TuitionFeeSumLimit")).Clear();
                fp.insertElment("sum end up learn", By.Name("TuitionFeeSumLimit"), sum);
                fp.selectForRequerd("מטבע מגבלת שכר לימוד", false);
            }
            return TZ;
            
        }

        private void isuk(string prenatel)
        {
            string[] typeWork = { " ללא ", " שכיר ", " עצמאי " };
           fp.selectForRequerd("עיסוק ה" + prenatel + "", false, fp.rand(typeWork.Length));
            //throw new NotImplementedException();
        }

        private void hok(string migbla,int p=-1,int closeCatch=2,string ofenSelect="")
        {
            string[] ofen = { "מסב", "אשראי" };
            ofenSelect= ofenSelect=="" ? ofen[fp.rand(ofen.Length)]:ofenSelect;
            Thread.Sleep(400);
            fp.SelectRequerdName("aria-label='בחר אופן הוק", ofenSelect,false);
            if (ofenSelect.Trim()  == "אשראי")
                asrai(" הו\"ק",p, closeCatch);
            else
            {
                try
                {
                    clickAdd();
                    cc.msab("מסב", migbla,closeCatch);
                }
                catch {
                    fp.selectForRequerd(" אמצעי תשלום ", false, 0);
                    fp.closePoup(closeCatch);
                }
                    if(p==1)
                fp.selectForRequerd("יום חיוב:", false, fp.rand(28));
            }
            //throw new NotImplementedException();
        }
        public void clickAdd()
        {
            driver.FindElement(By.CssSelector("div[class='wrap-add-value ng-star-inserted']")).Click();
        }
        private void asrai(string hok="",int p=-1 ,int closeCatch=2)
        {
            try
            {
                int approve = 1;
                 clickAdd();
                string typeAsrai = "אשראי";
                if (closeCatch == 2)
                    approve = 2;
                cc.addAsrai(3, typeAsrai + hok,approve);
                fp.checkElment("sub title sucess", By.XPath("//div[@class='sub-title'][text()=' מה תרצ/י לעשות הלאה']"));
              
            }
            catch {
                fp.selectForRequerd(" אמצעי תשלום ", false,0);
                fp.closePoup(closeCatch);
                try{fp.selectForRequerd(" אמצעי תשלום ", false, 0);
                if (driver.FindElements(By.ClassName("mat-option-text"))[0].Displayed)
                    driver.FindElements(By.ClassName("mat-option-text"))[0].Click();}catch{}
                
            }
            if(p==1)
                fp.selectForRequerd("יום חיוב:", false, fp.rand(28));
            //throw new NotImplementedException();
        }
        public int clickTableRandom(int i=-1,int hora=-1)
        {
            Thread.Sleep(300);
            string num = driver.FindElement(By.ClassName("row-number-txt")).Text;
            if (fp.splitnumber(num) > 0)
            {
                IWebElement tableName = driver.FindElement(By.TagName("mat-table"));
                IList<IWebElement> tableRow = tableName.FindElements(By.TagName("mat-row"));
                if (i == -1)
                    i = fp.rand(tableRow.Count);
                if (hora > -1)
                {
                    IList<IWebElement> tableCell = tableRow[i].FindElements(By.TagName("mat-cell"));
                    tableCell[hora].Click();
                        }
                else
                    tableRow[i].Click();
            }
            return i;
        }
        public  void openBilingClauses(string nameRow, string heder, string bilingType, string amount, string tadirot, string ofen, string type)
        {
            if (nameRow == "1")
            {

                    IWebElement tableName = driver.FindElement(By.TagName("mat-table"));
                IList<IWebElement> tableRow = tableName.FindElements(By.TagName("mat-row"));
                for (int i = 1; i < tableRow.Count; i++)
                {
                    tableRow[i].Click();
                    if (heder == "")
                        heder = bilingType;
                    BilingClauses(heder, bilingType, amount, tadirot, ofen, type);
                    Thread.Sleep(200);
                    fp.closePoup(1);
                    fp.closePoup(0);
                    if (i + 1 == tableRow.Count)
                    {
                        if (fp.clickLeft() == true)
                        {
                            i = 0;
                             tableName = driver.FindElement(By.TagName("mat-table"));
                             tableRow = tableName.FindElements(By.TagName("mat-row"));
                        }
                    }
                }
            }
        }

        public  void BilingClauses(string heder,string bilingType,string amount,string tadirot,string ofen,string type="")
        {
            pl.expansionPopUP();
            pl.clickStepHeder(4, "הגדרות סעיפי חיוב");
            fp.ClickButton("הוספת הגדרת סעיף חיוב", "span");
            checkElmentBiling();
            insertElmentBiling( heder,  bilingType,  amount,  tadirot,  ofen,  type);
            //throw new NotImplementedException();
        }
        
        public  void insertElmentBiling(string heder, string bilingType, string amount, string tadirot, string ofen, string type )
        {
            BilingClauses bc = new BilingClauses();

            bc.insertElmentBilingClauses(heder, bilingType, amount, tadirot,  type);

            fp.SelectRequerdName("aria-label='בחר אופן תשלום", ofen, false);
            fp.ClickButton("צור הגדרת סעיף חיוב חדש","span");
        }
       
        private void checkElmentBiling()
        {
            BilingClauses bc = new BilingClauses();
            bc.checkAllBiling();
            fp.checkElmentsText("check title-section", By.ClassName("title-section"), "אמצעי תשלום", 4);
            fp.checkElmentText("check ofen", By.XPath("//mat-select[@aria-label='בחר אופן תשלום']"), "בחר אופן תשלום");
            //throw new NotImplementedException();
        }

        public void addDataWork(int typeWork = 2, string sumWork = "8000", string typePrental = "אב")
        {
            fp.ClickButton(" יצירת נתון הכנסה להורה חדש ");
            checkWork();//throw new NotImplementedException();
            insertDataWork(typeWork, sumWork, typePrental);
            SucessEndCreate sec = new SucessEndCreate();
            sec.checkEndCreate("נתון הכנסה להורה", false, "", "שנתון להורים");
        }

        public void insertDataWork(int typeWork=2,string sumWork="8000",string typePrental="אב")
        {
            fp.selectForRequerd(" סוג הכנסה ",false,typeWork);
            fp.insertElment("sum work", By.Name("Sum"), sumWork);
           // cc.calanederCheck(1, -1);
            cc.calnderMont();
            fp.selectForRequerd(" סוג הורה ",false,-1);
            fp.selectText(typePrental);
            /*driver.FindElement(By.CssSelector("i[class='icon-attached-file']")).Click();
            StringSelection ss = new StringSelection("c:/‏‏תיקיה חדשה/IMG_9904.JPG");
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
            Robot robot = new Robot();

            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);*/
              fp.ClickButton(" צור נתון הכנסה להורה חדש ","span");
        }

        private void checkWork()
        {
            cnd.checkClasscreate("נתון הכנסה להורה");
            fp.checkElmentsText("check sub title", By.ClassName("title-section"), "פרטים", 0);
            fp.checkElmentsText("check sub title", By.ClassName("title-section"), "פרטי הורה", 1);
            fp.checkSelectPreform(" סוג הכנסה ");
            fp.checkElmentText("check sum", By.Name("Sum"), " סכום ");
            fp.checkSelectPreform("בחר/י  חודש ושנה");
            fp.checkSelectPreform(" סוג הורה ");
            fp.checkElmentText("check attacment", By.CssSelector("mat-label[class='label attachment-no-value']"), "מסמך מקושר ");

        }

        public void stepWork(int typeWork = 2, string sumWork = "8000", string typePrental = "אב")
        {
            pl.expansionPopUP();
            pl.clickStepHeder(5, "נתוני הורים לשנה\"ל");
            addDataWork(typeWork,  sumWork ,  typePrental);

        }

        public void checkFrind()
        {
            pl.expansionPopUP();
            pl.clickStepHeder(2, "פרטי הורה");
            checkBetwen(By.Name("LastName"), By.ClassName("name-text"));
        }
        public void checkBetwen(By by,By byy)
        {
            string nameFamily, nameFamilyS;
            char[] tav = { ' ' };
            try { nameFamily = driver.FindElement(by).Text; } catch { nameFamily = driver.FindElement(By.Name("P_LastName")).Text; }
            nameFamilyS = driver.FindElement(byy).Text.Split(tav)[0];
            if (nameFamily == nameFamilyS)
                Console.WriteLine("the name "+nameFamily+" good");
            else
                Debug.WriteLine("the familyName "+nameFamily+" not equals to family name student");
        }
        public  void checkPrenatal()
        {
            pl.clickStepHeder(3, "פרטי הורים");
            checkBetwen(By.Name("FatherLastName"), By.ClassName("name-text"));
            //throw new NotImplementedException();
        }

        public  void learn()
        {
            fp.checkElmentText("check title", By.ClassName("title-section"), "מקום לימודים");
        }

        public  void checkTable(string dateFullCreate="")
        {
            try {
                string []listTble = { "","","","","","","", " הוראות קבע ראשי (חיוב) ","", " שקל "," הו\"ק ","בוצע",DateTime.Today.ToString("dd/MM/yy"), " טבלת הוראות קבע ",dateFullCreate };
                fp.checkColumn("mat-table[role='grid']", listTble, "false", "mat-row", "mat-cell");
            } catch { fp.picter("chek table takbolim of process hoq"); throw new NotImplementedException(); }
        }

        public  void clickTable(string nameFamily,int cell=-1)
        {
            try
            {
                if(cell==-1)
                fp.insertElment("sherch", By.CssSelector("input[placeholder='מה תרצה לחפש?']"), nameFamily);
                Thread.Sleep(630);
                string[] listN = { "","","","-1"};
                fp.checkColumn("//",listN,"", "mat-row", "mat-cell",cell);
            }catch { fp.picter("clickTable "+ nameFamily); throw new NotImplementedException(); }
        }

        internal void pageNewCreate(string NameNew,int popup=-1)
        {
            fp.ClickButton(" ליצור " + NameNew);
            
            Thread.Sleep(100);
            fp.closePoup(popup);
            // driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();
            fp.closeChrome();
        }

        internal void stepPaymentParent()
        {
            try
            {
                Thread.Sleep(450);
                int i = clickTableRandom();
                string ofen = ofenM[fp.rand(ofenM.Length)];
                prentalDataPayment(ofen, "", true, 1);
                fp.ClickButton("שמירה");
                Thread.Sleep(1950);
                fp.closePoup();
                if (ofen == " הו\"ק ")
                {
                    ExecutionHQ ehq = new ExecutionHQ();
                    ehq.insertExecutionHQ();
                    ehq.checkelmentExecutionHQ("אשראי", DateTime.Today.ToString("dd"));
                }
                else
                {
                  /*  string[] frind = { "", "", "-1" };
                    fp.checkColumn("//", frind, "", "mat-row", "mat-cell");*/
                    clickTableRandom(i);

                    FinanceParentalCharges fpc = new FinanceParentalCharges();
                    fp.clickNameDefinitions (" הורים ");
                    Laiblites l = new Laiblites();
                    l.clickTableUser(i);
                    fpc.offset("חיובים", 9);
                }
                fp.closeChrome();
            }
            catch { fp.picter("stepPaymentParent"); throw new NotImplementedException(); }
        }

        public string changeStudentToLeave()
        {
            string nameStudent = "";
            try {
                Thread.Sleep(350);
                clickTableRandom();

                pl.popupEdit();
                driver.FindElement(By.Name("FirstName")).SendKeys(Keys.Tab + Keys.Enter);
                fp.selectText(" עזב ");
               // fp.ClickButton(" שמירה ");
               // fp.popupMessage("שמירת התלמיד");
                nameStudent = driver.FindElement(By.ClassName("name-text")).Text;
                pl.clickStepHeder(2, "מקום לימודים");
                fp.insertElment("date lavel", By.Name("trip-start"), DateTime.Today.ToString("dd/MM/yyyy"),false,"",2);
                fp.click(By.CssSelector("div[class='wrap-add-value ng-star-inserted']"));
                driver.FindElements(By.Name("trip-start"))[2].SendKeys(Keys.Tab + Keys.Tab +Keys.Tab +Keys.Tab + Keys.Enter + "חתונה"); //fp.selectForRequerd(" הזן ערך נוסף לרשימה ",false,-1,);
                fp.click(By.CssSelector("div[class='wrap-done-value ng-star-inserted']"));
                fp.selectForRequerd("סיבת עזיבה",false,-1);
                fp.selectText("חתונה");
                fp.ClickButton(" שמירה ");
                Thread.Sleep(900);
                fp.popupMessage("שמירת התלמיד");
                Thread.Sleep(600);
                fp.refres();
            }
            catch{throw new NotImplementedException(); }
            return nameStudent;
        }

        public string nameForMarge(int i,bool vadi=false)
        {
            string nameMarge = "";
            try {

                /* IList<IWebElement> rows = driver.FindElements(By.TagName("mat-row"));
                 rows[i].FindElements(By.TagName("mat-cell"))[1].*/
                if (vadi == false) driver.FindElements(By.XPath("//i[text()=' account_circle ']"))[i].Click();
                else
                { fp.click(By.TagName("mat-row"), i);
                    pl.btnEdit();
                }
               int numberU=fp.splitNumber(driver.FindElement(By.ClassName("identifying-text")).Text);
                nameMarge = numberU + ";" + driver.FindElement(By.CssSelector("div[aria-label='divOpenParentDialog']")).Text;
                fp.closePoup();
                Thread.Sleep(500);
            }
            catch { throw new NotImplementedException(); }
            return nameMarge;
        }
    }
}