﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PelesoftSelenuim
{
     class Institution:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void checkElment()
        {

            fp.checkCreate("מוסד");
            string[] title = { "פרטי מוסד","פרטי קשר"};
            for(int i=0;i<title.Length;i++)
             fp.checkElmentsText("subTitle", By.ClassName("title-section"), title[i],i);
            fp.checkElmentText("check name institution", By.Name("InstitutionDescription"), "שם מוסד");
            fp.checkElmentText("check Address", By.Name("Address"), "כתובת");
            fp.checkSelectPreform("בחר/י עיר עבור המוסד", By.CssSelector(""));
            fp.checkElmentText("check Institution Code", By.Name("InstitutionCode"), "קוד מוסד");
            fp.checkElmentText("check PhoneNumber", By.Name("PhoneNumber"), "מספר טלפון");
            fp.checkElmentText("check AdditionalPhoneNumber", By.Name("AdditionalPhoneNumber"), "טלפון נוסף");
            fp.checkElmentText("check Fax", By.Name("Fax"), "פקס");
            fp.checkElmentText("check Mail", By.Name("Mail"), "מייל");
            fp.checkElmentText("check button create mosad", By.ClassName("mat-button-wrapper"), "צור מוסד חדש");
           
            //throw new NotImplementedException();
        }
        public void insertElment(string nameInstution="אוטומציה")
        {
            fp.insertElment("check name institution", By.Name("InstitutionDescription"), nameInstution);
            fp.insertElment("check Address", By.Name("Address"),automation);
            driver.FindElement(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']")).Click();
            new Actions(driver).MoveToElement(driver.FindElements(By.TagName("mat-label"))[5]).Click().Build().Perform();
            fp.selectForRequerd("הזן ערך נוסף לרשימה", false, -1, automation);
            new Actions(driver).MoveToElement(driver.FindElements(By.TagName("mat-label"))[5]).Click().Build().Perform();
            new Actions(driver).MoveToElement(driver.FindElements(By.TagName("mat-label"))[5]).SendKeys(Keys.Enter).Build().Perform();
            driver.FindElement(By.XPath("//div/i[text()=' done ']")).Click();
            fp.insertElment("check Institution Code", By.Name("InstitutionCode"),"123");
            fp.insertElment("check PhoneNumber", By.Name("PhoneNumber"), "0583297747",true);
            fp.insertElment(" PhoneNumber send", By.Name("PhoneSendingMessage"), "0583297747");
            //fp.checkElmentText("check AdditionalPhoneNumber", By.Name("AdditionalPhoneNumber"), "טלפון נוסף");
            //fp.checkElmentText("check Fax", By.Name("Fax"), "פקס");
            Thread.Sleep(900);
            //fp.checkElmentText("check Mail", By.Name("Mail"), "מייל");
            fp.ClickButton("צור קופה חדשה", "span");
            fp.ClickButton("צור קופה חדשה", "span");


        }

        public void openCreateMosad()
        {
            fp.actionWork("הגדרות", "קופות");//fp.clickButoonToContains();
            fp.ButtonNew("");
            // throw new NotImplementedException();
        }

        public  void changeChaseBox(string chaseBox)
        {
            try
            {
                fp.selectForRequerd("קופה", false);
            }
            catch { }
        }

        public void checkChange(string model, string ParentTable, string onClick="",bool notNormal=false)
        {
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.openTable(model,ParentTable);
            if (onClick != "")
            {
                fp.ClickButton(onClick);
            }
            try
            {
               if(notNormal==false)
                {
                    checkNoData("לא נמצאו נתונים",ParentTable);
                  
                 if (driver.FindElement(By.ClassName("row-number-txt")).Text.Contains("יש לך 0 " + ParentTable))
                     Console.WriteLine("kopa change the data " + ParentTable + " remove");
                  else
                       Debug.WriteLine("kopa change the data " + ParentTable + "not remove");
                }
                else
                {
                    switch(ParentTable)
                    { 
                        case "הפקדת מזומן ":
                            if (driver.FindElement(By.XPath("//div[@class='balance']/div")).Text.Contains("היתרה 0 שקל"))
                                Console.WriteLine("kopa change the data " + ParentTable + " remove");
                            else
                                Debug.WriteLine("kopa change the data " + ParentTable + "not remove");
                        break;
                        case "הפקדת צ'קים ":
                            checkNoData(" אין כרגע צ'קים להפקדה ", ParentTable);
                            break;
                        case "טיפול בצ'קים ":
                            if(driver.FindElements(By.TagName("tbody")).Count==0)
                                Console.WriteLine("kopa change the data " + ParentTable + " remove");
                            else
                                Debug.WriteLine("kopa change the data " + ParentTable + "not remove");
                            break;
                        case "ביצוע הו\"ק":
                            checkNoData(" אין נתונים ",ParentTable);
                            break;
                        case "שידורי אשראי ":
                            checkNoData(" אין כרגע חיובים לשידור ",ParentTable);
                            break;
                        case "צפייה וטיפול בתשלומים":
                            checkNoData(" אין כרגע נתונים ", ParentTable);
                            break;
                        case "טיפול בקבלות":
                            checkNoData(" אין כרגע נתונים ", ParentTable);
                            break; 
                        case "טיפול בקבלות עבור הו\"ק":
                            checkNoData(" אין כרגע נתונים ", ParentTable);
                            break;
                    }
                    
                }

            }
            catch
            {
                Debug.WriteLine("kopa change the data " + ParentTable + "not remove");

            }
            //throw new NotImplementedException();
        }

        public void checkNoData(string data,string ParentTable)
        {
            if (driver.FindElement(By.XPath("//div[contains(@class,'no-data-msg')]")).Text.Contains(data))
                Console.WriteLine("kopa change the data " + ParentTable + " remove");
            else
                Debug.WriteLine("kopa change the data " + ParentTable + "not remove");
        }

        public void cheackNotChange(string model, string ParentTable,string row= "mat-row")
        {
            if (model.Contains("ניהול כספים") || model.Contains("הגדרות"))
                fp.actionWork(model.Trim(), ParentTable);
            else
            {
                fp.clickNameDefinitions(model);

                if(ParentTable==" ידידים"||ParentTable==" תלמידים "||ParentTable.Contains("ספקים"))
                fp.clickButoonToContains(ParentTable,1);
                fp.clickButoonToContains(ParentTable);
            }
            IList<IWebElement> checkTable = driver.FindElements(By.TagName(row));
            if (checkTable.Count > 0)
                Console.WriteLine("the kopa change the data " + ParentTable + "not change(is very good)");
            else
                Debug.WriteLine("the kopa change and data "+ParentTable+" change");
        }
    }
}