﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    public class Section
    {
        public string NameSection { get; set; }
        public string[] SelectSection { get; set; }
        public string[] Permissions { set; get; }
        public string DesplaySectionEdit { set; get; }
        public string DesplayCreateSection { set; get; }
        public Section(string nameSection,string[] selectSection,string[] permission,string desplaySectionEdit,string desplayCreateSection)
        {
            NameSection = nameSection;
            SelectSection = selectSection;
            Permissions = permission;
            DesplaySectionEdit = desplaySectionEdit;
            DesplayCreateSection = desplayCreateSection;
        }
        public string sectionDetials()
        {

            return string.Format("{0} SelectSection {1}. Permissions: {2}DesplaySectionEdit:{3}DesplayCreateSection:{4}", NameSection, SelectSection, Permissions, DesplaySectionEdit, DesplayCreateSection);
        }
    }
}
