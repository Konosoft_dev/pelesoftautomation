﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class createGradeLavel:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
      
        public void GradeLavelCreate(string lavelName="אוטומציה" ,string mosad="",string typeMosad="גן ילדים",string age="4")
        {
            
            popupList pl = new popupList();
            
            pl.clickAddGradeLavel();
            checkElment();
            insertElment(lavelName,age);
            creategrade();
           
        }
        public void stepLavel(string mosad = "", string typeMosad = "גן ילדים")
        {
            EducationalInstitution ei = new EducationalInstitution();

            popupList pl = new popupList();
            ei.clickOnTable(mosad, typeMosad);

            pl.checkPage();
            pl.checkElment("");
            pl.chekName(mosad);
            pl.clickLavel();
        }
        public void checkElment()
        {
            try
            {
                fp.checkElment("logo", By.ClassName("wrap-item-icon"));
                fp.checkElmentText("title gard lavel", By.ClassName("main-title"), "יצירת "+ gradeLavel);
                fp.checkElmentText("sub title", By.ClassName("sub-title"), "אנא מלא/י את הפרטים הבאים");
                fp.checkElmentText("title section", By.ClassName("title-section"), "פרטי "+ gradeLavel);
                fp.checkElmentText("Level Name", By.Name("LevelName"), "שם רמה");
                fp.checkElmentText("Age", By.Name("Age"), "גיל");
                fp.checkElmentText("button creat lavel grade", By.ClassName("mat-button-wrapper"), " צור "+ gradeLavel + " חדשה ");
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public  void insertElment(string lavelNaME="אוטומציה",string age="4")
        {
            try
            {
                fp.checkElment("Level Name", By.Name("LevelName"),false,true);
                fp.insertElment("Level Name", By.Name("LevelName"),lavelNaME,true);
                driver.FindElement(By.Name("Age")).SendKeys(Keys.Enter);
                fp.funError(5);
                fp.insertElment("Age", By.Name("Age"),age);
                fp.ClickButton(" צור "+ gradeLavel + " חדשה ");
            }
              catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        internal void stepgradeLavel(string[] EI, string[] type, string[] grade, string[] ageMin, string[] ageBig, string[] ageBigBig, string[] minAge)
        {
            EducationalInstitution ei = new EducationalInstitution();
            popupList pl = new popupList();
            ei.openPageEI();
            string age = "";
            int n = 0;
            for (int j = 0; j < EI.Length; j++)
               for (int m = 0; m < type.Length; m++)
                {
                    ei.clickOnTable(EI[j], type[m]);
                    pl.clickLavel();
                    for (int i = 0; i < grade.Length; i++)
                    {
                        if ((EI[j] == "תלמוד תורה") || (EI[j] == "בית ספר"))
                            age = ageMin[i];
                        else if (EI[j].Contains("ישיבה"))
                        {
                            if (type[m].Contains("גדולה"))
                            {
                                if (i >= ageBigBig.Length)
                                    break;
                                age = ageBigBig[i];
                            }
                            else
                            {
                                if (i >= ageBig.Length)
                                    break;
                                age = ageBig[i];
                            }

                        }
                        else if (EI[j] == "גן בנים")
                        {
                            if (i > 1)
                                break;
                            age = minAge[i];
                        }
                        n++;
                        GradeLavelCreate(grade[i] + "-" + type[m], EI[j], type[m], age);
                        Thread.Sleep(300);
                        if (n > 8)
                        {
                            n = 0;
                            Thread.Sleep(700);
                        }
                        fp.closePoup(1);
                    }
                fp.closePoup();

            }

        }

        public void creategrade()
        {
            try
            {

            }
              catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }
    }
}