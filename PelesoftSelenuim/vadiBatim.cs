﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
    [TestClass]
    public class UnitTest3
    {
        page p = new page();
        FunctionPelesoft fp = new FunctionPelesoft();
        //public IWebDriver driver = null;
        SucessEndCreate sec = new SucessEndCreate();
        CreateNewDenumic cnd = new CreateNewDenumic();
        Financical fl = new Financical();
        Frind f = new Frind();
        AddTable at = new AddTable();
        receStrct rs = new receStrct();
        CreateMesaur cm = new CreateMesaur();
        ClearingEntity ce = new ClearingEntity();
        public void Test01Login(string nameTasret = "")
        {
            if (nameTasret != "")
                fp.OpenExcel(nameTasret, "failed");
            login ln = new login();
            ln.openvadi();
            //ln.UserNamee("ציבי");
            ln.UserNamee("צביה");
            //  ln.UserNamee("aaa");
            ln.Password("צביה1234!");
            // ln.Password("ציבי1234!");
            //  ln.Password("aaa");
            ln.cLickLogIn();
        }
        [TestMethod]//חייב טיפול
        public void Test02InsertList()
        {
            Test01Login("Test02InsertList");
            fp.actionWork(" הגדרות ", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            for (int i = 0; i < p.nmeList1.Length; i++)
            {
                nl.clickTable(p.nmeList1[i]);
                Thread.Sleep(200);
                nl.saveEditData(p.newList1[i]);
                Thread.Sleep(1900);
            }
            fp.closeChrome();
            fp.OpenExcel("Test02InsertList", "passed");
        }
        /*  ClearingEntity ce = new ClearingEntity();
          [TestMethod, Order(3)]
          public void Test02CreateNewClearingEntityclose()
          {
              Test01Login("Test02CreateNewClearingEntityclose");
              ce.CreateNewClearingEntity();
              sec.closePage();
              fp.OpenExcel("Test02CreateNewClearingEntityclose", "Passed");
          }
          [TestMethod, Order(22)]//ישויות סליקה
          public void Test04CreateNewClearingEntityCard()
          {
              Test01Login("Test04CreateNewClearingEntityCard");
              ce.CreateNewClearingEntity(" אשראי הו\"ק ");
              sec.pageCard("ישות סליקה");
              fp.OpenExcel("Test04CreateNewClearingEntityCard", "Passed");
          }

          [TestMethod, Order(22)]
          public void Test04CreateNewClearingEntityCreateNew()
          {
              Test01Login("Test04CreateNewClearingEntityCreateNew");
              ce.CreateNewClearingEntity("מסב");
              sec.pageNewCreate("ישות סליקה נוספת");
              fp.OpenExcel("Test04CreateNewClearingEntityCreateNew", "Passed");
          }

          [TestMethod, Order(22)]//ישויות סליקה
          public void Test04CreateNewClearingEntityCardMosed()
          {
              Test01Login("Test04CreateNewClearingEntityCardMosed");
              ce.CreateNewClearingEntity("מסב נדרים");
              sec.pageCard("מוסד");
              fp.OpenExcel("Test04CreateNewClearingEntityCardMosed", "Passed");
          }
          //חשבון בנק
          CreateBank cb = new CreateBank();

          [TestMethod, Order(4)]
          public void Test05CreateNewAccountBankClose()
          {
              Test01Login("Test05CreateNewAccountBankClose");
              cb.CreateNewAccountBank();
              sec.closePage();
              fp.OpenExcel("Test05CreateNewAccountBankClose", "Passed");
          }
          [TestMethod, Order(5)]//נוסף שדה חובה //לטפל בה בריצה הבאה
          public void Test06CreateNewFinancicalSectionClose()
          {
              Test01Login("Test06CreateNewFinancicalSectionClose");
              fl.createNewFinancicalSection(" חיובים לטיפול ", "פיתוח","פיננסי חיובים"," חיוב ","מסב-איחוד ועדי בתים");
              sec.closePage();
              fp.OpenExcel("Test06CreateNewFinancicalSectionClose", "Passed");
          }

          [TestMethod, Order(9)]
          public void Test10BroadcastSdreenViewingPayments()
          {
              Test01Login("Test10BroadcastSdreenViewingPayments");
              ViewingHandalingPayment vhp = new ViewingHandalingPayment();
              vhp.BroadcastSdreenViewingPayments("אשראי");
              fp.closeChrome();
              fp.OpenExcel("Test10BroadcastSdreenViewingPayments", "passed");
          }

          [TestMethod]
          public void Test10createContacts()
          {
              Test01Login("Test10createContacts");
              f.createContacts("חנוך");
              sec.closePage();
              fp.OpenExcel("Test10createContacts");

          }
          [TestMethod]
          public void Test14CreatePayment()//closeCardAsraiOne
          {
              Test01Login("Test14CreatePayment");


              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", p.idIdid, p.automation, "", "Credit");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.closePage();
              fp.OpenExcel("Test14CreatePayment", "passed");
          }
          [TestMethod]
          public void Test22CreatePaymentCardCaseOne()
          {
              Test01Login("Test22CreatePaymentCardCaseOne");
              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום חד פעמי", "מזומן", p.idIdid, "אוט", "", "Cash", "רוזנברגר");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.pageCard("תשלום", "btnGoToObjectCreated");
              fp.OpenExcel("Test22CreatePaymentCardCaseOne", "passed");
          }
          [TestMethod]
          public void Test22CreatePaymentNewCheckOne()
          {
              Test01Login("Test22CreatePaymentNewCheckOne");
              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום חד פעמי", "צק", p.idIdid, p.automation, "", "Check", "אוטומציה");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.pageNewCreate("תשלום נוסף");
              fp.OpenExcel("Test22CreatePaymentNewCheckOne", "passed");
          }
          [TestMethod]
          public void Test22CreatePaymentFrindBankTransferOne()
          {
              Test01Login("Test22CreatePaymentFrindBankTransferOne");
              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", p.idIdid, p.automation, "", "BankDeposit");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.pageCard(p.nameFrind, "btnGoToParent");
              fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
          }
          [TestMethod]
          public void Test22CreatePaymentCreadetCardHok()
          {

              Test01Login("Test22CreatePaymentCreadetCardHok");
              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "כללי", "", "divPaymentMethodTypesCredit", "אוטומציה", "false");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.closePage();
              fp.OpenExcel("Test22CreatePaymentCreadetCardHok");
          }
          [TestMethod]
          public void Test22CreatePaymentMsabHok()
          {

              Test01Login("Test22CreatePaymentFrindBankTransferOne");
              createConatrabition cc = new createConatrabition();
              cc.openPayments();
              cc.stepPayment("תשלום בהו\"ק", "מס\"ב", p.idIdid, p.automation, "", "divPaymentMethodTypesMasav", "", "false");
              sec.checkEndCreate("תשלום", false, "", p.parent);
              sec.closePage();
              fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
          }
          [TestMethod, Order(6)]
          public void Test07CreatAddTable()
          {
              Test01Login("Test07CreatAddTable");
              fp.actionWork("הגדרות", "הוספת טבלה ");
              at.checkElment();
              at.insertElment();
              fp.closeChrome();
              fp.OpenExcel("Test07CreatAddTable", "Passed");
          }
          /// <summary>
          /// הגדרות טבלה הוספת שדות
          /// </summary>
          /// 
          [TestMethod, Order(7)]//לאוסיף X על popup//להריץ שוב
          public void Test08CreateFildes()//רץ תקין
          {
              Test01Login("Test08CreateFildes");
              at.openTable();
              at.OpenSettings();
              at.checkESettingTable();
              at.OpenSettingsTable();
              at.clickCreateFilds();
              at.addFildesTable();
              at.save();
              at.close();
              fp.closeChrome();
              fp.OpenExcel("Test08CreateFildes", "Passed");
          }
          [TestMethod, Order(8)]
          public void Test09SettingFildesTable()
          {
              Test01Login("Test09SettingFildesTable");
              at.openTable();
              at.OpenSettings();
              at.OpenSettingsTable();
              at.addFildes();
              at.checkAddFildesActive();
              at.save();
              fp.closeChrome();
              fp.OpenExcel("Test09SettingFildesTable", "Passed");
          }
          [TestMethod, Order(9)]
          public void Test10CreateSectionsTable()//רץ טוב
          {
              Test01Login("Test10CreateSectionsTable");
              at.openTable();
              at.OpenSettings();
              at.openSettingTest();
              at.clickaddSections();
              fp.closeChrome();
              fp.OpenExcel("Test10CreateSectionsTable", "Passed");
          }
          [TestMethod, Order(10)]
          public void Test11CreateSectionsTableApprove()
          {
              Test01Login("Test11CreateSectionsTableApprove");
              at.openTable();
              at.OpenSettings();
              at.openSettingTest();
              at.approveDisplay();
              Thread.Sleep(400);
              fp.closeChrome();
              fp.OpenExcel("Test11CreateSectionsTableApprove", "Passed");
          }
          [TestMethod, Order(11)]
          public void Test12CreateNewrowInNewTable()
          {
              Test01Login("Test12CreateNewrowInNewTable");
              at.openTable();
              fp.ButtonNew("ccc");
              at.checkPagecreateNewInTable(p.automation);
              sec.checkEndCreate("ccc", false);
              sec.closePage();
              fp.OpenExcel("Test12CreateNewrowInNewTable", "Passed");
          }
          [TestMethod, Order(5)]
          public void Test16MangerList()
          {
              Test01Login("Test16MangerList");
              fp.actionWork("הגדרות", "ניהול רשימות ");
              settingList nl = new settingList();
              nl.checkElment();
              Thread.Sleep(200);
              nl.clickAdd();
              string[] newValue = { "שעון", "מחשב", "מצלמה", "פלאפון" };
              nl.addList(p.automation, newValue);
              fp.closeChrome();
              // nl.checkTable();
              fp.OpenExcel("Test16MangerList", "Passed");
          }
          /// <summary>
          /// הוספת מוסד
          /// </summary>
          [TestMethod, Order(17)]
          public void Test18AddInstitutionClose()
          {
              Test01Login("Test18AddInstitutionClose");
              Institution i = new Institution();
              i.openCreateMosad();
              i.checkElment();
              i.insertElment();
              sec.checkEndCreate("מוסד", false);
              sec.closePage();
              fp.OpenExcel("Test18AddInstitutionClose", "Passed");
          }
          [TestMethod, Order(22)]
          public void Test22AddInstitutionNewCard()
          {
              Test01Login("Test22AddInstitutionNewCard");
              Institution i = new Institution();
              i.openCreateMosad();
              i.checkElment();
              i.insertElment();
              sec.checkEndCreate("מוסד", false);
              sec.pageNewCreate("מוסד נוסף");
              fp.OpenExcel("Test22AddInstitutionNewCard", "Passed");
          }
          [TestMethod, Order(22)]
          public void Test22AddInstitutionCard()
          {
              Test01Login("Test22AddInstitutionCard");
              Institution i = new Institution();
              i.openCreateMosad();
              i.checkElment();
              i.insertElment();
              sec.checkEndCreate("מוסד", false);
              sec.pageCard("מוסד");
              fp.OpenExcel("Test22AddInstitutionCard", "Passed");
          }
          [TestMethod, Order(17)]
          public void Test22CreatePDFClose()
          {
              Test01Login("Test22CreatePDFClose");
              rs.stepPDF();
              sec.closePage();
              fp.OpenExcel("Test22CreatePDFClose", "Passed");
          }
          [TestMethod, Order(22)]
          public void Test22CreatePDFCard()
          {
              Test01Login("Test22CreatePDFCard");
              rs.stepPDF();
              sec.pageCard("מסמך PDF");
              fp.OpenExcel("Test22CreatePDFCard", "Passed");
          }
          [TestMethod, Order(22)]//להריץ בדיבג
          public void Test22CreatePDFStartDesign()
          {
              Test01Login("Test22CreatePDFStartDesign");
              rs.stepPDF();
              sec.startDesign();
              sec.closePage();
              fp.OpenExcel("Test22CreatePDFStartDesign", "Passed");
          }
         
          [TestMethod, Order(22)]
          public void Test22CreatePDFNew()
          {
              Test01Login("Test22CreatePDFNew");
              rs.stepPDF();
              sec.pageNewCreate("מסמך PDF נוסף");
              fp.OpenExcel("Test22CreatePDFNew", "Passed");
          }
          [TestMethod, Order(22)]
          public void Test22CreatePDFMosad()
          {
              Test01Login("Test22CreatePDFMosad");
              rs.stepPDF();
              sec.pageCard("מוסד");
              fp.OpenExcel("Test22CreatePDFMosad", "Passed");
          }

          [TestMethod]
          public void Test33ChangeMethodPayments()
          {
              Test01Login("Test33ChangeMethodPayments");
              createConatrabition cc = new createConatrabition();
              cc.conatrabitionList();
              cc.createNew("כרטיס אשראי", "Credit", false, -1, "", 2);
              fp.refres();
              ViewingHandalingPayment vhp = new ViewingHandalingPayment();
              vhp.cheangeMethodPayments();
              fp.closeChrome();
              fp.OpenExcel("Test33ChangeMethodPayments");
          }


          [TestMethod]
          public void Test19CreateMaddPaiClose()
          {
              Test01Login("Test19CreateMaddPaiClose");
              string[] insertShilta = { "מטבע", "אינו ריק", "", "אופן תרומה", "שווה ל", "אשראי", "-1" };// [{ },{ }];
              cm.stepPai("תרומות", "אופן תרומה", " סכום ", insertShilta, "סכום", "יוצר הרשומה", "תאריך יצירה");
              sec.closePage();
              CheckMadd("תרומות", "תרומות", "pie");//נפל פה
              fp.OpenExcel("Test19CreateMaddPaiClose");
          }
          public void CheckMadd(string nameTitle, string nameButton, string typeGraph)
          {
              Test01Login();

              cm.checkClassMadd(nameTitle, nameButton, typeGraph);
              cm.openTable(nameButton, typeGraph);
              fp.closeChrome();
          }
          [TestMethod]
          public void Test30ImageLOGO()
          {
              Test01Login("Test30ImageLOGO");
              cnd.imageLogo();
              cnd.chancgeSizeOrImage(1);
              cnd.chancgeSizeOrImage(0);
              fp.closeChrome();
              fp.OpenExcel("Test30ImageLOGO");
          }
          [TestMethod]
          public void Test19CreateMaddGrafLineClose()
          {
              Test01Login("Test19CreateMaddGrafLineClose");
              string[] inShilta = { "-1" };
              cm.stepGrafLine(" אמצעי תשלום ", " סוג אמצעי תשלום ", "כמות", inShilta, " ישות סליקה ", "", "סטטוס", " ExpiryDate ");
              sec.closePage();
              fp.OpenExcel("Test19CreateMaddGrafLineClose", "Passed");
          }
          [TestMethod]
          public void Test19CreateMaddGrapColumnClose()
          {
              Test01Login("Test19CreateMaddGrapColumnClose");
              string[] inShilta = { "-1" };
              cm.stepGrafColumn(" מסמכים ", " מעדכן הרשומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
              sec.closePage();
              fp.OpenExcel("Test19CreateMaddGrapColumnClose", "Passed");
          }
          [TestMethod]
          public void Test23CreateMaddPaiCard()
          {
              Test01Login("Test23CreateMaddPaiCard");
              string[] inShilta = { "כיתה וסניף", "אינו ריק", "", "מוסד לימודים", "אינו ריק", "", "-1" };
              cm.stepPai("תלמידים", "רמת כיתה", "כמות ", inShilta, "", " רמת כיתה ", " תאריך כניסה ");
              sec.pageCard("מדד");
              fp.OpenExcel("Test23CreateMaddPaiCard");
          }
          [TestMethod, Order(22)]
          public void Test23CreateMaddCardCard()
          {
              Test01Login("Test23CreateMaddCardCard");
              string[] inShilta = { " הערות ", " שווה ל ", p.automation + "TEXT", " שם פרטי ", " מתחיל ב ", "אTEXT", "-1" };
              cm.stepCard(" תלמידים ", "כמות", inShilta, "apartment", "", " ללא ", " תאריך ");
              sec.pageCard("מדד");
            //  chckDasMini("תלמידים", "תלמידים");
              fp.OpenExcel("Test23CreateMaddCardCard", "Passed");
          }
          [TestMethod, Order(1)]//לא רץ תקין
          public void Test03CreateNameUser()
          {
              Test01Login("Test03CreateNameUser");
              fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
              newUser nu = new newUser();
              nu.openCreate();
              nu.checkCreate();
              nu.insertCreate();
              Thread.Sleep(400);
              fp.closeChrome();
              fp.OpenExcel("Test03CreateNameUser", "Passed");
          }
          [TestMethod, Order(2)]
          public void Test05CheckNewUser()
          {
              ExcelApiTest eat = new ExcelApiTest("", "tasret");
              eat.bugReturn("tasret", "Failed", "Test05CheckNewUser");
              login ln = new login();
              ln.openChrome();
              ln.UserNamee(p.automation + "משתמש");
              ln.Password(p.automation + "משתמש1234");
              ln.cLickLogIn();
              ln.newPassword();
              fp.closeChrome();
              fp.OpenExcel("Test05CheckNewUser", "Passed");
          }
          [TestMethod, Order(3)]
          public void Test04CreateRolse()
          {
              Test01Login("Test04CreateRolse");
              fp.actionWork("ניהול מערכת", "תפקידים ");//fp.clickNameDefinitions();
              Thread.Sleep(300);
              fp.ClickButton("הוסף תפקיד ");
              newUser nu = new newUser();
              nu.addRolse(true);
              fp.ClickButton("הוסף תפקיד ");
              nu.addRolse(false, true);
              fp.ClickButton("הוסף תפקיד ");
              nu.addRolse();
              fp.closeChrome();
              fp.OpenExcel("Test04CreateRolse", "Passed");
          }
          [TestMethod]
          public void Test18CreateProcesseSendMailAfterIdceonClose()
          {
              Test01Login("Test18CreateProcesseSendMailAfterIdceonClose");
              process ps = new process();
              ps.openProcess();
              ps.cehekstep1();
              ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה", "דירות", "אחרי עדכון");
              string[] filter = { "דוא\"ל", "שווה ל", p.MAIL + "TEXT", "מייל לקבלה", "שווה ל", p.MAIL + "TEXT", "-1" };
              ps.stepFrainLast(filter, 2, false, 1);
              ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
              sec.closePage();
              fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
          }
          [TestMethod]
          public void Test19CheckSendMailProcess()
          {
              Test01Login("Test19CheckSendMailProcess");
              f.openFrind();
              PupilNew pn = new PupilNew();
              pn.clickTableRandom();
              f.changeW();
              fp.closeChrome();
              Console.WriteLine("יש לבצע בדיקה שהכן נשלח מייל");
              fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
              // login l = new login();
              // l.openMail();
          }

          [TestMethod]
          public void Test18CreateProcesseUpdateValueBeforIdceonCard()//נופל בגלל ה==
          {
              Test01Login("Test18CreateProcesseUpdateValueBeforIdceonCard");
              process ps = new process();
              ps.openProcess();
              ps.cehekstep1();
              ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה לפני עדכון", "מוסדות", "לפני עדכון");
              string[] filter = { "קוד מוסד", "אינו ריק", "", "עיר", "שווה ל", "ערד", "-1" };
              ps.stepFrainLast(filter);
              ps.step3(" עדכון ערכים ", "עיר", "עיר", "ערד");
              sec.pageCard("תהליך");
              fp.OpenExcel("Test18CreateProcesseUpdateValueBeforIdceonCard", "Passed");
          }
          //עדכון תאריך
          [TestMethod]
          public void Test18CreateProcesseUpdateDateEbruBeforAddNew()
          {
              Test01Login("Test18CreateProcesseUpdateDateEbruBeforAddNew");
              process ps = new process();
              ps.openProcess();
              ps.cehekstep1();
              ps.insertStep1("ריצה הוספה", "נסיון ריצה ע\"י אוטומציה לפני הוספה", " תלמידים ", " לפני הוספה ");
              string[] filter = { " ת. לידה עברי ", " ריק ", "", " ת. לידה לועזי ", " אינו ריק ", "" };
              ps.stepFrainLast(filter, 1);
              ps.step3("עדכון תאריך עברי", "2", "ידידים ת. לידה לועזי ", "ידידים ת. לידה עברי ", true, 2);
              sec.pageNewCreate("תהליך נוסף");
              fp.OpenExcel("Test18CreateProcesseUpdateDateEbruBeforAddNew", "Passed");
          }
         

          [TestMethod]//לא רץ תקין//הוספת קובץC:\picterToPelesoft\zץpng

          public void Test18CreateProcesseDochTkofatAutomationCurrent()
          {
              Test01Login("Test18CreateProcesseDochTkofatAutomation");
              process ps = new process();
              ps.openProcess();
              ps.cehekstep1();
              ps.insertStep1("ריצה אוטומטי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " ידידים ", " אוטומטי ");
              string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
              ps.stepFrainLast(filter, 1);
              ps.Automation("שוטף", DateTime.Today, "שבועי", 3, 0235);
              ps.timeHour("מס' פעמים", "8:18", 10, "שעות", "20:18");
              // ps.numberCheckStatusAction("3 ימים, 0235, 10, "8:18", "20:18");
              fp.ClickButton(" לשלב הבא ");
              ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
              sec.closePage();
              fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");
          }
          [TestMethod]
          public void Test18ProcessGenratorAndHQ()
          {
              string dateTimeH = "";
              Test01Login("Test18ProcessGenratorAndHQ");
              process ps = new process();
              ps.openProcess();
              ps.insertStep1("מחולל ביצוע הוק", " אוטומציה", " הורים ", " אוטומטי ");
              string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
              ps.stepFrainLast(filter, 1, false, 0, " לשלב הבא ", true, true);
              ps.Automation("שוטף", DateTime.Today, "שבועי", 2, 1);
              dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
              ps.timeHour("חד פעמי", dateTimeH);
              fp.ClickButton(" לשלב הבא ");
              ps.step3(" מחולל חיובים וביצוע הו\"ק ", "בית אברהם", "כללי", "אשראי");
              fp.closePoup();
              fp.clickNameDefinitions(" הוצאות והכנסות ");
              fp.clickNameDefinitions(" תקבולים ");
              PupilNew pn = new PupilNew();
              pn.checkTable(DateTime.Today.ToString("dd/MM/yyyy-") + dateTimeH);
              fp.closeChrome();
              fp.OpenExcel("Test18ProcessGenratorAndHQ", "Passed");
          }
          [TestMethod]
          public void Test18ProcessreturenFail()//נבדק רק מקקומית ולא בטבלאות הקשורות
          {
              string dateTimeH = "12:13";
              Test01Login("Test18ProcessreturenFail");
              process ps = new process();
              ps.openProcess();
              ps.insertStep1("החזרת נכשלים", " אוטומציה", " הורים ", " אוטומטי ");
              string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
              ps.stepFrainLast(filter, 1);
              dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
              ps.Automation("חד פעמי", DateTime.Today, dateTimeH);
              fp.ClickButton(" לשלב הבא ", "span");
              ps.step3(" שידור חוזר לנכשלים ", "אשראי הו\"ק", "סירוב", "5");
              fp.closePoup();
              ps.checkSucess(dateTimeH);
              fp.closeChrome();
              fp.OpenExcel("Test18ProcessreturenFail");
          }
          [TestMethod]
          public void Test18createProcessTasx()
          {
              Test01Login("Test18createProcessTasx");
              process ps = new process();
              ps.openProcess();
              ps.insertStep1("יצירת משימה", "תהליך יצירת משימה", " ידידים ", " אוטומטי ");
              string dt = DateTime.Now.AddMinutes(2).ToString("HH:mm");
              fp.ClickButton(" לשלב הבא ", "span");
              ps.Automation("חד פעמי", DateTime.Today, dt);
              fp.ClickButton(" לשלב הבא ", "span");
              ps.step3(" יצירת משימה ", " תהליך משימה ע\"י אוטומציה", " לביצוע ", " נסיון אוטומציההרבה הצלחה", true, 3);
              fp.refres();
              fp.clickNameDefinitions(" משימות ");
              ps.checkProcesTasx();
              fp.closeChrome();
              fp.OpenExcel("Test18createProcessTasx");
          }

          [TestMethod]//ישנם באגים עוד לא סימתי
          public void Test24ProcessCreateFrind()
          {
              Test01Login("Test24ProcessCreateFrind");

              process ps = new process();
              ps.openProcess();
              ps.insertStep1("יצירת ידיד", " יצירת ידיד אוטומציה", " תלמידים ", " לפני עדכון ");
              string[] filter = { "", "", "", " סיבת עזיבה", " שווה ל ", " חתונה " };
              ps.stepFrainLast(filter, 2);
              ps.step3(" יצירת ידיד ", " סוג תורם ", " בוגר ", "", false, -1, false);
              ps.step3(" עדכון ערכים ", " סטטוס ", " סטטוס ", "עזב");
              fp.refres();
              PupilNew pn = new PupilNew();
              pn.openPupil();
              string nameStudent = pn.changeStudentToLeave();
              f.openFrind();
              string[] checkRow = { "", "", nameStudent.Split()[0], nameStudent.Split()[1], "-1" };
              fp.checkColumn("//", checkRow, "false", "mat-row", "mat-cell");
              fp.closeChrome();
              fp.OpenExcel("Test24ProcessCreateFrind");
          }
          [TestMethod]
          public void Test33ProcessExpenditure()
          {
              Test01Login("Test33ProcessExpenditure");
              process pr = new process();
              pr.openProcess();
              pr.insertStep1("יצירת הוצאה", "תהליך יצירת הוצאה", " ידידים ", " אחרי עדכון ");
              string[] filter = { "עיר", "שונה מ", "ירושלים", "מקום עבודה", "שווה ל", "conosoftTEXT", "-1" };
              pr.stepFrainLast(filter, 2, false, 0);
              pr.step3("יצירת הוצאה", "גרסה", "קבלת שירות", "4");
              fp.refres();
              f.openFrind();
              PupilNew pn = new PupilNew();
              pn.clickTableRandom();
              string nameFrind = f.changeW();
              fp.refres();
              fp.ClickList("הוצאות והכנסות");
              fp.clickListinListEqualName("הוצאות");
              string[] rowExp = { "", nameFrind, "", "קבלת שירות", "4", "-1" };
              fp.checkColumn("//", rowExp, "false", "mat-row", "mat-cell");
          }
          [TestMethod]
          public void Test21ProcessUpdateValueTableFather()
          {
              Test01Login("Test21ProcessUpdateValueTableFather");
              process pr = new process();
              pr.openProcess();
              pr.insertStep1("עדכון מטבלת אב", "עדכון ערכים מטבלת אב", " תרומות בהו\"ק ", " אוטומטי ");
              string dt = DateTime.Now.AddMinutes(1).ToString("HH:mm");
              fp.ClickButton(" לשלב הבא ", "span");
              pr.Automation("חד פעמי", DateTime.Today, dt);
              fp.ClickButton(" לשלב הבא ", "span");
              pr.step3("עדכון ערכים", "הערות", "low_priority", "משפחה");
              fp.refres();
              createConatrabition cc = new createConatrabition();
              cc.openPageCDDebit();
              PupilNew pn = new PupilNew();
              pn.clickTableRandom();
              pr.checkChange();
              fp.closeChrome();
              fp.OpenExcel("Test21ProcessUpdateValueTableFather");
          }
          [TestMethod]
          public void Test24CreatePromotionalOneTimeRange()
          {
              Test01Login("Test24CreatePromotionalOneTimeRange");
              //fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
              Promotional pr = new Promotional();
              pr.stepPromotional(" חד פעמי ", " טווח חודשים ", "0/0-0/1", "כולל", " כולל (זיכוי) ");
              sec.closePage();
              fp.OpenExcel("Test24CreatePromotionalOneTimeRange");
          }
          [TestMethod]
          public void Test25CreditGenerator()
          {
              Test01Login("Test25CreditGenerator");
              fp.actionWork("ניהול כספים", "מחולל זיכויים");
              Promotional pr = new Promotional();
              pr.stepCreditGenerator();
              fp.closeChrome();
              fp.OpenExcel("Test25CreditGenerator");
          }*/
       
      
        //ישויות סליקה
        [TestMethod, Order(3)]
        public void Test02CreateNewClearingEntityclose()
        {
            Test01Login("Test02CreateNewClearingEntityclose");
            ce.CreateNewClearingEntity();
            sec.closePage();
            fp.OpenExcel("Test02CreateNewClearingEntityclose", "Passed");
        }
        [TestMethod, Order(22)]//ישויות סליקה
        public void Test04CreateNewClearingEntityCard()
        {
            Test01Login("Test04CreateNewClearingEntityCard");
            ce.CreateNewClearingEntity(" אשראי הו\"ק ");
            sec.pageCard("ישות סליקה");
            fp.OpenExcel("Test04CreateNewClearingEntityCard", "Passed");
        }

        [TestMethod, Order(22)]
        public void Test04CreateNewClearingEntityCreateNew()
        {
            Test01Login("Test04CreateNewClearingEntityCreateNew");
            ce.CreateNewClearingEntity("מסב");
            sec.pageNewCreate("ישות סליקה נוספת");
            fp.OpenExcel("Test04CreateNewClearingEntityCreateNew", "Passed");
        }

        [TestMethod, Order(22)]//ישויות סליקה
        public void Test04CreateNewClearingEntityCardMosed()
        {
            Test01Login("Test04CreateNewClearingEntityCardMosed");
            ce.CreateNewClearingEntity("מסב נדרים");
            sec.pageCard("מוסד");
            fp.OpenExcel("Test04CreateNewClearingEntityCardMosed", "Passed");
        }
        //חשבון בנק//נפל בסגירה
        CreateBank cb = new CreateBank();

        [TestMethod, Order(4)]
        public void Test05CreateNewAccountBankClose()
        {
            Test01Login("Test05CreateNewAccountBankClose");
            cb.CreateNewAccountBank();
            sec.closePage();
            fp.OpenExcel("Test05CreateNewAccountBankClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCMosed()
        {
            Test01Login("Test05CreateNewAccountBankGTCMosed");
            cb.CreateNewAccountBank();
            sec.pageCard("קופה");
            fp.OpenExcel("Test05CreateNewAccountBankGTCMosed", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCard()
        {
            Test01Login("Test05CreateNewAccountBankGTCard");
            cb.CreateNewAccountBank();
            sec.pageCard("חשבון בנק");
            fp.OpenExcel("Test05CreateNewAccountBankGTCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCNew()
        {
            Test01Login("Test05CreateNewAccountBankGTCNew");
            cb.CreateNewAccountBank();
            sec.pageNewCreate("חשבון בנק נוסף");
            fp.OpenExcel("Test05CreateNewAccountBankGTCNew", "Passed");
        }

        //סעיפים פיננסים
        [TestMethod, Order(5)]//נוסף שדה חובה //לטפל בה בריצה הבאה
        public void Test06CreateNewFinancicalSectionClose()
        {
            Test01Login("Test06CreateNewFinancicalSectionClose");
            fl.createNewFinancicalSection(" תשלומי דיירים ","ניהול בתים","תשלום לדייר","חיוב","מסוף אשראי");
            sec.closePage();
            fp.OpenExcel("Test06CreateNewFinancicalSectionClose", "Passed");
        }
        [TestMethod, Order(22)]//נוסף שדה חובה
        public void Test06CreateNewFinancicalSectionCardFinanci()
        {
            Test01Login("Test06CreateNewFinancicalSectionCardFinanci");
            fl.createNewFinancicalSection(" חיובים לטיפול ", "ניהול בתים", "חיובים לטיפול", "חיוב", " מסב - איחוד ועדי בתים ");
            sec.pageCard(p.FinancicalSection);
            fp.OpenExcel("Test06CreateNewFinancicalSectionCardFinanci", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionCreatNew()
        {
            Test01Login("Test06CreateNewFinancicalSectionCreatNew");
            fl.createNewFinancicalSection(" חיובים לטיפול ", "ניהול בתים", "זיכוי לטיפול", "זיכוי", "");
            sec.pageNewCreate(p.FinancicalSection + " נוסף");
            fp.OpenExcel("Test06CreateNewFinancicalSectionCreatNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionMosad()
        {
            Test01Login("Test06CreateNewFinancicalSectionMosad");
            fl.createNewFinancicalSection(" תשלומי דיירים ", "ניהול בתים", "תשלום לדייר", "חיוב", "אשראי");
            sec.pageCard("מוסד");
            fp.OpenExcel("Test06CreateNewFinancicalSectionMosad", "Passed");
        }
        [TestMethod]
        public void Test07NewBulding()
        { Test01Login("Test07NewBulding");
            Bulding b = new Bulding();
            b.openNewB();
            b.insertB();//בנין 
            sec.closePage();
            fp.OpenExcel("Test10createContacts");
        }[TestMethod]
        public void Test07NewBuldingNew()
        { Test01Login("Test07NewBulding");
            Bulding b = new Bulding();
            b.openNewB();
            b.insertB();//בנין 
            Thread.Sleep(350);
            sec.pageNewCreate("בנין נוסף");
            fp.OpenExcel("Test10createContacts");
        }[TestMethod]
        public void Test07NewBuldingCard()
        { Test01Login("Test07NewBulding");
            Bulding b = new Bulding();
            b.openNewB();
            b.insertB();//בנין 
            Thread.Sleep(350);
            sec.pageCard("בנין");
            fp.OpenExcel("Test10createContacts");
        }
        [TestMethod]
        public void test08NewHome()
        {
            Test01Login("test08NewHome");
            Bulding b = new Bulding();
            b.openHome();
            b.insertHome();//דירהבנין
            sec.closePage();
            fp.OpenExcel("test08NewHome");
        }[TestMethod]
        public void Test08NewHomeCard()
        {
            Test01Login("test08NewHome");
            Bulding b = new Bulding();
            b.openHome();
            b.insertHome("כן","כן");//דירהבנין
            sec.pageCard("דירה");
            fp.OpenExcel("test08NewHome");
        }[TestMethod]
        public void Test08NewHomeNew()
        {
            Test01Login("Test08NewHomeNew");
            Bulding b = new Bulding();
            b.openHome();
            b.insertHome("לא","כן");//דירהבנין
            sec.pageNewCreate("דירה נוספת");
            fp.OpenExcel("Test08NewHomeNew");
        }[TestMethod]
        public void Test08NewHomeBuliding()
        {
            Test01Login("test08NewHome");
            Bulding b = new Bulding();
            b.openHome();
            b.insertHome("כן");//דירהבנין
            sec.pageCard("בנין");
            fp.OpenExcel("test08NewHome");
        }
        [TestMethod]
        public void Test09Tenant()
        {
            Test01Login("Test09Tenant");
            Bulding b = new Bulding();
            b.openNew(" דיירים ");
            b.insertTenant("כן");//דיירלדירה בנין
            sec.closePage();
            fp.OpenExcel("Test09Tenant");
        }[TestMethod]
        public void Test09TenantCard()
        {
            Test01Login("Test09Tenant");
            Bulding b = new Bulding();
            b.openNew(" דיירים ");
            b.insertTenant("לא","חנבנוביץ");//דיירלדירה בנין
            fp.ClickButton(" קח אותי לכרטיס דייר לדירה ");
            PupilNew pn = new PupilNew();
            pn.ofenT(" אשראי ", "20");
            fp.selectForRequerd("יום חיוב:", true, -1);
            fp.selectText(DateTime.Today.ToString("dd"));
            fp.ClickButton("שמירה");
            popupList pl = new popupList();
            pl.clickStepHeder(3, "חיובים לדייר");
            fp.ClickButton("הוספת חיוב לדייר");
            f.insertCharges(DateTime.Today.ToString("MM/yyyy"), DateTime.Today.AddMonths(-1).ToString("dd/MM/yyyy"), " לדייר");
            Thread.Sleep(500);
            fp.closeChrome();
            fp.OpenExcel("Test09Tenant");
        }[TestMethod]
        public void Test09TenantNew()
        {
            Test01Login("Test09Tenant");
            Bulding b = new Bulding();
            b.openNew(" דיירים ");
            b.insertTenant("כן","סלמוני"," שוכר לשעבר ");//דיירלדירה בנין
            sec.pageNewCreate("דייר לדירה נוספת");
            fp.OpenExcel("Test09Tenant");
        }[TestMethod]
        public void Test09TenantBuliding()
        {
            Test01Login("Test09Tenant");
            Bulding b = new Bulding();
            b.openNew(" דיירים ");
            b.insertTenant("כן","שעבובי"," שוכר ");//דיירלדירה בנין
            sec.pageCard("בנין");
            fp.OpenExcel("Test09Tenant");
        }

        [TestMethod]
        public void Test10createContacts()
        {
            Test01Login("Test10createContacts");
            f.createContacts("חנוך");
            sec.closePage();
            fp.OpenExcel("Test10createContacts");

        }
        [TestMethod]
        public void Test10createContactsCard()
        {
            Test01Login("Test10createContacts");
            f.createContacts("משה");
            sec.pageCard("איש קשר");
            fp.OpenExcel("Test25createContactsCard");
        }
        [TestMethod]
        public void Test10createContactsNew()
        {
            Test01Login("Test10createContacts");
            f.createContacts("אברהם");
            sec.pageNewCreate("איש קשר נוסף");
            fp.OpenExcel("Test25createContactsNew");
        }
        [TestMethod]
        public void Test08CreatePayment()//closeCardAsraiOne
        {
            Test01Login("Test14CreatePayment");
            // fl.createNewFinancicalSection(" תשלומים ");
            //  fp.closepopup();
            createConatrabition cc = new createConatrabition();
            cc.openPaymentsVa();
            cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", p.idIdid, p.automation, "", "Credit", "שנור");
            sec.checkEndCreate("תשלום", false, "", "דייר");
            sec.closePage();
            fp.OpenExcel("Test14CreatePayment", "passed");
        }
        [TestMethod, Order(7)]
        public void Test09CreaditTransmissionCT()
        {
            ceaditTransmission ct = new ceaditTransmission();
            Test01Login("Test09CreaditTransmissionCT");
            fp.actionWork(" ניהול כספים ", "שידורי אשראי");
            ct.checkElment();
            ct.checkClearing("true", "אשראי", 0,"","ועד בית");
            ct.viewingHandalingPayments("true", "");
            fp.closeChrome();
            fp.OpenExcel("Test09CreaditTransmissionCT", "Passed");
        }
        //טיפול בקבלות
        public void CreateProductionReceipt(string card, string idConatarbition = "", int all = 0)
        {
            if (idConatarbition == "")
                idConatarbition = p.mesaa;
            //idConatarbition = "4";
            Test01Login();
            Production_receipt pr = new Production_receipt();
            Thread.Sleep(900);
            pr.ProductionReceipt();
            pr.checkPageProductionReceipt();
            string[] listColumn = { "", "ועדי בתים", "", "", card, "", DateTime.Today.ToString("dd/MM/yyyy"), "10", "1", "-1" };
            pr.tableProductionReceipt(listColumn, all);
            fp.spinner();
            pr.printCancel();
        }
        
        //הפקדת צקים
        public void hcreateDepositCheeck()
        {
            Deposit d = new Deposit();
            fp.refres();
            d.dipositType("צ");
            // d.diposetCash("צ\'קים", p.numberCheck + " " + p.nameBank + " " + p.numBranch + " " + p.numberAmountBank + "30/09/2020" /*DateTime.Today.ToString("dd/MM/yyyy") */+ " 10 שקל לגבייה תרומה");
            //"12345678 04 007 123456 30/09/2020 00:00:00 10 שקל לגבייה תרומה"
            //p.numberCheck+" "+p.nameBank+" "+p.numBranch+" "+ p.numberAmountBank +DateTime.Today.ToString("dd/MM/yyyy")+ " 10 שקל לגבייה תרומה",
            string[] row = { "", "", p.numberCheck, p.nameBankNO0, p.numBranch, p.numberAmountBank, DateTime.Today.ToString("dd/MM/yyyy"), "10", "שקל", "לגבייה", "תרומה", "-1" };
            d.tableclickCheck(row);
            Thread.Sleep(250);
            fp.ClickButton("הפקדה ");
            Thread.Sleep(400);
            //fp.checkElment("open pdf", By.TagName("embed"));
            fp.refres();
        }
        //טיפול בצקים
        public void ihandlingCheck()
        {
            HandingCheeck hc = new HandingCheeck();
            fp.actionWork("ניהול כספים", "טיפול בצ");
            //click button filter
            hc.checkPagehandingCheeck();
            fp.closeChrome();
        }
       
       
        //שידור אשראי דרך ניהול כספים
       

        //טיפול בתשלומי אשראי
        [TestMethod, Order(8)]
        public void Test09ReturnBroadcast()
        {
            Test01Login("Test09ReturnBroadcast");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.returnBroadcast("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test09ReturnBroadcast", "Passed");
        }
        [TestMethod, Order(9)]//מוסד סליקה
        public void Test10BroadcastSdreenViewingPayments()
        {
            Test01Login("Test10BroadcastSdreenViewingPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.BroadcastSdreenViewingPayments("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test10BroadcastSdreenViewingPayments", "passed");
        }
        /// <summary>
        /// לא הורץ
        /// </summary>טיפול בקבלת אשראי
        [TestMethod, Order(10)]
        public void Test11HandingViewPayments()
        {
            //Test08CreaditCardConaatarbition();
            Test01Login("Test11HandingViewPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            //vhp.handingViewPayments("אשראי");//לבדוק עם ריקי
            vhp.checkchange();
            fp.OpenExcel("Test11HandingViewPayments", "Passed");
        }
        [TestMethod, Order(11)]
        public void Test12productionReceipt()//הפקת קבלות
        {

            CreateProductionReceipt("אשראי", "10");
            Production_receipt pr = new Production_receipt();
            Test01Login("Test12productionReceipt");
            pr.ProductionReceipt();//לא נבדק שלא מופיע
            fp.OpenExcel("Test12productionReceipt", "passed");
        }
        //העברה בנקאית
        
       
        //טיפול בקבלות עבור הו"ק
        [TestMethod, Order(13)]
        public void Test14HandlingReceiptsHQ()
        {
            Test01Login("Test14HandlingReceiptsHQ");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
           hhrq.checkPage("מסב - איחוד ועדי בתים");
            string faml = "";
            try
            {
                ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
                 faml = eat.createExcel("nameFamily", "nameFamily", false);
            }
            catch { }
            //if (faml == "false")
                faml = "סלוצקין יעקב";
            string[] listRow = { "", "חיובים", "", faml, "מסב", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow, 0);
            fp.ClickButton(" הפק קבלות ");
            fp.closeChrome();
            fp.OpenExcel("Test14HandlingReceiptsHQ", "Passed");
        }
        //יצירת מס"ב
        /// fl.createNewFinancicalSection(" הוראות קבע ראשי ");

        [TestMethod, Order(6)]
        public void Test07HQMSABClose()
        {
            Test01Login("Test07HQMSABClose");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.closePage();
            fp.OpenExcel("Test07HQMSABClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABAddCard()
        {
            Test01Login("Test07HQMSABAddCard");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת סכום ");
            sec.pageNewCreate("הו\"ק נוספת");
            fp.OpenExcel("Test07HQMSABAddCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABCardHQ()
        {
            Test01Login("Test07HQMSABCardHQ");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת חיובים ");
            sec.pageCard("הו\"ק");
            fp.OpenExcel("Test07HQMSABCardHQ", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22HQMSABCardFrind()
        {
            Test01Login("Test22HQMSABCardFrind");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.pageCard(p.parent);
            fp.OpenExcel("Test22HQMSABCardFrind", "Passed");
        }

        [TestMethod, Order(14)]
        public void Test15ExecutionHQMsab()
        {
            Test01Login("Test15ExecutionHQMsab");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            fp.spinner();
            ehq.clickGvia();
            fp.spinner();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false", "מסב");
            string[] listHQ = { "", "", p.adress, "מסב", "", "", "בוצע", "עסקה תקינה. ", "-1" };
            vhp.checkTable(listHQ);
            vhp.returnBroadcast("מסב", "מסב");
            vhp.handingViewPayments("מסב", "מסב");
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
            string faml = eat.createExcel("nameFamily", "nameFamily", false);
            if (faml == "false")
                faml = "";
            string[] listRow = { faml, p.adress, "", p.hoqRase, "מסב", p.amount, "שקל", "1", "לגבייה", "-1" };
            ehq.checkTable(listRow);
            fp.closeChrome();
            fp.OpenExcel("Test15ExecutionHQMsab", "Passed");
        }
        //הפקת קבלות למס"ב
        [TestMethod, Order(15)]//רץ תקין
        public void Test16HandlingReceiptsHQMsab()
        {
            Test01Login("Test16HandlingReceiptsHQMsab");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            string[] listRow = { "", "טבלת הוראות קבע", "", "", "מסב", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow);
            fp.ClickButton(" הפק קבלות ");
            fp.picter("הפק קבלות");
            fp.closeChrome();
            fp.OpenExcel("Test16HandlingReceiptsHQMsab", "Passed");
        }

        [TestMethod, Order(16)]//באג/בא
        public void Test17ReturenMsab()
        {
            Test01Login("Test17ReturenMsab");
            returnMSAB rm = new returnMSAB();
            rm.openMsabPage();
            rm.checkPageReturnMsab();
            rm.insertData();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.filterMsab();
            fp.closeChrome();
            fp.OpenExcel("Test17ReturenMsab", "Passed");
        }
        ///יצירת הורה
        [TestMethod]
        public void Test06createParents()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents(p.names[fp.rand(p.names.Length)]);
            sec.checkEndCreate("הורה", false);
            sec.closePage();
        }
        [TestMethod]
        public void Test06createParentsCard()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("חנוך");
            sec.checkEndCreate("הורה", false);
            f.clickCardFrind();
            fp.closeChrome();
        }
        [TestMethod]
        public void Test06createParentsNew()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("רוזנברגר");
            sec.checkEndCreate("הורה", false);
            sec.pageNewCreate("הורה");
        }
       
      
      
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkCard()
        {
            Test01Login("Test22CreateAddWorkCard");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.Sleep(300);
            pn.clickTableRandom();
            pn.stepWork(2, "8000", "אם");
            sec.pageCard(" קח אותי לכרטיס נתון הכנסה להורה ");
            fp.OpenExcel("Test22CreateAddWorkCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkNew()
        {
            Test01Login("Test22CreateAddWorkNew");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.Sleep(290);
            pn.clickTableRandom();
            pn.stepWork(1, "5000", "אפוטרופוס");
            sec.pageNewCreate("נתון הכנסה להורה נוסף", 1);
            fp.OpenExcel("Test22CreateAddWorkNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkSnatonPrenatal()
        {
            Test01Login("Test18CreateAddWorkSnatonPrenatal");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.Sleep(450);
            pn.clickTableRandom();
            pn.stepWork(3, "1400", "אב");
            sec.pageCard("שנתון להורים");
            fp.OpenExcel("Test18CreateAddWorkSnatonPrenatal", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22StudenTestExtension()//P_lastName
        {
            Test01Login("Test22StudenTestExtension");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            Thread.Sleep(400);
            pn.clickTableRandom();
            pn.checkFrind();
            Thread.Sleep(100);
            pn.learn();
            fp.closeChrome();
            fp.OpenExcel("Test22StudenTestExtension", "Passed");
        }
        

        /// <summary>
        /// יצירת הגדרת סעיף חיוב חד פעמי
        /// </summary>
        /// 
        /*[TestMethod, Order(10)]
        public void Test11CreateCurrentBillingOneTimeClose()
        {
            Test01Login("Test11CreateCurrentBillingOneTimeClose");

            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test11CreateCurrentBillingOneTimeClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCard()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardNew()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardMC()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardMC", "Passed");
        }*/
        /// <summary>
        /// יצירת הגדרת סעיף חיוב שוטף
        /// </summary>
        /// 
        [TestMethod, Order(11)]
        public void Test12CreateCurrentBillingClauseClose()
        {
            Test01Login("Test12CreateCurrentBillingClauseClose");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test12CreateCurrentBillingClauseClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCard()
        {
            Test01Login("Test12CreateCurrentBillingClauseCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test12CreateCurrentBillingClauseCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardNew()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף", "בית אברהם");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardMC()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף", "3");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardMC", "Passed");
        }
        [TestMethod]
        public void Test13ParentalCharges()
        {
            Test01Login("Test13ParentalCharges");
            fp.actionWork(" ניהול כספים ", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges("", "בית אברהם");
            fp.closeChrome();
            fp.OpenExcel("Test13ParentalCharges", "Passed");
        }
       
        [TestMethod]
        public void Test22CreatePaymentCardCaseOne()
        {
            Test01Login("Test22CreatePaymentCardCaseOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "מזומן", p.idIdid, "אוט", "", "Cash", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard("תשלום", "btnGoToObjectCreated");
            fp.OpenExcel("Test22CreatePaymentCardCaseOne", "passed");
        }
        [TestMethod]
        public void Test22CreatePaymentNewCheckOne()
        {
            Test01Login("Test22CreatePaymentNewCheckOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "צק", p.idIdid, p.automation, "", "Check", "אוטומציה");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageNewCreate("תשלום נוסף");
            fp.OpenExcel("Test22CreatePaymentNewCheckOne", "passed");
        }
        [TestMethod]
        public void Test22CreatePaymentFrindBankTransferOne()
        {
            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", p.idIdid, p.automation, "", "BankDeposit", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard(p.nameFrind, "btnGoToParent");
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }
        [TestMethod]//באג
        public void Test22CreatePaymentCreadetCardHok()
        {

            Test01Login("Test22CreatePaymentCreadetCardHok");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "כללי", "", "divPaymentMethodTypesCredit", "שושן", "false", "");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentCreadetCardHok");
        }
        [TestMethod]//באג
        public void Test22CreatePaymentMsabHok()
        {

            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            cc.stepPayment("תשלום בהו\"ק", "מס\"ב", p.idIdid, p.automation, "", "divPaymentMethodTypesMasav", "רוזנברגר", "false");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }
        [TestMethod]//להריץ
        public void Test14CreatePaymentParentsCase()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            fp.clickNameDefinitions(" " + p.niulBuldin + " ");
            fp.clickNameDefinitions("דיירים");
            pn.clickTable("אברהם רוטמן", 1);
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.offset("אברהם רוטמן", 7, false);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }
        [TestMethod]//להריץ
        public void Test14CreatePaymentParentsCaseT()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            fp.clickNameDefinitions(" " + p.niulBuldin + " ");
            fp.clickNameDefinitions("דיירים");
            pn.clickTable("אברהם רוטמן", 1);
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.offset("תשלומים לדייר", 3, false);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }
        
        [TestMethod]
        public void Test14CreatePaymentParents()//לא נימצאו חיובים
        {
            Test01Login("Test14CreatePaymentParents");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.stepPaymentParent();
            fp.OpenExcel("Test14CreatePaymentParents");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
      /*  [TestMethod]
        public void TableCreate()
        {
            creatAddTable();
            CreateFildes();
            SettingFildesTable();
            createSectionsTable();
            createNewrowInNewTable();}*/
        ///הוספת טבלה
        [TestMethod, Order(6)]
        public void Test07CreatAddTable()
        {
            Test01Login("Test07CreatAddTable");
            fp.actionWork("הגדרות", "הוספת טבלה ");
            at.checkElment();
            at.insertElment();
            fp.closeChrome();
            fp.OpenExcel("Test07CreatAddTable", "Passed");
        }
        /// <summary>
        /// הגדרות טבלה הוספת שדות
        /// </summary>
        /// 
        [TestMethod, Order(7)]//לאוסיף X על popup//להריץ שוב
        public void Test08CreateFildes()//רץ תקין
        {
            Test01Login("Test08CreateFildes");
            at.openTable();
            at.OpenSettings();
            at.checkESettingTable();
            at.OpenSettingsTable();
            at.clickCreateFilds();
            at.addFildesTable();
            at.save();
            at.close();
            fp.closeChrome();
            fp.OpenExcel("Test08CreateFildes", "Passed");
        }
        [TestMethod, Order(8)]
        public void Test09SettingFildesTable()
        {
            Test01Login("Test09SettingFildesTable");
            at.openTable();
            at.OpenSettings();
            at.OpenSettingsTable();
            at.addFildes();
            at.checkAddFildesActive();
            at.save();
            fp.closeChrome();
            fp.OpenExcel("Test09SettingFildesTable", "Passed");
        }
        [TestMethod, Order(9)]
        public void Test10CreateSectionsTable()//רץ טוב
        {
            Test01Login("Test10CreateSectionsTable");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.clickaddSections();
            fp.closeChrome();
            fp.OpenExcel("Test10CreateSectionsTable", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test11CreateSectionsTableApprove()
        {
            Test01Login("Test11CreateSectionsTableApprove");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.approveDisplay();
            Thread.Sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test11CreateSectionsTableApprove", "Passed");
        }
        [TestMethod, Order(11)]
        public void Test12CreateNewrowInNewTable()
        {
            Test01Login("Test12CreateNewrowInNewTable");
            at.openTable();
            fp.ButtonNew("ccc");
            at.checkPagecreateNewInTable(p.automation);
            sec.checkEndCreate("ccc", false);
            sec.closePage();
            fp.OpenExcel("Test12CreateNewrowInNewTable", "Passed");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
        [TestMethod, Order(5)]
        public void Test16MangerList()
        {
            Test01Login("Test16MangerList");
            fp.actionWork("הגדרות", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            nl.clickAdd();
            string[] newValue = { "שעון", "מחשב", "מצלמה", "פלאפון" };
            nl.addList(p.automation, newValue);
            fp.closeChrome();
            // nl.checkTable();
            fp.OpenExcel("Test16MangerList", "Passed");
        }
        /// <summary>
        /// הוספת מוסד
        /// </summary>//לא ניתן עדיין ליבדוק
        [TestMethod, Order(17)]
        public void Test18AddInstitutionClose()
        {
            Test01Login("Test18AddInstitutionClose");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("קופה", false, "ה");
            sec.closePage();
            fp.OpenExcel("Test18AddInstitutionClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22AddInstitutionNewCard()
        {
            Test01Login("Test22AddInstitutionNewCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("קופה", false, "ה");
            sec.pageNewCreate("קופה נוספת");
            fp.OpenExcel("Test22AddInstitutionNewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22AddInstitutionCard()
        {
            Test01Login("Test22AddInstitutionCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("קופה", false);
            sec.pageCard("קופה");
            fp.OpenExcel("Test22AddInstitutionCard", "Passed");
        }
        [TestMethod, Order(17)]
        public void Test22CreatePDFClose()
        {
            Test01Login("Test22CreatePDFClose");
            rs.stepPDF();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFCard()
        {
            Test01Login("Test22CreatePDFCard");
            rs.stepPDF();
            sec.pageCard("מסמך PDF");
            fp.OpenExcel("Test22CreatePDFCard", "Passed");
        }
        [TestMethod, Order(22)]//להריץ 
        public void Test22CreatePDFStartDesign()
        {
            Test01Login("Test22CreatePDFStartDesign");
            rs.stepPDF();
            sec.startDesign();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFStartDesign", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFNew()
        {
            Test01Login("Test22CreatePDFNew");
            rs.stepPDF();
            sec.pageNewCreate("מסמך PDF נוסף");
            fp.OpenExcel("Test22CreatePDFNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFMosad()
        {
            Test01Login("Test22CreatePDFMosad");
            rs.stepPDF();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22CreatePDFMosad", "Passed");
        }
       

        [TestMethod, Order(10)]//להריץ
        public void Test19CreateMaddPaiClose()
        {
            Test01Login("Test19CreateMaddPaiClose");
            string[] insertShilta = { "יום", "שווה ל", "ראשון", "מזהה מעקב שיחה", "גדול מ", "1TEXT", "-1" };// [{ },{ }];
            cm.stepPai("מעקב שיחות", "מתקשר", " כמות ", insertShilta, "סכום", "יוצר הרשומה", "תאריך יצירה");
            sec.closePage();
            CheckMadd("מעקב שיחות", "מעקב שיחות", "pie");//נפל פה
            fp.OpenExcel("Test19CreateMaddPaiClose");
        }
        public void CheckMadd(string nameTitle, string nameButton, string typeGraph)
        {
            Test01Login();

            cm.checkClassMadd(nameTitle, nameButton, typeGraph);
            cm.openTable(nameButton, typeGraph);
            fp.closeChrome();
        }
       
      
        [TestMethod, Order(10)]//להריץ
        public void Test19CreateMaddGrafLineClose()
        {
            Test01Login("Test19CreateMaddGrafLineClose");
            string[] inShilta = { "-1" };
            cm.stepGrafLine(" אמצעי תשלום ", " סוג אמצעי תשלום ", "כמות", inShilta, " ישות סליקה ", "", "סטטוס", " ExpiryDate ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test19CreateMaddGrafLineClose", "Passed");
        }
       

        [TestMethod, Order(10)]//להריץ בדיבג
        public void Test19CreateMaddGrapColumnClose()
        {
            Test01Login("Test19CreateMaddGrapColumnClose");
            string[] inShilta = { "-1" };
            cm.stepGrafColumn(" הוצאות ", " סוג הוצאה ", " סכום ", inShilta, " תאריך תיקון ", "בניין", " תאריך תיקון ", " שנה זו ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test19CreateMaddGrapColumnClose", "Passed");
        }
       
       
        [TestMethod, Order(10)]
        public void Test23CreateMaddTableClose()
        {
            Test01Login("Test23CreateMaddTableClose");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "משלמים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            Thread.Sleep(250);
            sec.closePage();
            fp.OpenExcel("Test23CreateMaddTableClose", "Passed");
        }
      
       
        [TestMethod, Order(22)]//להריץ בדיבג
        public void Test23CreateMaddCardCard()
        {
            Test01Login("Test23CreateMaddCardCard");
            string[] inShilta = { " הערה כללית ", " שווה ל ", "אוטומציהTEXT", " סטטוס תחזוקה ", " שונה מ ",  "טופלTEXT",  "-1" };
            cm.stepCard(" מעקבי תחזוקה ", "כמות", inShilta, "apartment", "בניין", " חודש זה ", "ביצוע תאריך ");
            sec.pageCard("מדד");
            chckDasMini(" מעקבי תחזוקה "," מעקבי תחזוקה ");
            fp.OpenExcel("Test23CreateMaddCardCard", "Passed");
        }
       
        //
        public void chckDasMini(string nameMIMI, string nameButton)
        {
            Test01Login();
            cm.chekmini(nameMIMI, nameButton);
            fp.closeChrome();
        }
        /// <summary>
        /// ניהול מערכת
        /// </summary>
        [TestMethod, Order(1)]//להריץ
        public void Test03CreateNameUser()
        {
            Test01Login("Test03CreateNameUser");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.openCreate();
            nu.checkCreate();
            nu.insertCreate();
            Thread.Sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test03CreateNameUser", "Passed");
        }
        [TestMethod, Order(2)]
        public void Test05CheckNewUser()
        {
            ExcelApiTest eat = new ExcelApiTest("", "tasret");
            eat.bugReturn("tasret", "Failed", "Test05CheckNewUser");
            login ln = new login();
            ln.openChrome();
            ln.UserNamee(p.automation + "משתמש");
            ln.Password(p.automation + "משתמש1234");
            ln.cLickLogIn();
            ln.newPassword();
            fp.closeChrome();
            fp.OpenExcel("Test05CheckNewUser", "Passed");
        }
        [TestMethod, Order(20)]//להריץ
        public void Test22ChangeChaseBox()
        {
            Institution i = new Institution();
            Test01Login("Test22ChangeChaseBox");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.clickEditUser();
            nu.addMosad(p.automation);
            fp.refres();
            i.changeChaseBox(p.automation);
            i.checkChange(" ניהול בתים ", " בניינים ");
            i.checkChange("", " דירות ");
            i.checkChange("", " דיירים ");
            i.checkChange("", " תקבולים ");
            i.checkChange("", " חיובים לדייר ");
            i.checkChange("", " חיוב בניין ");
            i.checkChange("", " הגדרות תחזוקה לבניין ");
            i.checkChange("", " מעקבי תחזוקה ");
            i.checkChange("", " תקלות ");
            i.checkChange("", " הוצאות ");
            i.checkChange("", " ספקים ");
            i.checkChange("", " מעקב שיחות ");
            i.checkChange(" ניהול כספים ", "הפקדת מזומן ", "", true);
            i.checkChange(" ניהול כספים ", "ביצוע הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "דיווח חזרות מסב", "", true);
            i.checkChange(" ניהול כספים ", "שידורי אשראי ", "", true);
            i.checkChange(" ניהול כספים ", "צפייה וטיפול בתשלומים", "", true);
            i.checkChange(" ניהול כספים ", "ניהול סעיפים פיננסיים");
            i.checkChange(" ניהול כספים ", "טיפול בקבלות", " הצג נתונים ", true);
            i.checkChange(" ניהול כספים ", "טיפול בקבלות עבור הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "צפיה בקבלות");
            i.checkChange(" הגדרות ", "ישויות סליקה");
            i.checkChange(" הגדרות ", "חשבונות בנק");
            i.checkChange(" הגדרות ", "מבני קבלות");
            i.checkChange(" הגדרות ", "מסמכי PDF");
            i.cheackNotChange(" הגדרות ", "קופות");
            i.cheackNotChange(" הגדרות ", "מדדים");
            i.cheackNotChange(" ניהול כספים ", "אמצעי תשלום");
            fp.closeChrome();
            fp.OpenExcel("Test22ChangeChaseBox", "Passed");
        }
        [TestMethod, Order(3)]
        public void Test04CreateRolse()
        {
            Test01Login("Test04CreateRolse");
            fp.actionWork("ניהול מערכת", "תפקידים ");//fp.clickNameDefinitions();
            Thread.Sleep(300);
            fp.ClickButton("הוסף תפקיד ");
            newUser nu = new newUser();
            nu.addRolse(true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse(false, true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse();
            fp.closeChrome();
            fp.OpenExcel("Test04CreateRolse", "Passed");
        }
        [TestMethod, Order(4)]//
        public void Test05CheackAddRolse()
        {
            Test01Login("Test05CheackAddRolse");
            fp.ClickList("פיתוח");
            fp.clickListinListEqualName("תשלומים לדייר");
            at.OpenSettings();
            at.openSettingTest("פיתוח");
            at.ceackAddSection(p.automation);
            fp.closeChrome();
            fp.OpenExcel("Test05CheackAddRolse", "Passed");
        }
       
       
        //התחיבויות
        [TestMethod, Order(9)]
        public void Test10CreateNewLaiblitesClose()
        {
            Test01Login("Test10CreateNewLaiblitesClose");
            fp.ClickList(p.niulBuldin);
            fp.clickNameDefinitions(" חיובים לדייר ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "זיגלבוים");
            sec.closePage();
            fp.OpenExcel("Test10CreateNewLaiblitesClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesCard()
        {
            Test01Login("Test22CreateNewLaiblitesCard");
            fp.ClickList(p.niulBuldin);
            fp.clickNameDefinitions(" חיובים לדייר ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "רוזנברגר");
            sec.pageCard("התחייבות");
            fp.OpenExcel("Test22CreateNewLaiblitesCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesNew()
        {
            Test01Login("Test22CreateNewLaiblitesNew");
            fp.ClickList(p.niulBuldin);
            fp.clickNameDefinitions(" חיובים לדייר ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites("", "רוזנברגר");
            sec.pageNewCreate("התחייבות נוספת");
            fp.OpenExcel("Test22CreateNewLaiblitesNew", "Passed");
        }
      /*  [TestMethod, Order(22)]
        public void Test10CreateNewLaiblitesFrind()
        {
            Test01Login("Test22CreateNewLaiblitesFrind");
            fp.ClickList(p.nameFrind + "ים");
            Laiblites l = new Laiblites();
            fp.clickNameDefinitions(" תרומות ");
            string name = l.shearchNameRequerd();
            fp.clickNameDefinitions(" התחייבויות ");
            l.stepNewLaiblites("", name);
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateNewLaiblitesFrind", "Passed");
        }
       */
        [TestMethod]//כולל בדיקה של סמיילי
        public void Test19OffsettingLiabilitiesFrind()
        {
            Test01Login("Test11CreatCehildrenClose");
            Frind f = new Frind();
            int numHora = f.stepLiabilities();
            fp.closePoup(1);
            f.checkChargesTitle(numHora);
            f.checkSmaile(numHora);
            fp.closeChrome();
            fp.OpenExcel("Test11CreatCehildrenClose");
        }
     
    
       
        [TestMethod, Order(20)]//להריץ
        public void Test20CreateTaskClose()
        {
            Test01Login("Test20CreateTaskClose");
            Thread.Sleep(200);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " דירות ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("דירה", "1", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("חיוב חודש קודם");
            sec.closePage();
            fp.OpenExcel("Test20CreateTaskClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskCard()//להריץ
        {
            Test01Login("Test22CreateTaskCard");
            Thread.Sleep(200);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " בניינים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("בנין", "1", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("התשלום לחודש הבא שולם");
            sec.pageCard("משימה");
            fp.OpenExcel("Test22CreateTaskCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskNew()//רץ תקין
        {
            Test01Login("Test22CreateTaskNew");
            Thread.Sleep(600);
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " דיירים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("דייר", "משה", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("הדייר עבר דירה יש להתעדכן ולעדכן את הכתובת החדשה");
            sec.closePage();
            checkTeask(p.niulBuldin, "דיירים", "משה");
            fp.OpenExcel("Test22CreateTaskNew", "Passed");
        }
        public void checkTeask(string nitov, string Tatnitov, string name)
        {
            Test01Login();
            fp.ClickList(nitov);
            if (nitov != Tatnitov)
                fp.clickNameDefinitions(Tatnitov);
            else
                fp.clickListinListEqualName(Tatnitov);
            string[] listc = { "", "", "", "-1" };
            if (name.Contains("1"))
                listc[1] = name;
            else
                listc[2] = name;
            fp.checkColumn("//", listc, "", "mat-row", "mat-cell");
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(16, "משימות");
            string[] checktaskl = { "", "", "לביצוע", "", " pageview ", " post_add ", " delete ", "-1" };
            fp.checkColumn("//", checktaskl, "false", "mat-row", "mat-cell", -1, 1);
            fp.closeChrome();
        }
        [TestMethod]//להריץ
        public void Test18CreateProcesseSendMailAfterIdceonClose()
        {
            Test01Login("Test18CreateProcesseSendMailAfterIdceonClose");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה", "דיירים", "אחרי עדכון");
            string[] filter = { "דוא\"ל", "שווה ל", p.MAIL + "TEXT", "מייל לקבלה", "שווה ל", p.MAIL + "TEXT", "-1" };
            ps.stepFrainLast(filter, 2, false, 1);
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
        }
        //לבדוק חיובים לשלחית מייל בכל תחילת חודש
        [TestMethod]
        public void Test19CheckSendMailProcess()
        {
            Test01Login("Test19CheckSendMailProcess");
            f.openFrind();
            PupilNew pn = new PupilNew();
            Thread.Sleep(350);
            pn.clickTableRandom();
            f.changeW();
            fp.closeChrome();
            Console.WriteLine("יש לבצע בדיקה שהכן נשלח מייל");
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
            // login l = new login();
            // l.openMail();
        }

        [TestMethod]
        public void Test18CreateProcesseUpdateValueBeforIdceonCard()//נופל בגלל ה==
        {
            Test01Login("Test18CreateProcesseUpdateValueBeforIdceonCard");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה לפני עדכון", "קופות", "לפני עדכון");
            string[] filter = { "קוד מוסד", "אינו ריק", "", "עיר", "שווה ל", "ערד", "-1" };
            ps.stepFrainLast(filter);
            ps.step3(" עדכון ערכים ", "עיר", "עיר", "ערד");
            sec.pageCard("תהליך");
            fp.OpenExcel("Test18CreateProcesseUpdateValueBeforIdceonCard", "Passed");
        }
        //עדכון תאריך
        [TestMethod]
        public void Test18CreateProcesseUpdateDateEbruBeforAddNew()
        {
            Test01Login("Test18CreateProcesseUpdateDateEbruBeforAddNew");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה הוספה", "נסיון ריצה ע\"י אוטומציה לפני הוספה", " דיירים ", " לפני הוספה ");
            string[] filter = { " ת. לידה עברי ", " ריק ", "", " ת. לידה לועזי ", " אינו ריק ", "" };
            ps.stepFrainLast(filter, 1);
            ps.step3("עדכון תאריך עברי", "2", "ידידים ת. לידה לועזי ", "ידידים ת. לידה עברי ", true, 2);
            sec.pageNewCreate("תהליך נוסף");
            fp.OpenExcel("Test18CreateProcesseUpdateDateEbruBeforAddNew", "Passed");
        }
       

        [TestMethod]//הוספת קובץ

        public void Test18CreateProcesseDochTkofatAutomationCurrent()
        {
            Test01Login("Test18CreateProcesseDochTkofatAutomation");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("דוח תקופתי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " בניינים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 3, 0235);
            ps.timeHour("מס' פעמים", "8:18", 10, "שעות", "20:18");
            // ps.numberCheckStatusAction("3 ימים, 0235, 10, "8:18", "20:18");
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");
        }
        [TestMethod]//??????????????????
        public void Test18ProcessGenratorAndHQ()
        {
            string dateTimeH = "";
            Test01Login("Test18ProcessGenratorAndHQ");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("מחולל ביצוע הוק", " אוטומציה", " הורים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1, false, 0, " לשלב הבא ", true, true);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 2, 1);
            dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            ps.timeHour("חד פעמי", dateTimeH);
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" מחולל חיובים וביצוע הו\"ק ", "בית אברהם", "כללי", "אשראי");
            fp.closePoup();
            fp.clickNameDefinitions(" הוצאות והכנסות ");
            fp.clickNameDefinitions(" תקבולים ");
            PupilNew pn = new PupilNew();
            pn.checkTable(DateTime.Today.ToString("dd/MM/yyyy-") + dateTimeH);
            fp.closeChrome();
            fp.OpenExcel("Test18ProcessGenratorAndHQ", "Passed");
        }
        [TestMethod]//להריץ
        public void Test18ProcessreturenFail()//נבדק רק מקקומית ולא בטבלאות הקשורות
        {
            string dateTimeH = "12:45";
            Test01Login("Test18ProcessreturenFail");
            process ps = new process();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            string[] name = { "", "", "" };
            name = vhp.changestatues();
            ps.openProcess();
            ps.insertStep1("החזרת נכשלים", " אוטומציה", "", " אחרי הוספה ");
         
            ps.step3(" שידור חוזר לנכשלים ", "אשראי הו\"ק", "סירוב", "5");
            fp.closePoup();
            // ps.checkSucess(dateTimeH);
            vhp.checkstatusProcess(name);
            fp.closeChrome();
            fp.OpenExcel("Test18ProcessreturenFail");
        }
        [TestMethod]
        public void Test18createProcessTasx()
        {
            Test01Login("Test18createProcessTasx");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("יצירת משימה", "תהליך יצירת משימה", " דירות ", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            ps.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            ps.step3(" יצירת משימה ", " תהליך משימה ע\"י אוטומציה", " לביצוע ", " נסיון אוטומציההרבה הצלחה", true, 3);
            fp.refres();
            fp.click(By.XPath("//button/span/div[@class='material-icons'][text()='event_available']"));
            ps.checkProcesTasx();
            fp.closeChrome();
            fp.OpenExcel("Test18createProcessTasx");
        }

        [TestMethod]//??????????????
        public void Test24ProcessCreateFrind()
        {
            Test01Login("Test24ProcessCreateFrind");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("יצירת ידיד", " יצירת ידיד אוטומציה", " תלמידים ", " לפני עדכון ");
            string[] filter = { "", "", "", " סיבת עזיבה", " שווה ל ", " חתונה " };
            ps.stepFrainLast(filter, 2);
            ps.step3(" יצירת ידיד ", " סוג תורם ", " בוגר ", "", false, -1, false);
            ps.step3(" עדכון ערכים ", " סטטוס ", " סטטוס ", "עזב");
            fp.refres();
            PupilNew pn = new PupilNew();
            pn.openPupil();
            string nameStudent = pn.changeStudentToLeave();
            f.openFrind();
            pn.clickTable(nameStudent);
            fp.closeChrome();
            fp.OpenExcel("Test24ProcessCreateFrind");
        }
        [TestMethod]//????????????????
        public void Test33ProcessExpenditure()
        {
            Test01Login("Test33ProcessExpenditure");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("יצירת הוצאה", "תהליך יצירת הוצאה", " הוצאות ", " אחרי עדכון ");
            string[] filter = { "עיר", "שונה מ", "ירושלים", "מקום עבודה", "שווה ל", "conosoftTEXT", "-1" };
            pr.stepFrainLast(filter, 2, false, 0);
            pr.step3("יצירת הוצאה", "גרסה", "קבלת שירות", "4");
            fp.refres();
            f.openFrind();
            Thread.Sleep(500);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            string nameFrind = f.changeW();
            fp.refres();
            fp.ClickList("הוצאות והכנסות");
            fp.clickListinListEqualName("הוצאות");
            string[] rowExp = { "", nameFrind, "", "קבלת שירות", "4", "-1" };
            fp.checkColumn("//", rowExp, "false", "mat-row", "mat-cell");
            fp.closeChrome();
            fp.OpenExcel("Test33ProcessExpenditure");
        }
        [TestMethod]//????????????????????
        public void Test21ProcessUpdateValueTableFather()
        {
            Test01Login("Test21ProcessUpdateValueTableFather");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("עדכון מטבלת אב", "עדכון ערכים מטבלת אב", " דיירים ", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(1).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            pr.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            pr.step3("עדכון ערכים", "הערות", "low_priority", "משפחה");
            fp.refres();
            createConatrabition cc = new createConatrabition();
            cc.openPageCDDebit();
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            popupList pl = new popupList();
            pl.clickEdit();
            pr.checkChange();
            fp.closeChrome();
            fp.OpenExcel("Test21ProcessUpdateValueTableFather");
        }
        [TestMethod]//להריץ
        public void Test36ProcessTableNoConnected()
        {
            Test01Login("Test36ProcessTableNoConnected");
            process pr = new process();
            pr.openProcess();
            pr.insertStep1("טבלה לא מקושרת", "שליחת מייל עם טבלה לא מקושרת", "", " אוטומטי ");
            string dt = DateTime.Now.AddMinutes(1).ToString("HH:mm");
            fp.ClickButton(" לשלב הבא ", "span");
            pr.Automation("חד פעמי", DateTime.Today, dt);
            fp.ClickButton(" לשלב הבא ", "span");
            pr.step3("שליחת מייל", "", " מייל ללא טבלה מקושרת", "אין פה טבלה מקושרת", true);
            fp.closeChrome();
            fp.OpenExcel("Test36ProcessTableNoConnected");
        }
    
      
     
        /// <summary>
        /// פרופיל אישי
        /// </summary>
        [TestMethod]
        public void Test27cheangePrivateProfil()//להריץ
        {
            Test01Login("Test27cheangePrivateProfil");
            PrivateProfil PP = new PrivateProfil();
            Thread.Sleep(750);
            PP.iconUser();
            string name = p.names[fp.rand(p.names.Length)] + " " + p.familys[fp.rand(p.familys.Length)];
            PP.changeName(name, "0556771165", "rvaitz22@gmail.com");
            string color = PP.changeColor("yellow_blue");
            string typeFont = PP.changeWrote(" מודגש ");
            PP.changeDisplayTable(true, true);
            fp.refres();
            char[] fon = { '-' };
            PP.checkChange(name, color, typeFont.Split(fon)[1]);
            PP.changeInTable(true, true, color);
            fp.closeChrome();
            fp.OpenExcel("Test27cheangePrivateProfil");
        }
        /// <summary>
        /// החזרת אשראי(לחיצה על הכפתור הקטן של העט)
        /// </summary>
        [TestMethod]//להריץ בדיבג
        public void Test20ReturnAsrai()
        {
            Test09CreaditTransmissionCT();
            Test01Login("Test20ReturnAsrai");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.returnAsrai();
            fp.closeChrome();
            fp.OpenExcel("Test20ReturnAsrai");
        }
        /// <summary>
        ///בדיקת פתיחת רשומה ושינויים נתונים
        /// </summary>
        [TestMethod]//לכתוב פונקציה חדשה
        public void Test28checkOpenListAndChange()
        {
            Test01Login("Test28checkOpenListAndChange");
            popupList pl = new popupList();
            pl.checkOpen();
            fp.closeChrome();
            fp.OpenExcel("Test28checkOpenListAndChange");
        }
        /// <summary>
        /// יצירת דוח
        /// </summary>
        [TestMethod]//להריץ
        public void Test28CreateDoc()
        {
            Test01Login("Test28CreateDoc");
            PupilNew pn = new PupilNew();
           fp.clickNameDefinitions(p.niulBuldin);
           fp.clickNameDefinitions(" בניינים ");
            string[] listS1 = {"מספר בית","נציג וועד בית","קוד בנין" };
            string[] list2 = {"נציג וועד בית" };
            cnd.createDoc(true, true, "בנין",listS1,list2);
            fp.refres();
            fp.clickNameDefinitions(p.niulBuldin);
            fp.clickNameDefinitions(" בניינים ");
            cnd.clickTab("בנין", 4);
            fp.closeChrome();
            fp.OpenExcel("Test28CreateDoc");
        }
        /// <summary>
        /// יצירת סינון מהיר
        /// </summary>
        [TestMethod]//לא רץ טוב
        public void Test28filterQuickly()
        {
            Test01Login("Test28filterQuickly");
            PupilNew pn = new PupilNew();
            fp.clickNameDefinitions(p.niulBuldin);
            fp.clickNameDefinitions(" בניינים ");
            cnd.filterQuickly();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
            cnd.filter("תאריך", "תאריך שנה", " תאריך ; שנה זו ;", true, 0, "", 0, 0, "", "yyDATE");
            cnd.filter("edit", "תאריך שבוע", " תאריך ; שבוע זה ;", true, 1, "תאריך", 0, 0, "", "שבוע זה");
            cnd.filter("editbin", "תאריך בין", " תאריך ;בין;" + DateTime.Today.AddDays(-7).ToString("dd/MM/yyyy") + "+" + DateTime.Today.ToString("dd/MM/yyyy"), true, 1, "תאריך", 0, 0, "", "בין");
            cnd.filter("editbin", "תאריךעדהיום", " תאריך ;עד היום;", true, 1, "תאריך", 0, 0, "", "עד היום");
            cnd.filter("editbin", "תאריךהיום", " תאריך ;היום;", true, 1, "תאריך", 0, 0, "", "היום");
            fp.actionWork("הגדרות", "תהליכים");
            fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
            cnd.filter("תאריך עתידי", "תאריךשנה+", " זמן ריצה הבא ;שנה הבאה;", true, 1, "תאריך", 0, 0, "", "year+");
            cnd.filter("editbin", "תאריךחודש+", " זמן ריצה הבא ;חודש הבא;", true, 1, "תאריך", 0, 0, "", "mont+");
            cnd.filter("editbin", "תאריךשבוע+", " זמן ריצה הבא ;שבוע הבא;", true, 1, "תאריך", 0, 0, "", "week+");
            cnd.filter("editbin", "תאריךיום+", " זמן ריצה הבא ;מהיום;", true, 1, "תאריך", 0, 0, "", "DAY+");
            fp.closeChrome();
            fp.OpenExcel("Test28filterQuickly");
        }
        [TestMethod]//להריץ
        public void Test23Nedarim()
        {
            Test01Login("Test23Nedarim");
            Nedarim np = new Nedarim();
            np.stepProcess();
            fp.refres();
            Thread.Sleep(1500);
            fp.actionWork(" ניהול כספים ", "טיפול בנתונים מנדרים פלוס");
            np.actionRowNdarim();
            fp.closeChrome();
            fp.OpenExcel("Test23Nedarim");
        }
        [TestMethod]//להריץ
        public void Test30ImageLOGO()
        {
            Test01Login("Test30ImageLOGO");
            cnd.imageLogo();
            cnd.chancgeSizeOrImage(1);
            cnd.chancgeSizeOrImage(0);
            fp.closeChrome();
            fp.OpenExcel("Test30ImageLOGO");
        }
       
        [TestMethod]//יש באג//באג בשם
        public void Test31CheckStutusCharges()
        {
            int numparents=2;
            Test01Login("Test31CheckStutusCharges");
            fp.clickNameDefinitions(" "+p.niulBuldin+" ");
            fp.clickNameDefinitions(" דיירים ");
            PupilNew pn = new PupilNew();
            Thread.Sleep(290);
            numparents = pn.clickTableRandom();
            popupList pl = new popupList();
            pl.btnEdit();
            pn.ofenT(" אשראי ","5");
            f.charges(" לדייר");
            f.parentsHok();
            f.payments(numparents," לדייר");
            string m = fp.dateD();
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", m);
            ehq.clickGvia();//לא סימתי יש באג עם התאריך
            Thread.Sleep(300);
            fp.closeChrome();
        }

        [TestMethod]//להריץ
        public void Test31Histori()
        {
            Test01Login("Test31Histori");
            Thread.Sleep(350);
            PupilNew pn = new PupilNew();
            string user = cnd.nameuser();
            string[] rowAction = { user, "דיירים", DateTime.Today.ToString("dd/MM/yyyy-"), "עדכון", "-1" };
            string[] listAdd = { "", "סטטוס", "", "פעיל", "-1" };
            fp.clickNameDefinitions(" "+p.niulBuldin+" ");
            fp.clickNameDefinitions(" דיירים ");
            Thread.Sleep(550);
            int row = pn.clickTableRandom();
            string nameM = cnd.change();
            char[] spli = { ';' };
            string[] listAction = { fp.splitnumber(nameM).ToString(), "שם פרטי", nameM.Split(spli)[0], nameM.Split(spli)[0] + "אאא", "-1" };
            Thread.Sleep(200);
            pn.clickTableRandom(row);
            cnd.histori(rowAction, listAction);
            cnd.historiAdd("דיירים לדירה");
            fp.clickNameDefinitions(" בניינים ", 1);
            Thread.Sleep(350);
             row = pn.clickTableRandom();
            rowAction[0] = "";
            rowAction[1] = "דיירים";
            rowAction[3] = "הוספה";
            cnd.histori(rowAction, listAdd);
            fp.closeChrome();
            fp.OpenExcel("Test31Histori");
        }
      
        //שינוי CVV
        [TestMethod]
        public void Test33Cheange3numberAsrai()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            Bulding b = new Bulding();
            b.open(" תשלומי דיירים ");
            string[] rowCard = { "", "", "", "", "", "", "", "", "", "", "", "", "אשראי", "-1" };
            int row = fp.checkColumn("//", rowCard, "", "mat-row", "mat-cell", 0);
            string row3 = fp.rand(999).ToString();
            cc.checkChangeCVV(row, "");
            row = fp.checkColumn("//", rowCard, "", "mat-row", "mat-cell", 0);
            string name = cc.checkChangeCVV(row, row3);
            fp.actionWork("ניהול כספים", "אמצעי תשלום");
            cc.checkValidity(name, row3);
            fp.closeChrome();
        }
        //החלפת סוג תשלום(לאשראי אחר(
        [TestMethod]
        public void Test33ChangeMethodPayments()
        {
            Test01Login("Test33ChangeMethodPayments");
            createConatrabition cc = new createConatrabition();
            Bulding b = new Bulding();
            b.open(" תשלומי דיירים ");
            cc.createNew("כרטיס אשראי", "Credit", false, -1, "", 2,"אשראי",true);
            fp.refres();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.cheangeMethodPayments(true);
            fp.closeChrome();
            fp.OpenExcel("Test33ChangeMethodPayments");
        }
        //שינוי תוקף כרטיס
        [TestMethod]
        public void Test33CheangeValidityAsrai()
        {
            Test01Login("Test33CheangeValidityAsrai");
            createConatrabition cc = new createConatrabition();
            Bulding b = new Bulding();
            b.open(" תשלומי דיירים ");
            string name = cc.changeValidity(true);
            fp.actionWork("ניהול כספים", "אמצעי תשלום");
            cc.checkValidity(name, DateTime.Today.AddYears(4).ToString("MM/yy"));
            fp.closeChrome();
            fp.OpenExcel("Test33CheangeValidityAsrai");
        }

       
        [TestMethod]//Kא ניתן להרצה במחשבת כתיבת הקוד
        public void Test35DownloadExcel()
        {
            Test01Login("Test35downloadExcel");
            cnd.allDownloads();
            fp.closeChrome();
            fp.OpenExcel("Test35downloadExcel");
        }
        [TestMethod]
        public void Test35DownloadPDF()
        {
            Test01Login("Test35DownloadPDF");
            Thread.Sleep(350);
            cnd.allDownloads(false);
            fp.closeChrome();
            fp.OpenExcel("Test35DownloadPDF");
        }
        [TestMethod]
        public void Test35DownloadMSB()
        {
            Test01Login("Test35DownloadMSB");
            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("מסב", m);
            ehq.clickGvia();
            Thread.Sleep(800);
            CreateNewDenumic cnd = new CreateNewDenumic();
            if (cnd.CheckFileDownloaded("\\Downloads\\MSV_0962210") == false)
                throw new System.Exception("לא ירד");
            fp.closeChrome();
            fp.OpenExcel("Test35DownloadMSB");
        }
       
       
     
        [TestMethod]//הכן יש באג
        public void Test36changeSumHoq()
        {
            Test01Login("Test36changeSumHoq");
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("מסוף אשראי", DateTime.Today.ToString("dd"));
            string nameOfChange = ehq.editSum();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false");
            //  string nameOfChange = "ברים גד";
            vhp.checkChangeSum(nameOfChange);
            fp.OpenExcel("Test36changeSumHoq");
        }
        [TestMethod]//הולך להשתנות
        public void Test35MargeContacts()
        {
            Test01Login("Test35MargeContacts");
            
            PupilNew pn = new PupilNew();
            Bulding b = new Bulding();
            b.open(" דיירים ");
            string IDname = pn.nameForMarge(0,true);
            int i = 0; string IDname2 = "";
            for (i = 0; i < 5; i++)
            {
                if (IDname == IDname2 || IDname2 == "") IDname2 = pn.nameForMarge(i,true);
                else
                    break;
            }
            fp.actionWork("אנשי קשר", "");
            fp.click(By.XPath("//i[text()='people_outline']"));
            f.Misug(IDname, IDname2);
            try { fp.click(By.CssSelector("button[aria-label='close']")); } catch { }
            Thread.Sleep(500);
            fp.refres();
            b.open(" דיירים ");
            if (IDname != pn.nameForMarge(0,true) || IDname2 == pn.nameForMarge(i,true))
            {
                throw new System.Exception("not marge good");
            }
            fp.closeChrome();
            fp.OpenExcel("Test35MargeContacts");
        }
        [TestMethod]
        public void Test36filterCharges()
        {
            Test01Login("Test36filterCharges");
            int numH =  f.stepLiabilities(-1," לדייר");
            fp.ClickButton("הוספת חיוב לדייר");
            f.insertCharges(DateTime.Today.ToString("MM/yyyy"), DateTime.Today.AddMonths(-1).ToString("dd/MM/yyyy")," לדייר");
            fp.ClickButton(" ליצור חיוב לדייר נוסף ", "span");
            f.insertCharges(DateTime.Today.ToString("MM/yyyy"), DateTime.Today.AddMonths(1).ToString("dd/MM/yyyy")," לדייר");
            fp.ClickButton(" ליצור חיוב לדייר נוסף ", "span");
            f.insertCharges(DateTime.Today.ToString("MM/yyyy"), DateTime.Today.AddYears(-1).ToString("dd/MM/yyyy")," לדייר");
            fp.closePoup(1);
            fp.refres();
            Bulding b = new Bulding();
            b.open(" דיירים ");
            Thread.Sleep(200);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom(numH);
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(10, "חיובים לדייר");
            f.checkFilter();
            fp.closeChrome();
            fp.OpenExcel("Test36filterCharges");
        }
       
        [TestMethod]
        public void Test36CheckGoParent()
        {
            Test01Login("Test36CheckGoParent");
            ExecutionHQ ehq = new ExecutionHQ();
            //מיותר
            /* string m = fp.dateD();
             ehq.insertExecutionHQ();
             ehq.checkelmentExecutionHQ("אשראי", m);
             cnd.namePopupParent("mat-row", "mat-cell");
             fp.actionWork("ניהול כספים", "צפייה וטיפול בתשלומים");
             fp.selectForRequerd("סטטוס", false, 0);
             cnd.namePopupParent("tr", "td", 0);*/
            fp.clickNameDefinitions(" "+p.niulBuldin+" ");
            fp.clickNameDefinitions(" חיובים לדייר ");
            cnd.namePopupParent("mat-row", "mat-cell", 34, 0);
            fp.closeChrome();
            fp.OpenExcel("Test36CheckGoParent");
        }
       
        [TestMethod]//באג קריטי
        public void Test37FilterBen()
        {
            Test01Login("Test37FilterBen");
            f.openParent();
            string[] ben = { " תלמידים ", "מזהה שנתון לתלמיד", "גדול מ", "1TEXT", "scatter_plot", " שם פרטי ", "מתחיל ב", "אTEXT" };
            cm.stepFilterBen(ben);
            f.openFrind();
            string[] benFrind = { " תרומות ", "מזהה תורם", "גדול מ", "0TEXT", "functions", " סכום ", "גדול מ", "1TEXT" };
            cm.stepFilterBen(benFrind);
            fp.ClickButton("לא");
            fp.actionWork("אנשי קשר", "");
            string[] benContacts = { " הוצאות ", "סכום", "שווה ל", "5TEXT", "arrow_downward", "אסמכתא", "אינו מכיל את", "יTEXT" };
            cm.stepFilterBen(benContacts);
            fp.actionWork("הגדרות", "מוסדות");
            string[] benInstitutes = { " מוסדות ", "קוד מוסד", "גדול או שווה ל", "1TEXT", "scatter_plot", "שם מוסד", "שווה ל", "בית אברהםTEXT" };
            cm.stepFilterBen(benInstitutes);
            dobut d = new dobut();
            d.openDoubt();
            string[] benDoubt = { " קבלות שירות ", "מזהה", "גדול מ", "1TEXT", "scatter_plot", "סכום לתשלום", "קטן או שווה ל", "70TEXT" };
            cm.stepFilterBen(benDoubt);
            fp.closeChrome();
            fp.OpenExcel("Test37FilterBen");
        }
        [TestMethod]
        public void test38checkPeleCard()
        {
            Test01Login("test38checkPeleCard");
            ce.openClearingEninty();
            //("אשראי", " פלאקארד ", );
            string[] asrai = { "", "", "אשראי", "-1" };
            fp.checkColumn("//", asrai, "", "mat-row", "mat-cell", -1, 0, true);
            popupList pl = new popupList();
            pl.clickEdit();
            fp.insertElment("SenderCode", By.Name("SenderCode"), "TEST PS");
            fp.insertElment("name", By.Name("Name"), "לבדיקה");
            fp.ClickButton("שמירה", "span");
            fp.closepopup();

            fp.refres();
            createConatrabition cc = new createConatrabition();
            Bulding b = new Bulding();
            b.open(" תשלומי דיירים ");
            cc.createNew("כרטיס אשראי", "Credit", false, -1, "", 1, "אשראילבדיקה",true);
            fp.ClickButton("קח אותי לכרטיס תשלומי דיירים", "span");
            Thread.Sleep(450);
            fp.ClickButton("שדר תשלום");
            Thread.Sleep(250);
            fp.closeChrome();
            Pelecard pd = new Pelecard();
            pd.login();
            fp.closeChrome();
            fp.OpenExcel("test38checkPeleCard");
        }

        [TestMethod]
        public void Test39OffsetOtumation()
        {
            Test01Login("Test39OffsetOtumation");
            popupList pl = new popupList();
            PupilNew pn = new PupilNew();
            Frind f = new Frind();
            int numHora = f.stepLiabilities(1," לדייר");
            fp.closePoup();
            pn.clickTableRandom(numHora, 1);
            pl.expansionPopUP();
            pl.clickStepHeder(2, "תשלומים לדייר");
            fp.ClickButton(" הוספת תשלום לדייר ", "span");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", "false", "הוראות קבע ראשי", "", "", "", "Remarks", "5", "כן"," לדייר");
            fp.refres();
            Bulding b = new Bulding();
            b.open(" דיירים ");
            Thread.Sleep(450);
            pn.clickTableRandom(numHora, 1);
            pl.expansionPopUP();
            pl.clickStepHeder(2, "תשלומים לדייר");
            fp.click(By.XPath("//i[text()=' pageview ']"));
            fp.ClickButton("שדר תשלום ", "span");
            
            fp.refres();
            b.open(" דיירים ");
            pn.clickTableRandom(numHora, 1);
            pl.expansionPopUP();
            pl.clickStepHeder(3, "חיובים לדייר");
            string[] offOC = { "", "", "1", "שקל", "", "1", DateTime.Today.ToString("dd/MM/yyyy"), "הושלם", "פעיל", "-1" };
            fp.checkColumn("//", offOC, "false", "mat-row", "mat-cell");
            fp.closeChrome();
            fp.OpenExcel("Test39OffsetOtumation");
        }
        [TestMethod]
        public void Test39sendMailContacts()
        {
            Test01Login("Test39sendMailContacts");
            Frind f = new Frind();
            
            f.MailContacts();
            Thread.Sleep(500);
            fp.closeChrome();
            fp.OpenExcel("Test39sendMailContacts");
        }
        [TestMethod]
        public void Test91LoadsConatarbition()
        {
            Test01Login("Test91LoadsConatarbition");

            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            for (int i = 0; i < 24; i++)
            {
                cc.createNew("מזומן", "Cash");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.createNew("כרטיס אשראי", "Credit");
                Thread.Sleep(350);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.createNew("צק", "Check");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.createNew("העברה בנקאית", "BankDeposit");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תרומה נוספת", "span");
            }
            fp.closeChrome();
            fp.OpenExcel("Test91LoadsConatarbition");
        }
        [TestMethod]
        public void Test91Loadspayments()
        {
            Test01Login("Test91Loadspayments");


            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", "", "כללי", "", "Credit");
                Thread.Sleep(200);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "מזומן", "", "כללי", "", "Cash", "רוזנברגר");
                Thread.Sleep(300);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "צק", "", "כללי", "", "Check", "אוטומציה");
                Thread.Sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", "", "כללי", "", "BankDeposit");
                Thread.Sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "כללי", "", "divPaymentMethodTypesCredit", "אוטומציה", "false", "");
                Thread.Sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            for (int i = 0; i < 24; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "מס\"ב", "", "כללי", "", "divPaymentMethodTypesMasav", "", "false", "");
                Thread.Sleep(250);
                fp.ClickButton(" ליצור תשלום נוסף ", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadPrintKabla()
        {
            CreateProductionReceipt("אשראי", " ", 1);

        }
        [TestMethod]
        public void Test91LoadDDebit()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openDebit();
            for (int i = 0; i < 50; i++)
            {
                cc.stepDebit();
                Thread.Sleep(250);
                fp.ClickButton("ליצור הו\"ק נוספת", "span");
            }
            for (int i = 0; i < 50; i++)
            {
                cc.stepDMsab();
                Thread.Sleep(250);
                fp.ClickButton("ליצור הו\"ק נוספת", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadConatarbitionDDebit()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openCDDebit();
            for (int i = 0; i < 50; i++)
            {
                Thread.Sleep(250);
                cc.stepConatarbitionHOK("כרטיס אשראי", "divPaymentMethodTypesCredit", "אוטומציה", "Remarks");
                Thread.Sleep(250);
                Console.WriteLine(i);
                fp.ClickButton("ליצור תרומה בהו\"ק נוספת", "span");
            }
            for (int i = 0; i < 50; i++)
            {
                cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav", "אוטומציה", "Remarks");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תרומה בהו\"ק נוספת", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]//באג
        public void Test91LoadDebitPayments()
        {
            Test01Login();
            createConatrabition cc = new createConatrabition();
            cc.openPayments();
            for (int i = 0; i < 25; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", "אוטומציה", "", "divPaymentMethodTypesCredit", "אוטומציה", "false", "");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תשלום נוסף", "span");
            }
            for (int i = 0; i < 25; i++)
            {
                cc.stepPayment("תשלום בהו\"ק", "מס\"ב", p.idIdid, p.automation, "", "divPaymentMethodTypesMasav", "", "false");
                Thread.Sleep(250);
                fp.ClickButton("ליצור תשלום נוסף", "span");
            }
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadExecutionHQ()
        {
            Test01Login("Test13ExecutionHQ");

            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ("אשראי", m);
            ehq.clickGvia();
            fp.spinner();
            fp.closeChrome();
        }
        [TestMethod]
        public void Test91LoadPrintRecipet()
        {
            Test01Login("Test91LoadPrintRecipet");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            fp.click(By.TagName("mat-checkbox"));
            fp.ClickButton(" הפק קבלות ");
            fp.refres();
            hhrq.insertpageHhrq();
            hhrq.checkPage("אשראי");
            fp.click(By.TagName("mat-checkbox"));
            fp.ClickButton(" הפק קבלות ");

        }
    }
}
