﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class CurrentBillingClause:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();

        public void openNewBillingClause()
        {
            fp.actionWork(" ניהול כספים ","הגדרת סעיפי חיוב");
            try { fp.clickButoonToContains("הגדרת סעיפי חיוב"); } catch { }
            Thread.Sleep(400);
            fp.ButtonNew(DBillingC);

        }
        public void createBillingClause(string typeBiling,string biling="1",string NAME="",string finance= " אוטומציה (חיוב) ")
        {
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.checkClasscreate(DBillingC, false);
            cc.checkStepHeder(1, 2);
            cc.clickFrind(MosadChinuc, biling);
            cc.checkStepHeder(2, 2);
            checkElmentStep2();
            inserElment(typeBiling,finance,NAME);
        }

        public void inserElment(string typeBiling,string finance= "אוטומציה (חיוב) ",string name="אוטומציה")
        {
            try
            {
                fp.checkElment("check name bulding", By.Name("ClauseName"));
                fp.insertElment("ClauseName", By.Name("ClauseName"), name, true);
                fp.selectForRequerd("סוג סעיף חיוב",true,-1);//fp.SelectRequerdName( "ng-reflect-name='BillingClauseTypeID", typeBiling);
                fp.selectText(typeBiling);
                fp.selectForRequerd("סעיף פיננסי",true,-1);//fp.SelectRequerdName("ng-reflect-name='FinancialClausesID",automation);
                fp.selectText(finance);
                fp.insertElment("check Sum", By.Name("Sum"),amount, true);

                fp.selectForRequerd("בחר/י מטבע עבור ההגדרת סעיף חיוב");//fp.SelectRequerdName("ng-reflect-name='Currency","שקל"); 
                fp.selectForRequerd("בחר/י תדירות גביה עבור ההגדרת סעיף חיוב");//fp.SelectRequerdName("ng-reflect-name='BillingFrequencyID", "חד פעמי");
                fp.checkElmentText("select month", By.TagName("app-month-year"), "בחר/י חודש");
                try { cc.calnderMont(); } catch { }
                fp.selectForRequerd("בחר/י שנה\"ל עבור ההגדרת סעיף חיוב",true,-1);//fp.SelectRequerdName("ng-reflect-name='SchoolYearID", yearHebru);
                fp.selectText(yearHebru);
               // fp.selectForRequerd("סניף", false);// driver.FindElement(By.XPath("//mat-select[@ng-reflect-name='EducationInstitutesBranchID']")).Click();

                fp.selectForRequerd("רמת כיתה", true, -1);//fp.SelectRequerdName("ng-reflect-name='ClassLevelsTypeID", "הכל");
                fp.selectText("טווח רמות כיתה");
                fp.checkSelectPreform("מרמת כיתה", By.TagName("mat-select"));
                fp.checkSelectPreform("עד רמת כיתה", By.TagName("mat-select"));
                fp.selectForRequerd("מרמת כיתה", false);
                fp.selectForRequerd("עד רמת כיתה", false);
                //driver.FindElements(By.ClassName("mat-option-text"))[1].Click();
               // fp.SelectRequerdName("ng-reflect-name='EducationInstitutesBranchID","אוטומציה",false);
                 
                fp.insertElment("check select Remarks", By.TagName("textarea"));
                fp.ClickButton(" צור הגדרת סעיף חיוב חדש ","span");
            }
            catch { throw new NotImplementedException("invaled inserElment" ); }
        }

        public  void checkElmentStep2()
        {
            try
            {
                fp.checkElment("check sub title page", By.XPath("//div[contains(@class,'sub-title')][contains(text(),'אנא מלא/י את הפרטים הבאים')]"));
                fp.checkElmentsText("check title",By.ClassName("title-section"), "פרטי סעיף חיוב",0);
                fp.checkElmentText("check name bulding", By.Name("ClauseName"), " שם סעיף חיוב ");
                fp.checkSelectPreform("בחר/י סוג סעיף חיוב עבור ההגדרת סעיף חיוב ", By.TagName("mat-select"));// "check select BillingClauseTypeID", By.XPath("//mat-select[@ng-reflect-name='BillingClauseTypeID']"), ); ;
                fp.checkSelectPreform("בחר/י סעיף פיננסי עבור ההגדרת סעיף חיוב ", By.TagName("mat-select"));//"check select FinancialClausesID", By.XPath("//mat-select[@ng-reflect-name='FinancialClausesID']"), );
                fp.checkElmentsText("check title",By.ClassName("title-section"), "פרטי גביה", 1);
                fp.checkElmentText("check Sum", By.Name("Sum"), " סכום ");
                fp.checkSelectPreform("בחר/י מטבע עבור ההגדרת סעיף חיוב", By.TagName("mat-select"));// "check select Currency", By.XPath("//mat-select[@ng-reflect-name='Currency']"),);
                fp.checkSelectPreform("בחר/י תדירות גביה עבור ההגדרת סעיף חיוב",By.TagName("mat-select"));// "check select BillingFrequencyID", By.XPath("//mat-select[@ng-reflect-name='BillingFrequencyID']"), );
                fp.checkElmentsText("check title",By.ClassName("title-section"), "עבור", 2);
                fp.checkSelectPreform("בחר/י שנה\"ל עבור ההגדרת סעיף חיוב", By.TagName("mat-select"));//"check select SchoolYearID", By.XPath("//mat-select[@ng-reflect-name='SchoolYearID']"), );
                fp.checkSelectPreform(" סניף ",By.TagName("mat-select"));//"check select EducationInstitutesBranchID", By.XPath("//mat-select[@ng-reflect-name='EducationInstitutesBranchID']"), );
                fp.checkSelectPreform("בחר/י סוג רמת כיתה ", By.TagName("mat-select"));//"check select ClassLevelsTypeID", By.XPath("//mat-select[@ng-reflect-name='ClassLevelsTypeID']"), );
                fp.checkElmentsText("check title",By.ClassName("title-section"), "פרטים נוספים", 3);
                //fp.checkElmentText("check select Remarks", By.XPath("//textarea[@ng-reflect-name='Remarks']"), " הערות ");
                fp.checkElmentText("check button btnSubmitCreateItem", By.XPath("//button[@aria-label='btnSubmitCreateItem']"), " צור הגדרת סעיף חיוב חדש ");
            }
            catch { throw new NotImplementedException("invaled checkElmentStep2"); }
        }
    }
}