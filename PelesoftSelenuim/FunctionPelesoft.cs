﻿using MySqlX.XDevAPI;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class FunctionPelesoft : page
    {
        Example e = new Example();

        //static IWebDriver driver;// = new ChromeDriver();
        public string result;
        public ReadOnlyCollection<IWebElement> buttonwr;
        /// <summary>
        /// class='mat-button-wrapper'
        /// </summary>
        /// <param name="valueTextButton"></param>
        public void ClickButton(string valueTextButton,string typeButton="*",int numClick=1)
        {
            try
            {
                /*Thread.Sleep(700);
                By @by = By.XPath("//" + typeButton + "[@class='mat-button-wrapper'][text()='" + valueTextButton + "']");
                waitUntil(@by);
                checkElment("button add "+ valueTextButton, @by);
                driver.FindElement(@by).Click();*/
                By @by = By.ClassName("mat-button-wrapper");
                for (int i = 0; i < driver.FindElements(by).Count; i++)
                    if (driver.FindElements(by)[i].Text.Contains(valueTextButton.Trim()))
                    {
                        if (numClick > 1)
                            numClick--;
                        else
                        {
                            driver.FindElements(@by)[i].Click();
                            break;
                        }
                    }

            }
            catch
            {
                try
                {
                    By @by = By.XPath("//" + typeButton + "[@class='mat-button-wrapper'][text()='" + valueTextButton + "']");
                    waitUntil(@by);
                    checkElment("button add " + valueTextButton, @by);
                    driver.FindElement(@by).Click();
                }
                catch {
                    picter("click button " + valueTextButton);
                }
            }

        }

        public  void popupMessage(string ValuePopup)
        {
            try {
                Thread.Sleep(300);
                if (driver.FindElement(By.CssSelector("div[class='mat-dialog-content msg-content ng-star-inserted']")).Displayed)
                {
                    IWebElement popup = driver.FindElement(By.CssSelector("div[class='mat-dialog-content msg-content ng-star-inserted']"));
                    string value = popup.FindElement(By.TagName("div")).Text;
                    if (value.Contains("בהצלחה") || value.Contains("אין שינויים לשמור"))
                        Console.WriteLine(" done saccess");
                    else
                    {
                        picter(ValuePopup+" not done");
                        Debug.WriteLine(ValuePopup +" not done ");
                        throw new NotImplementedException(ValuePopup+" not done");
                    }
                }
            } 
            catch { throw new NotImplementedException(ValuePopup+" not done"); }
        }

        internal void checkElmentTextAL(string valueCheckFromBy, string valueCheck)
        {
            try
            {
                if (valueCheck == "")
                    checkElment(valueCheckFromBy, By.XPath("//mat-select[@aria-label='" + valueCheckFromBy + "']"));
                else
                    checkElmentText(valueCheckFromBy, By.XPath("//mat-select[@aria-label='" + valueCheckFromBy + "']"), valueCheck);
            }
            catch { picter("checkElmentTextAL" + valueCheckFromBy);
                throw new NotImplementedException("checkElmentTextAL invaled"); }
        }

        public void closeChrome()
        {
            driver.Close();
            driver.Quit();
        }

        public int rand(int length,int start=0)
        {
            Random r = new Random();
            int langRand=r.Next(start, length);
            if(Math.Ceiling(Math.Log10(langRand))< Math.Ceiling(Math.Log10(length)))
            { int nummin = (int)(Math.Ceiling(Math.Log10(length)) - Math.Ceiling(Math.Log10(langRand)));
                try { langRand = langRand + r.Next(start, nummin); } catch { }
            }
            return langRand;
        }

        public void clickAddType(string nameClassAllDiv,string nameXpath="")
        {
            try
            {   if (nameXpath == "")
                    driver.FindElement(By.XPath("//div[@class='" + nameClassAllDiv + "']/div/i[text()=' add ']")).Click();
                else
                    driver.FindElement(By.XPath(nameXpath + "/div/i[text()=' add ']")).Click();

            }
            catch
            {
                picter("clickAddType"+ nameClassAllDiv);
                Console.WriteLine("click 'add' from div ,not clicking ");
            }
        }
       
        public int checkElmentText(string strinNameCheck, By by, string nameCheck, bool insertNu=false,bool insertString=false)
        {
            int ret = 0;
            try
            {
                    string textNameCheck = driver.FindElement(by).Text ;
                if (textNameCheck.Contains(nameCheck.Trim()))
                    Console.WriteLine("name check is equal:" + strinNameCheck);
                else if (nameCheck.Trim() != "" && textNameCheck.Trim() != "")
                {
                    picter("checkElmentText" + nameCheck);
                    Debug.WriteLine(textNameCheck);
                    ret = 1;
                }
                else {
                    picter("checkElmentText" + nameCheck);
                }
                if (insertNu == true)
                    driver.FindElement(by).SendKeys("123");
                if(insertString==true)
                    driver.FindElement(by).SendKeys("אוטומציה");
            }

            catch { ret = 1;
               picter("checkElmentText" + nameCheck);
                return ret;
                //throw new NotImplementedException(strinNameCheck+" invalid");
            }
            return ret;
        }

        public void approveAction(string yesorno)
        {
            try
            {
                checkElmentText("title approve action", By.ClassName("title"), " אשר פעולה ");
                driver.FindElement(By.XPath("//button/span[@class='mat-button-wrapper'][text()='כן']")).Click();
                //ClickButton(yesorno,"sapn");
            }
            catch
            {
                picter("approveAction");
            }
            //throw new NotImplementedException();
        }

        public int checkElmentsText(string strinNameCheck, By by, string nameCheck,int mikofCheck,bool add=false)
        {
            int ret = 0;
            string textNameCheck;
             try
             {if (mikofCheck==-1)
                {
                    for(int i=0;i< driver.FindElements(by).Count;i++)
                    {
                        if(driver.FindElements(by)[i].Text.Contains(nameCheck))
                        {
                            mikofCheck = i;
                            break;
                        }
                        if(i+1==driver.FindElements(by).Count)
                            throw new NotImplementedException(strinNameCheck + " invalid");
                    }
                }
            if (driver.FindElements(by)[mikofCheck].Displayed)
            {
                    if (add == true)
                        textNameCheck = driver.FindElements(by)[mikofCheck].Text.Replace(Environment.NewLine, "").Replace("add", "");
                textNameCheck = driver.FindElements(by)[mikofCheck].Text;
                    if (textNameCheck.Trim() == nameCheck.Trim())
                        Console.WriteLine("name check is equal:" + strinNameCheck + "," + nameCheck);
                    else if (nameCheck != "")
                    {
                        ret = 1;
                        Debug.WriteLine(textNameCheck);
                    }
            }
           
            
             }
              catch {
                picter("checkElmentsText" + nameCheck);
                ret = 1;
                    //throw new NotImplementedException(strinNameCheck+" invalid"); 
            }
            return ret;
        }
        public bool checkSelectPreform(string nameCheck,By byy=null)
        {
            try
            {
                IList<IWebElement> by = driver.FindElements(By.TagName("mat-label"));
                for (int i = 0; i < by.Count; i++)
                {
                    Console.WriteLine(by[i].Text);
                    if (by[i].Text.Contains(nameCheck))
                    {
                        
                        Console.WriteLine("find elmentExist" + nameCheck);
                        break;
                    }
                    if (i + 1 == by.Count)
                    {
                        Debug.WriteLine(nameCheck + " invalid");
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                picter("checkSelectPreform" + nameCheck);
                return false;
            }
        }
        public int splitnumber(string test)
        {
            string result="";
            foreach (Char i in test)
            {
                if (Char.IsNumber(i))
                {
                    result += i;
                }
            }
            return int.Parse(result);
        }
        public  void checkColumnTable(string tagRow, string tagCell, int mikom, string valueCheck,string oper="")
        {
            try {
                int ret = 0;
                string num = driver.FindElement(By.ClassName("row-number-txt")).Text;
               if(splitnumber(num)>0)
                {
                    IList<IWebElement> rows = driver.FindElements(By.TagName(tagRow));
                    IList<IWebElement> cell;
                    for(int i=0;i<rows.Count;i++)
                    {
                        cell = rows[i].FindElements(By.TagName(tagCell));
                        switch (oper)
                        {
                            case "שנים<":
                                try
                                {
                                    DateTime dy = DateTime.Parse(DateTime.Today.AddYears(Int32.Parse("-" + valueCheck)).ToString("dd/MM/yy"));
                                    if (DateTime.Parse(cell[mikom].Text) > dy)
                                    { ret = 1; }
                                }
                                catch { }
                               break;
                            case string big when big.Contains(">"):
                                {
                                    if (Int32.Parse(cell[mikom].Text.Trim()) <= Int32.Parse(valueCheck))
                                        ret = 1; break;
                                }
                            case string a when a.Contains("<"):
                                {
                                    if (Int32.Parse(cell[mikom].Text.Trim()) >= Int32.Parse(valueCheck))
                                        ret = 1; break;
                                }
                            case ",":
                                char[] tav = { ',' };
                                if (!cell[mikom].Text.Trim().Contains(valueCheck.Split(tav)[0].Trim()) && !cell[mikom].Text.Trim().Contains(valueCheck.Split(tav)[1].Trim()))
                                    ret = 1;
                                break;
                            case "notnull":
                                if (cell[mikom].Text.Trim().Equals(""))
                                    ret = 1;
                                break;
                            case string a when a.Contains("DATE"):
                                char[] tavD = {'D','A','T','E' };
                                if (!cell[mikom].Text.Contains(DateTime.Today.ToString(oper.Split(tavD)[0])))
                                    ret = 1;
                                break;
                            case "שבוע זה":
                                DateTime dW = DateTime.Today;//Parse(DateTime.Today.AddDays(6).ToString("dd/MM/yy"));
                                while (dW.DayOfWeek != DayOfWeek.Sunday)
                                  dW=  dW.AddDays(-1);
                                DateTime endD = dW.AddDays(7);
                                if (DateTime.Parse(cell[mikom].Text) >endD|| DateTime.Parse(cell[mikom].Text)<dW)//(DateTime.Parse(cell[mikom].Text) > dW|| DateTime.Parse(cell[mikom].Text)<DateTime.Today)
                                { ret = 1; }
                                break;
                            case "בין":
                                char []tav5 = { '+' };
                                if (DateTime.Parse(cell[mikom].Text) < DateTime.Parse(valueCheck.Split(tav5)[0]) || DateTime.Parse(cell[mikom].Text) > DateTime.Parse(valueCheck.Split(tav5)[1]))
                                    ret = 1;
                                break;
                            case "year+":
                                if (DateTime.Parse(cell[mikom].Text) < DateTime.Today.AddYears(1))
                                    ret = 1;
                                break; 
                            case "Month+":
                                if (DateTime.Parse(cell[mikom].Text) < DateTime.Today.AddMonths(1))
                                    ret = 1;
                                break; 
                            case "Week+":
                                DateTime dWA = DateTime.Today.AddDays(1);
                                while (dWA.DayOfWeek != DayOfWeek.Sunday)
                                    dWA = dWA.AddDays(1);
                                if (DateTime.Parse(cell[mikom].Text) <dWA|| DateTime.Parse(cell[mikom].Text)>dWA.AddDays(7))
                                    ret = 1;
                                break;
                            case "DAY+":
                                if (DateTime.Parse(cell[mikom].Text) < DateTime.Today)
                                    ret = 1;
                                break;
                            case "עד היום":
                                if (DateTime.Parse(cell[mikom].Text) > DateTime.Today)
                                    ret =1;
                                break; 
                            case "היום":
                                if (DateTime.Parse(cell[mikom].Text) != DateTime.Today)
                                    ret =1;
                                break;
                        }
                        

                        if (!cell[mikom].Text.Trim().Contains(valueCheck.Trim()) && (oper == "" || (oper.Contains("=") && ret == 1)))
                            ret = 1;
                        else if (oper.Contains("=") && ret == 1 && cell[mikom].Text.Trim().Contains(valueCheck.Trim()))
                            ret = 0;
                        if (ret == 1)
                        { picter("checkColumnTable not equals");  }
                    } 
                }
               
            } catch(Exception ex) { picter("checkColumnTable" + ex); }
        }

        public  int mikomNameTable(string heder,string valueCheck)
        {
            int mikom = 0;
            try {
                string num = driver.FindElement(By.ClassName("row-number-txt")).Text;
                if (splitnumber(num) > 0)
                {
                    IList<IWebElement> heders = driver.FindElements(By.TagName(heder));
                    for (int i = 0; i < heders.Count; i++)
                    {
                        if (heders[i].Text.Trim().Contains(valueCheck.Trim()))
                        {
                            mikom = i;
                            break;
                        }
                    }
                }
            } catch(Exception ex) { picter("mikomNameTable" + ex); throw new NotImplementedException(""+ex); }
            return mikom;
        }

        /*  public By by1 { get; set; }
public int iDe { get; set; }
public string SelectValue { get; set; }*/

        public void ClickList(string nameAction)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
           
            try
            {
                waitUntil(By.XPath("//mat-tree-node[@aria-label='toggle " + nameAction + "']"));
                if (driver.FindElement(By.XPath("//mat-tree-node[@aria-label='toggle "+ nameAction + "']")).Enabled)///div[@class,'wrap-action-name'][text()," + nameAction + "]")).Enabled)
                {
                    driver.FindElement(By.XPath("//mat-tree-node[@aria-label='toggle " + nameAction + "']")).Click();
                    Console.WriteLine("click list :" + nameAction);
                }
                else
                {
                    Debug.WriteLine("list not display:" + nameAction);
                }

            }
            catch
            {
                picter("ClickList" + nameAction);
                Console.WriteLine("not click in list :" + nameAction);
            }
        }
        public void mosadSlika(string mosd)
        {
           // checkElmentText("Clearing Institution", By.XPath("//mat-select[@formcontrolname='ClearingEntityID']"), "מוסד סליקה");
           /* driver.FindElement(By.XPath("//mat-select[@formcontrolname='ClearingEntityID']")).Click();
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='ClearingEntityID']")).SendKeys(Keys.Tab);
            funError(2, " בחר מוסד סליקה! ");*/
            driver.FindElement(By.XPath("//mat-select[@formcontrolname='ClearingEntityID']")).Click();

            selectText(mosd,true);
        }
        public void ClickListBetween(string nameAction)
        {
           // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            try
            {
                driver.FindElement(By.XPath("//div[text()='" + nameAction + "']")).Click();
                // WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
                // wait.Until(By.XPath("//div[@class,'wrap-action-name'][text()," + nameAction + "]"));
              /*  if (driver.FindElement(By.XPath("//div[@class,'wrap-action-name'][text()," + nameAction + "]")).Enabled)
                {
                    Console.WriteLine("click list :" + nameAction);
                    driver.FindElement(By.XPath("//mat-tree-node/div/div[@class,'wrap-action-name'][text()," + nameAction + "]")).Click();
                    Console.WriteLine("click list :" + nameAction);
                }
                else
                {
                    Debug.WriteLine("list not display:" + nameAction);
                }*/

            }
            catch
            {
                picter("ClickListBetween" + nameAction);
                Console.WriteLine("not click in list :" + nameAction);
            }
        }

        public string click(By by,int onBy=0)
        {
            string cs = driver.FindElements(by)[onBy].GetAttribute("aria-disabled");
            try { if (checkElment("click", by) == 0&&cs!="true")
                    driver.FindElements(by)[onBy].Click();
            } 
            catch { picter("click "+rand(99)); throw new NotImplementedException(); }
            return cs;
        }
        
        public void wait()
        {
            WebDriverWait customWait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

        }
        public void waitUntil(By by)
        {
            try
            {
                WebDriverWait customWait = new WebDriverWait(driver, TimeSpan.FromSeconds(1005));
                customWait.Until(driver => driver.FindElement(by));
            }
            catch
            { picter("waitUntil"); }
        }
      public void selectsss(string nameSelect,int mikom=0,string [] check=null,int cleft=0)
        {
            try
            {
                IList<IWebElement> dd = driver.FindElements(By.TagName("select"));
                IList<IWebElement> op = dd[mikom].FindElements(By.TagName("Option"));
                for (int i = 0; i < op.Count; i++)
                {
                    if (check != null)
                    {
                        if (check.Length != op.Count)
                        {
                            picter("the elment not display in select");
                            throw new NotImplementedException();
                        }
                        if (!op[i].Text.Contains(check[i].Trim()))
                        {
                            picter("the elment not display in select");
                            throw new NotImplementedException();
                        }
                       
                    }
                    if (nameSelect != "" && op[i].Text.Trim() == nameSelect.Trim())
                    {
                        op[i].Click();
                        click(By.XPath("//i[@class='material-icons'][text()=' chevron_left ']"), cleft);
                        if(check == null)
                            break;
                    }
                }
            }
            catch (Exception ex) { picter("selectin"); throw ex; }
        }
        public void clickListinListEqualName(string nameList)
        {
            try
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                // this.nameList = nameList;
                driver.FindElements(By.XPath(".//*[contains(@class,'wrap-action-name')][contains(text(),'" + nameList + "')]"))[1].Click();
            }
            catch
            {
                picter("clickListinListEqualName" + nameList);
                throw new NotImplementedException(nameList);
            }
        }

        public void clickElmentList(string by, string listClick,string valueCheck)
        {
           // checkElment(valueCheck, by);

            driver.FindElement(By.XPath(by)).Click();
            selectTextNF(listClick);
            //throw new NotImplementedException();
        }

        public void closePage()
        {
            driver.FindElement(By.XPath("//i[text()='close']")).Click();
        }

        public string  funError(int idErr, string stringValueError = "שדה חובה")
        {
            try
            {
                if (!driver.FindElement(By.TagName("mat-error")).Displayed)
                    Debug.WriteLine("the error not display");
                string valueError = driver.FindElement(By.TagName("mat-error")).Text;
                if (valueError != stringValueError)
                    Debug.WriteLine("the value error invalid");
            }
            catch
            {
                picter("funError");
                throw new NotImplementedException("funError");
            }
            return "";
        }
        public void selectTextNF(string nameSelect)
        {
            try
            {
                driver.FindElement(By.XPath("//span[@Class='mat-option-text'][text()='"+nameSelect+"']")).Click();
            }
            catch
            {
                picter("selectTextNF" + nameSelect);
                Debug.WriteLine("no select option " + nameSelect);
            }
        }
        public int selectText(string nameSelect,bool equals=false,int j=0)
        {int ret = 0;
            try
            {

                
                bool eqCon = true;
                int countArr = driver.FindElements(By.ClassName("mat-option-text")).Count();
                for (int i = j; i < countArr; i++)
                {
                    if (j == 0)
                        eqCon = driver.FindElements(By.ClassName("mat-option-text"))[i].Text.Trim().Contains(nameSelect.Trim());
                    // Console.WriteLine(driver.FindElements(By.ClassName("mat-option-text"))[i].Text);
                    if (equals == true)
                        eqCon = driver.FindElements(By.ClassName("mat-option-text"))[i].Text.Trim().Equals(nameSelect.Trim());
                    if (eqCon == true || j != 0)
                    {
                        driver.FindElements(By.ClassName("mat-option-text"))[i].Click();
                        Console.WriteLine("click list :" + nameSelect);
                        break;
                    }
                    if (i + 1 == countArr)
                    {
                        driver.FindElements(By.ClassName("mat-option-text"))[0].Click();
                        Debug.WriteLine("no select option " + nameSelect);
                        picter("selectText " + nameSelect);
                        ret = 1;
                    }
                    /*else
                        Console.WriteLine(driver.FindElements(By.ClassName("mat-option-text"))[i].Text);*/
                }
                
            }
            catch { picter("selctText"+nameSelect); }
return ret;
        }

        public  bool clickLeft()
        {
            try
            {
                if (driver.FindElement(By.XPath("//button/span/i[text()=' chevron_left ']")).Enabled)
                {
                    driver.FindElement(By.XPath("//button/span/i[text()=' chevron_left ']")).Click();
                    Thread.Sleep(900);
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                picter("clickleft");
                return false;
            }
            //throw new NotImplementedException();
        }

        public int checkElment(string nameCheck, By by, bool valueInsert = false, bool Requiredfield = false)//,string value,Array arr[]=null)
        {
            int ret = 0;
            try
            {
                SoftVerifier sv = new SoftVerifier();
                sv.VerifyElementIsPresent(driver.FindElement(by), "the elment " + nameCheck + "not display");
               if (valueInsert == true || Requiredfield == true)
                {
                    if (Requiredfield == true)
                    {
                        // driver.FindElement(by).Click();
                        driver.FindElement(by).SendKeys("");
                        driver.FindElement(by).SendKeys(Keys.Tab);

                        funError(1);
                    }
                    if (valueInsert == true)
                        insertElment(nameCheck, by);

                }
                Console.WriteLine("the elment:" + nameCheck + "check");
            }
            catch
            {
                ret = 1;
                picter("checkElment " + nameCheck);
                Debug.WriteLine("the elment not insert code good:" + nameCheck + " " + by.GetType());
                return ret ;
            }
            return ret;
        }

        public  void closepopup()
        {
            try
            {
                click(By.CssSelector("button[aria-label='edit']"));  //driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();
            }
            catch
            {
                picter("closepopup");
            }
        }

        public int splitNumber(string valueSplit)
        {
            string result = "";
            foreach (Char i in valueSplit)
            {
                if (Char.IsNumber(i))
                {
                    result += i;
                }
            }
            if (result == "")
                result = "-1";
            return Int32.Parse(result);
        }
        /// <summary>
        /// SelectRequerdName=check requerd and select value 
        /// </summary>
        /// <param name="name">"//mat-select[@"+name + "']"</param>
        /// <param name="val"></param>
        public void SelectRequerdName(string name, string val,bool requerd=true,bool equls=false ,int i=0)
        {
            try
            {
                if (val != "")
                {
                    checkElment("model", By.XPath("//mat-select[@" + name + "']"), false, requerd);
                    driver.FindElement(By.XPath("//mat-select[@" + name + "']")).Click();
                    waitUntil(By.ClassName("mat-option-text"));
                    selectText(val, equls, i);
                }
            }
            catch { }
        }
        public string dateD()
        {
            Thread.Sleep(100);
            string d = DateTime.Today.ToString("dd");//AddDays(1).
            if (Int16.Parse(d) < 10)
            {
                char[] tav = { '0' };
                d = d.Split(tav)[1];//AddDays(1).
            }
            return d;
        }
        public void highlight( IWebElement ele1)
        {
            IJavaScriptExecutor js1 = (IJavaScriptExecutor)driver;
            js1.ExecuteScript("arguments[0].setAttribute('style', 'background: blue; border: 2px solid yellow;');", ele1);
        }
        public void insertElment(string nameCheck, By by, string valueInserrt = null,bool checkRequerd=false,string valueRequerd="",int mikom=-1)
        {
            try
            {
                if (valueInserrt != "false")
                {
                    if (valueInserrt == null|| valueInserrt=="")
                    {
                        valueInserrt = "אוטומציה";
                    }
                    //  waitUntil(by);
                    if (driver.FindElements(by).Count > 0)
                    {
                        IWebElement iElment = driver.FindElement(by);
                        if (mikom != -1)
                            iElment = driver.FindElements(by)[mikom];
                        if (iElment.Text != "")
                            iElment.Clear();
                        if (checkRequerd == true)
                        {
                            iElment.SendKeys("");
                            iElment.SendKeys(Keys.Tab);
                            if (valueRequerd != null)
                                funError(1);
                            else
                                funErrorNull(by);
                        }
                        iElment.SendKeys(valueInserrt);
                        iElment.SendKeys(Keys.Tab);
                        /*if (iElment.Text.Trim()!= valueInserrt.Trim())
                            iElment.SendKeys(valueInserrt);*/
                    }

                }

                Console.WriteLine("insert value " + nameCheck);
            }
            catch { picter("insertElment " + nameCheck); }
        }
        public string  funErrorNull(By by)
        {
            Color ErrColour = Color.FromName(driver.FindElement(by).GetCssValue("color"));
           // Color ErrColour1 = Color.FromName(driver.FindElement(By.XPath("//span/label/mat-label")).FindElement(by).GetCssValue("border-bottom-color"));

            if (!ErrColour.Equals(Color.FromName("rgba(95, 31, 98, 1)"))/*|| !ErrColour1.Equals(Color.FromName("rgba(95, 31, 98, 1)"))*/)//"rgba(95, 31, 98, 1)"
            {//
                picter("funErrorNull " + driver.FindElement(by).Text);
                Console.WriteLine(Color.FromName(driver.FindElement(by).GetCssValue("color")));
                Console.WriteLine(Color.FromName(driver.FindElement(by).GetCssValue("background-color")));
                return "bug in not requerd";
            }
            else
                return ErrColour.ToString();
        }
        public void checkDataNot(string nameNot)
        {
            try
            {
                if (driver.FindElement(By.Name(nameNot)).Displayed)
                    Debug.WriteLine("the " + nameNot + " display ");
            }
            catch
            {
                picter("checkDataNot " + nameNot);
                Console.WriteLine("the " + nameNot + "not display,is very good");
            }
        }

        public void clickTagName(string tagName, string valueClick,string nameTasrit)
        {
            try
            {
                IList<IWebElement> by = driver.FindElements(By.TagName(tagName));
                for (int i = 0; i < by.Count; i++)
                {
                    if (by[i].Text.Contains(valueClick))
                        by[i].Click();
                }
            }
            catch
            {
                picter("clickTagName " + valueClick);
            }
            //throw new NotImplementedException();
        }

        public void ButtonNew(string nameNew,string ending="",int numButton=0)
        {
            //new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            try
            {
                if (numButton != 0)
                    driver.FindElements(By.CssSelector("button[class='button-Create mat-button mat-button-base ng-star-inserted']"))[numButton].Click();
                else
                {
                    //string tooltip = driver.FindElement(By.CssSelector("button[class='button-Create mat-button mat-button-base ng-star-inserted']")).GetAttribute("title");
                        driver.FindElement(By.CssSelector("button[class='button-Create mat-button mat-button-base ng-star-inserted']")).Click(); }
                //waitUntil(By.XPath("//span[@class='mat-button-wrapper'][text()=' יצירת " + nameNew + " חדש" + ending + " ']"));
                //ClickButton(" יצירת " + nameNew + " חדש"+ending+" ","span");
                //driver.FindElement(By.XPath(".//button/span[contains(@class,'wrap-action-name')][contains(text(),' יצירת " + nameNew + " חדש '])")).Click();
                //Console.WriteLine("the click button: " + nameNew);
            }
            catch
            {
                picter("ButtonNew " + nameNew);
                Console.WriteLine("the not click button: " + nameNew);
            }
        }
        /// <summary>
        /// משמש לבחרית סוג כמו הגדרותניהול כספים וכדו'
        /// </summary>
        /// <param name="valueName"></param>
        public void clickNameDefinitions(string valueName, int num = 0)
        {
            try
            {
                if (valueName != "")
                {
                    waitUntil(By.XPath("//div[@class='wrap-action-name'][text()='" + valueName + "']"));
                    driver.FindElements(By.XPath("//div[@class='wrap-action-name'][text()='" + valueName + "']"))[num].Click();
                    Console.WriteLine("click list :" + valueName);
                }
            }
            catch
            {
                Debug.WriteLine("list not display:" + valueName);
                picter("clickNameDefinitions" + valueName);
            }
        }
        public void actionWork(string nameToolTip,string pnimi)
        {
            try
            {
                By iconTool = By.CssSelector("button[mattooltip='" + nameToolTip.Trim() + "']");
                waitUntil(iconTool);
                driver.FindElement(iconTool).Click();
                Thread.Sleep(500);
                if(pnimi.Contains("מוסדות"))
                    clickButoonToContains(pnimi.Trim(),1);//clickNameDefinitions
               else if (pnimi!="")
                clickButoonToContains(pnimi.Trim());//clickNameDefinitions
            }
            catch
            {
                picter(nameToolTip + " " + pnimi);
                throw new Exception();
            }
        }
        public void closePoup(int i=-1)
        {
            try
            {
                if (i == -1)
                    driver.FindElement(By.CssSelector("button[aria-label='edit' ]")).Click();
                else
                    driver.FindElements(By.CssSelector("button[aria-label='edit']"))[i].Click();
            }
            catch
            {
                picter("closePoup");
                throw new NotImplementedException("closePoup");
            }
        }
        public void addValueList(string nameValue,int mikom,By classkodem=null,int keyTab=1)
        {
            try
            {
                driver.FindElement(classkodem).Click();
                if(keyTab==1)
                    driver.FindElement(classkodem).SendKeys(Keys.Tab+ nameValue);
                else
                    driver.FindElement(classkodem).SendKeys(Keys.Tab +Keys.Tab + nameValue);

                driver.FindElements(By.CssSelector("i[class='material-icons button-icon-done ng-star-inserted']"))[mikom].Click();
             }
            catch { picter("addValueList"); }
        }

        public int insertSum(bool checkSum=false,string sum=null,bool insertSum=false)
        {
            try
            {
                Thread.Sleep(800);
                IWebElement sumInsert = driver.FindElement(By.Name("Sum"));
                
                if (checkSum == true)
                {
                    if (sumInsert.Text != sum)
                    {
                        Debug.WriteLine("sum insert invalid");
                        picter("sumInsertValid");
                    }
                }
                else
                {
                    if(insertSum==false)
                        sumInsert.SendKeys("10"); 
                    else
                      sumInsert.SendKeys(rand(1000,10).ToString()); }
                return Int32.Parse(driver.FindElement(By.XPath("//input[@formcontrolname='Sum']")).Text);
            }
            catch
            {
                picter("insertSum");
                new NotImplementedException("insert sum not good");
                return 0;
            }
        }
        public void checkCreate(string nameCreate)
        {
            checkElment("close popup", By.CssSelector("button[aria-label='edit']"));
            checkElmentText("title", By.CssSelector("div[class='main-title ng-star-inserted']"), "יצירת " + nameCreate);
            checkElmentText("subtitle", By.ClassName("sub-title"), "אנא מלא/י את הפרטים הבאים");
        }
        public void checkTable(string listText,string listHeder,string byClass,string byHederRow= "//thead[@role='rowgroup']")
        {
            try
            {
                if (driver.FindElement(By.ClassName("no-data-msg ng-star-inserted")).Displayed)
                {
                    Debug.WriteLine("no data");
                }
            }
            catch { }
            
                checkElment("tableProductionReceipt", By.ClassName(byClass));
                checkElmentText("th table", By.XPath(byHederRow), listHeder);
                Console.WriteLine("checkTable");
                //  Console.WriteLine(driver.FindElement(By.XPath("//thead[@role='rowgroup']")).Text);
                //IWebElement tableElement = driver.FindElement(By.ClassName("receipts-2-treatment-table"));
                IWebElement tableElement = driver.FindElement(By.CssSelector("div[class='" + byClass + "']"));
                IList<IWebElement> tableRow = tableElement.FindElements(By.TagName("tr"));
                IList<IWebElement> rowTD;
                /*rowTD = tableRow[1].FindElements(By.TagName("td"));
                rowTD[1].Click();*/
                //rowTD = tableRow[i].FindElements(By.TagName("td"));
                for (int i = 1; i < tableRow.Count; i++)
                {
                    rowTD = tableRow[i].FindElements(By.TagName("td"));

                    if (tableRow[i].Text.Equals(listText))
                    {
                        if (byClass == "checks-table-wrapper ng-star-inserted")
                            driver.FindElement(By.CssSelector("mat-checkbox[ng-reflect-name='checkBok" + (i - 1) + "']")).Click();
                        else
                            tableRow[i].FindElement(By.TagName("mat-checkbox")).Click();
                        // rowTD[0].Click();

                        Console.WriteLine(tableRow[i].Text);
                        Console.WriteLine(rowTD[0].Text);
                        break;
                    }
                    else
                    {
                        Console.WriteLine(listText);
                        Console.WriteLine(tableRow[i].Text);
                    }
                }
        }
        public void checkHeder(string listHeder)
        {
            checkElmentText("th table", By.XPath("//thead[@role='rowgroup']"), listHeder);
        }
        public int checkColumn(string byClass, string[] columCheck, string clickList,string typeTagNAmerow= "tr",string typeTNCell="td",int cellClick=-1,int numTable=0,bool equals=false)
        {
            int ret = 0;
            try
            {
                By classBy;
                if (byClass.Contains("//"))
                    classBy = By.TagName("mat-table");
                else
                {
                    string byCLass1;
                    if (!byClass.Contains("["))
                        byCLass1 = "div[class='" + byClass + "']";
                    else
                        byCLass1 = byClass;
                    classBy = By.CssSelector(byCLass1);
                    if (byClass.Contains("[]"))
                        classBy = By.TagName("tbody");
                }
                IWebElement tableElement;
                if (numTable != 0)
                {
                    int s= driver.FindElements(classBy).Count - 1;
                     tableElement = driver.FindElements(classBy)[s]; }
                else
                    tableElement = driver.FindElement(classBy);
                ret = driver.FindElements(classBy).Count;
                //IWebElement tableElement = driver.FindElement(By.TagName("tbody"));
                IList<IWebElement> tableRow;
                //IList<IWebElement> tableRow = tableElement.FindElements(By.XPath("//tr[@role='row']"));
                if (!typeTagNAmerow.Contains("["))
                    tableRow = tableElement.FindElements(By.TagName(typeTagNAmerow));
                else
                    tableRow = tableElement.FindElements(By.CssSelector(typeTagNAmerow));
                IList<IWebElement> rowTD;
                // Console.WriteLine( tableRow.Count);
                for (int i = 0; i < tableRow.Count; i++)
                {
                    //ret = tableRow.Count;
                    if (!typeTNCell.Contains("["))
                        rowTD = tableRow[i].FindElements(By.TagName(typeTNCell));
                    else
                        rowTD = tableRow[i].FindElements(By.CssSelector(typeTNCell));
                    // rowTD = tableRow[i].FindElements(By.XPath("//td[@role='gridcell']"));
                    Console.WriteLine(rowTD.ToString());
                    // Console.WriteLine(rowTD[0].Text);

                    for (int j = 0; j <= rowTD.Count; j++)
                    {
                        try
                        {
                            if (!rowTD[j].Text.Contains(columCheck[j].Trim()) && columCheck[j] != ""||(columCheck[j] == " "&& rowTD[j].Text!="")||(equals==true&& columCheck[j]!= "" &&rowTD[j].Text.Trim()!=columCheck[j].Trim()))
                            {
                                Console.WriteLine(rowTD[2].ToString());
                                //Console.WriteLine(columCheck[j]);
                                if (j != 2 || columCheck[2] != "false")
                                    j = rowTD.Count();
                                //  break;
                            }
                            else if (j + 1 == rowTD.Count || columCheck[j + 1] == "-1" || j >= columCheck.Length)
                            {
                                //Console.WriteLine(tableRow[i].Text);
                                // Console.WriteLine(columCheck[j]);
                                if (clickList != "" && cellClick == -1 && clickList != "false")
                                    driver.FindElement(By.XPath(clickList)).Click();
                                else if (clickList == "false")
                                    Console.WriteLine("the row is good,is display in table");
                                else if (clickList.Contains("STRING"))
                                {
                                    char[] tav = { 'S', 'T', 'R', 'I', 'N', 'G', 'N', 'O' };
                                    if (rowTD[cellClick].FindElement(By.TagName("input")).Text.Trim() == "")
                                        rowTD[cellClick].FindElement(By.TagName("input")).Clear();
                                    rowTD[cellClick].FindElement(By.TagName("input")).SendKeys(clickList.Split(tav)[0]);
                                    if (!clickList.Contains("NO"))
                                        driver.FindElement(By.CssSelector("i[class='material-icons save-pay-to ng-star-inserted']")).Click();
                                }
                                else if (cellClick == -1 && clickList == "")
                                {
                                    tableRow[i].Click();
                                }
                                else if (cellClick != -1 && clickList == "")
                                {
                                    rowTD[cellClick].Click();
                                }
                                else
                                {
                                    if (clickList.Contains("YEC"))
                                    {
                                        char[] tav = { 'Y', 'E', 'C' };
                                        driver.FindElements(By.XPath(clickList.Split(tav)[0]))[i].Click();
                                        clickList = "-1";
                                    }
                                    if (clickList.Contains("MOVE"))
                                    {
                                        char[] tav = { 'M', 'O', 'V', 'E' };
                                        Actions actions = new Actions(driver);
                                        actions.MoveToElement(rowTD[cellClick]).Perform();
                                         try { driver.FindElement(By.XPath("//span/i[@class='material-icons payment-icon'][text()=' payment']")).Click(); } 
                                            catch { } 
                                    }
                                    else if (cellClick > 0 && clickList != "" && clickList.Contains("//"))
                                        rowTD[cellClick].FindElement(By.XPath(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();
                                    else if (cellClick > 0 && !clickList.Contains("//") && clickList.Contains("["))
                                        rowTD[cellClick].FindElement(By.CssSelector(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();//remove "button"
                                    else if ((cellClick > 0) || (cellClick == 0 && !clickList.Contains("//") && !clickList.Contains("[")))
                                        rowTD[cellClick].FindElement(By.TagName(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();//remove "button"

                                    else if (clickList.Contains("//"))
                                        tableRow[i].FindElement(By.XPath(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();
                                    else if (clickList.Contains("["))
                                        tableRow[i].FindElement(By.CssSelector(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();
                                    else
                                        tableRow[i].FindElement(By.TagName(clickList/*+ "[@id='mat-checkbox-"+(i)+"']"*/)).Click();
                                }
                                // rowTD[2].Click();
                                clickList = "-1";
                                break;

                            }
                        }
                        catch { 
                                    j = rowTD.Count();
                        }
                    }
                    if (clickList == "-1")
                        break;
                }
            }
             catch { picter("checkColumn");
                throw new Exception();
            }
            if (clickList != "-1")
                ret = -1;
             return ret;
                }
        public string  checkEndInput()
        {
            int count = driver.FindElements(By.TagName("input")).Count;
            return driver.FindElements(By.TagName("input"))[count - 1].GetAttribute("value");
        }
        public void OpenExcel(string nameTest,string trueOrFalse="passed")
        {
            ExcelApiTest eat = new ExcelApiTest("", "tasret");
            eat.bugReturn("tasret", trueOrFalse, nameTest);
            Thread.Sleep(100);
            newFile("c:/"+DateTime.Today.ToString("MM/dd") , nameTest);
           
        }
        public void foldertofolder(string folder1,string folder2)
        {
            if (Directory.Exists(folder1))
            {
                foreach (var file in new DirectoryInfo(folder1).GetFiles())
                {
                    file.MoveTo($@"{folder2}\{file.Name}");
                }
            }
        }
        public void clickRadio(string valueClick)
        {
            for (int i = 0; i < driver.FindElements(By.TagName("mat-radio-button")).Count; i++)
                if (driver.FindElements(By.TagName("mat-radio-button"))[i].Text.Contains(valueClick))
                { driver.FindElements(By.TagName("mat-radio-button"))[i].Click(); break; }
        }
        public void checkTableofColum(string byClass, string listHeder, string []columCheck, string clickList)
        {
           // checkElment("tableProductionReceipt", By.ClassName("receipts-2-treatment-table"));
            checkHeder(listHeder);
            //  Console.WriteLine(driver.FindElement(By.XPath("//thead[@role='rowgroup']")).Text);
            //IWebElement tableElement = driver.FindElement(By.ClassName("receipts-2-treatment-table"));
            checkColumn(byClass, columCheck, clickList,"tr","td",6);
        }
        public void checkTableHeder(string [] columCheck,string xpathHeder= "//mat-header-row[@role='row']",string tagtr="tr",string tagtd="td")
        {
            IWebElement tableElement;
            if (xpathHeder.Contains("@"))
                 tableElement = driver.FindElement(By.XPath(xpathHeder));
            else
                tableElement = driver.FindElement(By.CssSelector(xpathHeder));

            IList<IWebElement> tableRow = tableElement.FindElements(By.TagName(tagtr));
            IList<IWebElement> rowTD;
           // rowTD = tableRow[1].FindElements(By.TagName("td"));
            for (int i = 1; i < tableRow.Count; i++)
            {
                rowTD = tableRow[0].FindElements(By.TagName(tagtd));
                int checkList = 0;
                for (int j = 0; j < rowTD.Count - 1; j++)
                    if (!rowTD[j].Text.Contains(columCheck[j]) && columCheck[j] != "")
                    {
                       // Console.WriteLine(tableRow[i].Text , columCheck[j]);
                        break;
                    }
                    else
                    {
                       // Console.WriteLine(tableRow[i].Text);
                       // Console.WriteLine(columCheck[j]);
                        checkList = 1;
                        break;
                    }
                if (checkList == 1)
                    break;
            }
        }
       
        public void selectForRequerd(string nameSelect, bool requerd = true,int numClick=1,string clik= "true",int num=0)
        {
            try
            {
                
                int i = 0;
                IList<IWebElement> by = driver.FindElements(By.TagName("mat-label"));
                for (i = 0; i <= by.Count; i++)
                {
                    if (by[i].Text.Contains(nameSelect) && num == 0)
                    {
                        if (requerd == true)
                        {
                            new Actions(driver).MoveToElement(by[i]).Click().Perform();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).Perform();
                            //by[i].SendKeys(Keys.Tab);
                            funError(0);
                        }
                        if (clik == "true")
                        {
                            new Actions(driver).MoveToElement(by[i]).Click().Perform();
                          //  new Actions(driver).MoveToElement(by[i]).Click().Perform();
                        }
                        else if (clik.Contains("bulid"))
                        {
                            char[] bul = { 'b', 'u', 'l', 'i', 'd' };
                            clik = clik.Split(bul)[0];
                            new Actions(driver).MoveToElement(by[i]).SendKeys(clik).Build().Perform();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).Perform();
                        }
                        else
                        {
                            // new Actions(driver).MoveToElement(by[i]).Click();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(clik).Perform();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).Perform();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(clik).Perform();
                            new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).Perform();
                        }
                        break;
                    }
                    else { if (by[i].Text.Contains(nameSelect) && num > 0) num--; }
                }
                
                if (numClick != -1)
                {
                    driver.FindElements(By.ClassName("mat-option-text"))[numClick].Click();
                    try { new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).Perform(); } catch { }
                }

            }
            catch { picter("selectForRequerd"); }
        }
        public void insertclickTabinser( string nameSelect, bool requerd = true, int numClick = 1, string clik = "true")
        {
            try
            {

                int i = 0;
                IList<IWebElement> by = driver.FindElements(By.TagName("mat-label"));
                for (i = 0; i < by.Count; i++)
                {
                    if (by[i].Text.Contains(nameSelect))
                    {
                        if (requerd == true)
                        {
                            by[i].SendKeys("");
                            funError(0);
                        }
                        if (clik == "true")
                            by[i].Click();
                        else
                        {
                            //driver.FindElement(By.CssSelector("#input>mat-label[" + i + "]")).SendKeys(clik);
                            new Actions(driver).MoveToElement(by[i]).SendKeys(Keys.Tab).SendKeys(clik).Build().Perform(); //by[i].SendKeys(clik);

                        }

                        break;
                    }
                }
                if (numClick != -1)
                    driver.FindElements(By.ClassName("mat-option-text"))[numClick].Click();
            }
            catch { picter("insertclickTabinser"); throw new Exception(); }
        }
        public void spinner()
        {
            try
            {
               // while (driver.FindElement(By.TagName("mat-progress-spinner")).Displayed)
                    Thread.Sleep(230);
            }
            catch { }
        }
        public void clickButoonToContains(string nameButton,int num=0)
        {
            buttonwr = driver.FindElements(By.XPath("//div[contains(@class,'wrap-action-name' )]"));
            waitUntil(By.XPath("//div[contains(@class,'wrap-action-name' )]"));
            // Thread.Sleep(200);
            for (int i = 0; i < buttonwr.Count; i++)
            {
                if (buttonwr[i].Text.Contains(nameButton))
                {
                    if (num > 0)
                        num--;
                    else
                    {
                        buttonwr[i].Click();
                        break;
                    }
                }
            }
        }
        public void newFile(string pathFolder,string nameFolder)
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddUserProfilePreference(nameFolder, pathFolder);
        }
        public string  picter(string nativ)
        {
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;

            Screenshot screenshot = screenshotDriver.GetScreenshot();
            //ExcelApiTest eat = new ExcelApiTest("", "tasret");
            //  string valueName = eat.createExcel("tasret", "", false, 2,-2);
           
            char[] tav = { '\'', '\"',':','/','?' };
            if (nativ.Contains(tav[0])|| nativ.Contains(tav[1])|| nativ.Contains(tav[2])|| nativ.Contains(tav[3]))
                 nativ = nativ.Split(tav)[0];
            screenshot.SaveAsFile("c:/"+DateTime.Today.ToString("MM/dd")+"/" + nativ+".png");
           // screenshot.SaveAsFile("c:/bug/" + nativ+".png");
            return "c:/" + DateTime.Today.ToString("MM/dd") + "/" + nativ + ".png";//"c:/bug/" + nativ + ".png";
        }
        public void checkTitles(string[] titles)
        {
            ReadOnlyCollection<IWebElement> title = driver.FindElements(By.ClassName("title-section"));
            for(int i=0;i<titles.Length;i++)
            {
                if (title[i].Text.Contains(titles[i]))
                {
                    Console.WriteLine("the title " + titles[i] + "check");
                }
            }
            // throw new NotImplementedException();
        }

        public void refres()
        {
            try { driver.Navigate().Refresh(); } catch { throw new NotImplementedException(); }
        }

        public void checkInRow(string mllRow)
        {
            try {
                IList<IWebElement> rows = driver.FindElements(By.TagName("mat-row"));
                for(int i=0;i<rows.Count;i++)
                {
                    if (rows[i].Text.Contains(mllRow))
                        break;
                    if(i+1>rows.Count)
                        throw new NotImplementedException();
                }
            } catch { throw new NotImplementedException(); }
        }
    }
}
