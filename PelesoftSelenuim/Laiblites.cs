﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PelesoftSelenuim
{
    class Laiblites : page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();

        public void stepNewLaiblites(string p="",string name="")
        {
            fp.ButtonNew("התחייבות", "ה");
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.checkClasscreate("התחייבות", false);
            cc.checkStepHeder(1, 2);
            cc.clickFrindNew(name,"ידיד","","",p+"LastName");
            step2("תרומה");
        }

        public void step2(string finance="אוטומציה")
        {
            cc.checkStepHeder(2, 2);
            checkElment();
            insertElment(finance);
        }

        public void insertElment(string finance = "אוטומציה")
        {
            fp.insertElment("check Sum requerd", By.XPath("//input[@formcontrolname='Sum']"), amount, true);
            fp.SelectRequerdName("formcontrolname='CurrencyID", "שקל", false);
            fp.SelectRequerdName("formcontrolname='FinancialClausesID", finance);
            cc.calanederCheck(1);
            //fp.checkSelectPreform("תאריך תחילת התחייבות");
            fp.insertElment("check Sum Num Payments", By.XPath("//input[@formcontrolname='NumPayments']"), "2");
            fp.ClickButton(" צור התחייבות חדשה ", "span");
            SucessEndCreate sec = new SucessEndCreate();
            sec.checkEndCreate("התחייבות", false, "ה", nameFrind);
        }

        public void checkElment()
        {
            fp.checkElmentText("check Sum requerd", By.XPath("//input[@formcontrolname='Sum']"), " סכום התחייבות ");
            fp.checkElmentText("check type sum", By.XPath("//mat-select[@formcontrolname='CurrencyID']"), " סוג מטבע");
            fp.checkElmentText("check Sum financial", By.XPath("//mat-select[@formcontrolname='FinancialClausesID']"), " סעיף ");
            fp.checkSelectPreform("תאריך תחילת התחייבות");
            fp.checkElmentText("check Sum Num Payments", By.XPath("//input[@formcontrolname='NumPayments']"), "מספר תשלומים");
        }

        public void offsetting()
        {
            Thread.Sleep(500);
            popupList pl = new popupList();
            pl.expansionPopUP();
            //driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' קח אותי לכרטיס התחייבות ']")).Click();
            Thread.Sleep(900);
           if (driver.FindElement(By.TagName("some-element")).Text.Contains("לקיזוז ההתחייבות"))
                driver.FindElement(By.TagName("some-element")).Click();
            //fp.click(By.XPath("//button[aria-label@='btnOpenCommitmentOffsetting']"));

            checkPageOffsetting();
            Thread.Sleep(900);
            offse();
        }
        public void offse(bool exap=true)
        {
            fp.ClickButton(" לקיזוז ", "span");
            approvePopup();
            checkRemove(exap);
        }
        private void checkRemove(bool exap=true)// לבדוק איך נראה בתשלום
        {
            //driver.Navigate().Refresh();//בהו"ק יראה אחרת
            Thread.Sleep(300);
            fp.checkElmentText("cencel offset",By.XPath("//button[@aria-label='btnDisconnectionOffsetting']"), " לניתוק קיזוזים ");
            fp.click(By.XPath("//button[@aria-label='btnDisconnectionOffsetting']"));
            popupCanecl(exap);
            int i = exap == false ? 2:1;
            try
            {
                for (int j = i; j >= 0; j--)
                    fp.closePoup(j);
            }
            catch { }
        }

        private void popupCanecl(bool exap=true)
        {
            try { checkpoupCancel();
                string[] clickCut = { "","-1"};
                fp.checkColumn("tbody[role='rowgroup']", clickCut,"button","tr","td",0);
                fp.checkElment("popup approve",By.ClassName("wrap-confirm-dialog"));
                fp.checkElmentText("masage popup remove",By.ClassName("title"), " אשר פעולה ");
                fp.checkElmentText("masage popup remove",By.XPath("//div[@class='wrap-message']/div[@class='ng-star-inserted']"), "האם אתה בטוח שברצונך לנתק רשומת קיזוז זו ?");
                fp.checkElment("masage popup remove no",By.XPath("//span[@class='mat-button-wrapper'][text()='לא']"));
                fp.checkElment("masage popup remove yes",By.XPath("//span[@class='mat-button-wrapper'][text()='כן']"));
                fp.ClickButton("לא");
                fp.checkColumn("tbody[role='rowgroup']", clickCut, "button", "tr", "td", 0);
                fp.ClickButton("כן");
                Thread.Sleep(500);
                fp.checkElmentText("no data",By.CssSelector("div[class='no-data-msg ng-star-inserted']"), " לא נמצאו רשומות ");
                int i = exap == false ? 2 : 1;
                try { fp.closePoup(i); } catch { }
                fp.checkElment("button offse",By.XPath("//some-element[@class='ng-star-inserted'][text()='לקיזוז החיוב ']"));
            }
            catch {
                fp.picter("popupCanecl קיזוז");
                    throw new NotImplementedException(); }
        }

        private void checkpoupCancel()
        {
            try { fp.checkElmentText("title cancel",By.CssSelector("div[class='main-txt title']"), "בחר/י את הרשומות שברצונך לנתק");
                 string[] listHeder = {"", "תאריך לתשלום" , "סכום התשלום", "חודש חיוב", "סעיף חיוב","הסכום שקוזז","-1" };
                fp.checkColumn("thead[role='rowgroup']", listHeder,"false","tr","th",-1,2);
                fp.checkElmentText("no-data-msg warning",By.CssSelector("div[class='no-data-msg warning']"), " אזהרה: במסך זה לא מופיעים תשלומים שבוצעו בהו\"ק ");
            } catch { throw new NotImplementedException(); }
        }

        private void approvePopup()
        {
            fp.checkElmentText("check title approve action", By.ClassName("title"), " אשר פעולה ");
            fp.checkElmentText("check message approve action", By.ClassName("ng-star-inserted"), " האם אתה בטוח שברצונך לקזז ");
            fp.ClickButton("כן");
            fp.checkElment("check approve", By.XPath("//div[text()=' הפעולה בוצע בהצלחה ']"));
            // throw new NotImplementedException();
        }

        public void checkPageOffsetting(string requerd= "התחייבויות",int requerdone=0,int payment=1)
        {

            fp.checkElmentText("check title", By.ClassName("main-txt"), "בחר/י את "+requerd+" ותשלומים שברצונך לקזז");
            fp.checkElmentsText("check sb title offset", By.CssSelector("div[class='main-txt sub']"), requerd, 0);
            fp.checkElmentsText("check sb title offset", By.CssSelector("div[class='main-txt sub']"), "תשלומים", 1);
            string[] checkTableOffsetting = { "תאריך", "סכום כללי", "יתרה לתשלום","סעיף פיננסי", "מטבע", "סטטוס" };//סכום שנותר
            checkColumn("thead", requerdone, checkTableOffsetting, "false", "th");
            string[] checkTablePayments = { "סכום כללי", "יתרה", "מטבע", "שער מטבע", "סעיף פיננסי", "סוג תשלום" };//סכום שנותר
            checkColumn("thead", payment, checkTablePayments, "false", "th");
            fp.checkElmentText("button offset", By.ClassName("mat-button-wrapper"), " לקיזוז ");
            //throw new NotImplementedException();
        }
        public void checkColumn(string byClass, int numTable, string[] columCheck, string clickList, string typeTNCell = "td")
        {
            By classBy = By.TagName(byClass);

            IWebElement tableElement = driver.FindElements(classBy)[numTable];
            IList<IWebElement> tableRow = tableElement.FindElements(By.CssSelector("tr"));
            IList<IWebElement> rowTD;
            // Console.WriteLine( tableRow.Count);
            for (int i = 0; i < tableRow.Count; i++)
            {
                rowTD = tableRow[i].FindElements(By.CssSelector(typeTNCell));
                for (int j = 1; j < rowTD.Count; j++)
                    if (!rowTD[j].Text.Contains(columCheck[j]) && columCheck[j] != "")
                    {
                        Console.WriteLine(rowTD[j].GetType());
                        if (j != 2 || columCheck[2] != "false")
                            j = rowTD.Count();
                    }
                    else if (j + 1 == rowTD.Count - 1 || columCheck[j + 1] == "-1")
                    {
                        if (clickList == "false")
                            Console.WriteLine(tableRow[i].Text);
                        else
                        {
                            tableRow[0].FindElement(By.TagName("mat-checkbox")).Click();
                        }
                        clickList = "-1";
                        break;

                    }
                if (clickList == "-1")
                    break;
            }
        }

        public string shearchNameRequerd()
        {
            string name = "";
          try{ IList<IWebElement> tableRow=driver.FindElements(By.TagName("mat-row"));
                IList<IWebElement> tableColumn = tableRow[0].FindElements(By.TagName("mat-cell"));
                name = tableColumn[3].Text+" "+ tableColumn[4].Text;

            } catch { fp.picter("shearchNameRequerd"); throw new NotImplementedException(); }
            return name;
        }

        public void clickTableUser(int mikom=0)
        {
            try {
                IWebElement tableName = driver.FindElement(By.TagName("mat-table"));
                IList<IWebElement> tableRow = tableName.FindElements(By.TagName("mat-row"));
                 tableRow[mikom].Click();
            } catch { throw new NotImplementedException(); }
        }
    }
}