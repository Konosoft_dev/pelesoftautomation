﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class BilingClauses:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        CreateNewDenumic cnd = new CreateNewDenumic();
        createConatrabition cc = new createConatrabition();
        PupilNew pn = new PupilNew();
        public  void stepBilingClauses(string forMosad, string heder, string bilingType, string amount, string tadirot, string type)
        {
            cnd.checkClasscreate("הגדרת סעיף חיוב ",false);
            cc.checkStepHeder(1, 2);
            cc.clickFrind("מוסד חינוך", forMosad, "מוסד חינוך");
            cc.checkStepHeder(2, 2);
            checkAllBiling();
            insertElmentBilingClauses(heder,bilingType,amount,tadirot,type);
            fp.ClickButton(" צור הגדרת סעיף חיוב חדש ", "span");

            //throw new NotImplementedException();
        }
        public void checkAllBiling()
        {
            fp.checkElmentText("check title", By.CssSelector("div[class='main-title ng-star-inserted']"), " יצירת הגדרת סעיף חיוב ");
            fp.checkElmentsText("check title-section", By.ClassName("title-section"), "פרטי סעיף חיוב", 0);
            fp.checkElmentsText("check title-section", By.ClassName("title-section"), "פרטי גביה", 1);
            fp.checkElmentsText("check title-section", By.ClassName("title-section"), "עבור", 2);
            fp.checkElmentsText("check title-section", By.ClassName("title-section"), "פרטים נוספים", 3);
            fp.checkElmentText("check ClauseName", By.Name("ClauseName"), " שם סעיף חיוב ");
            fp.checkSelectPreform(" סוג סעיף חיוב ");
            fp.checkSelectPreform("בחר/י סעיף פיננסי עבור ההגדרת סעיף חיוב");
            fp.checkElmentText("check Sum", By.Name("Sum"), " סכום ");
            fp.checkSelectPreform("בחר/י מטבע עבור ההגדרת סעיף חיוב");
            fp.checkSelectPreform("בחר/י תדירות גביה עבור ההגדרת סעיף חיוב");
            fp.checkSelectPreform("בחר/י שנה\"ל עבור ההגדרת סעיף חיוב");
            fp.checkSelectPreform("סניף");
            fp.checkSelectPreform(" סוג רמת כיתה ");
            fp.checkElmentText("check textarea", By.TagName("textarea"), "הערות ");
            fp.checkElmentText("check button", By.ClassName("mat-button-wrapper"), " צור הגדרת סעיף חיוב חדש ");
        }
        public void insertElmentBilingClauses(string heder, string bilingType, string amount, string tadirot,  string type)
        {
            fp.insertElment("check ClauseName", By.Name("ClauseName"), heder);
            selectP(" סוג סעיף חיוב ", type);
            selectP("בחר/י סעיף פיננסי עבור ההגדרת סעיף חיוב", bilingType);
            selectP("בחר/י מטבע עבור ההגדרת סעיף חיוב", "שקל");
            Tadirot(tadirot);
            fp.selectForRequerd("בחר/י שנה\"ל עבור ההגדרת סעיף חיוב", true, 2);
            fp.selectForRequerd("בחר/י סניף עבור ההגדרת סעיף חיוב", false, -1);
            string branch = driver.FindElements(By.ClassName("mat-option-text"))[1].Text;
            fp.selectText(branch);
            if (branch.Contains("ישיבה"))
                amount = (Int32.Parse(amount) * 2).ToString();
            fp.insertElment("check Sum", By.Name("Sum"), amount);

            fp.selectForRequerd("סוג רמת כיתה", false);
            fp.insertElment("check textarea", By.TagName("textarea"), automation);
        }
        public void selectP(string name, string nameSelect)
        {
            fp.selectForRequerd(name, true, -1);
            fp.selectText(nameSelect);
        }
        public void Tadirot(string tadirot)
        {
            char[] tav = { ',' };
            char[] tavCl = { '/' };
            string[] mount1 = tadirot.Split(tav)[1].Split(tavCl);
            selectP("בחר/י תדירות גביה עבור ההגדרת סעיף חיוב", tadirot.Split(tav)[0]);
            if (tadirot.Split(tav).Length > 1)
            {
                string[] mount2 = tadirot.Split(tav)[2].Split(tavCl);
                cc.calanederCheck(-1, 0, mount1[0], Int16.Parse(mount1[1]), Int16.Parse(mount1[2]));
                cc.calanederCheck(-1, 1, mount2[0], Int16.Parse(mount2[1]), Int16.Parse(mount2[2]));

            }
            else
                cc.calanederCheck(-1, -1, mount1[0], Int16.Parse(mount1[1]), Int16.Parse(mount1[2]));


        }
    }
}