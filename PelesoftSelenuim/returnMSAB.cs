﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class returnMSAB:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public returnMSAB()
        {
        }

        public void openMsabPage()
        {
            try
            {
                fp.actionWork(" ניהול כספים ","דיווח חזרות מסב");
            }
            catch { throw new NotImplementedException("not open page return msab"); }
        }

        public  void checkPageReturnMsab()
        {
            try
            {
                fp.checkElmentText("checkTitle", By.ClassName("title-wrapper"), " החזרות מס\"ב");
               /* fp.selectForRequerd("מוסד לסליקה", true,-1);
                fp.selectText(massav);*/
                //fp.checkElmentTextAL("InstitutionClearing", "מוסד לסליקה");
                fp.checkElment("date", By.TagName("app-long-date"));
                fp.checkElment("print file return Bank", By.Name("Description"));
                fp.checkElment("edit print file return Bank", By.CssSelector("i[class='material-icons button-icon-add edit']"));
                fp.checkElmentText("button upload file", By.ClassName("mat-button-wrapper"), " טען קובץ ");
            }
            catch { throw new NotImplementedException("checkPageReturnMsab invaled"); }
        }

        public  void insertData()
        {
            try
            {
                createConatrabition cc = new createConatrabition();
                fp.selectForRequerd("מוסד סליקה", false, -1);
                fp.selectText(massav);
                cc.calanederCheck(1);
               // fp.checkElment("print file return Bank", By.Name("Description"));
                driver.FindElement( By.CssSelector("i[class='material-icons button-icon-add edit']")).Click();
                popupMsab();
                fp.ClickButton(" טען קובץ ","span");
                checkTableCreate();
            }
            catch { throw new NotImplementedException(""); }
        }

        public void checkTableCreate()
        {
            try
            {
                string[] listHeder = {"מזהה לקוח","שם לקוח","בנק","סניף","חשבון","סכום החיוmat-header-row","הודעת שגיאה","יום חיוב","נמצאה רשומת החיוב"};
                fp.checkTableHeder(listHeder, "//mat-table[@role='grid']", "mat-header-row", "mat-header-cell");
                string[] listRow = {"123",family, nameBank.Trim(new char[] { ' ','0'}),numBranch.Trim(new char[] { ' ', '0' }),numberAmountBank,amount+".00","בטול הרשאה","-1"};
                fp.checkColumn("mat-table[role='grid']", listRow, "false", "mat-row", "mat-cell", 8);
            }
            catch { throw new NotImplementedException("checkTableCreate invaled"); }

        }

        public void popupMsab()
        {
            try
            {
                checkPOPUP();
                insertValueAndCeckRequired("ClientId", "10006");
                insertValueAndCeckRequired("ClientName",family);

                insertValueAndCeckRequired("Bank", nameBank);
                insertValueAndCeckRequired("Branch", numBranch);
                insertValueAndCeckRequired("Account", numberAmountBank);
                insertValueAndCeckRequired("Amount", amount);
                driver.FindElement( By.XPath("//mat-select[@formcontrolname='Reason']")).Click();
                fp.selectText("בטול הרשאה");
                fp.ClickButton(" הוסף שורה ידנית ","span");
                fp.checkElmentText("insert add value list return bank", By.Name("Description"), " 123" + numberAmountBank + " " + numBranch + " " + nameBank + " " + family + " " + amount + " בטול הרשאה");
            }
            catch { throw new NotImplementedException("poup msab invaled"); }
        }
        public void insertValueAndCeckRequired(string byname,string valueInsert)
        {
            try
            {
                driver.FindElement(By.XPath("//input[@formcontrolname='" + byname + "']")).SendKeys("");
                driver.FindElement(By.XPath("//input[@formcontrolname='" + byname + "']")).SendKeys(Keys.Tab);
                fp.funError(22);
                fp.insertElment(byname, By.XPath("//input[@formcontrolname='" + byname + "']"), valueInsert);
            }
            catch { throw new NotImplementedException("insertValueAndCeckRequired invaled"+ byname); }

        }
        public void checkPOPUP()
        {
            try
            {
                fp.checkElment("close popup", By.ClassName("wrap-close"));
                fp.checkElmentText("title", By.ClassName("title"), "הזנת שורת החזר מס\"ב ידנית");
                fp.checkElmentText("idCustomer", By.XPath("//input[@formcontrolname='ClientId']"), "מזהה לקוח");
                fp.checkElmentText("ClientName", By.XPath("//input[@formcontrolname='ClientName']"), "שם לקוח");
                fp.checkElmentText("Bank", By.XPath("//input[@formcontrolname='Bank']"), "בנק");
                fp.checkElmentText("Branch", By.XPath("//input[@formcontrolname='Branch']"), "סניף");
                fp.checkElmentText("Account", By.XPath("//input[@formcontrolname='Account']"), "מספר חשבון");
                fp.checkElmentText("Amount", By.XPath("//input[@formcontrolname='Amount']"), "סכום החיוב");
                fp.checkElmentText("Reason", By.XPath("//mat-select[@formcontrolname='Reason']"), "סיבת חזרה");
                fp.checkElmentText("button add list", By.ClassName("mat-button-wrapper")," הוסף שורה ידנית ");

            }
            catch { throw new NotImplementedException("check popup msab invled"); }
        }
    }
}