﻿using com.sun.tools.javac.util;
using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class religiousInstitutions:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public religiousInstitutions()
        {
        }

        public  void stepNewMosad(string MosadName,string idMosad)
        {
            fp.ButtonNew("מוסד לפי דתות");
            checkElment();
            insertElment(MosadName, idMosad);
        }

        public void insertElment(string mosadName, string idMosad)
        {
            fp.insertElment("name mosad", By.Name("InstitutionalName"), mosadName,true);
            fp.insertElment("id MOsad inDatot", By.Name("InstitutionalDatotCode"), idMosad,true);
            fp.ClickButton(" צור מוסד לפי דתות חדש ","span");

        }

        private void checkElment()
        {
            fp.checkElment("dialog", By.TagName("mat-dialog-container"));
            fp.checkElmentText("title", By.CssSelector("div[class='main-title ng-star-inserted']"), " יצירת מוסד לפי דתות ");
            fp.checkElmentText("sub title", By.ClassName("sub-title"), "אנא מלא/י את הפרטים הבאים");
            fp.checkElmentText("title section", By.ClassName("title-section"), "כללי");
            //fp.checkElmentText("Id list", By.Name("InstitutionsByDatotID"), " מזהה רשומה ");
            fp.checkElmentText("name mosad", By.Name("InstitutionalName")," שם מוסד ");
            fp.checkElmentText("id MOsad inDatot", By.Name("InstitutionalDatotCode"), " קוד מוסד בדתות ");
            fp.checkElmentText("Button create MosadDatot", By.ClassName("mat-button-wrapper"), " צור מוסד לפי דתות חדש ");
        }

        internal void openMosad()
        {
            fp.clickNameDefinitions(" מוסדות ");
            fp.clickNameDefinitions(" מוסדות לפי דתות ");
        }
    }
}