﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class children:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();

        public children()
        {
        }

        public  void stepNewChildren()
        {
            try {
                fp.ButtonNew("ילד");
                CreateNewDenumic cnd = new CreateNewDenumic();
                cnd.checkClasscreate("ילד", false);
                cc.clickFrindNew();
                checkStep2();
                SucessEndCreate sec = new SucessEndCreate();
                sec.checkEndCreate("ילד", false, "", nameFrind);
            }
            catch { }
        }

        public  void checkStep2()
        {
            try {
                cc.checkStepHeder(2, 2);
                checkElment();
                insertElment();
            }
            catch { }
        }

        public  void checkElment()
        {
            try {
                fp.checkElmentText("check title",By.ClassName("title-section"), "פרטים אישיים");
                fp.checkElmentText("check name children",By.Name("Name"), " שם הילד ");
                fp.checkElmentText("check IdentityNumber", By.Name("IdentityNumber"), " תעודת זהות ");
                fp.checkElmentsText("check date foreign",By.TagName("input"), "בחר/י  תאריך לידה לועזי",3);
                fp.checkElmentText("check HebrewBirthDay", By.Name("HebrewBirthDay"), " תאריך לידה עברי ");
                fp.checkElmentsText("check migdar",By.TagName("input"), "בחר/י מגדר עבור הילד",5);
                fp.checkElmentsText("check shaicot",By.TagName("input"), "בחר/י שייכות עבור הילד",6);
            }
            catch { }
        }

        public  void insertElment()
        {
            Financical fl = new Financical();
            try {
                fp.insertElment ("check name children", By.Name("Name"),names[fp.rand(names.Length)] );
                fp.insertElment ("check IdentityNumber", By.Name("IdentityNumber"), fl.tzPupil());
                cc.calanederCheck(-1,-1,"2",2,-3);//fp.checkElmentsText("check date foreign", By.TagName("input"), "בחר/י  תאריך לידה לועזי", 3);
                fp.insertElment("check HebrewBirthDay", By.Name("HebrewBirthDay"), " תאריך לידה עברי ");
                fp.selectForRequerd("מגדר עבור", false);
                driver.FindElements(By.CssSelector("i[Class='material-icons button-icon-add ng-star-inserted']"))[1].Click();
                fp.addValueList("ילד", 0, By.Name("HebrewBirthDay"), 2);
                fp.selectForRequerd( "בחר/י שייכות עבור הילד",false,-1);
                fp.selectText("ילד");
                fp.ClickButton(" צור ילד חדש ","span");
            }
            catch { throw new NotImplementedException(); }
        }
    }
}