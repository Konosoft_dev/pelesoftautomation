﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class Frind:page
    {
        //static IWebDriver driver;// = new ChromeDriver();
        //public string nameFrind = "ידיד";
        FunctionPelesoft fp = new FunctionPelesoft();
        CreateNewDenumic cnd = new CreateNewDenumic();
        createConatrabition cc = new createConatrabition();

        public void openFrind()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            fp.ClickList(nameFrind + "ים");
            fp.clickListinListEqualName(nameFrind + "ים");
        }
        public void createNewFrined(string frind="אוטומציה")
        {
            openFrind();
            newFrind(frind);
            
           
        }

        public void newFrind(string frind)
        {
           // fp.waitUntil(By.XPath("//span[contains(text(),' יצירת " + nameFrind + " חדש ')]"));
            fp.ButtonNew(nameFrind);
            CreateQueick(frind);
        }

        public void CreateQueick(string frind )
        {
            SucessEndCreate sec = new SucessEndCreate();
            checkElmentInPage(frind);
            try { fp.click(By.CssSelector("button[class='btn-yes mat-raised-button mat-button-base mat-primary']")); } catch { }
            sec.checkEndCreate(nameFrind, false);
        }
        public void checkElmentInPage(string frind)//string nameCreate)
        {
            send s = new send();
            By nameSelect = By.XPath("//span[contains(text(),'בחר/י')]");

            personalInformation(frind, nameFrind);
               // address();
                media();
                fp.checkSelectPreform(" שם נוסף לקבלה  ");
                fp.checkSelectPreform(" אופן משלוח קבלה עבור הידיד");
            fp.selectForRequerd("אופן משלוח קבלה", false, 3);

            try
            {
                 s.checkDataInset(" מייל + דואר ");
              //  fp.checkElment("add name", By.Id("mat-select-8"));
                // insertNameAcc(); }
            }
            catch { }
            fp.ClickButton(" צור " + nameFrind + " חדש ");
           //fp.checkElment("button create new:" + nameFrind, By.XPath("//span[contains(@class,'mat-button-wrapper')][contains(text(),' צור " + nameFrind + " חדש ')]"));
            //driver.FindElement(By.XPath("//*[@class='mat-button-wrapper'][text()=' צור " + nameFrind + " חדש ']")).Click();

        }
        public void  personalInformation(string personal,string typePersonal)
        {
            string ret = "";
            try
            {
                cnd.checkClasscreate(typePersonal);
                fp.checkElment("form details", By.ClassName("information-unit"));
                fp.checkElment("form data", By.ClassName("wrap-section"));
                fp.checkElmentsText("title form", By.ClassName("title-section"), "פרטים אישיים", 0);
               
                    fp.click(By.CssSelector("div[class='wrap-add-value ng-star-inserted']"));
                    new Actions(driver).MoveToElement(driver.FindElements(By.XPath("//div/i[@class='material-icons button-icon-close ng-star-inserted'][text()=' close ']"))[0]).SendKeys(Keys.Tab + "החשוב").Build().Perform();
                    fp.click(By.CssSelector("div[class='wrap-done-value ng-star-inserted']"));
               
                fp.insertElment("insert lastName", By.Name("LastName"), personal);
                // if (personal != "אוטומציה")

                ret = names[fp.rand(names.Length)];
                fp.insertElment("insert FirstName", By.Name("FirstName"),ret );
                ExcelApiTest eat = new ExcelApiTest("","nameFamily");
                eat.bugReturn("nameFamily","passed", "nameFamily",ret);
              //  else
                //    fp.insertElment("insert FirstName", By.Name("LastName"), personal);
               // driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[1].Click();
                fp.selectForRequerd("סיומת", false);
            }
            catch { }
        }
        public  void media()
        {
            checkMedia();
            insertMedia();
            //throw new NotImplementedException();
        }

        public  void checkMedia()
        {
            fp.checkElmentsText("check title adress", By.ClassName("title-section"), "אמצעי תקשורת", 2);
            fp.checkElmentText("check phone home",By.Name("PhoneNumber"), " מס' טלפון בבית ");
            fp.checkElmentText("check phone",By .Name("CellPhone"), " נייד ");
            fp.checkElmentText("check fax",By.Name("Fax"), " פקס ");
            fp.checkElmentText("check doael",By.Name("email"), " דוא\"ל");
        }

        public  void insertMedia()
        {
            int num = fp.rand(9);
            Thread.Sleep(120);
            fp.insertElment ("check phone home", By.Name("PhoneNumber"), poneHome);
            fp.insertElment("check phone", By.Name("CellPhone"), phonME);
            fp.insertElment("check fax", By.Name("Fax"), poneHome);
            fp.insertElment("check doael", By.Name("email"),MAIL);
        }

        public  void address()
        {
            checkAdress();
            insertAdress();
        }

        public  void insertAdress()
        {
            Thread.Sleep(400);
            try
            {
                driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[2].Click();

                fp.addValueList(city[fp.rand(city.Length)], 0, By.Name("Apartment"));
            }
            catch { }
            fp.insertElment("check negaboard", By.Name("Street"),Streets[fp.rand(Streets.Length)]);
            fp.insertElment("check num home", By.Name("Number"), fp.rand(25).ToString());
            fp.insertElment("check Entrance", By.Name("Entrance"), fp.rand(6).ToString());
            fp.insertElment("check Apartment", By.Name("Apartment"), fp.rand(30).ToString());
            fp.selectForRequerd(" מדינה ",false, 1);
        }

       

        public  void checkAdress()
        {
            fp.checkElmentsText("check title adress", By.ClassName("title-section"), "כתובת", 1);
            fp.checkElmentText("check negaboard", By.Name("Street"), " רחוב "); 
            fp.checkElmentText("check num home", By.Name("Number"), " מס' בית "); 
            fp.checkElmentText("check Entrance", By.Name("Entrance"), " כניסה "); 
            fp.checkElmentText("check Apartment", By.Name("Apartment"), " דירה "); 
           // fp.checkElmentText("check Country", By.Name("Country"), " מדינה "); 

        }

        public void insertNameAcc()
        {
            try
            {
               // fp.checkDataNot("ReceiptName");
                fp.selectForRequerd(" שם נוסף לקבלה  ", false,0);
                fp.checkElment("add name acceptance", By.Name("ReceiptName"), true);
            }
            catch
            {
                throw new NotImplementedException("not add name acceptance");
            }
        }
        public void clickCardFrind()
        {
            try
            {
                fp.ClickButton(" קח אותי לכרטיס " + parent + " ");
                //driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' קח אותי לכרטיס " + nameFrind + " ']")).Click();
                fp.checkElment("card frind", By.ClassName("wrap-tab-content"));
                idFrind = driver.FindElement(By.ClassName("identifying-text")).Text;
                idFrindint= fp.splitNumber(idFrind);
                // driver.FindElement(By.XPath("//button[@class]"))
                new WebDriverWait(driver, TimeSpan.FromMinutes(1));
                fp.checkElment("button close", By.XPath("//button[@aria-label='edit']"));//XPath("//i[contains(text(),' close ')]"));
                expansionFrind();
                driver.FindElement(By.XPath("//button[@aria-label='edit']")).Click();

            }
            catch
            {
              //  throw new NotImplementedException("card frind not good");
            }
        }

        private void expansionFrind()
        {
            PupilNew pn = new PupilNew();
            pn.methodPaymens(ofen[fp.rand(ofen.Length)], "400",false);
            fp.closePoup(1);
            
            //הוספת תשלום לא נוצר
         //charges(); //  payments();
        }
        popupList pl = new popupList();
        public void charges(string name="")
        {
            pl.clickStepHeder(5, "חיובים"+name);
            fp.ClickButton(" הוספת חיוב ", "span");
            createCharges(5,name);
            //throw new NotImplementedException();
        }

       /* private void payments()
        {
            pl.clickStepHeder(4, "תשלומים");
            fp.ClickButton(" הוספת חיוב ", "span");

            //throw new NotImplementedException();
        }*/

        private void createCharges(int sum,string  vadi="")//חסר פה משהו
        {
            cnd.checkClasscreate("חיוב");
            fp.checkElmentsText("sub title", By.ClassName("title-section"), "פרטי חיוב", 0);
            if (vadi == "")
            {
                fp.checkElmentsText("sub title", By.ClassName("title-section"), "פרטים נוספים", 1);
                fp.selectForRequerd(" סוג מטבע ", false);
                cc.calanederCheck(1, -1, (-(fp.rand(28, 1))).ToString(), -3);
                fp.insertElment("DebitMonth", By.Name("DebitMonth"), DateTime.Today.ToString("yyyyMM"));
            }
            else
            {
                fp.selectForRequerd("חודש חיוב",false,-1);
                fp.selectText(DateTime.Today.ToString("MM/yyyy"));
                fp.selectForRequerd("סטטוס",false,-1);
                fp.selectText("פעיל");
            }
            fp.insertElment("sum", By.Name("Sum"),sum.ToString());
            fp.selectForRequerd(" סעיף חיוב ",false,-1);
            fp.selectText("כללי");
            fp.ClickButton(" צור חיוב"+vadi+" חדש ");
            Thread.Sleep(500);
            fp.closePoup(1);
        }

        public void clickCNfrind()
        {
            try
            {
                fp.ClickButton(" ליצור " + nameFrind + " נוסף ");
                cnd.onInit();
                fp.checkElment("button close", By.XPath("//i[contains(text(),' close ')]"));
                 driver.FindElement(By.XPath("//i[contains(text(),' close ')]")).Click();
                driver.Close();
            }
            catch
            {
                throw new NotImplementedException("");

            }
        }

        public  void stepTackingFrind(string typeTacking, string detailsTacking ,int i=0)
        {
            openMakavFrind();
             fp.ButtonNew("מעקב ידיד", "", 1);
            cnd.checkClasscreate("מעקב ידיד", false);
            if(i==0)
             cc.clickFrindNew();
            else
             cc.clickFrindNew("","10001"+i);
            checkElmentTacking();
            insertElmentTacking( typeTacking,detailsTacking,i);
        }

        public void openMakavFrind()
        {
            try
            {
                fp.ClickList(nameFrind + "ים");
                fp.clickNameDefinitions(" מעקב ידידים ");
            } catch { throw new NotImplementedException(); }
        }

        public void insertElmentTacking(string typeTacking,string detailsTacking,int i)
        {
            if (i==0)
            {
                try { driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[1].Click(); } catch { driver.FindElements(By.CssSelector("i[class='material-icons button-icon-add ng-star-inserted']"))[0].Click(); }
                  fp.selectForRequerd("הזן ערך נוסף לרשימה",false,-1,typeTacking);
                fp.selectForRequerd("הזן ערך נוסף לרשימה",false,-1,typeTacking);
                try { driver.FindElement(By.CssSelector("i[class='material-icons button-icon-done ng-star-inserted']")).Click(); } catch { }
            }
            fp.selectForRequerd("סוג מעקב",false,-1);
            fp.selectText(typeTacking);
            try { driver.FindElements(By.TagName("textarea"))[1].SendKeys(detailsTacking); } 
            catch { }
            try { driver.FindElements(By.TagName("textarea"))[0].SendKeys(detailsTacking); } catch { }
            try { calanederCheck(); } catch { }
            fp.selectForRequerd("משתמש מבצע",false,-1);
            fp.selectText(cnd.nameuser());
            fp.ClickButton(" צור מעקב ידיד חדש ", "span");
        }
        public int  checkEndCreateTracking()
        {
            
            fp.checkElmentText("end create tracking",By.CssSelector("div[class='main-title ng-star-inserted']"), " מעקבי ידידים נוצרו בהצלחה");
            fp.checkElmentsText("create new tacking",By.ClassName("mat-button-wrapper"), " ליצור מעקב ידיד נוסף ",0);
            fp.checkElmentsText("create new tacking",By.ClassName("mat-button-wrapper"), " ליצור מעקב ידיד נוספים ", 1);
            return fp.splitNumber(driver.FindElement(By.CssSelector("div[class='main-title ng-star-inserted']")).Text);
        }
        /// <summary>
        /// calander inclued houer 
        /// </summary>
        public void calanederCheck(string time="",int numCalnder=0)
        {
            
            By @by = By.XPath("//button[@aria-label='Open calendar']");
            driver.FindElements(@by)[numCalnder].Click();
            Thread.Sleep(300);
            fp.waitUntil(By.TagName("ngx-mat-calendar"));
            if (!driver.FindElement(By.TagName("ngx-mat-calendar")).Displayed)
                Debug.WriteLine("calender open not display");
            else
            {
                DateTime moment = DateTime.Today;
                driver.FindElement(By.XPath("//div[text()='" + moment.Day + "']")).Click();
            }
            if (time != "")
            { char[] split = {':' };
                fp.insertElment("hour",By.XPath("//input[@formcontrolname='hour']"),time.Split(split)[0]);
                fp.insertElment("minute",By.XPath("//input[@formcontrolname='minute']"), time.Split(split)[1]);
            }

           /* try
            {
                Thread.Sleep(100);
                driver.FindElements(By.TagName("button"))[driver.FindElements(By.TagName("button")).Count - 1].Click();
                //new Actions(driver).MoveToElement(driver.FindElement(By.XPath("//div[@class='mat-ripple mat-button-ripple']"))).Click().Perform(); //d.Click();//driver.FindElement(By.ClassName("mat-button-focus-overlay")).Click();//driver.FindElement(By.TagName("button")).Click();
            }
            catch {  } */
            try { driver.FindElement(By.XPath("//button/span/mat-icon[text()='done']")).Click(); } catch { try { driver.FindElement(By.XPath("//button/span/mat-icon[text()='done']")).Click(); } catch { } }
        }

        private void checkElmentTacking()
        {
            fp.checkElmentText("check title", By.ClassName("title-section"), "פרטי מעקב");
            fp.checkSelectPreform("בחר/י סוג מעקב עבור המעקב ידיד");
            fp.checkElmentText("check Details", By.TagName("textarea"), " פרטים ");
            fp.checkSelectPreform("בחר/י תאריך לביצוע");
            fp.checkSelectPreform("בחר/י משתמש מבצע עבור המעקב ידיד");
            //throw new NotImplementedException();
        }
        public string nameRow()
        {
            string nameFrined = driver.FindElement(By.CssSelector("div[aria-label='divOpenParentDialog']")).Text;
            return nameFrined;
        }
        public string  changeW()
        {
            popupList pl = new popupList();
            pl.popupEdit();
            fp.insertElment("work", By.Name("WorkPlace"),"conosoft");
            string nameFrined = nameRow();
            fp.ClickButton(" שמירה ");
            return nameFrined;
            //throw new NotImplementedException();
        }

        public  int stepLiabilities(int numPupil=-1,string  vadi="")
        {
            int numHora;
            try
            {
                popupList pl = new popupList();
                PupilNew pn = new PupilNew();
                if (numPupil == -1)
                {
                    if (vadi == "") openParent();
                    else {
                        Bulding b = new Bulding();
                        b.open(" דיירים ");
                        Thread.Sleep(350);
                    }
                    Thread.Sleep(550);
                    numHora = pn.clickTableRandom();
                    pl.expansionPopUP();
                }
                else
                {
                    if (vadi == "")
                        pn.openPupil();
                    else
                    {
                        Bulding b = new Bulding();
                        b.open(" דיירים ");
                        Thread.Sleep(350);
                    }
                    Thread.Sleep(500);
                    numHora = pn.clickTableRandom(-1, numPupil);
                    if(vadi!="")
                        pl.expansionPopUP();
                }
                pl.clickStepHeder(3, "חיובים"+vadi);//הוחלף מ3ל 8
                fp.ClickButton(" הוספת חיוב ", "span");
                createCharges(5,vadi);
                try
                {
                    fp.closePoup(1);
                    fp.closePoup();
                }
                catch { }
            }
            catch { fp.picter("stepLiabilities"); throw new NotImplementedException(); }
            return numHora;
        }
        public void openParents()
        {
            fp.clickNameDefinitions(" תלמידים ");
            fp.clickNameDefinitions(" הורים ");
        }
        public void createCharges(string  vadi="")
        {
            try
            {
                CreateNewDenumic cnd = new CreateNewDenumic();
                cnd.checkClasscreate("חיוב");
                checkCharges(vadi);
                insertCharges("","",vadi);
            }
            catch { fp.picter("createCharges"); throw new NotImplementedException(); }
        }

        public void insertCharges(string dMont="",string end="",string  vadi="")
        {
            try
            {
                dMont = dMont == "" ? DateTime.Today.ToString("yyyyMM") : dMont;
                fp.insertElment("Sum", By.Name("Sum"), "1");
                fp.selectForRequerd(" סוג מטבע ", false);
                if (vadi == "")
                { fp.insertElment("check DebitMonth", By.Name("DebitMonth"), dMont); 
                end = end == "" ? DateTime.Today.ToString("dd/MM/yyyy") : end;
                fp.insertElment("calnder",By.Id("inputMDEx"),end);}
                else
                {
                    fp.selectForRequerd("חודש חיוב", false, -1);
                    fp.selectText(dMont);
                }
                
                /*createConatrabition cc = new createConatrabition();
                cc.calanederCheck(1);*/
                fp.selectForRequerd(" סעיף חיוב ");
               if(vadi=="")
                    fp.insertElment("check Details", By.Name("Details"));
                fp.ClickButton(" צור חיוב"+vadi+" חדש ", "span");
            }
            catch { fp.picter("insertCharges"); throw new NotImplementedException(); }
        }

        private void checkCharges(string  vadi="")
        {
            try {
                fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "פרטי חיוב",0);
                fp.checkElmentsText("checkTitle", By.ClassName("title-section"), "פרטים נוספים", 1);
                fp.checkElmentText("check Sum", By.Name("Sum"), " סכום ");
                fp.checkSelectPreform(" סוג מטבע ");
                if (vadi == "")
                {
                    fp.checkElmentText("check DebitMonth", By.Name("DebitMonth"), " חודש חיוב ");
                    fp.checkSelectPreform("בחר/י  תאריך לסיום חיוב ");
                }
                fp.checkSelectPreform(" סעיף חיוב ");
                fp.checkElmentText("check Details", By.Name("Details"), " פרטים ");

            } catch { throw new NotImplementedException(); }
        }
        public void openParent()
        {
            fp.clickNameDefinitions(" תלמידים ");
            fp.clickNameDefinitions(" הורים ");
        }
        public  void stepCreateParents(string parents)
        {
            try { fp.ButtonNew("");
                personalInformation(parents, parent);
                address();
                media();
                fp.ClickButton(" צור הורה חדש ");
                Thread.Sleep(450);
            } catch { fp.picter("stepCreateParents"); throw new NotImplementedException(); }
        }

        public  void openContacts()
        {
            try { fp.actionWork("אנשי קשר","");
                fp.ButtonNew("");
            } catch { fp.picter("openContacts"); throw new NotImplementedException(); }
        }
        public void createContacts(string firstName)
        { try {
                SucessEndCreate sec = new SucessEndCreate();
               openContacts();
                personalInformation(firstName, "איש קשר");
                address();
                media();
                fp.ClickButton(" צור איש קשר חדש ","span");
                sec.checkEndCreate("איש קשר", false);
            }
            catch { fp.picter("createContacts"); throw new NotImplementedException(); } }

        public  void checkChargesTitle(int numHora)
        {
            try { fp.refres(); 
                fp.clickNameDefinitions(" תלמידים ");
                fp.clickNameDefinitions(" הורים ");
                Thread.Sleep(200);
                PupilNew pn = new PupilNew();
                pn.clickTableRandom(numHora);
                pl.expansionPopUP();
                pl.clickStepHeder(10, "חיובים");
                fp.checkElmentText("debt parent", By.CssSelector("div[class='row-number-txt balance-txt debt ng-star-inserted']"), " יתרת החובה היא  -1 ש\"ח ");
                fp.checkElmentText("icon smaile",By.CssSelector("mat-icon[class='mat-icon notranslate material-icons mat-icon-no-color ng-star-inserted']"), "sentiment_very_dissatisfied");
            } catch { throw new NotImplementedException(); }
        }

        public void checkSmaile(int numHora)
        {
            try {
                fp.refres();
                fp.clickNameDefinitions(" תלמידים ");
                fp.clickNameDefinitions(" הורים ");
                PupilNew pn = new PupilNew();
                Thread.Sleep(200);
                pn.clickTableRandom(numHora);
                pl.expansionPopUP();
                pl.clickStepHeder(3, "תשלומים");
                fp.ClickButton(" הוספת תשלום ","span");
                createConatrabition cc = new createConatrabition();
                cc.paymentStep("תשלום חד פעמי", "מזומן", "כללי", "", "Cash","Remarks",3);
                fp.refres();
                fp.clickNameDefinitions(" תלמידים ");
                fp.clickNameDefinitions(" הורים ");
                pn.clickTableRandom(numHora);
                pl.expansionPopUP();
                pl.clickStepHeder(10, "חיובים");
                fp.checkElmentText("debt parent", By.CssSelector("div[class='row-number-txt balance-txt ng-star-inserted']"), " יתרת הזכות היא ");
                fp.checkElmentText("icon smaile",By.CssSelector("mat-icon[class='mat-icon notranslate material-icons mat-icon-no-color ng-star-inserted']"), "insert_emoticon");
            }
            catch { throw new NotImplementedException(); }
        }

        public  void parentsHok()
        {
            string[] checktaskl = { "", "", "", "", "", "", "", "", " pageview ", "-1" };
            fp.checkColumn("//div[@class='wrap-table']/div/mat-table", checktaskl, "//i[@class='material-icons'][text()=' pageview ']", "mat-row", "mat-cell", 8, 1);
            try { fp.click(By.XPath("//some-element[text()='לקיזוז החיוב ']")); } catch { fp.click(By.XPath("//some-element[text()='לקיזוז התשלום']")); }
            try { fp.checkElmentsText("hok", By.CssSelector("div[class='main-txt sub']"), "שלם יתרה בהו\"ק", 1); } catch { }
            fp.click(By.XPath("//mat-checkbox/label/span/div[text()='שלם יתרה בהו\"ק']"));
            fp.selectForRequerd("אמצעי תשלום",false,0);
            fp.insertElment("sum gvia",By.CssSelector("input[placeholder='הזן סכום לגביה']"),"5");
            fp.ClickButton(" לקיזוז ");
            fp.checkElmentText("title",By.ClassName("title"), " אשר פעולה ");
            fp.checkElmentText("message popup",By.XPath("//div[@class='wrap-message']/div"), " האם אתה בטוח שברצונך לקזז ?");
            fp.checkElment("button no",By.CssSelector("button[class='btn-no mat-button mat-button-base']"));
            fp.ClickButton("כן");
        }

        public  void payments(int numparents,string  vadi="")
        {
            try {
                fp.refres();
                if(vadi=="")
                    openParent();
                else
                {
                    fp.clickNameDefinitions(" "+niulBuldin+" ");
                    fp.clickNameDefinitions(" דיירים ");
                }
                Thread.Sleep(200);
                PupilNew pn = new PupilNew();
                pn.clickTableRandom(numparents);
                pl.btnEdit();
                pl.clickStepHeder(2,"תשלומים"+vadi);
                string[] checkAddRow = { "","","","0","הו\"ק","-1"};
                fp.checkColumn("//div[@class='wrap-table']/div/mat-table", checkAddRow,"false", "mat-row", "mat-cell",-1,1);
                fp.closePoup();
            } catch { throw new NotImplementedException(); }
        }

        public void checkTablenumberRow(int numCreate, string typeTracking)
        {
            try {
                IWebElement table = driver.FindElement(By.TagName("mat-table"));
                IList<IWebElement> rows = table.FindElements(By.TagName("mat-row"));
                IList<IWebElement> cell;
                bool flag = true;
                for(int i=0; i<rows.Count;i++)
                {
                    cell = rows[i].FindElements(By.TagName("mat-cell"));
                    if(cell[4].Text.Trim()==typeTracking.Trim())
                    { flag=numCreate < i+1 ? true : true; }
                    else
                    { flag = numCreate >= i+1 ? false : true; }
                    if(flag==false)
                    { fp.picter("check table tracking not equals"); throw new NotImplementedException(); }
                }
            } catch { throw new NotImplementedException("check table tracking not equals"); }
        }

        public void Misug(string idPepole1, string idPepole2,bool checkBoxPrent=false)
        {
            try { fp.selectForRequerd("מזהה איש קשר עקרי",false,-1, fp.splitNumber(idPepole1).ToString());
                fp.click(By.XPath("//mat-icon[text()='person_search']"));
                fp.selectForRequerd("מזהה איש קשר למחיקה", false,-1, fp.splitNumber(idPepole2).ToString());
                fp.click(By.XPath("//mat-icon[text()='person_search']"),1);
                fp.SelectRequerdName("placeholder='שדות להעברה מאיש הקשר למחיקה","אופן משלוח קבלה",false);
                fp.selectText(" דוא\"ל ");
                fp.selectText(" הורי האם ");
                fp.selectText(" נייד ");
                new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
                if (checkBoxPrent == true)
                {
                    if (driver.FindElement(By.TagName("mat-checkbox")).Text.Contains("שני אנשי הקשר הינם הורים, האם תרצה להשתמש בנתוני ההורים של ההורה למחיקה?"))
                    { fp.click(By.TagName("mat-checkbox")); }
                }
                fp.click(By.CssSelector("button[aria-label='btnMergePeople']"));
                approveAction();
            }
            catch { throw new NotImplementedException(); }
        }

        private void approveAction()
        {
            try { fp.checkElmentText("title popup",By.ClassName("title"), " אשר פעולה ");
                fp.checkElmentText("wrap-message",By.ClassName("wrap-message"), "האם אתה בטוח שברצונך למזג");
                fp.ClickButton("כן","span");
            } catch { throw new NotImplementedException(); }
        }

        public  void checkFilter()
        {
            try {
                 bool charges=fp.splitNumber(driver.FindElement( By.CssSelector("div[class='row-number-txt ng-star-inserted']")).Text)!=0?true:false ;
                if (charges == true)
                {
                    IList<IWebElement> radioBu = driver.FindElements(By.TagName("mat-radio-button"));
                    for (int i = 0; i < radioBu.Count; i++)
                    {
                        radioBu[i].Click();
                        string []esit = null;
                        if (radioBu[i].Text.Contains("אישית"))
                            esit = custom();
                        checkTableFilter(radioBu[i].Text,esit);
                    }
                }
            } catch { fp.picter("chek filter charges"); throw new NotImplementedException(); }
        }

        private string[] custom()
        {
            string[] dateSE = null;
            try {
                fp.insertElment("start date",By.Id("inputMDEx"),DateTime.Today.AddYears(-1).ToString("dd/MM/yyyy"));
                fp.insertElment("start date",By.Id("inputMDEx"),DateTime.Today.AddYears(1).ToString("dd/MM/yyyy"),false,"",1);
                string startE = DateTime.Today.AddYears(-1).ToString("dd/MM/yyyy") + "/" + DateTime.Today.AddYears(1).ToString("dd/MM/yyyy");
                char[] tav = { '/' };
                dateSE = startE.Split(tav);
            } catch { throw new NotImplementedException(); }
            return dateSE;
        }

        private void checkTableFilter(string valueRadioB,string []esit=null)
        { try
            {
                bool charges = fp.splitNumber(driver.FindElement(By.CssSelector("div[class='row-number-txt ng-star-inserted']")).Text) != 0 ? true : false;
                if (charges == true)
                { 
                    
                    IWebElement table = driver.FindElements(By.TagName("mat-table"))[1];
                    IList<IWebElement> rows = table.FindElements(By.TagName("mat-row"));
                    IList<IWebElement> cells;
                    for (int i=0;i<rows.Count;i++)
                    {
                        cells = rows[i].FindElements(By.TagName("mat-cell"));
                        char[] slach = { '/'};
                        string[] dat = cells[4].Text.Split(slach);
                        checkCellFilter(dat, valueRadioB,esit);
                    }
                }
            }
            catch { } }

        private void checkCellFilter(string[] dat, string valueRadioB,string []esit=null)
        {
            try {
                int toza=0;
                switch (valueRadioB)
                { case "חודש זה":
                        toza = dat[1] == DateTime.Today.ToString("MM") ? 0 : 1;
                        break;
                    case "שנה זו":
                        toza = dat[2] == DateTime.Today.ToString("yyyy") ? 0 : 1;
                        break;
                    case "חודש קודם":
                        toza = Int16.Parse(dat[1]) < Int16.Parse(DateTime.Today.ToString("MM")) ? 0 : 1;
                        break;
                    case "חודש הבא":
                        toza = Int16.Parse(dat[1]) > Int16.Parse(DateTime.Today.ToString("MM")) ? 0 : 1;
                        break;
                    case "מותאם אישית":
                        if (DateTime.Parse( dat[0]+"/"+dat[1]+"/"+dat[2]) < DateTime.Parse(esit[0]+"/"+esit[1]+"/"+esit[2]) || DateTime.Parse(dat[0] + "/" + dat[1] + "/" + dat[2]) > DateTime.Parse(esit[3]+"/"+esit[4]+"/"+esit[5]))
                            toza = 1;
                        else
                            toza = 0;
                        break;}
                if(toza==1)
                        {
                            fp.picter(valueRadioB+"date not good");
                            throw new Exception(valueRadioB + "date not good");
                        }
                 } catch { throw new NotImplementedException(valueRadioB + "date not good"); }
        }

        public void MailContacts()
        {
            try {
                
                clickContacts();
               try {
                    new Actions(driver).MoveToElement(driver.FindElement(By.ClassName("phoneOrMail"))).SendKeys(Keys.Tab+Keys.Tab+Keys.Tab+Keys.Enter).Perform();
                    
                } catch {
                    if (driver.FindElement(By.ClassName("massageWhenPhonesIsNull")).Text.Contains("מייל לא קיים"))
                    {
                        editMail();
                        fp.refres();
                        clickContacts();
                        new Actions(driver).MoveToElement(driver.FindElement(By.ClassName("phoneOrMail"))).SendKeys(Keys.Tab+Keys.Enter).Perform();
                    }
                }
                new Actions(driver).MoveToElement(driver.FindElement(By.ClassName("phoneOrMail"))).Perform();
                fp.insertElment("subject", By.Name("subject"),"גם אתה חבר בצוות CONOSOFT");
                process pr = new process();
                string []pnimi = {"שם משפחה","שם פרטי","תואר" };
                pr.editTextLink("אנשי קשר",pnimi,"אני",-1,false,0);
                DocumentDesigned dd = new DocumentDesigned();
                dd.addPicterUrl(false);
                pr.tableLink(true, "table");
                fp.ClickButton(" שליחה ","span");
            }
            catch (Exception ex) { throw new Exception(); }
        }
        public void clickContacts()
        {
            fp.click(By.ClassName("action-icon1"));
            fp.insertElment("searchText", By.Id("searchText"), "א");
            driver.FindElement(By.Id("searchText")).SendKeys(Keys.Enter);
            fp.click(By.ClassName("person-in-list"));
            fp.ClickButton("mail_outline", "span");
            //fp.click(By.CssSelector("div[class='personItem full-name']"));
        }
        private void editMail()
        {
            try {
                fp.click(By.CssSelector("button[mattooltip='עריכה']"));
                fp.insertElment("mail",By.Name("email"), MAIL) ;
                fp.ClickButton(" שמירה ");
                Thread.Sleep(250);
                try { fp.closePoup(1); } 
                catch { }

            } catch { throw new NotImplementedException(); }
        }
    }
}
