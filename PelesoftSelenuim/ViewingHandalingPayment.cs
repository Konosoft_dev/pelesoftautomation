﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PelesoftSelenuim
{
     class ViewingHandalingPayment:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();

        public void openViewingHandalingPayment()
        {
            fp.actionWork(" ניהול כספים ","צפייה וטיפול בתשלומים");
        }
        public void BroadcastSdreenViewingPayments(string typeCard)
        {
            openViewingHandalingPayment();
            fp.selectForRequerd("סטטוס", false,0);
            string[] listRow = {"","", adress, typeCard, DateTime.Today.ToString("dd/MM/yyyy"), amount + " ש\"ח", "לטיפול", "עסקה תקינה.", "-1" };
            /*if (typeCard == HQmsab)
                listRow[3] = "";*/
                clickTable(listRow, "false");
            clickBuSidorMiade();
            checkListedit();
        }
        public void returnBroadcast(string typeCard,string mosadS="",string faml="")
        {
            openViewingHandalingPayment();

            checkElmentVHP("false",mosadS);
            //  ct.viewingHandalingPayments("false");
            if (faml == "")
            {
                ExcelApiTest eat = new ExcelApiTest("nameFamliy");
                faml = eat.createExcel("nameFamily", "nameFamily", false);
                if (faml == "false")
                    faml = "";
            }
            string[] listRow = { "",faml, adress, typeCard, DateTime.Today.ToString("dd/MM/yyyy"),amount + " ש\"ח", "בוצע", "עסקה תקינה.", "-1" };

            clickTable(listRow, "//span[text()=' בוצע ']");
            if (typeCard!="מסב")
            {
                checkstatusPopupStatus();
                editStatus();
            }
            else
                 checkPOPUPStatusMsab();

            checkEditStatusINTable();
        }

        public  void checkPOPUPStatusMsab()
        {
            try
            {
                fp.checkElmentText("title",By.ClassName("title"), "עדכון חזרת מס\"ב");
                fp.checkElment("close poup", By.ClassName("wrap-close"));
                fp.checkElmentText("check box retern", By.XPath("//mat-checkbox[@formcontrolname='Refused']"), "החיוב חזר");
                driver.FindElement(By.XPath("//mat-checkbox[@formcontrolname='Refused']")).Click();
                fp.checkElment("Reason for charge refund", By.XPath("//mat-select[@formcontrolname='RefusedID']"));
                driver.FindElement(By.XPath("//mat-select[@formcontrolname='RefusedID']")).Click();
                fp.selectText("בטול הרשאה");
                fp.checkElment("Remarks", By.Name("Remarks"), true);
                fp.ClickButton("שמירה");
            }
            catch { throw new NotImplementedException("checkPOPUPStatusMsab"); }
        }

        public void handingViewPayments(string typeCard,string mosadS="")
        {
            openViewingHandalingPayment();
            checkElmentVHP("false", mosadS);
            string[] listRow = { "","", adress, typeCard, DateTime.Today.ToString("dd/MM/yyyy"), amount + " ש\"ח", "לטיפול", "עסקה תקינה.", "-1" };
            if(typeCard=="מסב")
            {
                listRow[4] = "";
                listRow[7] = " בטול הרשאה ";
            }
            clickTable(listRow, "true");
            clickPayments();
            checkListedit();
        }
        public void checkElmentVHP(string exportInPage,string mosadS="")
        {
            fp.checkElment("title page 'Viewing Handaling Payment'", By.XPath("//div[text()='צפייה וטיפול בתשלומים']"));
            fp.selectForRequerd("סטטוס",false,0);

            /* fp.checkElment("list box clearing", By.XPath("//mat-select[@aria-label='clearing']"));
             fp.checkElment("list type", By.XPath("//mat-select[@aria-label='type']"));
             fp.checkElment("list select file", By.XPath("//mat-select[@aria-label='select file']"));*/
            if (mosadS != "")
            { fp.selectForRequerd("מוסד לסליקה",false,-1);
                fp.selectText(mosadS,true);
                fp.ClickButton(" סנן ");
            }
            if(exportInPage == "true")
            {
                fp.checkElment("export pdf", By.XPath("//div/i[contains(@class,'material-icons ')][text()='picture_as_pdf']"));
                fp.checkElment("export excel", By.XPath("//div[@mattooltip='ייצא לאקסל']"));
            }
           
            fp.checkElment("from date", By.XPath("//app-long-date[@txtlabel='מ תאריך']"));
            fp.checkElment("to date", By.XPath("//app-long-date[@txtlabel='עד תאריך']"));
            fp.checkElment("filter boardcasts",By.XPath("//span[@class='mat-button-wrapper'][text()=' סנן ']"));
            //throw new NotImplementedException();
        }

        public void checkTable(string[] listRow)
        {
            string[] listHeder = { "שם", "כתובת", "סוג א.תשלום", "תאריך", "סך הכל", "סטטוס", "תיאור" };
            fp.checkTableHeder(listHeder, "//thead[@role='rowgroup']");
            clickTable( listRow,"" );
            //throw new NotImplementedException();
        }

        public string  clickTable(string[] listRow, string clickrow,string typeRow="tr",string typeCell="td")
        {
            string name = "";
            IWebElement tableElement = driver.FindElement(By.XPath("//tbody[@role='rowgroup']"));
            //IWebElement tableElement = driver.FindElement(By.TagName("tbody"));

            //IList<IWebElement> tableRow = tableElement.FindElements(By.XPath("//tr[@role='row']"));
           // IList<IWebElement> tableRow = tableElement.FindElements(By.XPath("//"+typeRow+"[@role='row']"));
            IList<IWebElement> tableRow = tableElement.FindElements(By.XPath("//"+typeRow+"[@role='row']"));
            //IList<IWebElement> rowTD;
            for (int i = 1; i < tableRow.Count - 1; i++)
            {
                try { Console.WriteLine(tableRow[i].Text); } catch { }
                try
                {
                    if (tableRow[i].Text.Contains("ש\"ח"))
                    {
                        IList<IWebElement> rowTD = tableRow[i].FindElements(By.TagName(typeCell));
                        for (int j = 0; j <= rowTD.Count - 1; j++)
                        {

                            // Console.WriteLine(rowTD.Count);
                            //  Console.WriteLine(listRow[j]);
                            if (/*j + 1 == rowTD.Count - 2*/ listRow[j] == "-1")
                            {
                                name = rowTD[0].Text;
                                if (clickrow == "true")
                                    tableRow[i].FindElements(By.CssSelector("td[role='gridcell']"))[5].Click();
                                else if (clickrow == "falsefalse")
                                    Console.WriteLine("is very good the row in table is display");
                                else if (clickrow == "false")
                                    tableRow[i].FindElements(By.CssSelector(typeCell + "[role='gridcell']"))[1]/*.FindElement(By.XPath("//span[@ng-reflect-message='עדכון סטטוס']"))*/.Click();
                                else if (clickrow.Contains("["))
                                    tableRow[i].FindElements(By.CssSelector(typeCell + "[role='gridcell']"))[j].FindElement(By.XPath(clickrow)).Click();
                                ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
                                eat.bugReturn("conatarbition", "passed", "statusI", "" + i + "", false);
                                //eat.CloseExcel();
                                i = -1;
                                ///  Console.WriteLine(tableRow[i].Text);
                                break;
                            }
                            if (!rowTD[j].Text.Trim().Contains(listRow[j].Trim()) && listRow[j] != "" && listRow[j] != "-1")
                            {
                                Console.WriteLine(rowTD[j].Text);
                                //  Console.WriteLine(tableRow[i].FindElements(By.CssSelector(typeCell+"[role='gridcell']"))[5].Text);
                                break;

                            }
                            /*  if (rowTD[j].Text == listRow[j])
                                  Console.WriteLine(rowTD[j].Text, listRow[j]);*/
                        }
                        if (i == -1)
                            break;
                        if (i + 1 == tableRow.Count - 1)
                            throw new NotImplementedException("the row not display :" + listRow[0]);
                        // i = i + 1;
                    }
                }
                catch { }
            }

            return name;
        }
        public void checkEditStatusINTable()
        {
            fp.spinner();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            string f=eat.createExcel("conatarbition", "statusI");
            /*if (IntPtr(f) == 0)
                f = f + 1;*/
            //f = f - 1;
            if (f == "false")
                f = "1";
            int j = Int32.Parse(f);
            driver.FindElements(By.CssSelector("tr[role='row']"))[j].Click();
           // listRow[5] = "לטיפול";
            if (driver.FindElements(By.CssSelector("tr[role='row']"))[j].FindElements(By.CssSelector("td[role='gridcell']"))[5].Text == "לטיפול")
                Console.WriteLine("the row change to treatment");
            else
                Debug.WriteLine("the row not change " + driver.FindElements(By.CssSelector("tr[role='row']"))[j].FindElements(By.CssSelector("td[role='gridcell']"))[3].Text);
        }
        public void editStatus()
        {
            try
            {
                driver.FindElement(By.XPath("//mat-checkbox[@formcontrolname='Success']")).Click();
                fp.ClickButton("שמירה","span");
            }
            catch { throw new NotImplementedException("not edit status"); }

        }
        public void editAsraiStatus(string status,string valueStatus)
        { 
                driver.FindElement(By.XPath("//mat-checkbox[@formcontrolname='Success']")).Click();
            driver.FindElement(By.Name("Status")).Clear();
            fp.insertElment("insert status",By.Name("Status"),status);
            driver.FindElement(By.Name("ErrorDescription")).Clear();
            fp.insertElment("value status", By.Name("ErrorDescription"),valueStatus);
            fp.ClickButton("שמירה","span");

        }
        public void checkstatusPopupStatus()
        {
            fp.checkElmentText("title status brodcast", By.ClassName("title"), "עדכון שידור אשראי");
            fp.checkElment("checkBox broadcast", By.XPath("//mat-checkbox[@formcontrolname='Success']"));
            fp.checkElment("checkBox broadcast text", By.XPath("//mat-label[text()='השידור הצליח']"));
            fp.checkElment("status broadcast", By.Name("Status"));
            fp.checkElment("discraption", By.Name("ErrorDescription"));
            fp.checkElment("num Aprowe", By.Name("MisparIshur"));
            fp.checkElment("shovar", By.Name("Shovar"));
            fp.checkElment("Remarks", By.Name("Remarks"));
            fp.checkElment("button submit", By.XPath("//span[@class='mat-button-wrapper'][text()='שמירה']"));
            fp.checkElment("close", By.ClassName("wrap-close"));

        }
        public void clickBuSidorMiade(bool changePayments = false)
        {
            fp.ClickButton(" שידור מיידי ", "span");
            checkPopUpInstantBadcast();
            insertValueInstantBadcast(changePayments);
        }

        public void insertValueInstantBadcast(bool changePayments=false)
        {
            fp.insertElment("number approve", By.Name("MisparIshur"), "4546");
            if(changePayments==true)
            {
                fp.SelectRequerdName("formcontrolname='PaymentMethodID", " **** 3040, אשראי ",false);
            }
            fp.ClickButton("שדר");
            fp.wait();
           
            //throw new NotImplementedException();
        }

        public void checkPopUpInstantBadcast()
        {
            fp.checkElment("popup Broadcast", By.TagName("mat-dialog-container"));
            fp.checkElment("close popup", By.ClassName("wrap-close"));
            fp.checkElmentText("title popup", By.ClassName("title"), "שידור אשראי מיידי");
            fp.checkElmentText("number approwe", By.Name("MisparIshur"), "מספר אישור");
            fp.checkElmentText("button Broadcast", By.ClassName("mat-button-wrapper"), "שדר");
        }

        public void checkListedit()
        {
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            int f = 1;//Int32.Parse( eat.createExcel("conatarbition", "statusI"));
            fp.waitUntil(By.CssSelector("tr[role='row']"));
            Thread.Sleep(1100);
            if (driver.FindElements(By.TagName("tr"))[f].FindElements(By.TagName("td"))[6].Text.Contains("בוצע"))
                Console.WriteLine("the row change to treatment");
            else
            {
                fp.picter("the row not change for טופל");
                Debug.WriteLine("the row not change " /*+ driver.FindElements(By.CssSelector("tr[role='row']"))[f - 1].FindElements(By.CssSelector("td[role='gridcell']"))[3].Text*/);
             //   eat.imageSave("bug_checkListedit_row_not_change");
            }
        }

        public void clickPayments()
        {
            fp.ClickButton(" טיפול ");
            checkPopUpEdit();
            //throw new NotImplementedException();
            inserEdit();
        }

        public void inserEdit()
        {
            createConatrabition cc = new createConatrabition();
            cc.calanederCheck(2);
            fp.ClickButton("עדכן");

            //throw new NotImplementedException();
        }

        public void checkPopUpEdit()
        {
            fp.checkElment("popup edit", By.TagName("mat-dialog-container"));
            fp.checkElment("close popup", By.ClassName("wrap-close"));
            fp.checkElmentText("title popup", By.ClassName("title"), "עדכון תשלומים לגבייה");
            fp.checkElmentText("date collection", By.TagName("app-long-date"), "תאריך לגבייה");
            fp.checkElmentText("button edit", By.ClassName("mat-button-wrapper"), "עדכן");
            //throw new NotImplementedException();
        }

        public void checkchange()
        {
            fp.actionWork(" ניהול כספים ","שידורי אשראי");
            ceaditTransmission ct = new ceaditTransmission();
            ct.checkClearing("true","אשראי",0);
            fp.closeChrome();
        }

        public void filterMsab()
        {
            try {
                fp.selectForRequerd("מוסד לסליקה", false, -1);
                fp.selectText(" מסב ");
                fp.selectForRequerd("סטטוס", false,0);
                string[] listRow = { family + " " + automation, adress, "מסב", DateTime.Today.ToString("dd/MM/yyyy"), amount + " ש\"ח", "לטיפול", "בטול הרשאה", "-1" };
               
                clickTable(listRow, "falsefalse");
            }
            catch { fp.picter("filterMsab"); throw new NotImplementedException(); }
        }

        public string  stepHQ()
        {
            string faml = "";
            try {
                openViewingHandalingPayment();
                fp.selectForRequerd("מוסד לסליקה",false,-1);
                fp.selectText(" אשראי הו\"ק ");
                checkElmentVHP("false");
                ExcelApiTest eat = new ExcelApiTest("","nameFamily");
                faml= eat.createExcel("nameFamily","nameFamily",false);
                if (faml == "false")
                    faml = "";
                string[] listHQ = { "",faml, adress, " אשראי ", DateTime.Today.ToString("dd/MM/yyyy"), amount + " ש\"ח", "בוצע", "עסקה תקינה. ", "-1" };
                checkTable(listHQ);
               returnBroadcast("אשראי","אשראי הו\"ק");
                BroadcastSdreenViewingPayments("אשראי");
                returnBroadcast("אשראי","אשראי הו\"ק");
                handingViewPayments(asrai,asraiHok);//עבר ל טופל
            } catch { throw new NotImplementedException(); }
            return faml;
         }

        public  void returnAsrai()
        {
            try { fp.click(By.XPath("//i[text()='reply']"));
                checkElmentReturn();
                insertElmentReturn();
                checkReturn();
            } catch { fp.picter("returen asrai"); throw new NotImplementedException(); }
        }

        private void checkReturn()
        {
            try
            {
                fp.refres();
                openViewingHandalingPayment();
                fp.selectForRequerd("סטטוס", false, -1);
                fp.selectText(" שידורים שנכשלו ");
                string[] list = { "","","אשראי",DateTime.Today.ToString("dd/MM/yyyy"),amount+" ש\"ח","-1"};
                fp.checkColumn("tbody[role='rowgroup']", list, "false");
            } catch { throw new NotImplementedException(); }
        }

        private void insertElmentReturn()
        {
            try
            {
                fp.SelectRequerdName("formcontrolname='selectedClearingEntity", "אשראי");
                fp.insertElment("calnder", By.Id("inputMDEx"), DateTime.Today.ToString("dd/MM/yyyy"),false,"",1);
                fp.insertElment("4numbers", By.CssSelector("input[formcontrolname='fourNumbers']"), creaditcard.Substring(creaditcard.Length - 4,4 ));
                fp.click( By.CssSelector("mat-icon[aria-label='Example home icon']"));
                fp.checkElmentText("value display user", By.CssSelector("span[class='title-property first-property']"), " שם: "); ;
                fp.checkElmentsText("value display userreturn asrai",By.ClassName("title-property"), " כתובת: ",0);
                fp.checkElmentsText("value display userreturn asrai",By.ClassName("title-property"), " מספר שובר: ",1);
                fp.checkElmentsText("value display userreturn asrai",By.ClassName("title-property"), " סכום: ",2);
               try { if(driver.FindElements(By.TagName("mat-list-option")).Count>1)
                        fp.click(By.TagName("mat-pseudo-checkbox"));
                  //  fp.click(By.TagName("mat-pseudo-checkbox"));
                } catch { }
                driver.FindElement(By.CssSelector("input[formcontrolname='fourNumbers']")).SendKeys(Keys.Tab + Keys.Tab + Keys.Enter);
                fp.selectText(" ביטול הרשאה ");
                try
                {
                    if (driver.FindElements(By.TagName("mat-list-option")).Count > 1)
                        fp.click(By.TagName("mat-pseudo-checkbox"));
                    //  fp.click(By.TagName("mat-pseudo-checkbox"));
                }
                catch { }
                fp.selectForRequerd("הערות",false,-1,automation);
                fp.selectForRequerd("הערות",false,-1,automation);
                fp.ClickButton("עדכן לנכשל","span");
               

            } catch { fp.picter("insertElmentReturn"); throw new NotImplementedException(); }
        }

        private void checkElmentReturn()
        {
            try { fp.checkElmentText("title return",By.ClassName("main-txt"), "החזרת אשראי");
                fp.checkElmentText("slika",By.CssSelector("mat-select[formcontrolname='selectedClearingEntity']"), "מוסד לסליקה ");
                fp.checkElmentText("calnder",By.Id("inputMDEx"), "תאריך עסקה");
                fp.checkElmentText("4numbers", By.CssSelector("input[formcontrolname='fourNumbers']"), "4 ספרות");
                fp.checkElmentText("sherch",By.CssSelector("mat-icon[aria-label='Example home icon']"), " search");
                fp.checkElmentText("mo data",By.ClassName("no-data-msg"), "אין כרגע נתונים ");
                fp.checkSelectPreform("סיבת ההחזר ");
                fp.checkSelectPreform("הערות");
                fp.checkElmentsText("button return",By.ClassName("mat-button-wrapper"), "עדכן לנכשל", 1);
            }
            catch { throw new NotImplementedException(); }
        }

        internal void cheangeMethodPayments(bool vadi=false)
        {
            createConatrabition cc = new createConatrabition();
            if(vadi==false)
                cc.conatrabitionList();
            { Bulding b = new Bulding();
                b.open(" תשלומי דיירים ");
            }
            Thread.Sleep(650);
            PupilNew pn = new PupilNew();
            pn.clickTableRandom(0);
            Frind f = new Frind();
            popupList pl = new popupList();
            pl.btnEdit();
            string familyName = f.nameRow();
            fp.closePoup();
            fp.ClickButton("לא");
            fp.actionWork(" ניהול כספים ", "שידורי אשראי");
            ceaditTransmission ct = new ceaditTransmission();
            char[] tav = { ' ' };
            ct.checkClearing("true", "אשראי", 0,familyName.Split(tav)[0]+" "+familyName.Split(tav)[1]);
            ct.viewingHandalingPayments("true", familyName.Split(tav)[0] + " " + familyName.Split(tav)[1]);
            returnBroadcast("אשראי", "", familyName.Split(tav)[0] + " " + familyName.Split(tav)[1]);
            /*  string[] row = { familyName.Split(tav)[0] + " " + familyName.Split(tav)[1], "", "אשראי", "", "", "", "-1" };
              clickTable(row, "false");*/
            Thread.Sleep(350);
            clickBuSidorMiade(true);
        }

        internal string[] changestatues()
        {
            string[] name = {"","","" }; 
            openViewingHandalingPayment();
            checkElmentVHP("", "אשראי הו\"ק");
            string[] row = { "", "","", "אשראי", "", "", "בוצע", "-1" };
            name[0] =clickTable(row, "true");
            editAsraiStatus("003", "התקשר לחברת האשראי");
            name[1] = clickTable(row, "true");
            editAsraiStatus("004", "סירוב");
            name[2] = clickTable(row, "true");
            editAsraiStatus("173", "עסקה כפולה");
            return name;
        }

        public  void checkstatusProcess(string[] name)
        {
            try
            {
               openViewingHandalingPayment();
                checkElmentVHP("", "אשראי הו\"ק");
                for (int i = 0; i < name.Length; i++)
                {
                    string[] rowCheck = { "",name[i], "", "אשראי", "", "", "בוצע", "-1" };
                    clickTable(rowCheck, "falsefalse");
                }
            } catch(Exception ex) { throw new NotImplementedException(ex+""); }
        }

        public void checkChangeSum(string nameOfChange)
        {
            try { fp.insertElment("FILTER contact",By.CssSelector("input[placeholder='חיפוש']"),nameOfChange);
                string[] rowChange = {nameOfChange,"","",DateTime.Today.ToString("dd/MM/yyyy")," 18 ש\"ח","-1" };
                checkTable(rowChange);
            } catch { throw new NotImplementedException(); }
        }
    }
}