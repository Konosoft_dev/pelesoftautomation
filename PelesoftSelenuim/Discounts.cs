﻿using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
    internal class Discounts:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        createConatrabition cc = new createConatrabition();
        public  void createNewDiscount(string nameD, string financeD, string learaenMosad,string financeCh)
        {
            try {
                string amount =DiscountDetails(nameD, financeD);
                details();
                frqunce();
                placeStudy(learaenMosad,financeCh,amount);
                fp.ClickButton(" לשלב הבא ","span");
            } catch { throw new NotImplementedException(); }
        }
        private string  DiscountDetails(string nameD,string financeD)
        {
            string amount = "";
            try
            {
                fp.checkElmentText("title-section", By.ClassName("title-section"), "פרטי הנחה");
                fp.insertElment("DiscountName", By.Name("DiscountName"), nameD, true);
                fp.selectForRequerd(" סעיף פיננסי ", true, -1);
                fp.selectText(financeD);
                fp.selectForRequerd(" סוג סכום הנחה ", true, -1);
                string[] typeAmount = { "אחוזים", "קבוע" };
                 amount = typeAmount[fp.rand(typeAmount.Length)];
                fp.selectText(amount);
                fp.insertElment("sum", By.Name("Sum"), "5", true);
                if (amount == "קבוע")
                { fp.selectForRequerd(" סוג מטבע ", true); }
            }
            catch { throw new NotImplementedException("DiscountDetails"); }
            return amount;
        }
        private void details()
        {
            fp.checkElmentsText("title6-section", By.ClassName("title-section"), "פרטים נוספים", 1);
            fp.selectForRequerd("פעיל",false, 0);
        }
        private void frqunce()
        {
            try {
                fp.checkElmentsText("title-section", By.ClassName("title-section"), "תדירות", 2);
                fp.selectForRequerd(" תדירות ", true, -1);
                string[] frqunces = { " חודש בודד ", " טווח חודשים ", " תשלומים " };
                string frqunceStr = frqunces[fp.rand(frqunces.Length)];
                fp.selectText(frqunceStr);
                cc.calnderMont(0);
                switch (frqunceStr)
                { case " חודש בודד ":
                        break;
                    case " טווח חודשים ":
                        cc.calnderMont(1,1);
                        break;
                    case " תשלומים ":
                        fp.insertElment("NumPayments",By.Name("NumPayments"),"1",true);
                        break; }
            } catch { throw new NotImplementedException(); }
        }
        private void placeStudy(string learnMosad,string financeCh, string amount ,string DiscounDestination="חיוב")
        {
            fp.checkElmentsText("title-section", By.ClassName("title-section"), "עבור", 3);
            fp.selectForRequerd(" שנה\"ל ", false, -1);
            fp.selectText(yearHebru);
            fp.selectForRequerd("מוסד לימודים",true,-1);
            fp.selectText(learnMosad);
            //fp.selectForRequerd(" סניף ",false,-1);
            fp.selectForRequerd("סוג רמת כיתה");
            fp.selectForRequerd("על סעיפי חיוב",false,-1);
            fp.selectText(financeCh);
            try
            {if(amount=="קבוע")
                fp.selectForRequerd("יעד ההנחה", true, -1);
                fp.selectText(DiscounDestination);
            }
            catch { }
        }
        public void filter(string []filters,string []ben=null,int flag=0,int save=2)
        {
            try { fp.ClickButton(" סינון להנחה ","span");
                CreateMesaur cm = new CreateMesaur();
                cm.checkSailta(filters[0], filters[1], filters[2]);
                if (ben != null)
                {
                    cm.filterBen(ben,flag,save);
                }
                fp.ClickButton("שמירה","span",save-1);
                fp.ClickButton("שמירה","span",save-1);
            } catch { throw new NotImplementedException(); }
        }
    }
}