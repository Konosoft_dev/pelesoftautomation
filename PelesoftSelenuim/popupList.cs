﻿using com.sun.org.apache.xerces.@internal.impl.xpath;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;

namespace PelesoftSelenuim
{
    class popupList : page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void checkPage()
        {
            try
            {
                fp.checkElment("page popup", By.CssSelector("div[class='form-preview-warpper ng-star-inserted']"));
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void checkElment(string add="add")
        {

            try
            {
                fp.checkElment("poup close",By.XPath("//button[@aria-label='edit']"));
                fp.checkElment("popup icon edit",By.XPath("//div[@class='icon-edit']"));
                fp.checkElmentText("popup icon edit",By.XPath("//div[@class='txt-edit']"), "מצב עריכה");
                string[] valueCheck = {"","delete","attach_file",add };
                for (int i=1;i<=3;i++)
                     fp.checkElment(valueCheck[i],By.XPath("//div/i[class='material-icons tool-icon'][text()='"+ valueCheck[i]+"'"));

            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void chekName(string namepopup)
        {
            try
            {
                fp.checkElmentText("name popup", By.TagName("h2"), namepopup);
            }
            catch(System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void popupEdit()
        {
            clickEdit();
            fp.wait();
            checkpageEdit();
            //throw new NotImplementedException();
        }
        public void clickEdit()
        {
            try
            {
             //   fp.waitUntil(By.CssSelector("div[class='icon-button icon-edit']"));
                driver.FindElement(By.CssSelector("div[class='icon-button icon-edit']")).Click();
            }
            catch {
                driver.FindElement(By.ClassName("btn-edit")).Click();
            }
        }
        public void btnEdit()
        { 
                driver.FindElement(By.ClassName("btn-edit")).Click();
        }
        public void clickPaymentTransmission()
        {
            fp.ClickButton("שדר תשלום ", "span");
            clickPaymentTransmissionPopUP();
            fp.approveAction("כן");
            //fp.ClickButton(" שמירה ", "span");
            
        }
        public void clickPaymentTransmissionPopUP()
        {
            //לבדיקה בTEST הבא ולא אמור לעבוד באוטומציה
            fp.closePoup();
        }
        public void expansionPopUP()
        {
            driver.FindElement(By.XPath("//div[@aria-label='btnEdit']")).Click();
        }
        public void checkpageEdit()
        {
            fp.checkElment("poup close", By.XPath("//button[@aria-label='edit']"));
            fp.checkElmentText("title popup", By.CssSelector("h2[class='tab-title ng-star-inserted']"), " כללי");
            fp.checkElment("Expand reduce", By.ClassName("material-icons"));
            fp.checkElmentText("date right", By.CssSelector("div[class='date right']"), "נוצר בתאריך " + DateTime.Today.ToString("dd/MM/yyyy"));
            fp.checkElmentText("title section", By.ClassName("title-section"), "פרטי ה" + conatrabitionName + "ה");

        }
        public void clickLavel()
        {
            clickEdit();
            clickStepHeder(1, "רמות כיתה");
        }
        public  void clickAddGradeLavel()
        {
            try
            {
                
                driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()=' הוספת רמת כיתה ']")).Click();
                //fp.ClickButton(" הוספת "+ gradeLavel + " ", "psan");
            }
             catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public  void clickStepHeder(int numRow, string nameRow)
        {
            try
            {
                fp.waitUntil(By.TagName("mat-step-header"));
                IList<IWebElement>  stepheder =driver.FindElements(By.TagName("mat-step-header"));
                for (int i = 0; i < stepheder.Count - 1; i++)
                {
                    if (stepheder[i].Text.Trim() == nameRow.Trim())
                    {
                        stepheder[i].Click();
                        break;
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }
        public void clickDisplayInTable(int snifNum=-1)
        {
            try
            {
                fp.waitUntil(By.CssSelector("button[class='mat-icon-button mat-button-base']"));
                //string[] listheder = {"קוד סניף","שם סניף","מוסד חינוך","כתובת","עיר","הצגה","מחיקה" };
                //fp.checkTableHeder(listheder);
                string[] listRow = {"",automation,automation,automation,"","-1" };
                Thread.Sleep(300);
                //fp.checkColumn("wrap-data-table", listRow, "//i[@class='material-icons']", "mat-row"/*[role='row']"*/, "mat-cell"/*[role='gridcell']"*/);
                if(snifNum==-1)
                driver.FindElement(By.CssSelector("button[class='mat-icon-button mat-button-base']")).Click();
                else
                    driver.FindElements(By.CssSelector("button[aria-label='display']"))[snifNum-1].Click();

            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public void checkOpen()
        {
            try
            {
                PupilNew pn = new PupilNew();
               pn.openPupil();
                stepCheckOpen(By.Name("FirstName"), "א"); 
                fp.clickNameDefinitions(" הורים ");
              stepCheckOpen(By.Name("FirstName"), "א");
                fp.clickNameDefinitions(" תשלומים ");
                stepCheckOpen(By.Name("BillingFor"), "ח"); 
                fp.clickNameDefinitions(" הוראות קבע ");
                stepCheckOpen(By.Name("MainRemarks"), "ח");
                fp.clickNameDefinitions(" מוסדות ");
                fp.clickNameDefinitions(" מוסדות לפי דתות ");
               stepCheckOpen(By.Name("InstitutionalDatotCode"), "4"); 
                fp.clickNameDefinitions(" ילדים ");
                stepCheckOpen(By.Name("Name"), "ל");
                pn.openStudentColl();
               stepCheckOpen(By.Name("FirstName"), "א"); 
                Frind f = new Frind();
                f.openFrind();
               stepCheckOpen(By.Name("FirstName"), "ס");
                fp.clickNameDefinitions(" תרומות ");
                    stepCheckOpen(By.CssSelector("input[formcontrolname='NumPayments0']"), "2"); 
                fp.clickNameDefinitions(" תרומות בהו\"ק ");
                stepCheckOpen(By.CssSelector("input[formcontrolname='DebitSum']"), "2");  
                fp.clickNameDefinitions(" התחייבויות ");
                stepCheckOpen(By.CssSelector("input[formcontrolname='Sum']"), "2"); 
                fp.clickNameDefinitions(" מעקב ידידים ");
               stepCheckOpen(By.Name("trip-start"), DateTime.Today.ToString("HH:mm dd/MM/yyyy")); 
                fp.actionWork("אנשי קשר", "");
                stepCheckOpen(By.Name("FirstName"), "א"); 

            }
            catch { throw new NotImplementedException(); }
        }
        public int stepCheckOpen(By byChange,string valueChange)
        {
            int ret = 0;
            PupilNew pn = new PupilNew();
            try
            {
                Thread.Sleep(300);
                int i = pn.clickTableRandom();
                popupEdit();
                fp.insertElment("change name", byChange, valueChange);
                fp.ClickButton(" שמירה ", "span");
                if (fp.checkElment("", By.XPath("//div[text()='הנתונים נשמרו בהצלחה']")) == 1)
                    ret = 1;
                fp.closePoup();
                pn.clickTableRandom(i);
                    if (driver.FindElement(byChange).GetCssValue("text").Contains(valueChange) == false)
                { fp.picter("FirstName notChange"); throw new NotImplementedException(); }
            }
            catch { fp.closepopup();  }
            return ret;
            if (ret==1)
            {
                fp.picter("FirstName notChange"); throw new NotImplementedException();
            }
        }
    }
}