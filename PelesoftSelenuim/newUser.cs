﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PelesoftSelenuim
{
     class newUser:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void openCreate()
        {
            fp.ClickButton(" יצירת " + "משתמש" + " חדש" +  " ", "span");
            fp.waitUntil(By.XPath("//h2[text()='הוספת משתמש']"));
            //throw new NotImplementedException();
        }

        public void insertCreate()
        {
            fp.insertElment("name user", By.Name("UserLogin"), automation+"משתמש",true);
            fp.insertElment("first name", By.Name("FirstName"),automation+ "פרטי");
            fp.insertElment("famely name", By.Name("LastName"), automation+"משפחה");
            Thread.Sleep(100);
            fp.insertElment("ContactPhoneNumber", By.Name("ContactPhoneNumber"), "0527612148");
            fp.SelectRequerdName("formcontrolname='Institutes", automation,false,true);
            new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[0]).SendKeys(Keys.Tab).Perform();
            Thread.Sleep(200);
            fp.SelectRequerdName("formcontrolname='Roles", " מזכיר ",false);
            new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[1]).SendKeys(Keys.Tab).Perform();
            Thread.Sleep(100);
            fp.insertElment("RestorePasswordEmail", By.Name("RestorePasswordEmail"), MAIL,true);
            driver.FindElement(By.XPath("//mat-checkbox[@formcontrolname='SystemSettings']")).Click();
            fp.ClickButton(" שמירה ");
            //throw new NotImplementedException();
        }

        public  void checkCreate()
        {
            fp.checkElmentText("check popup title", By.TagName("h2"), "הוספת משתמש");
            fp.checkElmentText("check name user", By.Name("UserLogin"),"שם משתמש");
            fp.checkElmentText("check first name", By.Name("FirstName"),"שם פרטי");
            fp.checkElmentText("check famely name", By.Name("LastName"),"שם משפחה");
            fp.checkElmentText("check ContactPhoneNumber", By.Name("ContactPhoneNumber"),"מספר ליצירת קשר");
            fp.checkElmentText("check mosdot", By.XPath("//mat-select[@formcontrolname='Institutes']"),"מוסדות");
            fp.checkElmentText("check tafkidim", By.XPath("//mat-select[@formcontrolname='Roles']"),"תפקידים");
            fp.checkElmentText("check RestorePasswordEmail", By.Name("RestorePasswordEmail"),"מייל לשחזור סיסמה");
            fp.checkElmentText("check ceecbox", By.XPath("//mat-checkbox[@formcontrolname='SystemSettings']"), "הרשאה להגדרות מערכת");
            //throw new NotImplementedException();
        }

        public  void addRolse(bool branch=false,bool zeroChange=false)
        {
            try {
                IList<IWebElement> roles = driver.FindElements(By.TagName("mat-row"));
                for(int i=0;i<roles.Count;i++)
                {
                    IList<IWebElement> row = roles[i].FindElements(By.TagName("mat-cell"));
                    if (row[0].Text == "")
                    {
                        /*if (row[1].FindElement(By.TagName("input")).Text.Contains("תפקיד חדש"))
                        {*/
                            row[1].FindElement(By.TagName("input")).Clear();
                            row[1].FindElement(By.TagName("input")).SendKeys(automation);
                            row[2].Click();
                        row[2].FindElements(By.XPath("//mat-form-field/div/div/div/mat-select"))[i+3].Click();
                            fp.actionWork(" הגדרות "," ניהול מערכת ");
                            new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[1]).SendKeys(Keys.Tab).Perform();
                            if (branch == true)
                            {
                                row[3].FindElement(By.TagName("button")).Click();
                                if (driver.FindElements(By.TagName("mat-row")).Count - 1 == roles.Count)
                                    Console.WriteLine("the row add roles delet");
                                else
                                    Debug.WriteLine("the row add rolse not delet");
                            }
                       // }
                    }
                }
                if(zeroChange==true)
                {
                    fp.ClickButton("אפס שינויים ");
                    if(branch==false && driver.FindElements(By.TagName("mat-row")).Count - 1 == roles.Count)
                        Console.WriteLine("the row add roles delet");
                    else
                        Debug.WriteLine("the row add rolse not delet");
                }
                fp.ClickButton("שמירה ");
            }
            catch { }
            //throw new NotImplementedException();
        }

        public void clickEditUser()
        {
            Thread.Sleep(500);
            string[] columnCheck = { "2", "", "", "", "", "", "", "", "", "", "", "-1" };
            fp.checkColumn("//", columnCheck, "button[class='mat-icon-button mat-button-base']", "mat-row", "mat-cell", 11);
            //throw new NotImplementedException();
        }

        public void addMosad(string mosad)
        {
            fp.SelectRequerdName("formcontrolname='Institutes", mosad, false,true);
            new Actions(driver).MoveToElement(driver.FindElements(By.ClassName("mat-option-text"))[1]).SendKeys(Keys.Tab).Perform();
            fp.ClickButton(" שמירה ","span");
            try
            {
                if (driver.FindElement(By.TagName("app-generic-message")).Text == "רשומת משתמש עודכנה בהצלחה")
                    Console.WriteLine("the row edit");
            } 
            catch
            {
                Debug.WriteLine("the row not edit");
                fp.ClickButton(" שמירה ");
            }
            driver.FindElement(By.XPath("//span[@class='mat-button-wrapper']/i[text()=' close ']")).Click();
            // throw new NotImplementedException();
        }
    }
}