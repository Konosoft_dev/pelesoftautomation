﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace PelesoftSelenuim
{
    [TestClass]
    public class UnitTest2
    {
         page p = new page();
        FunctionPelesoft fp = new FunctionPelesoft();
        //public IWebDriver driver = null;
        SucessEndCreate sec = new SucessEndCreate();
        CreateNewDenumic cnd = new CreateNewDenumic();
        Financical fl = new Financical();
        Frind f = new Frind();
        AddTable at = new AddTable();
        receStrct rs = new receStrct();
        CreateMesaur cm = new CreateMesaur();

        //public TimeSpan timeSpan;
        //[SetUp]
        ClearingEntity ce = new ClearingEntity();
        // ExcelApiTest eat = new ExcelApiTest();
        //@Test(Priority=1)
        //  [TestMethod]
        public void Test01Login(string nameTasret = "")
        {
            if (nameTasret != "")
                fp.OpenExcel(nameTasret, "failed");
            login ln = new login();
            ln.openChrome_2();
            ln.UserNamee("צביה");
            //  ln.UserNamee("aaa");
            ln.Password("צביה1234!");
            //  ln.Password("aaa");
            ln.cLickLogIn();
        }
        [TestMethod]
        public void Test02InsertList()
        {
            Test01Login("Test02InsertList");
            fp.actionWork(" הגדרות ", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            for (int i = 0; i < p.nmeList.Length; i++)
            {
                nl.clickTable(p.nmeList[i]);
                Thread.Sleep(200);
                nl.saveEditData(p.newList[i]);
                Thread.Sleep(1900);
            }
            fp.closeChrome();
            fp.OpenExcel("Test02InsertList", "passed");
        }
        //יצירת ידיד
        [TestMethod]
        public void Test03CreateNewFrinedClose()
        {
            Test01Login("Test03CreateNewFrinedClose");
            f.createNewFrined();
            sec.closePage();
            fp.OpenExcel("Test03CreateNewFrinedClose", "passed");

        }

     /*   [TestMethod, Order(21)]
        public void Test22CreateNewFrinedGTCardFrind()
        {
            //לא רץ תקין
            Test01Login("Test03CreateNewFrinedGTCardFrind");
            f.createNewFrined();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test03CreateNewFrinedGTCardFrind", "passed");

        }
        [TestMethod, Order(22)]
        public void Test03CreateNewFrinedGTCNFrind()
        {
            Test01Login("Test03CreateNewFrinedGTCNFrind");
            f.createNewFrined();
            f.clickCNfrind();
            fp.OpenExcel("Test03CreateNewFrinedGTCNFrind", "passed");

        }*/
        //ישויות סליקה
        [TestMethod, Order(3)]
        public void Test02CreateNewClearingEntityclose()
        {
            Test01Login("Test02CreateNewClearingEntityclose");
            ce.CreateNewClearingEntity();
            sec.closePage();
            fp.OpenExcel("Test02CreateNewClearingEntityclose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test04CreateNewClearingEntityCard()
        {
            Test01Login("Test04CreateNewClearingEntityCard");
            ce.CreateNewClearingEntity(" אשראי הו\"ק ");
            sec.pageCard("ישות סליקה");
            fp.OpenExcel("Test04CreateNewClearingEntityCard", "Passed");
        }

        [TestMethod, Order(22)]
        public void Test04CreateNewClearingEntityCreateNew()
        {
            Test01Login("Test04CreateNewClearingEntityCreateNew");
            ce.CreateNewClearingEntity("מסב");
            sec.pageNewCreate("ישות סליקה נוספת");
            fp.OpenExcel("Test04CreateNewClearingEntityCreateNew", "Passed");
        }

       /* [TestMethod, Order(22)]
        public void Test04CreateNewClearingEntityCardMosed()
        {
            Test01Login("Test04CreateNewClearingEntityCardMosed");
            ce.CreateNewClearingEntity();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test04CreateNewClearingEntityCardMosed", "Passed");
        }*/
        //חשבון בנק
        CreateBank cb = new CreateBank();

        [TestMethod, Order(4)]
        public void Test05CreateNewAccountBankClose()
        {
            Test01Login("Test05CreateNewAccountBankClose");
            cb.CreateNewAccountBank();
            sec.closePage();
            fp.OpenExcel("Test05CreateNewAccountBankClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCMosed()
        {
            Test01Login("Test05CreateNewAccountBankGTCMosed");
            cb.CreateNewAccountBank();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test05CreateNewAccountBankGTCMosed", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCard()
        {
            Test01Login("Test05CreateNewAccountBankGTCard");
            cb.CreateNewAccountBank();
            sec.pageCard("חשבון בנק");
            fp.OpenExcel("Test05CreateNewAccountBankGTCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test24CreateNewAccountBankGTCNew()
        {
            Test01Login("Test05CreateNewAccountBankGTCNew");
            cb.CreateNewAccountBank();
            sec.pageNewCreate("חשבון בנק נוסף");
            fp.OpenExcel("Test05CreateNewAccountBankGTCNew", "Passed");
        }

        //סעיפים פיננסים
        [TestMethod, Order(5)]
        public void Test06CreateNewFinancicalSectionClose()
        {
            Test01Login("Test06CreateNewFinancicalSectionClose");
            fl.createNewFinancicalSection();
            sec.closePage();
            fp.OpenExcel("Test06CreateNewFinancicalSectionClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionCardFinanci()
        {
            Test01Login("Test06CreateNewFinancicalSectionCardFinanci");
            fl.createNewFinancicalSection(" הוראות קבע ראשי ", "", "הוראות קבע ראשי");
            sec.pageCard(p.FinancicalSection);
            fp.OpenExcel("Test06CreateNewFinancicalSectionCardFinanci", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionCreatNew()
        {
            Test01Login("Test06CreateNewFinancicalSectionCreatNew");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "כולל", "זיכוי");
            sec.pageNewCreate(p.FinancicalSection + " נוסף");
            fp.OpenExcel("Test06CreateNewFinancicalSectionCreatNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateNewFinancicalSectionMosad()
        {
            Test01Login("Test06CreateNewFinancicalSectionMosad");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "בונוס", "בונוס");
            sec.pageCard("מוסד");
            fp.OpenExcel("Test06CreateNewFinancicalSectionMosad", "Passed");
        }

        /// <summary>
        /// תרומות -תרומה במזומן
        /// </summary>//run 
        [TestMethod, Order(6)]
        public void Test07CreateNewConatrabitionCase()//נופל על יצירת קבלה
        {
            string idConatarbition = "78";
            createConatrabition cc = new createConatrabition();
            Test01Login("Test07CreateNewConatrabitionCase");
            cc.conatrabitionList();
            cc.createNew("מזומן", "Cash");
            cc.checknumConatarbition();
            createDepositcase();
           
            fp.OpenExcel("Test07CreateNewConatrabitionCase", "Passed");
        }
        //  [TestMethod]
        public void createDepositcase()
        {
            Deposit d = new Deposit();
            Test01Login();
            Thread.Sleep(100);
            d.dipositType("מזומן ");

            d.diposetCash("מזומן");
            fp.closeChrome();
        }
        //טיפול בקבלות
        public void CreateProductionReceipt(string card, string idConatarbition = "")
        {
            
            //idConatarbition = "4";
            Test01Login();
            Production_receipt pr = new Production_receipt();
            Thread.Sleep(900);
            pr.ProductionReceipt();
            pr.checkPageProductionReceipt();
            string[] listColumn = { "", "תרומות", idConatarbition, "", card, "", "",/* DateTime.Today.ToString("dd/MM/yyyy") ,*/"10", "1", "-1" };
            pr.tableProductionReceipt(listColumn);
            fp.spinner();
            pr.printCancel();
        }
        //תרומה בצק//run
        [TestMethod, Order(6)]
        public void Test07CreateNewConatrabitionBag()
        {
            Test01Login("Test07CreateNewConatrabitionBag");
            string idConatarbition = 64.ToString();
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("צק", "Check");
            Thread.Sleep(200);
            cc.checknumConatarbition();
            hcreateDepositCheeck();
            ihandlingCheck();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            idConatarbition = eat.createExcel("conatarbition", "mesaa");
            CreateProductionReceipt("צ\'ק", idConatarbition);//נופל כאן
            fp.OpenExcel("Test07CreateNewConatrabitionBag", "Passed");
        }
        //הפקדת צקים
        public void hcreateDepositCheeck()
        {
            Deposit d = new Deposit();
            Test01Login();
            d.dipositType("צ");
            // d.diposetCash("צ\'קים", p.numberCheck + " " + p.nameBank + " " + p.numBranch + " " + p.numberAmountBank + "30/09/2020" /*DateTime.Today.ToString("dd/MM/yyyy") */+ " 10 שקל לגבייה תרומה");
            //"12345678 04 007 123456 30/09/2020 00:00:00 10 שקל לגבייה תרומה"
            //p.numberCheck+" "+p.nameBank+" "+p.numBranch+" "+ p.numberAmountBank +DateTime.Today.ToString("dd/MM/yyyy")+ " 10 שקל לגבייה תרומה",
            string[] row = { "", p.numberCheck, p.nameBankNO0, p.numBranch, p.numberAmountBank, DateTime.Today.ToString("dd/MM/yyyy"), "10", "שקל", "לגבייה", "תרומה" };
            d.tableclickCheck(row);
            fp.ClickButton("הפקדה ");
            fp.checkElment("open pdf", By.TagName("embed"));
            fp.closeChrome();
        }
        //טיפול בצקים
        public void ihandlingCheck()
        {
            HandingCheeck hc = new HandingCheeck();
            Test01Login();
            fp.actionWork("ניהול כספים", "טיפול בצ");
            //click button filter
            hc.checkPagehandingCheeck();
            fp.closeChrome();
        }
        //תרומה באשראי
        [TestMethod, Order(6)]
        public void Test07CreateDipositCreadetCard()
        {
            Test01Login("Test07CreateDipositCreadetCard");
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("כרטיס אשראי", "Credit",false,-1);
            sec.checkEndCreate("תרומה", false, "ה", "ידיד");
            sec.closePage();
            fp.OpenExcel("Test07CreateDipositCreadetCard", "Passed");
        }
        //שידור אשראי דרך הכרטיס
        [TestMethod, Order(7)]
        public void Test08CreaditCardConaatarbition()
        {
            ///מתבצע בדיקה שהכל תקין אך לא מתבצע באמת שידור עקב כך שבאוטומציה אי אפשר לדמות שיחת טלפון
            Test07CreateDipositCreadetCard();
            createConatrabition cc = new createConatrabition();
            Test01Login("Test08CreaditCardConaatarbition");
            cc.conatrabitionList();
            string[] listRow = { "", "", "", "", "", ""/*DateTime.Today.ToString("dd/MM/yy")*/, "שקל", "1", "", "", "", "אשראי" };
            cc.checkTabl(listRow);
            popupList ppl = new popupList();
            ppl.checkPage();
            ppl.checkElment();
            ppl.chekName(p.family);
            ppl.popupEdit();
            ppl.clickPaymentTransmission();
            fp.closeChrome();
            fp.OpenExcel("Test08CreaditCardConaatarbition", "Passed");
        }
        //שידור אשראי דרך ניהול כספים
        [TestMethod, Order(7)]//רץ תקין
        public void Test08CreaditTransmissionCT()
        {
            Test07CreateDipositCreadetCard();
            ceaditTransmission ct = new ceaditTransmission();
            Test01Login("Test08CreaditTransmissionCT");
            fp.actionWork(" ניהול כספים ", "שידורי אשראי");
            ct.checkElment();
            ct.checkClearing("true", "אשראי", 0);
            ct.viewingHandalingPayments("true");
            fp.closeChrome();
            fp.OpenExcel("Test08CreaditTransmissionCT", "Passed");
        }

        //טיפול בתשלומי אשראי
        [TestMethod, Order(8)]
        public void Test09ReturnBroadcast()
        {
            Test01Login("Test09ReturnBroadcast");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.returnBroadcast("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test09ReturnBroadcast", "Passed");
        }
        [TestMethod, Order(9)]//run good
        public void Test10BroadcastSdreenViewingPayments()
        {
            Test01Login("Test10BroadcastSdreenViewingPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.BroadcastSdreenViewingPayments("אשראי");
            fp.closeChrome();
            fp.OpenExcel("Test10BroadcastSdreenViewingPayments", "passed");
        }
        /// <summary>
        /// לא הורץ
        /// </summary>טיפול בקבלת אשראי
        [TestMethod, Order(10)]
        public void Test11HandingViewPayments()
        {
            Test08CreaditCardConaatarbition();
            Test01Login("Test11HandingViewPayments");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            //vhp.handingViewPayments("אשראי");//לבדוק עם ריקי
            vhp.checkchange();
            fp.OpenExcel("Test11HandingViewPayments", "Passed");
        }
        [TestMethod, Order(11)]
        public void Test12productionReceipt()
        {

            CreateProductionReceipt("אשראי", "");
            Production_receipt pr = new Production_receipt();
            Test01Login("Test12productionReceipt");
            pr.ProductionReceipt();//לא נבדק שלא מופיע
            fp.ClickButton(" הצג נתונים ");
            fp.closeChrome();
            fp.OpenExcel("Test12productionReceipt", "passed");
        }
        //העברה בנקאית
        [TestMethod, Order(6)]//נכשל ביצירת קבלה//בגרסה הבאה להחזיראת התאריכון
        public void Test07CreateDipositBankTransfer()
        {
            Test01Login("Test07CreateDipositBankTransfer");
            string idConatarbition = "8";
            createConatrabition cc = new createConatrabition();
            cc.conatrabitionList();
            cc.createNew("העברה בנקאית", "BankDeposit");
            cc.checknumConatarbition();
            ExcelApiTest eat = new ExcelApiTest("", "conatarbition");
            idConatarbition = eat.createExcel("conatarbition", "mesaa");
            fp.closeChrome();
            CreateProductionReceipt("העברה בנקאית", idConatarbition);
            fp.OpenExcel("Test07CreateDipositBankTransfer", "Passed");
        }
        /// <summary>
        /// הו"ק אשראי
        /// </summary>

        [TestMethod, Order(6)]
        public void Test07CreateDirectDebitClose()
        {
            Test01Login("Test07CreateDirectDebitClose");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.closePage();
            fp.OpenExcel("Test07CreateDirectDebitClose", "Passed");
        }
        //הוראת קבע
        [TestMethod, Order(22)]
        public void Test07CreateDirectDebitFrind()
        {
            Test01Login("Test07CreateDirectDebitFrind");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test07CreateDirectDebitFrind", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07CreateDirectDebitCardDD()
        {
            Test01Login("Test07CreateDirectDebitCardDD");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageCard("הו\"ק");
            fp.OpenExcel("Test07CreateDirectDebitCardDD", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07CreateDirectDebitAddCard()
        {
            Test01Login("Test07CreateDirectDebitAddCard");
            createConatrabition cc = new createConatrabition();
            cc.stepHOK();
            sec.pageNewCreate("הו\"ק נוספת");
            fp.OpenExcel("Test07CreateDirectDebitAddCard", "passed");
        }
        //ביצוע הו"ק
        [TestMethod, Order(12)]
        public void Test13ExecutionHQ()
        {
            Test01Login("Test13ExecutionHQ");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            ExecutionHQ ehq = new ExecutionHQ();
            string m = fp.dateD();
            ehq.checkelmentExecutionHQ("אשראי הו\"ק", m);
            ehq.clickGvia();
            fp.spinner();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false","אשראי הו\"ק");
            string[] listHQ = { "", p.adress, " אשראי ", DateTime.Today.ToString("dd/MM/yyyy"), p.amount + " ש\"ח", "בוצע", "עסקה תקינה. ", "-1" };
            vhp.checkTable(listHQ);
            vhp.returnBroadcast(p.dDebitCard,"אשראי הו\"ק");
            vhp.BroadcastSdreenViewingPayments(p.dDebitCard);
            vhp.returnBroadcast(" אשראי ", "אשראי הו\"ק");
            vhp.handingViewPayments(p.dDebitCard);//עבר ל טופל
            Test01Login();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.ClearingHouse, m);
            string[] listRow = { p.family + " " + p.family, p.adress, "", p.fClausess, "", p.amount, "שקל", "1", "לגבייה", "-1" };
            ehq.checkTable(listRow);
            fp.closeChrome();
            fp.OpenExcel("Test13ExecutionHQ", "Passed");
        }
        //טיפול בקבלות עבור הו"ק//רץ תק
        [TestMethod, Order(13)]
        public void Test14HandlingReceiptsHQ()
        {
            Test01Login("Test14HandlingReceiptsHQ");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("אשראי");
            string[] listRow = { "טבלת הוראות קבע", "","", " אשראי ", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow, 0);
            fp.ClickButton(" הפק קבלות ");
            fp.closeChrome();
            fp.OpenExcel("Test14HandlingReceiptsHQ", "Passed");
        }
        //יצירת מס"ב
        /// fl.createNewFinancicalSection(" הוראות קבע ראשי ");

        [TestMethod, Order(6)]
        public void Test07HQMSABClose()
        {
            Test01Login("Test07HQMSABClose");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.closePage();
            fp.OpenExcel("Test07HQMSABClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABAddCard()
        {
            Test01Login("Test07HQMSABAddCard");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת סכום ");
            sec.pageNewCreate("הו\"ק נוספת");
            fp.OpenExcel("Test07HQMSABAddCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07HQMSABCardHQ()
        {
            Test01Login("Test07HQMSABCardHQ");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab(" מגבלת חיובים ");
            sec.pageCard("הו\"ק");
            fp.OpenExcel("Test07HQMSABCardHQ", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22HQMSABCardFrind()
        {
            Test01Login("Test22HQMSABCardFrind");
            createConatrabition cc = new createConatrabition();
            cc.stepHQMsab();
            sec.pageCard(p.parent);
            fp.OpenExcel("Test22HQMSABCardFrind", "Passed");
        }
        //ביצוע הו"ק עבור מס"ב
        [TestMethod, Order(14)]
        public void Test15ExecutionHQMsab()
        {
            Test01Login("Test15ExecutionHQMsab");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            ExecutionHQ ehq = new ExecutionHQ();
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            fp.spinner();
            ehq.clickGvia();
            fp.spinner();
            vhp.openViewingHandalingPayment();
            vhp.checkElmentVHP("false", "מסב");
            string[] listHQ = { "", p.adress, "מסב", "", "", "בוצע", "עסקה תקינה. ", "-1" };
            vhp.checkTable(listHQ);
            vhp.returnBroadcast("מסב", "מסב");
            vhp.handingViewPayments("מסב", "מסב");
            ehq.insertExecutionHQ();
            ehq.checkelmentExecutionHQ(p.massav, fp.dateD());
            ExcelApiTest eat = new ExcelApiTest("", "nameFamily");
            string faml = eat.createExcel("nameFamily", "nameFamily", false);
            string[] listRow = { faml, p.adress, "", p.hoqRase, "מסב", p.amount, "שקל", "1", "לגבייה", "-1" };
            ehq.checkTable(listRow);
            fp.closeChrome();
            fp.OpenExcel("Test15ExecutionHQMsab", "Passed");
        }
        //הפקת קבלות למס"ב
        [TestMethod, Order(15)]
        public void Test16HandlingReceiptsHQMsab()
        {
            Test01Login("Test16HandlingReceiptsHQMsab");
            HandlingHQReceipts hhrq = new HandlingHQReceipts();
            hhrq.insertpageHhrq();
            hhrq.checkPage("מסב");
            string[] listRow = { "טבלת הוראות קבע", "", "", "מסב", "", "", "", p.amount, "1", "-1" };
            hhrq.checkTable(listRow);
            fp.ClickButton(" הפק קבלות ");
            fp.closeChrome();
            fp.OpenExcel("Test16HandlingReceiptsHQMsab", "Passed");
        }
        // דיווח חזרות מס"ב//לא גמור לבדוק שמגיע לצפייה וטיפול בתשלומים 
        [TestMethod, Order(16)]
        public void Test17ReturenMsab()
        {
            Test01Login("Test17ReturenMsab");
            returnMSAB rm = new returnMSAB();
            rm.openMsabPage();
            rm.checkPageReturnMsab();
            rm.insertData();
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.filterMsab();
            fp.closeChrome();
            fp.OpenExcel("Test17ReturenMsab", "Passed");
        }
        ///יצירת הורה
        [TestMethod]
        public void Test06createParents()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents(p.familys[fp.rand(p.familys.Length)]);
            sec.checkEndCreate("הורה", false);
            sec.closePage();
        }
        [TestMethod]
        public void Test06createParentsCard()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("ליכטנשטיין");
            sec.checkEndCreate("הורה", false);
            f.clickCardFrind();
            fp.closeChrome();
        }
        [TestMethod]
        public void Test06createParentsNew()
        {
            Test01Login("Test06createParents");
            f.openParent();
            f.stepCreateParents("רוזנברגר");
            sec.checkEndCreate("הורה", false);
            sec.pageNewCreate("הורה");
        }
        /// <summary>
        /// יצירת תלמיד
        /// </summary>
        /// 

        ///מוסד חנוך
        [TestMethod, Order(5)]
        public void Test06CreateEducationalInstitutionClose()
        {
            Test01Login("Test6CreateEducationalInstitutionClose");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI("בית אברהם");
            sec.checkEndCreate(p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test6CreateEducationalInstitutionClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateEducationalInstitutionCard()
        {
            Test01Login("Test06CreateEducationalInstitutionCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI();
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test06CreateEducationalInstitutionCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test06CreateEducationalInstitutionAddNew()
        {
            Test01Login("Test06CreateEducationalInstitutionAddNew");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI();
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageNewCreate(p.MosadChinuc + " נוסף");
            fp.OpenExcel("Test06CreateEducationalInstitutionAddNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateEducationalInstitutionCardMosad()
        {
            Test01Login("Test22CreateEducationalInstitutionCardMosad");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI();
            sec.checkEndCreate(p.MosadChinuc);
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22CreateEducationalInstitutionCardMosad", "Passed");
        }

        /// <summary>
        /// רמת כיתה
        /// </summary>
        [TestMethod, Order(6)]
        public void Test07GradeLavelClose()
        {
            Test01Login("Test07GradeLavelClose");
            createGradeLavel cgl = new createGradeLavel();
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            cgl.stepLavel("בית אברהם");
            cgl.GradeLavelCreate();
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.closePage(1);
            fp.OpenExcel("Test07GradeLavelClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test07GradeLavelCard()
        {
            Test01Login("Test07GradeLavelCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel();
            cgl.GradeLavelCreate();
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageCard(p.gradeLavel);
            fp.OpenExcel("Test07GradeLavelCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22GradeLavelnewCard()
        {
            Test01Login("Test07GradeLavelnewCard");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel();
            cgl.GradeLavelCreate();
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageNewCreate(p.gradeLavel + " נוספת", 1);
            fp.OpenExcel("Test07GradeLavelnewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22GradeLavelMosadChinuch()
        {
            Test01Login("Test22GradeLavelMosadChinuch");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            createGradeLavel cgl = new createGradeLavel();
            cgl.stepLavel();
            cgl.GradeLavelCreate();
            sec.checkEndCreate(p.gradeLavel, false, "ה", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22GradeLavelMosadChinuch", "Passed");
        }
        /// <summary>
        /// יצירת כיתה
        /// </summary>
        [TestMethod, Order(7)]
        public void Test08CreateGradeClose()
        {
            Test01Login("Test08CreateGradeClose");
            Grade g = new Grade();
            g.stepCreateGard("בית אברהם");
            sec.closePage(2);
            fp.OpenExcel("Test08CreateGradeClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test08CreateGradecardGrade()
        {
            Test01Login("Test08CreateGradecardGrade");
            Grade g = new Grade();
            g.stepCreateGard();
            sec.pageCard("כיתה");
            fp.OpenExcel("Test08CreateGradecardGrade", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateGradeAddNew()
        {
            Test01Login("Test22CreateGradeAddNew");
            Grade g = new Grade();
            g.stepCreateGard();
            sec.pageNewCreate("כיתה נוספת", 2);
            fp.OpenExcel("Test22CreateGradeAddNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateGradeCardMosadChinuc()
        {
            Test01Login("Test22CreateGradeCardMosadChinuc");
            Grade g = new Grade();
            g.stepCreateGard();
            sec.pageCard("סניף " + p.MosadChinuc + " ");
            fp.OpenExcel("Test22CreateGradeCardMosadChinuc", "Passed");
        }
        /// <summary>
        /// יצירת תלמיד
        /// </summary>
        [TestMethod, Order(8)]
        public void Test09CreatePupilClose()
        {
            Test01Login("Test09CreatePupilClose");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL("רוזנברגר", "בית אברהם");
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test09CreatePupilClose", "Passed");
        }
        [TestMethod, Order(22)]//בגרסה הבאה להחזיר את כיתה וסניף pupil->114/124,127
        public void Test09CreatePupilCard()//run
        {
            Test01Login("Test09CreatePupilCard");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL("", "בית אברהם");
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            fp.click(By.CssSelector("button[aria-label='btnGoToObjectCreated']"));
            pn.insertDate("ישיבה", 1);
            string sum = "1400";
            string[] ofen = p.ofenM;
            pn.prentalDataiInset(ofen[fp.rand(ofen.Length)], "", false, 1);
            pn.addDataWork(3, "2000");
            fp.closePoup(1);
            fp.closeChrome();
            fp.OpenExcel("Test09CreatePupilCard", "Passed");
        }

        [TestMethod, Order(22)]
        public void Test22CreatePupilNewCard()
        {
            Test01Login("Test22CreatePupilNewCard");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil(fl.tzPupil(), "בית אברהם");
            pn.stepCreatePUPIL();
            sec.checkEndCreate("תלמיד", false, "", "הורה");
            pn.pageNewCreate("תלמיד נוסף");
            fp.OpenExcel("Test22CreatePupilNewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePupilCardFrind()
        {
            Test01Login("Test22CreatePupilCardFrind");
            PupilNew pn = new PupilNew();
            pn.openCreatePupil("229931464", "בית אברהם");
            pn.stepCreatePUPIL();
            sec.checkEndCreate("תלמיד", false, "", p.parent);
            sec.pageCard(p.parent);
            fp.OpenExcel("Test22CreatePupilCardFrind", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkCard()
        {
            Test01Login("Test22CreateAddWorkCard");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTableRandom();
            pn.stepWork(2, "8000", "אם");
            sec.pageCard(" קח אותי לכרטיס נתון הכנסה להורה ");
            fp.OpenExcel("Test22CreateAddWorkCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkNew()
        {
            Test01Login("Test22CreateAddWorkNew");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTableRandom();
            pn.stepWork(1, "5000", "אפוטרופוס");
            sec.pageNewCreate("נתון הכנסה להורה נוסף", 1);
            fp.OpenExcel("Test22CreateAddWorkNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateAddWorkSnatonPrenatal()
        {
            Test01Login("Test18CreateAddWorkSnatonPrenatal");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTableRandom();
            pn.stepWork(3, "1400", "אב");
            sec.pageCard("שנתון להורים");
            fp.OpenExcel("Test18CreateAddWorkSnatonPrenatal", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22StudenTestExtension()
        {
            Test01Login("Test22StudenTestExtension");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTableRandom();
            pn.checkFrind();
            Thread.Sleep(100);
            pn.learn();
            fp.closeChrome();
            fp.OpenExcel("Test22StudenTestExtension", "Passed");
        }
        /// <summary>
        /// מחולל חיובי הורים
        /// </summary>
        /// 
        [TestMethod, Order(9)]
        public void Test10FinanceParentalCharges()
        {
            Test01Login("Test10FinanceParentalCharges");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים");
            sec.closePage();
            fp.OpenExcel("Test10FinanceParentalCharges", "Paassed");
        }

        /// <summary>
        /// יצירת הגדרת סעיף חיוב חד פעמי
        /// </summary>
        /// 
        [TestMethod, Order(10)]
        public void Test11CreateCurrentBillingOneTimeClose()
        {
            Test01Login("Test11CreateCurrentBillingOneTimeClose");

            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test11CreateCurrentBillingOneTimeClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCard()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardNew()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingOneTimeCardMC()
        {
            Test01Login("Test22CreateCurrentBillingOneTimeCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("חד פעמי");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingOneTimeCardMC", "Passed");
        }
        /// <summary>
        /// יצירת הגדרת סעיף חיוב שוטף
        /// </summary>
        /// 
        [TestMethod, Order(11)]
        public void Test12CreateCurrentBillingClauseClose()
        {
            Test01Login("Test12CreateCurrentBillingClauseClose");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.closePage();
            fp.OpenExcel("Test12CreateCurrentBillingClauseClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCard()
        {
            Test01Login("Test12CreateCurrentBillingClauseCard");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.DBillingC);
            fp.OpenExcel("Test12CreateCurrentBillingClauseCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardNew()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardNew");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageNewCreate(p.DBillingC + " נוסף");
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateCurrentBillingClauseCardMC()
        {
            Test01Login("Test22CreateCurrentBillingClauseCardMC");
            CurrentBillingClause cbc = new CurrentBillingClause();
            cbc.openNewBillingClause();
            cbc.createBillingClause("שוטף", "בית אברהם");
            sec.checkEndCreate(p.DBillingC, false, "", p.MosadChinuc);
            sec.pageCard(p.MosadChinuc);
            fp.OpenExcel("Test22CreateCurrentBillingClauseCardMC", "Passed");
        }
        [TestMethod]
        public void Test13ParentalCharges()
        {
            Test01Login("Test13ParentalCharges");
            fp.actionWork(" ניהול כספים ", "מחולל חיובי הורים");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.stepFinanceParentalCharges(p.idIdid, "בית אברהם");
            fp.closeChrome();
            fp.OpenExcel("Test13ParentalCharges", "Passed");
        }
        [TestMethod]
        public void Test14CreatePayment()//closeCardAsraiOne
        {
            Test01Login("Test14CreatePayment");
            fl.createNewFinancicalSection(" תשלומים ");
            fp.closepopup();
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "כרטיס אשראי", p.idIdid, p.automation, "", "Credit");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test14CreatePayment", "passed");
        }
        [TestMethod]//no display saif requerd "כללי"
        public void Test22CreatePaymentCardCaseOne()
        {
            Test01Login("Test22CreatePaymentCardCaseOne");
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "מזומן", p.idIdid, "כללי", "", "Cash", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard("תשלום", "btnGoToObjectCreated");
            fp.OpenExcel("Test22CreatePaymentCardCaseOne", "passed");
        }
        [TestMethod]
        public void Test22CreatePaymentNewCheckOne()
        {
            Test01Login("Test22CreatePaymentNewCheckOne");
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "צק", p.idIdid, p.automation, "", "Check");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageNewCreate("תשלום נוסף");
            fp.OpenExcel("Test22CreatePaymentNewCheckOne", "passed");
        }
        [TestMethod]
        public void Test22CreatePaymentFrindBankTransferOne()
        {
            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום חד פעמי", "העברה בנקאית", p.idIdid, p.automation, "", "BankDeposit");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.pageCard(p.nameFrind, "btnGoToParent");
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }
        [TestMethod]
        public void Test22CreatePaymentCreadetCardHok()
        {

            Test01Login("Test22CreatePaymentCreadetCardHok");
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום בהו\"ק", "כרטיס אשראי", "", p.automation, "", "divPaymentMethodTypesCredit", "רוזנברגר");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentCreadetCardHok");
        }
        [TestMethod]
        public void Test22CreatePaymentMsabHok()
        {

            Test01Login("Test22CreatePaymentFrindBankTransferOne");
            fp.clickNameDefinitions(p.student);
            fp.clickNameDefinitions(" תשלומים ");
            fp.ButtonNew("תשלום");
            createConatrabition cc = new createConatrabition();
            cc.stepPayment("תשלום בהו\"ק", "מס\"ב", "רוזנברגר", p.automation, "", "divPaymentMethodTypesMasav");
            sec.checkEndCreate("תשלום", false, "", p.parent);
            sec.closePage();
            fp.OpenExcel("Test22CreatePaymentFrindBankTransferOne");
        }
        [TestMethod]
        public void Test14CreatePaymentParentsCase()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTable("רוזנברגר");
            string ofen = "מזומן";
            pn.prentalDataPayment(ofen, "", true, 1);
            fp.ClickButton("שמירה");
            fp.closePoup();
            fp.clickNameDefinitions(" הורים ");
            string[] frind = { "", "רוזנברגר", "", "", "", "", "פעיל", p.yearHebru, "", "", "", "", "", "", "-1" };
            fp.checkColumn("//", frind, "", "mat-row", "mat-cell");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            fpc.offset("חיובים", 7);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }
       [TestMethod]
        public void Test14CreatePaymentParentsCaseT()
        {
            Test01Login("Test14CreatePaymentParentsCase");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.clickTable("רוזנברגר");
            string ofen = "מזומן";
            pn.prentalDataPayment(ofen, "", true, 1);
            fp.ClickButton("שמירה");
            fp.closePoup();
            fp.clickNameDefinitions(" הורים ");
            string[] frind = { "", "רוזנברגר", "", "", "", "", "פעיל", p.yearHebru, "", "", "", "", "", "", "-1" };
            fp.checkColumn("//", frind, "", "mat-row", "mat-cell");
            FinanceParentalCharges fpc = new FinanceParentalCharges();
            Thread.Sleep(100);
            fpc.offset("תשלומים", 3);
            fp.closeChrome();
            fp.OpenExcel("Test14CreatePaymentParentsCase");
        }
        [TestMethod]//לבדוק שרץ תשלום במזומן כללי
        public void Test14CheckOfsset()
        {
            Test01Login("Test14CheckOfsset");

        }
       // [TestMethod]
        public void Test14CreatePaymentParents()
        {
            Test01Login("Test14CreatePaymentParents");
            PupilNew pn = new PupilNew();
            pn.openPupil();
            pn.stepPaymentParent();
            fp.OpenExcel("Test14CreatePaymentParents");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
      /*  [TestMethod]
        public void TableCreate()
        {
            creatAddTable();
            CreateFildes();
            SettingFildesTable();
            createSectionsTable();
            createNewrowInNewTable();}*/
        ///הוספת טבלה
      //  [TestMethod, Order(6)]
        public void Test07CreatAddTable()
        {
            Test01Login("Test07CreatAddTable");
            fp.actionWork("הגדרות", "הוספת טבלה ");
            at.checkElment();
            at.insertElment();
            fp.closeChrome();
            fp.OpenExcel("Test07CreatAddTable", "Passed");
        }
        /// <summary>
        /// הגדרות טבלה הוספת שדות
        /// </summary>
        /// 
      //  [TestMethod, Order(7)]
        public void Test08CreateFildes()//רץ תקין
        {
            Test01Login("Test08CreateFildes");
            at.openTable();
            at.OpenSettings();
            at.checkESettingTable();
            at.OpenSettingsTable();
            at.clickCreateFilds();
            at.addFildesTable();
            at.save();
            at.close();
            fp.closeChrome();
            fp.OpenExcel("Test08CreateFildes", "Passed");
        }
      //  [TestMethod, Order(8)]
        public void Test09SettingFildesTable()
        {
            Test01Login("Test09SettingFildesTable");
            at.openTable();
            at.OpenSettings();
            at.OpenSettingsTable();
            at.addFildes();
            at.checkAddFildesActive();
            at.save();
            fp.closeChrome();
            fp.OpenExcel("Test09SettingFildesTable", "Passed");
        }
       // [TestMethod, Order(9)]
        public void Test10CreateSectionsTable()
        {
            Test01Login("Test10CreateSectionsTable");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.clickaddSections();
            fp.closeChrome();
            fp.OpenExcel("Test10CreateSectionsTable", "Passed");
        }
       // [TestMethod, Order(10)]
        public void Test11CreateSectionsTableApprove()
        {
            Test01Login("Test11CreateSectionsTableApprove");
            at.openTable();
            at.OpenSettings();
            at.openSettingTest();
            at.approveDisplay();
            Thread.Sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test11CreateSectionsTableApprove", "Passed");
        }
    //    [TestMethod, Order(11)]
        public void Test12CreateNewrowInNewTable()
        {
            Test01Login("Test12CreateNewrowInNewTable");
            at.openTable();
            fp.ButtonNew("ccc");
            at.checkPagecreateNewInTable(p.automation);
            sec.checkEndCreate("ccc", false);
            sec.closePage();
            fp.OpenExcel("Test12CreateNewrowInNewTable", "Passed");
        }
        /// <summary>
        /// הגדרות
        /// </summary>
        /// 
   //     [TestMethod, Order(5)]
        public void Test16MangerList()
        {
            Test01Login("Test16MangerList");
            fp.actionWork("הגדרות", "ניהול רשימות ");
            settingList nl = new settingList();
            nl.checkElment();
            Thread.Sleep(200);
            nl.clickAdd();
            string[] newValue = { "שעון", "מחשב", "מצלמה", "פלאפון" };
            nl.addList(p.automation, newValue);
            fp.closeChrome();
            // nl.checkTable();
            fp.OpenExcel("Test16MangerList", "Passed");
        }
        /// <summary>
        /// הוספת מוסד
        /// </summary>
        [TestMethod, Order(17)]
        public void Test18AddInstitutionClose()
        {
            Test01Login("Test18AddInstitutionClose");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("מוסד", false);
            sec.closePage();
            fp.OpenExcel("Test18AddInstitutionClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22AddInstitutionNewCard()
        {
            Test01Login("Test22AddInstitutionNewCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("מוסד", false);
            sec.pageNewCreate("מוסד נוסף");
            fp.OpenExcel("Test22AddInstitutionNewCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22AddInstitutionCard()
        {
            Test01Login("Test22AddInstitutionCard");
            Institution i = new Institution();
            i.openCreateMosad();
            i.checkElment();
            i.insertElment();
            sec.checkEndCreate("מוסד", false);
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22AddInstitutionCard", "Passed");
        }
        [TestMethod, Order(17)]
        public void Test22CreatePDFClose()
        {
            Test01Login("Test22CreatePDFClose");
            rs.stepPDF();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFClose", "Passed");
        }
      //  [TestMethod, Order(22)]
        public void Test22CreatePDFCard()
        {
            Test01Login("Test22CreatePDFCard");
            rs.stepPDF();
            sec.pageCard("מסמך PDF");
            fp.OpenExcel("Test22CreatePDFCard", "Passed");
        }
     //   [TestMethod, Order(22)]
        public void Test22CreatePDFStartDesign()
        {
            Test01Login("Test22CreatePDFStartDesign");
            rs.stepPDF();
            sec.startDesign();
            sec.closePage();
            fp.OpenExcel("Test22CreatePDFStartDesign", "Passed");
        }
      //  [TestMethod, Order(22)]
        public void Test22CreatePDFNew()
        {
            Test01Login("Test22CreatePDFNew");
            rs.stepPDF();
            sec.pageNewCreate("מסמך PDF נוסף");
            fp.OpenExcel("Test22CreatePDFNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreatePDFMosad()
        {
            Test01Login("Test22CreatePDFMosad");
            rs.stepPDF();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22CreatePDFMosad", "Passed");
        }
      //  [TestMethod, Order(18)]
        public void Test22ReceptionStructuresClose()
        {
            Test01Login("Test22ReceptionStructuresClose");
            rs.stepRec();
            sec.closePage();
            fp.OpenExcel("Test22ReceptionStructuresClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22ReceptionStructuresGoCard()
        {
            Test01Login("Test22ReceptionStructuresGoCard");
            rs.stepRec();
            sec.pageCard("מבנה קבלה");
            fp.OpenExcel("Test22ReceptionStructuresGoCard", "Passed");
        }
       // [TestMethod, Order(22)]
        public void Test22ReceptionStructuresNew()
        {
            Test01Login("Test22ReceptionStructuresNew");
            rs.stepRec();
            sec.pageNewCreate("מבנה קבלה נוסף");
            fp.OpenExcel("Test22ReceptionStructuresNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22ReceptionStructuresMosad()
        {
            Test01Login("Test22ReceptionStructuresMosad");
            rs.stepRec();
            sec.pageCard("מוסד");
            fp.OpenExcel("Test22ReceptionStructuresMosad");
        }


       // [TestMethod, Order(10)]
        public void Test19CreateMaddPaiClose()
        {
            Test01Login("Test19CreateMaddPaiClose");
            string[] insertShilta = { "מטבע", "אינו ריק", "", "אופן תרומה", "שווה ל", "אשראי", "-1" };// [{ },{ }];
            cm.stepPai("תרומות", "אופן תרומה", " סכום ", insertShilta, "סכום", "יוצר הרשומה", "תאריך יצירה");
            sec.closePage();
            CheckMadd("תרומות", "תרומות", "pie");
            fp.OpenExcel("Test19CreateMaddPaiClose");
        }
        public void CheckMadd(string nameTitle, string nameButton, string typeGraph)
        {
            Test01Login();

            cm.checkClassMadd(nameTitle, nameButton, typeGraph);
            cm.openTable(nameButton, typeGraph);
            fp.closeChrome();
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddPaiCard()
        {
            Test01Login("Test23CreateMaddPaiCard");
            string[] inShilta = { "כיתה וסניף", "אינו ריק", "", "מוסד לימודים", "אינו ריק", "", "-1" };
            cm.stepPai("תלמידים", "רמת כיתה", "כמות ", inShilta, "", " רמת כיתה ", " תאריך כניסה ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddPaiCard");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddPaiNew()
        {
            Test01Login("Test23CreateMaddPaiNew");
            string[] inShilta = { "ידידים: רחוב ", "ריק", "", "שם הילד", "אינו ריק", "", "-1" };
            cm.stepPai("ילדים", "מגדר", "כמות", inShilta, "", " שייכות ", " תאריך לידה לועזי ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddPaiNew", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test19CreateMaddGrafLineClose()
        {
            Test01Login("Test19CreateMaddGrafLineClose");
            string[] inShilta = { "-1" };
            cm.stepGrafLine(" אמצעי תשלום ", " סוג אמצעי תשלום ", "כמות", inShilta, " ישות סליקה ", "", "סטטוס", " ExpiryDate ");
            sec.closePage();
            fp.OpenExcel("Test19CreateMaddGrafLineClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrafLineCard()
        {
            Test01Login("Test23CreateMaddGrafLineCard");
            string[] inShilta = { " שנה\"ל ", "אינו ריק", "", " רמת כיתה ", " שווה ל ", p.automation, "-1" };
            cm.stepGrafLine(" כיתות ", " רמת כיתה ", "כמות", inShilta, " סניף מוסד חינוך ", "", " רמת כיתה ", "false", false);
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddGrafLineCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrafLineNew()
        {
            Test01Login("Test23CreateMaddGrafLineNew");
            Thread.Sleep(500);
            string[] inShilta = { " יוצר הרשומה ", " אינו ריק ", "", " הערות ", " שווה ל ", p.automation + "TEXT", "-1" };
            cm.stepGrafLine(" תרומות ", " מטבע ", "סכום", inShilta, " אופן תרומה ", " סכום ", " איש קשר ", " תאריך יצירה ");
            sec.pageNewCreate("מדד נוסף");
            CheckMadd("תרומות", "תרומות", "line");
            fp.OpenExcel("Test23CreateMaddGrafLineNew", "Passed");
        }

        [TestMethod, Order(10)]
        public void Test19CreateMaddGrapColumnClose()
        {
            Test01Login("Test19CreateMaddGrapColumnClose");
            string[] inShilta = { "-1" };
            cm.stepGrafColumn(" מסמכים ", " מעדכן הרשומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
            sec.closePage();
            fp.OpenExcel("Test19CreateMaddGrapColumnClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrapColumnCard()
        {
            Test01Login("Test23CreateMaddGrapColumnCard");
            string[] inShilta = { " תאריך יצירה ", " אינו ריק ", "", " מעדכן הרשומה ", " שונה מ ", " מערכת מנהל ", "-1" };
            cm.stepGrafColumn(" תרומות ", " אופן תרומה ", "כמות", inShilta, " תאריך יצירה ", "", " ללא ", " תאריך יצירה ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddGrapColumnCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddGrapColumnNew()
        {
            Test01Login("Test23CreateMaddGrapColumnNew");
            string[] inShilta = { " תאריך יצירה ", " אינו ריק ", "", " מעדכן הרשומה ", "שונה מ", " וייץ רבקה ", "-1" };
            cm.stepGrafColumn(" ידידים ", " עיר ", "כמות", inShilta, " תואר ", "", " ללא ", " תאריך יצירה ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddGrapColumnNew", "Passed");
        }
        [TestMethod, Order(10)]
        public void Test23CreateMaddTableClose()
        {
            Test01Login("Test23CreateMaddTableClose");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            sec.closePage();
            fp.OpenExcel("Test23CreateMaddTableClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddTableCard()
        {
            Test01Login("Test23CreateMaddTableCard");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            sec.pageCard("מדד");
            fp.OpenExcel("Test23CreateMaddTableCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test23CreateMaddTableNew()
        {
            Test01Login("Test23CreateMaddTableNew");
            string[] inShilta = { " רשומה ", " שונה מ ", "חייחTEXT", " טבלה ", " שווה ל ", "תלמידים", "-1" };
            cm.stepGrafTable(inShilta, " טבלה ", " תאריך ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("Test23CreateMaddTableNew", "Passed");
        }
       
        [TestMethod, Order(22)]
        public void Test23CreateMaddCardCard()
        {
            Test01Login("Test23CreateMaddCardCard");
            string[] inShilta = { " הערות ", " שווה ל ", p.automation + "TEXT", " שם פרטי ", " מתחיל ב ", "אTEXT", "-1" };
            cm.stepCard(" תלמידים ", "כמות", inShilta, "apartment", "", " ללא ", " תאריך ");
            sec.pageCard("מדד");
            chckDasMini("תלמידים", "תלמידים");
            fp.OpenExcel("Test23CreateMaddCardCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void TestCreateMaddCardNew()
        {
            Test01Login("TestCreateMaddCardNew");
            string[] inShilta = { " אופן תרומה ", " אינו ריק ", "", " הערות ", " שווה ל ", p.automation + "TEXT", "-1" };
            cm.stepCard(" תרומות ", "סכום", inShilta, "attach_money", " סכום ", " ללא ", " תאריך ");
            sec.pageNewCreate("מדד נוסף");
            fp.OpenExcel("TestCreateMaddCardNew", "Passed");
        }
        //
        public void chckDasMini(string nameMIMI, string nameButton)
        {
            Test01Login();
            cm.chekmini(nameMIMI, nameButton);
            fp.closeChrome();
        }
        /// <summary>
        /// ניהול מערכת
        /// </summary>
        [TestMethod, Order(1)]
        public void Test03CreateNameUser()
        {
            Test01Login("Test03CreateNameUser");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.openCreate();
            nu.checkCreate();
            nu.insertCreate();
            Thread.Sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test03CreateNameUser", "Passed");
        }
        [TestMethod, Order(2)]//התסריט לא ירוץ כי הסיסמה לא חזקה
        public void Test05CheckNewUser()
        {
            ExcelApiTest eat = new ExcelApiTest("", "tasret");
            eat.bugReturn("tasret", "Failed", "Test05CheckNewUser");
            login ln = new login();
            ln.openChrome();
            ln.UserNamee(p.automation + "משתמש");
            ln.Password(p.automation + "משתמש1234");
            ln.cLickLogIn();
            fp.closeChrome();
            fp.OpenExcel("Test05CheckNewUser", "Passed");
        }
        [TestMethod, Order(20)]//רץ מאוד לאט נא ליבדוק//run good
        public void Test22ChangeChaseBox()
        {
            Test01Login("Test22ChangeChaseBox");
            fp.actionWork("ניהול מערכת", "משתמשים ");//fp.clickNameDefinitions();
            newUser nu = new newUser();
            nu.clickEditUser();
            nu.addMosad(p.automation);
            fp.refres();
            Institution i = new Institution();
            i.changeChaseBox(p.automation);
            i.checkChange(" ידידים ", " תרומות ");
            i.checkChange("", " תרומות בהו\"ק ");
            i.checkChange(p.student, " הוראות קבע ");
            i.checkChange("", " קמפיינים ");
            i.checkChange("", " הוצאות ");
            i.checkChange("", " תלמידים ");
            i.checkChange(p.student, " מוסדות חינוך ");
            i.checkChange(" ניהול כספים ", "הפקדת מזומן ", "", true);
            // i.checkChange(" ניהול כספים ", "הפקדת צ\'קים ", "", true);
            //  i.checkChange(" ניהול כספים ", "טיפול בצ\'קים ", "", true);
            i.checkChange(" ניהול כספים ", "ביצוע הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "דיווח חזרות מסב", "", true);
            i.checkChange(" ניהול כספים ", "שידורי אשראי ", "", true);
            i.checkChange(" ניהול כספים ", "צפייה וטיפול בתשלומים", "", true);
            i.checkChange(" ניהול כספים ", "ניהול סעיפים פיננסיים");
            i.checkChange(" ניהול כספים ", "טיפול בקבלות", " הצג נתונים ", true);
            i.checkChange(" ניהול כספים ", "טיפול בקבלות עבור הו\"ק", "", true);
            i.checkChange(" ניהול כספים ", "צפיה בקבלות");
            i.checkChange(" הגדרות ", "ישויות סליקה");
            i.checkChange(" הגדרות ", "חשבונות בנק");
            i.checkChange(" הגדרות ", "מבני קבלות");
            i.checkChange(" הגדרות ", "מסמכי PDF");
            i.cheackNotChange(" הגדרות ", "מוסדות");
            i.cheackNotChange(" הגדרות ", "מדדים");
            i.cheackNotChange(" הגדרות ", "ניהול רשימות ", "tr");
            i.cheackNotChange("", " ידידים ");
            i.cheackNotChange(" ניהול כספים ", "אמצעי תשלום");
            fp.OpenExcel("Test22ChangeChaseBox", "Passed");
        }
        [TestMethod, Order(3)]
        public void Test04CreateRolse()
        {
            Test01Login("Test04CreateRolse");
            fp.actionWork("ניהול מערכת", "תפקידים ");//fp.clickNameDefinitions();
            Thread.Sleep(300);
            fp.ClickButton("הוסף תפקיד ");
            newUser nu = new newUser();
            nu.addRolse(true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse(false, true);
            fp.ClickButton("הוסף תפקיד ");
            nu.addRolse();
            fp.closeChrome();
            fp.OpenExcel("Test04CreateRolse", "Passed");
        }
        [TestMethod, Order(4)]
        public void Test05CheackAddRolse()
        {
            Test01Login("Test05CheackAddRolse");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickListinListEqualName(p.nameFrind + "ים");
            at.OpenSettings();
            at.openSettingTest(p.nameFrind + "ים");
            at.ceackAddSection(p.automation);
            fp.closeChrome();
            fp.OpenExcel("Test05CheackAddRolse", "Passed");
        }
        [TestMethod, Order(9)]
        public void Test10MosadDatotClose()
        {
            Test01Login("Test10MosadDatotClose");
            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.closePage();
            fp.OpenExcel("Test10MosadDatotClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22MosadDatotCard()
        {
            Test01Login("Test22MosadDatotCard");
            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.pageCard("מוסד לפי דתות");
            fp.OpenExcel("Test22MosadDatotCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22MosadDatotNew()
        {
            Test01Login("Test22MosadDatotNew");
            religiousInstitutions rn = new religiousInstitutions();
            rn.openMosad();
            rn.stepNewMosad("ישיבה גור ערד", "215");
            sec.checkEndCreate("מוסד לפי דתות", false);
            sec.pageNewCreate("מוסד לפי דתות נוסף");
            fp.OpenExcel("Test22MosadDatotNew", "Passed");
        }
        //יצירת תרומה בהו"ק
        [TestMethod, Order(6)]
        public void Test07CreateConatarbitionHokClose()
        {
            Test01Login("Test07CreateConatarbitionHokClose");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK();
            sec.closePage();
            fp.OpenExcel("Test07CreateConatarbitionHokClose", "Passed");
        }
        [TestMethod, Order(22)]//נפל בגללי
        public void Test07CreateConatarbitionHokAsraiCard()
        {
            Test01Login("Test07CreateConatarbitionHokAsraiCard");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK();
            sec.pageCard("תרומה בהו\"ק");
            fp.OpenExcel("Test07CreateConatarbitionHokAsraiCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokAsraiFrind()
        {
            Test01Login("Test22CreateConatarbitionHokAsraiFrind");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateConatarbitionHokAsraiFrind", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokAsraiNew()//לטפל ב+
        {
            Test01Login("Test22CreateConatarbitionHokAsraiNew");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK();
            sec.pageNewCreate("תרומה בהו\"ק נוספת");
            fp.OpenExcel("Test22CreateConatarbitionHokAsraiNew", "Passed");
        }
        //תרומות הו"ק מס"ב
        [TestMethod, Order(6)]///יש צורך לטפל ב+ דחוף
        public void Test07CreateConatarbitionHokMsadClose()
        {
            Test01Login("Test07CreateConatarbitionHokMsadClose");
            Thread.Sleep(400);
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.closePage();
            fp.OpenExcel("Test07CreateConatarbitionHokMsadClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokMsadCard()//לשנות את הסעיף לתרומה
        {
            Test01Login("Test22CreateConatarbitionHokMsadCard");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.pageCard("תרומה בהו\"ק");
            fp.OpenExcel("Test22CreateConatarbitionHokMsadCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokMsadFrind()
        {
            Test01Login("Test22CreateConatarbitionHokMsadFrind");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateConatarbitionHokMsadFrind");
        }
        [TestMethod, Order(22)]
        public void Test22CreateConatarbitionHokMsadNew()
        {
            Test01Login("Test22CreateConatarbitionHokMsadNew");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" תרומות בהו\"ק ");
            createConatrabition cc = new createConatrabition();
            cc.stepConatarbitionHOK("מס\"ב", "divPaymentMethodTypesMasav");
            sec.pageNewCreate("תרומה בהו\"ק נוספת");
            fp.OpenExcel("Test22CreateConatarbitionHokMsadNew", "Passed");
        }
        //התחיבויות
        [TestMethod, Order(9)]
        public void Test10CreateNewLaiblitesClose()
        {
            Test01Login("Test10CreateNewLaiblitesClose");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites();
            sec.closePage();
            fp.OpenExcel("Test10CreateNewLaiblitesClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesCard()
        {
            Test01Login("Test22CreateNewLaiblitesCard");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites();
            sec.pageCard("התחייבות");
            fp.OpenExcel("Test22CreateNewLaiblitesCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateNewLaiblitesNew()
        {
            Test01Login("Test22CreateNewLaiblitesNew");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.stepNewLaiblites();
            sec.pageNewCreate("התחייבות נוספת");
            fp.OpenExcel("Test22CreateNewLaiblitesNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test10CreateNewLaiblitesFrind()
        {
            Test01Login("Test22CreateNewLaiblitesFrind");
            fp.ClickList(p.nameFrind + "ים");
            Laiblites l = new Laiblites();
            fp.clickNameDefinitions(" תרומות ");
            string name=l.shearchNameRequerd();
            fp.clickNameDefinitions(" התחייבויות ");
            l.stepNewLaiblites("",name);
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateNewLaiblitesFrind", "Passed");
        }
     //   [TestMethod, Order(10)]//רץ תקיןנופל על פתיחה חוזרת
        public void Test11OffsettingLaiblites()
        {
            Test01Login("Test11OffsettingLaiblites");
            fp.ClickList(p.nameFrind + "ים");
            fp.clickNameDefinitions(" התחייבויות ");
            Laiblites l = new Laiblites();
            l.clickTableUser();
            l.offsetting();
            fp.closeChrome();
            fp.OpenExcel("Test11OffsettingLaiblites", "Passed");
        }
        [TestMethod]///קיזוז חיובים
        public void Test19OffsettingLiabilitiesFrind()
        {
            Test01Login("Test11CreatCehildrenClose");
            Frind f = new Frind();
            f.stepLiabilities();
            fp.closeChrome();
            fp.OpenExcel("Test11CreatCehildrenClose");
        }
        [TestMethod, Order(10)]
        public void Test11CreatCehildrenClose()
        {
            Test01Login("Test11CreatCehildrenClose");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.closePage();
            fp.OpenExcel("Test11CreatCehildrenClose");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenCard()
        {
            Test01Login("Test22CreatCehildrenCard");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageCard("ילד");
            fp.OpenExcel("Test22CreatCehildrenCard");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenNew()
        {
            Test01Login("Test22CreatCehildrenNew");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageNewCreate("ילד נוסף");
            fp.OpenExcel("Test22CreatCehildrenNew");
        }
        [TestMethod, Order(22)]
        public void Test22CreatCehildrenFrind()
        {
            Test01Login("Test22CreatCehildrenFrind");
            fp.ClickList(p.student.Trim());
            fp.clickNameDefinitions(" ילדים ");
            children c = new children();
            c.stepNewChildren();
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreatCehildrenFrind", "Passed");
        }
        [TestMethod, Order(18)]
        public void Test19CreateTackingFrindClose()
        {
            Frind f = new Frind();
            Test01Login("Test19CreateTackingFrindClose");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.closePage();
            fp.OpenExcel("Test19CreateTackingFrindClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindCard()
        {
            Frind f = new Frind();
            Test01Login("Test22CreateTackingFrindCard");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageCard("מעקב ידיד");
            fp.OpenExcel("Test22CreateTackingFrindCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindNew()
        {
            Frind f = new Frind();
            Test01Login("Test22CreateTackingFrindNew");
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageNewCreate("מעקב ידיד נוסף");
            fp.OpenExcel("Test22CreateTackingFrindNew", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTackingFrindGFrind()
        {
            Test01Login("Test22CreateTackingFrindGFrind");
            Frind f = new Frind();
            f.stepTackingFrind("מתנת ראש השנה", "חלוקת רימון ודבש");
            sec.pageCard(p.nameFrind);
            fp.OpenExcel("Test22CreateTackingFrindGFrind", "Passed");
        }
        [TestMethod, Order(20)]
        public void Test20CreateTaskClose()
        {
            Test01Login("Test20CreateTaskClose");
            Thread.Sleep(200);
            fp.clickNameDefinitions(" משימות ");
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " כיתות ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("כיתה", "א", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("יש צורך לשנות את שם הכיתה");
            sec.closePage();
            fp.OpenExcel("Test20CreateTaskClose", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskCard()//לטפל
        {
            Test01Login("Test22CreateTaskCard");
            Thread.Sleep(200);
            fp.clickNameDefinitions(" משימות ");
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " תלמידים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("תלמיד", "א", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("לתלמיד מגיע מתנה על היתנהגותו הנהותה");
            sec.pageCard("משימה");
            fp.OpenExcel("Test22CreateTaskCard", "Passed");
        }
        [TestMethod, Order(22)]
        public void Test22CreateTaskNew()//רץ תקין
        {
            Test01Login("Test22CreateTaskNew");
            Thread.Sleep(600);
            fp.clickNameDefinitions(" משימות ");
            fp.ButtonNew("משימה", "ה");
            fp.SelectRequerdName("aria-label='slctTableToTask", " ידידים ", false, true);
            createConatrabition cc = new createConatrabition();
            cc.clickFrind("ידיד", "100001", "1");
            cc.checkStepHeder(2, 2);
            CreateNewDenumic cnd = new CreateNewDenumic();
            cnd.insertTask("הידיד עבר דירה יש להתעדכן ולעדכן את הכתובת החדשה");
            sec.closePage();
            checkTeask("ידידים", "ידידים", "100001");
            fp.OpenExcel("Test22CreateTaskNew", "Passed");
        }
        public void checkTeask(string nitov, string Tatnitov, string name)
        {
            Test01Login();
            fp.ClickList(nitov);
            if (nitov != Tatnitov)
                fp.clickNameDefinitions(Tatnitov);
            else
                fp.clickListinListEqualName(Tatnitov);
            string[] listc = { "", "", "", "-1" };
            if (name.Contains("1"))
                listc[1] = name;
            else
                listc[2] = name;
            fp.checkColumn("//", listc, "", "mat-row", "mat-cell");
            popupList pl = new popupList();
            pl.expansionPopUP();
            pl.clickStepHeder(16, "משימות");
            string[] checktaskl = { "", "", "לביצוע", "", " pageview ", " post_add ", " delete ", "-1" };
            fp.checkColumn("//", checktaskl, "false", "mat-row", "mat-cell", -1, 1);
            fp.closeChrome();
        }
        [TestMethod]
        public void Test18CreateProcesseSendMailAfterIdceonClose()
        {
            Test01Login("Test18CreateProcesseSendMailAfterIdceonClose");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה", "ידידים", "אחרי עדכון");
            string[] filter = { "דוא\"ל", "שווה ל", p.MAIL + "TEXT", "מייל לקבלה", "שווה ל", p.MAIL + "TEXT", "-1" };
            ps.stepFrainLast(filter, 2, false, 1);
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
        }
        [TestMethod]
        public void Test19CheckSendMailProcess()
        {
            Test01Login("Test19CheckSendMailProcess");
            f.openFrind();
            PupilNew pn = new PupilNew();
            pn.clickTableRandom();
            f.changeW();
            fp.closeChrome();
            Console.WriteLine("יש לבצע בדיקה שהכן נשלח מייל");
            fp.OpenExcel("Test18CreateProcesseSendMailAfterIdceonClose", "Passed");
            // login l = new login();
            // l.openMail();
        }
        [TestMethod]
        public void Test18CreateProcesseUpdateValueBeforIdceonCard()
        {
            Test01Login("Test18CreateProcesseUpdateValueBeforIdceonCard");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה", "נסיון ריצה ע\"י אוטומציה לפני עדכון", "מוסדות", "לפני עדכון");
            string[] filter = { "קוד מוסד", "אינו ריק", "", "עיר", "שווה ל", "ערד", "-1" };
            ps.stepFrainLast(filter);
            ps.step3(" עדכון ערכים ", "עיר", "רגיל", "ערד");
            sec.pageCard("תהליך");
            fp.OpenExcel("Test18CreateProcesseUpdateValueBeforIdceonCard", "Passed");
        }
        [TestMethod]
        public void Test18CreateProcesseUpdateDateEbruBeforAddNew()
        {
            Test01Login("Test18CreateProcesseUpdateDateEbruBeforAddNew");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה הוספה", "נסיון ריצה ע\"י אוטומציה לפני הוספה", " תלמידים ", " לפני הוספה ");
            string[] filter = { " ת. לידה עברי ", " ריק ", "", " ת. לידה לועזי ", " אינו ריק ", "" };
            ps.stepFrainLast(filter, 1);
            ps.step3("עדכון תאריך עברי", "2", "ידידים ת. לידה לועזי ", "ידידים ת. לידה עברי ", true, 2);
            sec.pageNewCreate("תהליך נוסף");
            fp.OpenExcel("Test18CreateProcesseUpdateDateEbruBeforAddNew", "Passed");
        }
        [TestMethod]
        public void Test18CreateProcesseDochTkofatAutomationOneTime()
        {
            Test01Login("Test18CreateProcesseDochTkofatAutomation");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה אוטומטי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " ידידים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("חד פעמי", DateTime.Today);
            fp.ClickButton(" לשלב הבא ", "span");
            ps.step3("שליחת דוח תקופתי", "תקופתי", "1/08/-1", "01/0/0", true, 1);
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");

        }
        [TestMethod]
        public void Test18CreateProcesseDochTkofatAutomationCurrent()
        {
            Test01Login("Test18CreateProcesseDochTkofatAutomation");
            process ps = new process();
            ps.openProcess();
            ps.cehekstep1();
            ps.insertStep1("ריצה אוטומטי", "נסיון ריצה ע\"י אוטומציה אוטומציה", " ידידים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 3, 0235);
            ps.timeHour("מס' פעמים", "8:18", 10, "שעות", "20:18");
            // ps.numberCheckStatusAction("3 ימים, 0235, 10, "8:18", "20:18");
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" שליחת מייל ", " דוא\"ל ", "שליחת מייל ניסוי", "שליחת מייל אוטומציה");
            sec.closePage();
            fp.OpenExcel("Test18CreateProcesseDochTkofatAutomation", "Passed");
        }
        [TestMethod]
        public void Test18ProcessGenratorAndHQ()
        {
            string dateTimeH = "";
            Test01Login("Test18ProcessGenratorAndHQ");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("מחולל ביצוע הוק", " אוטומציה", " הורים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            ps.Automation("שוטף", DateTime.Today, "שבועי", 2, 1);
            dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            ps.timeHour("חד פעמי", dateTimeH);
            fp.ClickButton(" לשלב הבא ");
            ps.step3(" מחולל חיובים וביצוע הו\"ק ", "בית אברהם", "כללי", "אשראי");
            fp.closePoup();
            fp.clickNameDefinitions(" הוצאות והכנסות ");
            fp.clickNameDefinitions(" תקבולים ");
            PupilNew pn = new PupilNew();
            pn.checkTable(DateTime.Today.ToString("dd/MM/yyyy-") + dateTimeH);
            fp.closeChrome();
            fp.OpenExcel("Test18ProcessGenratorAndHQ", "Passed");
        }
        [TestMethod]
        public void Test18ProcessreturenFail()//נבדק רק מקקומית ולא בטבלאות הקשורות
        {
            string dateTimeH = "12:13";
            Test01Login("Test18ProcessreturenFail");
            process ps = new process();
            ps.openProcess();
            ps.insertStep1("החזרת נכשלים", " אוטומציה", " הורים ", " אוטומטי ");
            string[] filter = { " דוא\"ל ", " שווה ל ", p.MAIL + "TEXT" };
            ps.stepFrainLast(filter, 1);
            dateTimeH = DateTime.Now.AddMinutes(2).ToString("HH:mm");
            ps.Automation("חד פעמי", DateTime.Today, dateTimeH);
            fp.ClickButton(" לשלב הבא ", "span");
            ps.step3(" שידור חוזר לנכשלים ", "אשראי הו\"ק", "סירוב", "5");
            fp.closePoup();
            ps.checkSucess(dateTimeH);
            fp.closeChrome();
        }


        [TestMethod, Order(5)]
        public void Test19CreateEducationalInstitutionCOLL()
        {
            Test01Login("Test19CreateEducationalInstitutionCOLL");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.createEI(" כולל ", "כולל", "באר שבע", 3);
            Thread.Sleep(400);
            fp.closeChrome();
            fp.OpenExcel("Test19CreateEducationalInstitutionCOLL", "Passed");
        }
        [TestMethod]
        public void Test20GradeLavelCOLL()
        {
            Test01Login("Test20GradeLavelCOLL");
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.clickOnTable("כולל", "כולל");
            popupList pl = new popupList();
            pl.clickLavel();
            createGradeLavel cgl = new createGradeLavel();
            cgl.GradeLavelCreate("סדר א", "כולל", "כולל", "22");
            Thread.Sleep(200);
            fp.closeChrome();
            fp.OpenExcel("Test20GradeLavelCOLL", "Passed");
        }
        [TestMethod, Order(7)]
        public void Test21CreateGradeCOLL()
        {

            Test01Login("Test21CreateGradeCOLL");
            Grade g = new Grade();
            g.stepCreateGard("כולל", "כולל", "סדר א");
            fp.closeChrome();
            fp.OpenExcel("Test21CreateGradeCOLL", "Passed");
        }
        
        [TestMethod]
        public void Test23CreatePupilCOLL()
        {
            Test01Login("Test23CreatePupilCOLL");
            PupilNew pn = new PupilNew();
            pn.createPupilColl();
            fp.OpenExcel("Test23CreatePupilCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalCurentOneCloseCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentOneCloseCOLL");

            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " חודש בודד ", "00/0000" + "-", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalCurentOneCloseCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalCurentRangeCardCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentRangeCardCOLL");
            //fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " טווח חודשים ", "0/0-0/1", "כולל", " כולל (זיכוי) ");
            sec.pageCard("הגדרת סעיף זיכוי");
            fp.OpenExcel("Test24CreatePromotionalCurentRangeCardCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalCurentPaymentAddCOLL()
        {
            Test01Login("Test24CreatePromotionalCurentPaymentAddCOLL");
            //  fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional("שוטף", " תשלומים ", "0/0-5/0", "כולל", " כולל (זיכוי) ");
            sec.pageNewCreate("הגדרת סעיף זיכוי נוסף");
            fp.OpenExcel("Test24CreatePromotionalCurentPaymentAddCOLL");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimeMosadOne()
        {
            Test01Login("Test24CreatePromotionalOneTimeMosadOne");
            // fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים",p.automation,"זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " חודש בודד ", "0/0-", p.automation, " כולל (זיכוי) ");
            sec.pageCard("מוסד חינוך");
            fp.OpenExcel("Test24CreatePromotionalOneTimeMosadOne");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimeRange()
        {
            Test01Login("Test24CreatePromotionalOneTimeRange");
            //fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " טווח חודשים ", "0/0-0/1", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalOneTimeRange");
        }
        [TestMethod]
        public void Test24CreatePromotionalOneTimetPayment()
        {
            Test01Login("Test24CreatePromotionalOneTimetPayment");
            //  fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים","כולל","זיכוי");
            Promotional pr = new Promotional();
            pr.stepPromotional(" חד פעמי ", " תשלומים ", "0/0-5/0", "כולל", " כולל (זיכוי) ");
            sec.closePage();
            fp.OpenExcel("Test24CreatePromotionalOneTimetPayment");
        }
        [TestMethod]
        public void Test25CreditGenerator()
        {
            Test01Login("Test25CreditGenerator");
            fp.actionWork("ניהול כספים", "מחולל זיכויים");
            Promotional pr = new Promotional();
            pr.stepCreditGenerator();
            fp.closeChrome();
            fp.OpenExcel("Test25CreditGenerator");
        }
        [TestMethod]
        public void Test26PrintCheck()
        {
            Test01Login("Test26PrintCheck");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCraditPayments();
            // pr.clickCancel();
            fp.closeChrome();
            fp.OpenExcel("Test26PrintCheck");
        }

        [TestMethod]
        public void Test27cancelCheck()
        {
            Test01Login("Test27cancelCheck");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            int numCheck = 20000;
            pr.cenelloctionCheck(numCheck);
            fp.picter("print cancel check");
            fp.closeChrome();
            fp.OpenExcel("Test27cancelCheck");
        }
        [TestMethod]
        public void Test27CheckPromotional()
        {
            Test01Login("Test27CheckPromotional");
            fl.createNewFinancicalSection(" מוסדות חינוך ", "תלמידים", "קיזוז", "קיזוז");
            fp.closepopup();
            fp.refres();
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.clickOnTableP();
            fp.closeChrome();
            fp.OpenExcel("Test27CheckPromotional");
        }
        [TestMethod]
        public void Test10createContacts()
        {
            Test01Login("Test10createContacts");
            f.createContacts("חנוך");
            sec.closePage();
            fp.OpenExcel("Test10createContacts");

        }
        [TestMethod]
        public void Test25createContactsCard()
        {
            Test01Login("Test10createContacts");
            f.createContacts("משה");
            sec.pageCard("איש קשר");
            fp.OpenExcel("Test25createContactsCard");
        }
        [TestMethod]
        public void Test25createContactsNew()
        {
            Test01Login("Test10createContacts");
            f.createContacts("אברהם");
            sec.pageNewCreate("איש קשר נוסף");
            fp.OpenExcel("Test25createContactsNew");
        }
        [TestMethod]
        public void Test28CreateCreaditStudentBonos()
        {
            Test01Login("Test28CreateCreaditStudent");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCreaditStudent();
            fp.closeChrome();
            fp.OpenExcel("Test28CreateCreaditStudent");
        }
        [TestMethod]
        public void Test28CreateCreaditStudent()
        {
            Test01Login("Test28CreateCreaditStudent");
            fp.actionWork("ניהול כספים", "זיכויים ותשלומים");
            Promotional pr = new Promotional();
            pr.stepCreaditStudent("זיכוי");
            fp.closeChrome();
            fp.OpenExcel("Test28CreateCreaditStudent");
        }
        /// <summary>
        /// שמירה פרטית
        /// </summary>
      //  [TestMethod, Order(7)]//לא תקין לבדוק בהרצה הבאה
        public void Test29savePraivet()
        {
            Test01Login("Test29savePraivet");
            at.stepSavePrivate();
            fp.OpenExcel("Test29savePraivet");
        }
        //[TestMethod]
        public void Test29checkCreditTableExpanses()
        {
            Test01Login("Test29checkCreditTableExpanses");
            at.openTable(p.student, "הוצאות");
            Promotional pr = new Promotional();
            pr.checkTable();
            fp.closeChrome();
            fp.OpenExcel("Test29checkCreditTableExpanses");
        }
        /// <summary>
        /// פרופיל אישי
        /// </summary>
        [TestMethod]
        public void Test27cheangePrivateProfil()
        {
            Test01Login("Test27cheangePrivateProfil");
            PrivateProfil PP = new PrivateProfil();
            PP.iconUser();
            string name = p.names[fp.rand(p.names.Length)]+" "+ p.familys[fp.rand(p.familys.Length)];
            PP.changeName(name, "0556771165", "rvaitz22@gmail.com");
            string color = PP.changeColor("yellow_blue");
            string typeFont = PP.changeWrote(" מודגש ");
            PP.changeDisplayTable(true, true);
            fp.refres();
            char[] fon = { '-' };
            PP.checkChange(name, color, typeFont.Split(fon)[1]);
            PP.changeInTable(true, true, color);
            fp.closeChrome();
            fp.OpenExcel("Test27cheangePrivateProfil");
        }
        /// <summary>
        /// החזרת אשראי(לחיצה על הכפתור הקטן של העט)
        /// </summary>
        [TestMethod]
        public void Test20ReturnAsrai()
        {
            Test08CreaditTransmissionCT();
            Test01Login("Test20ReturnAsrai");
            ViewingHandalingPayment vhp = new ViewingHandalingPayment();
            vhp.openViewingHandalingPayment();
            vhp.returnAsrai();
            fp.OpenExcel("Test20ReturnAsrai");
        }

        /// <summary>
        ///בדיקת פתיחת רשומה ושינויים נתונים
        /// </summary>
        [TestMethod]
        public void Test28checkOpenListAndChange()
        {
            Test01Login("Test28checkOpenListAndChange");

            popupList pl = new popupList();
            pl.checkOpen();
            fp.closeChrome();
            fp.OpenExcel("Test28checkOpenListAndChange");
        }

    }
}

