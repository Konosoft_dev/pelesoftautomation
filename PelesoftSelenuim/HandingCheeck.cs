﻿using OpenQA.Selenium;
using System;
using System.Diagnostics;

namespace PelesoftSelenuim
{
     class HandingCheeck:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void checkPagehandingCheeck()
        {
           // try
           // {
                string listHeder = "פרטי צ'ק תאריך הפקדה תאריך פרעון סכום הצ'ק מטבע סטטוס עריכה ";
                //12345678-04-007-123456 01/10/2020 30/09/2020 10 שקל בוצע edit
                
                string[] listEditCheck = { "12345678","4","007","123456"/*numberCheck + "-" + nameBank + "-" + numBranch + "-" + numberAmountBank*/,DateTime.Today.ToString("dd/MM/yyyy"),"","10", "שקל", "בוצע", "edit" };
                fp.checkElmentText("title page handding check", By.ClassName("main-txt"), "טיפול בצ'קים");
                fp.checkElmentsText("calnder date from date",By.TagName("mat-label"), "מ תאריך",0);
                fp.checkElmentsText("calnder date from date",By.TagName("mat-label"), "עד תאריך", 1);
                //fp.checkElment("calnder date to date",By.Id("mat-input-3"));
                fp.checkElment("button filter", By.XPath("//span[@class='mat-button-wrapper'][text()='סנן צקים ']"));
                fp.checkTableofColum("tbody[role='rowgroup']", listHeder, listEditCheck, "//button[@aria-label='edit']");
                EditCheck();
         //   }
          //  catch
          // {
            //    throw new NotImplementedException("cheack  handing cheeck invaled");

           // }
        }

        private void EditCheck()
        {
            /*try
            {*/
                fp.checkElment("page edit check", By.ClassName("ng-star-inserted"));
                //fp.checkElment("close", By.XPath("//button/span/i[@class='material-icons close-btn'][text()='close']"));
                fp.checkElment("title", By.XPath("//h2[text()='עדכון צ\'ק']"));
                fp.checkElment("checkBox returen check", By.XPath("//mat-label[text()='צ\'ק חזר']"));
                driver.FindElement(By.ClassName("mat-checkbox-layout")).Click();
               // driver.FindElement(By.ClassName("mat-button-focus-overlay")).Click();

                fp.wait();
                fp.checkElmentText("reason check", By.XPath("//mat-select[@formcontrolname='RefusedID']"), "סיבת חזרת הצ'ק");
                fp.checkElmentText("remaks", By.Name("Remarks"), "הערות",false,true);
                 driver.FindElement(By.XPath("//span[@class='mat-button-wrapper'][text()='שמירה']")).Click();
            try
            {
                if (driver.FindElement(By.XPath("//div[text()=' עידכון צ\'ק ']")).Displayed)
                    Console.WriteLine("he check edit");
            }
            catch
            {
               Debug.WriteLine("page edit check invaled");
            }
            // driver.FindElement(By.XPath("//span[class='mat-button-wrapper'][text()='שמור']")).Click();
            //  fp.clickButoonToContains("שמור");
            /*}
            catch { throw new NotImplementedException("page edit check invaled"); }*/
        }
    }
}