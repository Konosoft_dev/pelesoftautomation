﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace PelesoftSelenuim
{
    class SoftVerifier
    {
        private StringBuilder verificationErrors;

        public SoftVerifier()
        {
            verificationErrors = new StringBuilder();
        }

        public void VerifyElementIsPresent(IWebElement element,string valueError)
        {
            try
            {
                Assert.IsTrue(element.Displayed);
               
            }
            catch (AssertionException)
            {
                verificationErrors.Append(valueError);
            }
        }
    
    }
}
