﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using System.Threading;

namespace PelesoftSelenuim
{
    class dobut1 : page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        CreateNewDenumic cnd = new CreateNewDenumic();
        createConatrabition cc = new createConatrabition();

        public void openDoubt()
        {
            fp.ClickList("ספקים");
            fp.clickListinListEqualName("ספקים");
        }
        public void createnewDoubt(string lastName, string firstName)
        {
            try
            {
                openDoubt();
                fp.ButtonNew("");
                checkElmentDoubt();
                insertElmentDoubt(lastName, firstName);
                SucessEndCreate sec = new SucessEndCreate();
                sec.checkEndCreate("ספק", false);
            }
            catch { fp.picter("create new doubt"); throw new NotImplementedException(); }
        }

        public void insertElmentDoubt(string lastName, string firstName)
        {
            try
            {
                fp.insertElment("name", By.Name("LastName"), lastName);
                fp.insertElment("FirstName", By.Name("FirstName"), firstName);
                fp.insertElment("PhoneNumber", By.Name("PhoneNumber"), fp.rand(999999999).ToString());
                fp.insertElment("CellPhone", By.Name("CellPhone"), fp.rand(199) + "-" + fp.rand(9999999));
                fp.insertElment("WorkPhone", By.Name("WorkPhone"), fp.rand(199999999).ToString());
                fp.insertElment("email", By.Name("email"), "tr.konosoft@gmail.com");
                fp.ClickButton(" צור ספק חדש ", "span");
            }
            catch { throw new NotImplementedException(); }
        }

        private void checkElmentDoubt()
        {
            cnd.checkClasscreate("ספק");
            fp.checkElmentsText("title section", By.ClassName("title-section"), "פרטי ספק", 0);
            fp.checkElmentsText("title section", By.ClassName("title-section"), "אמצעי תקשורת", 1);
            fp.checkElmentText("name", By.Name("LastName"), " שם משפחה ");
            fp.checkElmentText("FirstName", By.Name("FirstName"), " שם פרטי ");
            fp.checkElmentText("PhoneNumber", By.Name("PhoneNumber"), " מס' טלפון ");
            fp.checkElmentText("CellPhone", By.Name("CellPhone"), " נייד ");
            fp.checkElmentText("WorkPhone", By.Name("WorkPhone"), " טלפון בעבודה ");
            fp.checkElmentText("email", By.Name("email"), " דוא\"ל ");
        }

        public void clickTab(int mikom, string nameTab)
        {
            try
            {
                fp.click(By.CssSelector("div[role='tab']"), mikom);
                fp.checkElmentsText("clicktab title", By.CssSelector("div[role='tab']"), nameTab, mikom);
            }
            catch { fp.picter("click tab"); throw new NotImplementedException(); }
        }

        public void ServiceReceipts(bool chsbonit = true)
        {
            try
            {
                openDoubt();
                IList<IWebElement> row = driver.FindElements(By.TagName("mat-row"));
                string cell = row[fp.rand(row.Count)].FindElements(By.TagName("mat-cell"))[2].Text;
                clickTab(2, "הקבלות שירות שלי");
                Thread.Sleep(400);
                fp.ButtonNew("");
                cnd.checkClasscreate("קבלת שירות", false);
                cc.clickFrindNew(cell);
                checkElmentServiceReceipts();
                fp.selectForRequerd("סעיף פיננסי");
                cc.calanederCheck(1);
                fp.insertElment("sum", By.Name("Sum"), "5");
                fp.selectForRequerd("מטבע");
                process pr = new process();
                if (chsbonit == true)
                    pr.popupAttachFile("C:\\picterToPelesoft\\ללא שם.png", "חשבונית", "");
                Thread.Sleep(200);
                fp.checkElment("preview-icon", By.ClassName("preview-icon"));
                fp.checkElment("icon-download", By.ClassName("icon-download"));
                fp.checkElment("icon-trash", By.ClassName("icon-trash"));
                fp.insertElment("textarea", By.TagName("textarea"), "אוטומציה");
                fp.ClickButton(" צור קבלת שירות חדש ", "span");
                SucessEndCreate sec = new SucessEndCreate();
                sec.checkEndCreate("קבלת שירות", false, "", "ספק");
            }
            catch { throw new NotImplementedException(); }
        }

        private void checkElmentServiceReceipts()
        {
            try
            {
                fp.checkElment("calnder", By.Name("trip-start"));
                fp.checkElmentText("sum", By.Name("Sum"), " סכום לתשלום ");
                fp.checkElmentText("chasbonit", By.CssSelector("mat-label[class='label attachment-no-value']"), "חשבונית");
            }
            catch { }
        }

        public void addPayment(bool kabla = false)
        {
            try
            {
                string[] typePayments = { "מזומן", "צ'ק", "העברה בנקאית" };
                string typePayment = typePayments[fp.rand(typePayments.Length)];
                fp.SelectRequerdName("formcontrolname='PaymentType", typePayment);
                if (typePayment == "העברה בנקאית")
                {
                    try { fp.click(By.XPath("//div/i[@class='material-icons button-icon-add'][text()=' add ']")); }
                    catch { try { fp.click(By.CssSelector("i[class='material-icons button-icon-add']")); } catch { } }
                    try { fp.click(By.XPath("//i[text()=' add ']")); }
                    catch
                    { }
                    cc.checkPoupBank("d");//לא גמור חסר סוג מוסד סליקה
                    fp.SelectRequerdName("formcontrolname='ClearingEntityID", "מסב זיכוים");
                    fp.ClickButton("שמירה", "span");
                }

                if (kabla == true)
                {
                    process pr = new process();
                    try { fp.click(By.XPath("//label/i[@class='icon-attached-file']")); }
                    catch { }
                    pr.popupAttachFile("C:\\picterToPelesoft\\logo.png", "", "");
                }
                fp.ClickButton("צור תשלום", "span");
            }
            catch { throw new NotImplementedException(); }
        }

        public void checkTablePaymen()
        {
            try
            {
                string[] title = { "", "פרטי השירות", "פרטי התשלום", "-1" };
                string[] ttTitle = { "", "שם", "סעיף פיננסי", "תאריך קבלת השירות", "סכום לתשלום", "מטבע", "פרטים נוספים", "חשבונית", "", "תאריך גביית התשלום", "סכום התשלום", "סטטוס התשלום", "אופן התשלום", "קבלה", "-1" };
                string[] row = { "", "", "ספק", DateTime.Today.ToString("dd/MM/yy"), "5", "שקל", "אוטומציה", "", "", " ", "-1" };
                string[] row1 = { "", "", "ספק", "", "5", "שקל", "אוטומציה", "", "", DateTime.Today.ToString("dd/MM/yy"), "5", " לגבייה", "", "", "-1" };
                fp.checkColumn("thead[role='rowgroup']", title, "false", "tr", "th");
                fp.checkColumn("thead[role='rowgroup']", ttTitle, "false", "tr", "th");
                fp.checkColumn("tbody[role='rowgroup']", row, "MOVEbutton[mattooltip='הוספת תשלום']", "tr", "td[role='gridcell']", 7);
                addPayment(true);
                fp.refres();
                openDoubt();
                clickTab(1, "תשלומים לספקים");
                fp.checkColumn("tbody[role='rowgroup']", row1, "false", "tr", "td[role='gridcell']");
                IList<IWebElement> rowTr = driver.FindElements(By.TagName("tr"));
                IList<IWebElement> cell;
                for (int i = 0; i < rowTr.Count; i++)
                {
                    cell = rowTr[i].FindElements(By.CssSelector("td[role='gridcell']"));
                    if (cell.Count > 0)
                    {
                        if (cell[7].Text != "highlight_off" && cell[7].Text != "task_alt")
                            throw new NotImplementedException();

                        if (cell[12].Text != "")
                        {
                            if (cell[13].Text != "highlight_off" && cell[13].Text != "task_alt")
                                throw new NotImplementedException();
                            else
                                break;
                        }
                    }
                }
                fp.selectForRequerd("סטטוס התשלום", false, -1);
                fp.selectText("לגבייה");
                fp.checkColumn("tbody[role='rowgroup']", row1, "false");
                fp.selectForRequerd("אופן תשלום", false, -1);
                fp.selectText("מזומן");
                row1[12] = "מזומן";
                fp.checkColumn("tbody[role='rowgroup']", row1, "false");
                fp.selectForRequerd("אופן תשלום", false, -1);
                fp.selectText("צ'ק");
                row1[12] = "צ'ק";
                fp.checkColumn("tbody[role='rowgroup']", row1, "false"); fp.selectForRequerd("אופן תשלום", false, -1);
                fp.selectText("העברה בנקאית");
                row1[12] = "העברה בנקאית";
                fp.checkColumn("tbody[role='rowgroup']", row1, "false");
            }
            catch { fp.picter("checkTablePaymen"); throw new NotImplementedException(); }
        }

        public void makePayment()
        {
            try
            {
                fp.click(By.XPath("//div/i[text()='price_check']"));
                fp.checkElmentText("title popup", By.ClassName("main-title"), "נתוני תשלומים לחודש " + DateTime.Today.ToString("MM/yyyy"));
                fp.checkElmentsText("tab", By.ClassName("mat-tab-label-content"), "מזומן", 0);
                fp.checkElmentsText("tab", By.ClassName("mat-tab-label-content"), "צ'ק", 1);
                fp.checkElmentsText("tab", By.ClassName("mat-tab-label-content"), "העברה בנקאית", 2);
                string[] heder = { "", "שם", "סעיף פיננסי", "סכום", "מטבע", "תאריך התשלום", "סטטוס התשלום", "אופן התשלום", "-1" };
                fp.checkColumn("thead[role='rowgroup']", heder, "mat-checkbox", "tr", "mat-header-cell", 0);
                string[] rowCheck = { "", "", "ספק", "5", "שקל", DateTime.Today.ToString("dd/MM/yy"), "לגבייה", "-1" };
                fp.checkColumn("tbody[role='rowgroup']", rowCheck, "mat-checkbox", "tr", "//mat-cell/div/div", 0);
                fp.ClickButton("ביצוע תשלומים ");
                fp.closepopup();
                fp.selectForRequerd("סטטוס התשלום", false, -1);
                fp.selectText("בוצע");
                string[] row1 = { "", "", "ספק", "", "5", "שקל", "אוטומציה", "", "", DateTime.Today.ToString("dd/MM/yyyy"), "5", "בוצע", "מזומן", "", "-1" };
                fp.checkColumn("tbody[role='rowgroup']", row1, "false");
            }
            catch { fp.picter("makePayment"); throw new NotImplementedException(); }
        }
    }
}
