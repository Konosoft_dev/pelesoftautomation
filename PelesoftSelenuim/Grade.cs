﻿using java.lang;
using OpenQA.Selenium;
using System;

namespace PelesoftSelenuim
{
     class Grade:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public Grade()
        {
        }

        public int insertData(string gLavel)
        {
            int selectReturn = 0;
            try
            {
                
                Financical f = new Financical();
                if (gLavel == "")
                {
                    fp.insertElment("select grade lavel", By.Name("ClassName"), "כיתה א");
                    fp.selectForRequerd("רמת כיתה");//"ng-reflect-name='ClassLevelID",automation);
                }
                   
                else
                {
                    fp.insertElment("select grade lavel", By.Name("ClassName"), gLavel);
                    fp.selectForRequerd("רמת כיתה", true, -1);//"ng-reflect-name='ClassLevelID",automation);
                    if (fp.selectText(gLavel, true) == 1)
                        selectReturn = 1;
                }
                if (selectReturn != 1)
                {
                    fp.selectForRequerd("שנה\"ל", true, -1);
                    fp.selectText(yearHebru);//"ng-reflect-name='SchoolYearID",yearHebru);
                    fp.ClickButton(" צור כיתה חדשה ");
                }
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
            return selectReturn;
        }

        public  void checkElment()
        {
            try
            {
                fp.checkElment("logo", By.ClassName("wrap-item-icon"));
                fp.checkElmentText("title gard lavel", By.ClassName("main-title"), "יצירת כיתה" );
                fp.checkElmentText("sub title", By.ClassName("sub-title"), "אנא מלא/י את הפרטים הבאים");
                fp.checkElmentText("title section", By.ClassName("title-section"), "פרטי כיתה" );
                fp.checkSelectPreform("בחר/י רמת כיתה עבור הכיתה", By.TagName("mat-select"));
                fp.checkElmentText("select grade lavel", By.Name("ClassName"), " שם כיתה  ");
                fp.checkSelectPreform("בחר/י  שנה\"ל עבור הכיתה ", By.TagName("mat-select"));
                fp.checkElmentText("select grade lavel", By.XPath("//button[@aria-label='btnSubmitCreateItem']"), " צור כיתה חדשה ");

            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }

        public  void checkPage()
        {
            try
            {
                fp.checkElment("popup grade", By.ClassName("quick-object-creation"));
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index", ex);
                throw argEx;
            }
        }
        public void addGreadeStep(int snifNum=-1)
        {
            popupList pl = new popupList();
            pl.checkPage();
            pl.checkElment("");
            pl.chekName(nameMosad);
            pl.btnEdit();
            Thread.sleep(400);

            pl.clickStepHeder(3, "סניפי מוסדות חינוך");
            Thread.sleep(400);
            pl.clickDisplayInTable(snifNum);
            Thread.sleep(400);
            pl.clickStepHeder(6, "כיתות");

        }
        public void openStepGrade(string nm = "אוטומציה", string tm = "גן ילדים")
        {
            EducationalInstitution ei = new EducationalInstitution();
            ei.openPageEI();
            ei.clickOnTable(nm, tm);
        }
        public void clickGrade(int snifNum)
        {
            
            fp.ClickButton(" הוספת כיתה ", "span");

            checkPage();
            checkElment();
        }
        public  void stepCreateGard(string nm="אוטומציה",string tm="גן ילדים",string gLavel="",int snifNum=-1)
        {
            openStepGrade(nm, tm);
            addGreadeStep(snifNum);

            clickGrade(snifNum);

                insertData(gLavel);
                SucessEndCreate sec = new SucessEndCreate();
                sec.checkEndCreate("כיתה", false, "ה", "סניף " + MosadChinuc+" ");
            
        }

        public  void Close()
        {
            driver.FindElements(By.CssSelector("button[aria-label='edit']"))[1].Click();
          //  throw new NotImplementedException();
        }
    }
}