﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;

namespace PelesoftSelenuim
{
    internal class Nedarim:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public  void stepProcess()
        {
            try
            {
                process ps = new process();
                ps.openProcess();
                ps.insertStep1("נדרים פלוס", "נדרים פלוס", "", "אוטומטי");
                
                ps.Automation("חד פעמי", DateTime.Today,DateTime.Now.AddMinutes(2).ToString("HH:mm"));
                fp.ClickButton(" לשלב הבא ", "span");
                ps.step3(" יבוא נתונים ממערכת נדרים פלוס ", "", "", "");
                fp.closePoup();
            } catch { fp.picter("stepProcess nedarim"); throw new NotImplementedException(); }
        }

        public  void actionRowNdarim()
        {
            try {
                string[] heder = { " נתונים שהתקבלו מנדרים פלוס ", " קליטת הנתונים במערכת ", "-1" };
                fp.checkColumn("thead[role='rowgroup']",heder,"false","tr","th");
                string[] ttheder = { "שם","כתובת","טלפון","סכום העסקה","תאריך ביצוע העסקה","הערות","סוג עסקה","סעיף פיננסי","מזהה איש קשר","-1"};
                fp.checkColumn("thead[role='rowgroup']", ttheder, "false", "tr", "th");

                IWebElement tableNdarim = driver.FindElement(By.TagName("tbody"));
                IList<IWebElement> tableRow = tableNdarim.FindElements(By.TagName("tr"));
                IList<IWebElement> tableColumn;
                string nameNedarim="";
               
                for (int i=0;i<4;i++)
                {
                    tableColumn = tableRow[i].FindElements(By.TagName("td"));
                    nameNedarim  = nameNedarim+ tableColumn[0].Text;
                    nameNedarim = nameNedarim +","+ tableColumn[3].Text;
                    tableColumn[8].FindElement(By.TagName("mat-select")).Click();
                    string finan=saifFinanse[fp.rand(saifFinanse.Length)];
                    nameNedarim = nameNedarim + "," + finan+")";
                    int m;
                    fp.selectText(finan);if (i == 0) m = i; else m = i - 1;
                    try { tableColumn[9].FindElement(By.ClassName("linked")).Click(); } 
                    catch { driver.FindElement(By.XPath("//tr["+m+"]/td[9]/div/div/i[text()=' person_search ']")).Click(); }
                    createConatrabition cc = new createConatrabition();
                    char[] sogar = { ')' };
                    string nameUser = nameNedarim.Split(sogar)[i];
                    char[] splitt = {' ', ','};
                    cc.clickFrindNisoyAddFrind("",nameUser.Split(splitt)[0], nameUser.Split(splitt)[1],false);
                    Thread.Sleep(250);
                    driver.FindElements(By.XPath("//i[text()=' done ']"))[i].Click();
                    Thread.Sleep(800);
                   tableRow = tableNdarim.FindElements(By.TagName("tr"));

                }
                char[] splitT = { ')'};
                string[] checkrow = nameNedarim.Split(splitT);
                for (int i = 0; i < checkrow.Length; i++)
                {
                    if (checkrow[i].Contains("הוראות קבע ראשי"))
                        hoq(checkrow[i]);
                    else if (nameNedarim.Split(splitT)[i].Contains("תרומה"))
                        conatarbitaion(checkrow[i]);
                }
                    } catch(Exception ex) { fp.picter("actionRowNdarim"); throw new NotImplementedException(ex+""); }
        }
        public void hoq(string checkRow)
        {
            fp.ClickList("הוצאות והכנסות");
            fp.clickNameDefinitions(" תקבולים ");
            char[] splitRow = { ' ', ',' };
            char[] splitp = { ',' };
            string[] checkrown = { "", checkRow.Split(splitRow)[0], checkRow.Split(splitRow)[1], "", checkRow.Split(splitp)[2], checkRow.Split(splitp)[1],"-1" };
            fp.checkColumn("//", checkrown, "false", "mat-row", "mat-cell");
            fp.ClickList("הוצאות והכנסות");
            fp.ClickList("תלמידים");
            fp.clickNameDefinitions(" תשלומים ");
            string[] checkRowT = { "", "", checkRow.Split(splitRow)[0], checkRow.Split(splitRow)[0]+" "+checkRow.Split(splitRow)[1], checkRow.Split(splitp)[2], "", checkRow.Split(splitp)[1],"-1" };
            fp.checkColumn("//", checkRowT, "false", "mat-row", "mat-cell");
            fp.ClickList("תלמידים");
        }
        public void conatarbitaion(string checkrow)
        {
            fp.ClickList("ידידים");
            fp.clickNameDefinitions(" תרומות ");
            char[] splitRow = { ' ', ',' };
            char[] splitP = { ',' };
            string[] checkrown = { "", "","", checkrow.Split(splitRow)[0], checkrow.Split(splitRow)[1],"", checkrow.Split(splitP)[1] ,"-1"};
            fp.checkColumn("//", checkrown, "false", "mat-row", "mat-cell");
            fp.ClickList("ידידים");
        }
    }
}