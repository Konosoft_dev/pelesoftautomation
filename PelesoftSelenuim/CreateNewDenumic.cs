﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class CreateNewDenumic:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void checkClasscreate(string nameCreate, bool RequestDetails = true)
        {
            onInit();
            fp.checkElment("button close", By.XPath("//button[@aria-label='edit']"));
            fp.checkElment("image logo", By.ClassName("logo"));
            fp.checkElment("title page", By.XPath("//div[contains(@class,'main-title')][contains(text(),' יצירת " + nameCreate + " ')]"));
            if (RequestDetails == true)
                fp.checkElment("sub title page", By.XPath("//div[contains(@class,'sub-title')][contains(text(),'אנא מלא/י את הפרטים הבאים')]"));
        }
       // public IWebDriver driver;
        public void onInit()//string nameCreate)
        {
            

            /*if (!driver.FindElement(By.Id("cdk-overlay-2")).Displayed)
            {*/
               // Debug.WriteLine("page create:" + nameFrind + "not dispaly");
            //}
        }

        public void insertTask(string value)
        { string task = "משימה";
            try {
                fp.checkElmentText("sub title", By.ClassName("title-section"), "פרטי משימה ");
                fp.SelectRequerdName("formcontrolname='TaskTypeID", "אחר", false);
                Thread.Sleep(100);

                fp.SelectRequerdName("formcontrolname='TaskOwnerUserID", nameuser(),false,false);
                fp.insertElment("insert value task", By.Name("Description"), value);
                createConatrabition cc = new createConatrabition();
                fp.insertElment("calnder", By.Name("trip-start"), DateTime.Today.ToString("HH:mm dd/MM/yyyy"));
               // cc.calanederCheckOld(1,-1,"5",3);
                fp.ClickButton(" צור "+task+" חדשה ");
                SucessEndCreate sec = new SucessEndCreate();
                sec.checkEndCreate(task, false, "ה");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // throw new NotImplementedException();
        }
        public void createDoc(bool prati , bool Tab ,string nameDoc,string [] listSelect, string []listSelect2)
        {
            try
            {
               
                 fp.click(By.XPath("//i[@class='material-icons icon-report ng-star-inserted'][text()=' view_headline ']"));
                checkElmentDoch();
                insertElmentDoc(listSelect, listSelect2, prati, Tab,nameDoc);
                string[] checkList = { "",listSelect[0],listSelect[1],listSelect[2]};
                fp.checkColumn("//",checkList,"false", "mat-header-row", "mat-header-cell");
                fp.click(By.XPath("//mat-select/div/div/span/span[text()='" +nameDoc.Trim() + "']"));
                fp.selectText("ללא דוח");
                int countNoDo = driver.FindElements(By.TagName("mat-header-cell")).Count;
                try { driver.FindElement(By.XPath("//mat-form-field[@mattooltip='דוחות']/div/div/div/mat-select")).Click(); } catch { }
                fp.selectText(nameDoc);
                if (countNoDo<= driver.FindElements(By.TagName("mat-header-cell")).Count)
                {
                    fp.picter("createDoc");
                    Debug.WriteLine("noDoch equals to doch");
                }
                fp.click(By.XPath("//i/button/span/mat-icon[text()='more_vert']"));
                fp.click(By.CssSelector("button[aria-label='edit icon-button']"));
                fp.checkElmentText("reportName", By.CssSelector("input[formcontrolname='reportName']"), nameDoc);
                fp.ClickButton("שמירה בשם","span");
                Thread.Sleep(450);
                if(!driver.FindElement(By.CssSelector("div[class='mat-dialog-content msg-content ng-star-inserted']")).Text.Contains("לא ניתן לשמור דוח בשם זהה לדוח אחר"))
                { fp.picter("edit doch is save"); throw new NotImplementedException(); }
                fp.selectsss("", 1, listSelect);
                fp.selectsss("", 3, listSelect2);
                fp.closePoup();
            }
            catch (Exception ex) { fp.picter("createDoc"+ex); throw new NotImplementedException(""+ex); }
        }
        public void insertElmentDoc(string []select1,string []select2,bool prati,bool Tab,string nameDoch)
        { try {
                driver.FindElement(By.CssSelector("input[formcontrolname='reportName']")).Clear();
                fp.insertElment("name doch", By.CssSelector("input[formcontrolname='reportName']"), nameDoch);
                if (prati == true)
                    fp.click(By.TagName("mat-checkbox"), 0);
                if (Tab == true)
                    fp.click(By.TagName("mat-checkbox"), 1);
                fp.insertElment("sharch", By.CssSelector("input[placeholder='חיפוש']"), "מוסד לימודים", false, "", 0);
                string[] listCheckMo = { "מוסד לימודים", " מוסד לימודים קודם ", " סניף מוסד לימודים " };
                fp.selectsss(" מוסד לימודים ", 0, listCheckMo,1);
                driver.FindElement(By.CssSelector("input[placeholder='חיפוש']")).Clear();
                fp.insertElment("sharch", By.CssSelector("input[placeholder='חיפוש']"), " ", false, "", 0);
                fp.selectsss(" שם משפחה ", 0,null,1);
                fp.selectsss(" שם פרטי ", 0,null,1);
                fp.selectsss("", 1, select1);
                fp.insertElment("sharch", By.CssSelector("input[placeholder='חיפוש']"), "מוסד לימודים", false, "", 1);
                fp.selectsss(" מוסד לימודים ", 2, listCheckMo, 2);
                driver.FindElements(By.CssSelector("input[placeholder='חיפוש']"))[1].Clear();
                fp.insertElment("sharch", By.CssSelector("input[placeholder='חיפוש']"), " ", false, "", 1);
                fp.selectsss("", 3, select2);
                fp.ClickButton(" שמירה ","span",2);
            } catch(Exception ex) { fp.picter("insertElmentDoc" + ex); } }
        private void checkElmentDoch()
        {
            try { 
                fp.checkElmentText("name doch", By.CssSelector("input[formcontrolname='reportName']"), "1 דוח חדש");
                fp.checkElmentsText("checBoxPrivate", By.TagName("mat-checkbox"), "דוח פרטי", 0);
                try { fp.checkElmentsText("checBoxTab", By.TagName("mat-checkbox"), "הצג כטאב", 1); } catch { }
                fp.checkElment("filter in doch",By.CssSelector("div[class='icon-button icon-filter']"));
                fp.checkElmentsText("title-dual-listbox", By.ClassName("title-dual-listbox"), "עמודות לתצוגה",0);
                fp.checkElmentsText("title-dual-listbox", By.ClassName("title-dual-listbox"), "עמודות למיון",1);
                fp.checkElment("sherch",By.CssSelector("input[placeholder='חיפוש']"));
                fp.checkElmentsText("list-title",By.ClassName("list-title"), "עמודות לבחירה",0);
                fp.checkElmentsText("list-title",By.ClassName("list-title"), "עמודות שנבחרו", 1);
                fp.checkElmentsText("list-title",By.ClassName("list-title"), "עמודות לבחירה",2);
                fp.checkElmentsText("list-title",By.ClassName("list-title"), "מיון לפי העמודות", 3);
                //fp.checkElment("select column in doch",By.CssSelector("select[class='select-listbox ng-pristine ng-valid ng-touched']"));
                fp.checkElment("cover left",By.CssSelector("i[class='material-icons move-all']"));
                fp.checkElment("cover left",By.CssSelector("i[class='material-icons']"));
            }
            catch (Exception ex) { fp.picter("checkElmentDoch"+ex); throw new NotImplementedException(); }
        }

        public void filterQuickly(string nameTable= "תלמידים")
        {
            try { fp.click(By.CssSelector("div[class='icon-button icon-filter']"));
                checkFilter(nameTable);
                insertFilter();
            } catch (Exception ex) { fp.picter("filterQuickly"+ex); throw new NotImplementedException(""+ex); }
        }

        public void insertFilter(bool filterPrivate=true)
        {
            PupilNew pn = new PupilNew();

            try
            { 
             filter("סינון תלמידים","בדיקה שווהל"," מוסד לימודים ; שווה ל ; בית אברהם ",filterPrivate);
                filter("edit", "בדיקה ריק", " כיתה וסניף ; ריק ;", false,0,"", 1);
                filter("false", "בדיקה מתחיל ב", " הערות; מתחיל ב ;אTEXT", false, -1, "סינון תלמידים");
                try
                {
                    if (pn.clickTableRandom() != -1)
                    {
                        fp.checkElmentText("textarea", By.TagName("textarea"), "א");
                        fp.closePoup();
                    }
                }
                catch { }
                filter("מספר זהותfalse", "בדיקהשווה ל", "  מספר זהות ;מתחיל ב;1TEXT",false,0, "סינון חדש 1",0,0,"",">");
                filter("false", "בדיקהגדול שווה ל", "   מזהה שנתון לתלמיד ; גדול או שווה ל ;2TEXT", true,0, " מספר זהות ",0,0,"",">=");
                filter("editbin", "בדיקה קטן שווהל", "   מזהה שנתון לתלמיד ; קטן או שווה ל ;2TEXT", false,0,"",0,0,"","<=");
                filter("editbin", "בדיקה אחד מתוך", " סטטוס ; אחד מתוך ; פעיל , נרשם ", false, 0, "", 0, 0, "אחד מתוך", ",");
                filter("editbin","אינוריק"," מגמה ; אינו ריק ;",false,1, "אחד מתוך",0,0,"פילטר משתנה","notnull");
                filter("editbin","מכיל את", "  מס' בית ; מכיל את ;1TEXT", false,-1, "פילטר משתנה");
                if (pn.clickTableRandom() != -1)
                {
                    popupList pl = new popupList();
                    pl.expansionPopUP();
                    pl.clickStepHeder(1, "פרטי הורה");
                    try { if (!driver.FindElement(By.Name("P_Number")).ToString().Contains(""))
                        { fp.picter("number home not contains"); throw new NotImplementedException("filter number home not contains"); }
                        fp.closePoup();
                        fp.ClickButton("כן","span");
                   } catch { }
                }
                    filter("false", "תאריך לידהעדלפני", " ת. לידה לועזי ; עד לפני ;3שנים", true, 0, "פילטר משתנה", 0, 0, "תאריך", "שנים<");
                
                }
            catch (Exception ex) { fp.picter("insertFilter" + ex); throw new NotImplementedException(""+ex); }
        }
        public void filter(string nameFilter, string valueF,string listV,bool filterPrivate = true,int mikomCheckAfterFilter=0,string filterOld="",int mikomGroup=0,int mikomRow=0,string saveName="",string oper="")
        {
            try
            {
                string bin = "";
                if (nameFilter.Contains("false"))
                {
                    fp.click(By.XPath("//mat-select/div/div/span/span[text()='"+ filterOld.Trim() + "']"));
                    fp.selectText("סינון חדש ");
                    char []tav = { 'f' };
                    bin= nameFilter.Split(tav)[1];
                    nameFilter = nameFilter.Split(tav)[0];
                }
                else if (nameFilter.Contains("edit"))
                {
                    fp.click(By.CssSelector("div[class='icon-button icon-edit']"));
                    fp.click(By.XPath("//div[@class='mat-tab-label-content']/button/span/mat-icon[text()='add_circle']"));
                    char[] tav = { 'e' };
                    bin= nameFilter.Split(tav)[1];
                    nameFilter = nameFilter.Split(tav)[0];
                }
               if(nameFilter!="")
                {
                    changeName(nameFilter);
                    if (filterPrivate == true)
                        fp.click(By.TagName("mat-checkbox"));
                }
                if (bin.Contains("bin"))
                {
                    fp.click(By.CssSelector("mat-icon[class='mat-icon notranslate close-tab-icon material-icons mat-icon-no-color ng-star-inserted']"));
                    fp.ClickButton("כן");
                }
                insertGroup(valueF, listV, mikomGroup, mikomRow, saveName);
                char[] cut = { ';' };
                string shearch = "";
                if (mikomCheckAfterFilter > -1)
                {
                    mikomCheckAfterFilter = fp.mikomNameTable("mat-header-cell", listV.Split(cut)[0]);
                    try
                    {
                         shearch = "" + fp.splitNumber(listV.Split(cut)[2]);
                        if (shearch == "-1")
                            shearch = listV.Split(cut)[2];
                    }
                    catch { shearch = listV.Split(cut)[2]; }
                    fp.checkColumnTable("mat-row", "mat-cell", mikomCheckAfterFilter, shearch, oper);
                }
            }
            catch(Exception ex) { fp.picter(ex + "insert filter"+ valueF); throw new Exception(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameGroup"></param>
        /// <param name="listFilter">מופרד ב;</param>
        public void insertGroup(string nameGroup, string listFilter,int mikomGroup=0,int mikomRowFilter=0,string saveName="")
        {
            try { char[] cut = { ';' };
                try { driver.FindElements(By.XPath("//div/div[@class='mat-tab-label-content']/input"))[mikomGroup].Click(); } catch { }
                driver.FindElements(By.XPath("//div/div[@class='mat-tab-label-content']/input"))[mikomGroup].Clear();
                fp.insertElment("group", By.XPath("//div[@class='mat-tab-label-content']/input"), nameGroup,false,"",mikomGroup);
                process ps = new process();
                string[] filter = listFilter.Split(cut);
                ps.stepFrainLast(filter, 1, false,0, "שמירה " + saveName,false,true);

           }
            catch (Exception ex) { fp.picter("insertGroup" + ex); throw new NotImplementedException(""+ex); }
        }

        private void changeName(string cName)
        {
            try {
                driver.FindElement(By.CssSelector("input[formcontrolname='FilterName']")).Clear();
                fp.insertElment("filter",By.CssSelector("input[formcontrolname='FilterName']"), cName);
            }
            catch { fp.picter("change name filter"); throw new NotImplementedException(); }
        }

        private void checkFilter(string nameTable)
        {
            try
            {
                fp.checkElmentText("txt title", By.ClassName("txt-title"), "שם סינון לתצוגה");
                fp.checkElment("filter",By.CssSelector("input[formcontrolname='FilterName']"));
                fp.checkElmentText("filter private",By.TagName("mat-checkbox"), "סינון פרטי");
                fp.checkElment("group",By.XPath("//div[@class='mat-tab-label-content']/input"));
                fp.checkElmentText("grabg",By.CssSelector("mat-icon[class='mat-icon notranslate close-tab-icon material-icons mat-icon-no-color ng-star-inserted']"), "delete");
                fp.checkElmentText("add group",By.CssSelector("mat-icon[class='mat-icon notranslate material-icons mat-icon-no-color'][aria-hidden='true']"), "more_vert" );
                fp.checkElmentText("titleTable filter",By.TagName("r"),nameTable);
                fp.checkElmentText("*", By.ClassName("new-condition-icon"), "*");
                fp.checkElmentsText("add filter table son",By.ClassName("mat-button-wrapper"), "שמירה בשם  ",13);
                fp.checkElmentsText("add filter table son",By.ClassName("mat-button-wrapper"), "שמירה ",14);
                fp.checkElmentsText("add filter table son",By.ClassName("mat-button-wrapper"), "הוסף סינון על טבלת בן ",16);
            }
            catch(Exception ex) { fp.picter("checkFilter"+ex); throw new NotImplementedException(""+ex); }
        }

        public void clickTab(string nameTabDoch,int numCHeder)
        {
            try {
                IWebElement elmentsTab = driver.FindElement(By.ClassName("mat-tab-labels"));
                IList<IWebElement> tabs = elmentsTab.FindElements(By.CssSelector("div[role='tab']"));
                if (!tabs[tabs.Count - 1].Text.Contains(nameTabDoch.Trim()))
                { fp.picter("the value in tab noot equals to:" + nameTabDoch.Trim()); throw new NotImplementedException("name tab doch not good"); }
                else
                    tabs[tabs.Count - 1].Click();
                Thread.Sleep(450);
               if(!driver.FindElement(By.ClassName("row-number-txt")).Text.Contains("0") && numCHeder!=driver.FindElements(By.TagName("mat-header-cell")).Count-1)
                { fp.picter("the number columen not equals to :" + numCHeder.ToString()); throw new NotImplementedException("number columen in table in tab doch not good"); }

            }
            catch { throw new NotImplementedException(); }
        }

        public void imageLogo()
        {
            try {
               // fp.click(By.ClassName("imgDiv"));
                fp.click(By.CssSelector("mat-icon[class='mat-icon notranslate material-icons mat-icon-no-color']"));
                fp.click(By.CssSelector("div[aria-label='btnAddImgToPerson']"));
                process pr = new process();
                 pr.popupAttachFile("C:\\picterToPelesoft\\logo.png", "");
                
            } catch { fp.picter("imageLogo"); throw new NotImplementedException(); }
        }

        public void chancgeSizeOrImage(int size=1)
        {
            try {
                int sizeimg=-1;
                if (size==1)
                    sizeimg = driver.FindElement(By.CssSelector("img[class='item-img ng-star-inserted']")).Size.Width;
               driver.FindElement( By.XPath("//div[@class='imgDiv']")).Click();
                fp.click(By.CssSelector("mat-icon[class='mat-icon notranslate material-icons mat-icon-no-color']"));
                    fp.click(By.CssSelector("div[aria-label='btnAddImgToPerson']"), size);
                if (size == 1)
                {
                    if (sizeimg >= driver.FindElement(By.CssSelector("img")).Size.Width)
                    { fp.picter("size image"); throw new NotImplementedException(); }
                }
                else {
                    process pr = new process();
                    pr.popupAttachFile("C:\\picterToPelesoft\\ללא שם.png", "");
                }
            } catch { fp.picter("size image"); throw new NotImplementedException(); }
        }

        public  void histori(string[] historiList,string []actionList)
        {
            try {
                try { fp.click(By.CssSelector("div[aria-label='divAddDocument'][mattooltip='היסטוריה']")); } catch { }
                fp.checkElmentText("title histor",By.ClassName("main-txt"), "היסטוריה");
                string[] hederRow = { "משתמש","טבלה","תאריך","פעולה","-1"};
                fp.checkColumn("thead[role='rowgroup']",hederRow,"false","tr","th",-1);
                fp.checkColumn("tbody[role='rowgroup']", historiList,"");
                string[] hederRowAction  = { "רשומה","שדה","ערך קודם","ערך מעודכן","-1"};
                fp.checkColumn("thead[role='rowgroup']",hederRowAction,"false","tr","th",-1,1);
                fp.checkColumn("tbody[role='rowgroup']", actionList,"false","tr","td",-1,1);
                fp.closePoup(1);
                fp.closePoup();
            } catch { fp.picter("histori"); throw new NotImplementedException(); }
        }

        public string  change()
        {
            string name="";
            try { popupList pl = new popupList();
                pl.btnEdit();
                name = driver.FindElement(By.Name("FirstName")).Text;
                fp.insertElment("change FirstName",By.Name("FirstName"),"אאאא");
                name = name + ";" + driver.FindElement(By.ClassName("identifying-text")).Text;
                fp.ClickButton("שמירה");
                fp.closePoup(); 

            } catch { throw new NotImplementedException(); }
            return name;
        }

        public  string nameuser()
        {
            string user = driver.FindElement(By.ClassName("user-name")).Text;
            char []spli = { ' ' };
            if(user.Split(spli).Length>1)
                user=user.Split(spli)[1] + " " + user.Split(spli)[0];
            return user;
        }

        public void historiAdd(string  vadi="הורים")
        {
            try {
                Frind f = new Frind();
                if(vadi=="הורים")
                f.stepCreateParents("קנובלוביץ");
                else
                { Bulding b = new Bulding();
                    fp.ButtonNew("");
                    b.insertTenant("לא", "קנובלוביץ");
                }
                fp.refres();
                if (vadi == "הורים")
                    f.openParent();
                else
                {
                    fp.clickNameDefinitions(" " + niulBuldin + " ");
                    fp.clickNameDefinitions(" דיירים ");
                }
                string[] rowAdd = { "","קנובלוביץ","-1"};
                fp.checkColumn("//",rowAdd,"","mat-row","mat-cell",0);
                string user = nameuser();
                string[] rowAction = { user, vadi, DateTime.Today.ToString("dd/MM/yyyy-"), "הוספה", "-1" };
                string[] listAction = {"","שם משפחה","","קנובלוביץ","-1" };
                histori(rowAction,listAction);
            } catch { throw new NotImplementedException(); }
        }
        public void openTable(string model,string ParentTable="")
        {
            if (model.Contains("ניהול כספים") || model.Contains("הגדרות")|| model.Contains("אנשי קשר")||model.Contains("מסמכים"))
                fp.actionWork(model.Trim(), ParentTable);
            else
            {
                if (model != "")
                    fp.clickNameDefinitions(model);
                if (model == ParentTable)
                    fp.clickListinListEqualName(ParentTable);
                else
                if (ParentTable!="" && ParentTable!=" משימות ")
                fp.clickNameDefinitions(ParentTable);
                if(ParentTable==" משימות ")
                    fp.click(By.CssSelector("button[class='conosoft-icon-button button-settings1 mat-icon-button mat-button-base']"),2);

            }
        }
        public void checkDownload(string nameTablePnimi, string namelist,bool excel=true,string nameChangeExcel="")
        {
            try { openTable(namelist,nameTablePnimi);
                Thread.Sleep(400);
                 string num = driver.FindElement(By.ClassName("row-number-txt")).Text;
                int countRow = fp.splitnumber(num) + 1;
                if (countRow > 1)
                {
                    IWebElement heder = driver.FindElement(By.TagName("mat-header-row"));
                    IList<IWebElement> hederCell = heder.FindElements(By.TagName("mat-header-cell"));
                    var header = hederCell.Where(se => se.Displayed);

                    String[] allText = new String[hederCell.Count + 1];
                    int i = 0;
                    foreach (IWebElement element in hederCell)
                    {
                        if (i == hederCell.Count)
                            break;
                        try { allText[i++] = element.Text; }
                        catch { }
                    }
                    By typeDownload = excel == true ? By.CssSelector("div[mattooltip='יצא נתונים לאקסל']") : By.XPath("//i[text()='picture_as_pdf']");
                    fp.click(typeDownload);
                    string typeEnd = excel == true ? ".xlsx" : ".PDF";
                    if (nameChangeExcel != "")
                        nameTablePnimi = nameChangeExcel;
                    if (excel == true)
                    {
                        ExcelApiTest eat = new ExcelApiTest(" ", "", "C:\\Users\\konosoftUser\\Downloads\\" + nameTablePnimi.Trim() + typeEnd);
                        eat.checkDownloadFromAbove(allText, countRow, nameTablePnimi.Trim() + typeEnd);
                    }
                    else
                    { CheckFileDownloaded("C:\\Users\\konosoftuser\\Downloads\\" + nameTablePnimi.Trim() + typeEnd); }
                } } catch(Exception ex) { throw new NotImplementedException(ex+""); }

        }
        public  bool CheckFileDownloaded(string filename)
        {
            bool exist = false;
            string Path = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Downloads";
            string[] filePaths = Directory.GetFiles(Path);
            foreach (string p in filePaths)
            {
                if (p.Contains(filename))
                {
                    FileInfo thisFile = new FileInfo(p);
                    //Check the file that are downloaded in the last 3 minutes
                    if (thisFile.LastWriteTime.ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    thisFile.LastWriteTime.AddMinutes(1).ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    thisFile.LastWriteTime.AddMinutes(2).ToShortTimeString() == DateTime.Now.ToShortTimeString() ||
                    thisFile.LastWriteTime.AddMinutes(3).ToShortTimeString() == DateTime.Now.ToShortTimeString())
                    {
                        exist = true;
                        File.Delete(p);
                        break;
                    }
                }
            }
            return exist;
        }
        internal void allDownloads(bool downloadsExcel=true)
        {
            /*checkDownload(" משימות ", "",downloadsExcel);
            checkDownload(" תלמידים ", " תלמידים ",downloadsExcel);
            checkDownload(" הורים ", "",downloadsExcel);
           checkDownload(" תשלומים ", "",downloadsExcel);*/
            checkDownload(" הוראות קבע ", "",downloadsExcel);
            checkDownload(" ילדים ", "",downloadsExcel);
            fp.clickNameDefinitions(" תלמידים ");
            checkDownload(" אברכי כולל ", " תלמידי ישיבה ",downloadsExcel,"אברכי כולל");
            Thread.Sleep(350);
            checkDownload(" הוצאות ממילגות ", "",downloadsExcel);
            fp.clickNameDefinitions(" תלמידי ישיבה ");
            checkDownload(" מוסדות חינוך ", " מוסדות ",downloadsExcel);
            checkDownload(" מוסדות לפי דתות ", "",downloadsExcel);
            fp.clickNameDefinitions(" מוסדות ");
            checkDownload(" ידידים ", " ידידים ",downloadsExcel);
            checkDownload(" תרומות ", "",downloadsExcel);
            checkDownload(" תרומות בהו\"ק ", "",downloadsExcel,"תרומות בהו_ק");
            Thread.Sleep(250);
            checkDownload(" התחייבויות ", "",downloadsExcel);
            checkDownload(" מעקב ידידים ", "",downloadsExcel);
            Thread.Sleep(250);
            fp.clickNameDefinitions(" ידידים ");
            checkDownload(" עניינים ", " טפסים ",downloadsExcel);
            checkDownload(" ספקים ", " ספקים ",downloadsExcel);
            //checkDownload(" הוצאות עבור ספקים ", "");
            checkDownload(" תקבולים ", " הוצאות והכנסות ",downloadsExcel);
            checkDownload(" הוצאות ", "",downloadsExcel);
            checkDownload(" קופות ", " הגדרות ",downloadsExcel);
            Thread.Sleep(250);
            checkDownload(" ישויות סליקה ", " הגדרות ",downloadsExcel);
            checkDownload(" חשבונות בנק ", " הגדרות ",downloadsExcel);
            checkDownload(" מבני קבלות ", " הגדרות ",downloadsExcel);
            checkDownload(" מסמכי PDF ", " הגדרות ",downloadsExcel);
            checkDownload(" מדדים ", " הגדרות ",downloadsExcel);
            Thread.Sleep(250);
            checkDownload(" תהליכים ", " הגדרות ",downloadsExcel  ,"תהליכים ");
            checkDownload(" אמצעי תשלום ", " ניהול כספים ",downloadsExcel);
            checkDownload(" ניהול סעיפים פיננסיים ", " ניהול כספים ",downloadsExcel,"סעיפים פיננסיים");
            checkDownload(" קבצי שידור ", " ניהול כספים ",downloadsExcel);
            checkDownload(" צפיה בקבלות ", " ניהול כספים ",downloadsExcel ,"קבלות");
            checkDownload(" הגדרת סעיפי חיוב ", " ניהול כספים ",downloadsExcel,"הגדרות סעיפי חיוב");
            checkDownload(" הגדרת סעיפי זיכוי ", " ניהול כספים ",downloadsExcel,"הגדרות סעיפי זיכוי");
            // checkDownload(" מחולל חיובי הורים "," ניהול כספים ");
            // checkDownload("אנשי קשר","");
        }

        public void namePopupParent(string row,string cell,int noOne=1,int clo=-1)
        {
            try { //IWebElement table = driver.FindElement(By.TagName("tbody"));
                IList<IWebElement> matrow = driver.FindElements(By.TagName(row));
                IList<IWebElement> matcell = matrow[0].FindElements(By.TagName(cell));
                string name = noOne<=9? matcell[noOne].Text : matcell[3].Text+" "+matcell[4].Text;
                fp.click(By.XPath("//i[text()=' account_circle ']"));
                Frind f = new Frind();
                string nameU = f.nameRow();

                char[] splace = { ' ' };
                if(nameU.Split(splace)[0]!=name.Split(splace)[0])
                    throw new NotImplementedException("the name not exuals to av");
                fp.closePoup(clo);
            } catch { throw new NotImplementedException(); }
        }
    }
}
