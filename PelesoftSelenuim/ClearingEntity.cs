﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PelesoftSelenuim
{
    class ClearingEntity:page
    {
        //static IWebDriver driver;// = new ChromeDriver();
        FunctionPelesoft fp = new FunctionPelesoft();
        SucessEndCreate sec = new SucessEndCreate();
        public void openClearingEninty()
        {
            fp.actionWork(" הגדרות ","ישויות סליקה");
        }
        public void CreateNewClearingEntity(string ce="אשראי", string spak = " פלאקארד ")
        {

            openClearingEninty();
            Thread.Sleep(700);
            stepClaeringEninety(ce,spak);
           // fp.waitUntil(By.XPath("//span[@class='mat-button-wrapper'][text()=' יצירת ישות סליקה חדשה ']"));

        }
        public void stepClaeringEninety(string nameEnenity, string spak = " פלאקארד ")
        {
            fp.ButtonNew("ישות סליקה", "ה");
            clearingEntity(nameEnenity,spak);
            sec.checkEndCreate("ישות סליקה", true, "ה");
        }
        public void clearingEntity(string ce,string spak= " פלאקארד ")
        {
            /*try
            {*/
            fp.checkElment("close", By.XPath("//button[@aria-label='edit']"));
            fp.checkElment("logo", By.ClassName("icon-table"));//logo
            fp.checkElment("title create slika", By.XPath("//div[@class='main-title ng-star-inserted'][text()=' יצירת ישות סליקה ']"));
           // fp.checkElment("sub title", By.ClassName("main-title"));
            fp.checkElment("sub title page", By.XPath("//div[contains(@class,'sub-title')][contains(text(),'אנא מלא/י את הפרטים הבאים')]"));
            fp.checkElment("details", By.XPath("//div[@class='title-section'][text()='פרטים']"));
           // fp.checkElment("id", By.Name("ClearingEntityID"));
            fp.checkElment("name", By.Name("Name"), false, true);
            fp.insertElment("name", By.Name("Name"), ce, true);
            slikaType(ce);
            fp.insertElment("userName", By.Name("User"), "shivukTest");
            fp.insertElment("password", By.Name("Password"), "r4FW1BDP");
            //fp.checkElment("doubt", By.XPath("//mat-select[@ng-reflect-name='Provider']"));
            /*driver.FindElement(By.Id("mat-select-7")).Click();
            fp.selectText("ללא");
            funError(10, "שדה חובה");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);*/
            fp.selectForRequerd("ספק", false, -1);//לא נבדק שהספק הוא שדה חובה
            //driver.FindElement(By.XPath("//mat-select[@ng-reflect-name='Provider']")).Click();
            fp.selectText(spak);
            fp.checkElment("code", By.Name("Code"));
            fp.insertElment("code", By.Name("Code"), "0962210", true);
            fp.selectForRequerd("הגדרת אימות", false, -1);
            fp.selectText("ללא אימות");
            fp.insertElment("numberPhone",By.Name("ValidatePhoneNumbers"),phonME);
            if (ce.Contains("נדרים"))
            {
                fp.insertElment("MosadIdInNedarimPlus", By.Name("MosadIdInNedarimPlus"), "7001038");
                fp.insertElment("ApiPasswordInNedarimPlus", By.Name("ApiPasswordInNedarimPlus"), "rn372");

            }
            
            fp.ClickButton(" צור ישות סליקה חדשה ");

            /*}
            catch
            {
                throw new NotImplementedException();

            }*/
        }
        public void slikaType(string skila)
        {
            try
            {
                fp.SelectRequerdName("formcontrolname='ClearingType",skila,false);
                //fp.selectText(skila);
                //fp.checkElment("type slika", By.XPath("//div[starts-with(@class,'mat-select-trigger')]"));
                //driver.FindElement(By.XPath("//mat-select[@ng-reflect-name='ClearingType']")).Click();
                //fp.selectText("אשראי");
            }
            catch
            {
                Debug.WriteLine("בחירת סוג סליקה לא תקין");
            }
        }


    }
}
