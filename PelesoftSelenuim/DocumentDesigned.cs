﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace PelesoftSelenuim
{
     class DocumentDesigned:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public DocumentDesigned()
        {
        }

        public void newDocumint(string nameDocuminet,string nameAicon)
        {
            try {
                fp.ButtonNew("");
                DocumentStep1( nameDocuminet,  nameAicon);
                documentStep2(nameDocuminet);
                fp.ClickButton(" צור מסמך מעוצב חדש ", "span");
            } catch {
                fp.picter("newDocumint");
                    throw new NotImplementedException(); }
        }

        private void documentStep2(string nameDocument)
        {
            try { fp.click(By.ClassName("tox-tbtn__select-label"));
                process pr = new process();
                Thread.Sleep(600);
                     pr.popupAttachFile("C:\\picterToPelesoft\\תמונת רקע.png", "");
                string []pnimi = { "עיצובים", "כותרות" };
                topToolbar("הוסף",pnimi);
                fp.click(By.CssSelector("div[title='כותרת 2']"));
                insertHtml("מסמך מעוצב");
                string[] linkS = {"שם פרטי","שם משפחה" };
                pr.editTextLink(nameDocument, linkS, "מסמך מעוצב", 0, false,-1);
                addPicterUrl();
                insertHtml(" ", "p", 3);
                fp.click(By.XPath("//div[@title='צבע רקע']/span[@class='tox-tbtn tox-split-button__chevron']"));
                fp.click(By.CssSelector("div[title='צהוב בהיר']"));
                insertHtml("מסמך מעוצב", "p", 4);
                fp.click(By.XPath("//div[@title='צבע טקסט']/span[@class='tox-tbtn tox-split-button__chevron']"));
                fp.click(By.CssSelector("div[title='ירוק בהיר']"));
                insertHtml("מסמך מעוצב", "p", 1);
                fp.click(By.CssSelector("button[title='עוד...']"));
                fp.click(By.CssSelector("div[title='הכנס תאריך/שעה']"));

            }
            catch { throw new NotImplementedException(); }
        }

        public void addPicterUrl(bool htm=true)
        {
            try {
                fp.click(By.CssSelector("button[aria-label='הוסף/ערוך תמונה']"));
                fp.click(By.CssSelector("button[title='מקור']"));
                process pr = new process();
                pr.popupAttachFile("C:\\picterToPelesoft\\ללא שם.png", "");
                Thread.Sleep(400);
                fp.click(By.CssSelector("button[title='שמור']"));
                if (htm==true)
                 insertHtml(" ", "img", 3);
                fp.click(By.CssSelector("button[aria-label='הוסף/ערוך קישור']"));
                fp.insertElment("url", By.CssSelector("input[type='url']"), "https://translate.google.com/?sl=iw&tl=en&op=translate");
                fp.insertElment("title", By.ClassName("tox-textfield"), "translate", false, "", 1);
                fp.click(By.CssSelector("button[title='שמור']"));
            } catch { throw new NotImplementedException(); }
        }

        public void insertHtml(string value,string tagName="h2",int mikom=-1)
        {
            driver.SwitchTo().Frame(driver.FindElements(By.TagName("iframe"))[1]);
            driver.FindElement(By.TagName("h2")).SendKeys(Keys.Tab);
            if (value != "" && value != " ")
                fp.insertElment("value", By.TagName(tagName),value,false,"",mikom);
            driver.FindElement(By.TagName(tagName)).SendKeys(Keys.Enter);
            driver.SwitchTo().DefaultContent();
        }
        public void topToolbar(string nameIToolBar,string []pnimi)
        {
            try { fp.click(By.XPath("//button/span[@class='tox-mbtn__select-label'][text()='" + nameIToolBar + "']"));
                if (pnimi != null)
                 for(int i=0;i<pnimi.Length;i++)
                        fp.click(By.XPath("//div[@class='tox-collection__item-label'][text()='"+pnimi[i]+"']"));
            } catch { throw new NotImplementedException(); }
        }

        private void DocumentStep1(string nameDocuminet, string nameAicon)
        {
            try { fp.checkElmentText("DocumentName", By.Name("DocumentName"), " שם מסמך ");
                fp.insertElment("DocumentName",By.Name("DocumentName"),nameDocuminet);
                fp.selectForRequerd("מקושר לטבלה",false,-1);
                fp.selectText(nameDocuminet);
                fp.selectForRequerd("סוג המסמך", false);
                fp.checkElmentText("aicon display",By.TagName( "mat-panel-title"), "בחר אייקון לתצוגה");
                CreateMesaur cm = new CreateMesaur();
                cm.clickNamePicter(nameAicon);
                fp.ClickButton("לשלב הבא");
            } catch { throw new NotImplementedException(); }
        }
        public void checkKiomDocuments(string valueDocumen, string iconDocument)
        {
            try
            {
                fp.checkElmentText("button documents", By.CssSelector("div[class='btn-pdf mat-raised-button ng-star-inserted']"), valueDocumen);
                fp.checkElmentText("icons", By.CssSelector("i[class='material-icons tool-icon']"), iconDocument);
            }
            catch { fp.picter("check kiom documents"); throw new NotImplementedException(); }
        }
    }
}