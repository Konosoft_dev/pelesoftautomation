﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PelesoftSelenuim
{
     class PrivateProfil:page
    {
        FunctionPelesoft fp = new FunctionPelesoft();
        public void changeName(string name, string numberPhone, string mail)
        {
            try {
                Thread.Sleep(300);
                changeElment("FirstName", name);
                changeElment("ContactPhoneNumber", numberPhone);
                changeElment("RestorePasswordEmail",mail);
                fp.click(By.CssSelector("button[mattooltip='שמירה']"));
            } catch { fp.picter("changeName in private profil"); throw new NotImplementedException(); }
        }

        private void changeElment(string byName, string name, bool requerd=false)
        {
            try {
                driver.FindElement(By.Name(byName)).Clear();
                if (requerd == true)
                    fp.funError(0);
                driver.FindElement(By.Name(byName)).SendKeys(name);
            } catch { fp.picter("changeElment in private profil"+name); throw new NotImplementedException(); }
        }

        public string changeColor(string nameColor)
        {
            string colorelect = "";
            try {
                systemDisaigen(" סוגי צבעים ");
                fp.click(By.Id(nameColor));
                fp.click(By.CssSelector("button[mattooltip='איפוס']"));
                fp.click(By.Id(nameColor));
                colorelect = driver.FindElement(By.Id(nameColor)).GetCssValue("background-color");
               // fp.click(By.XPath("//button//span/i[text()=' save ']"));
               systemDisaigen(" סוגי צבעים ");
            }
            catch { fp.picter("change color in private profil"); throw new NotImplementedException(); }
            return colorelect;
        }
        public void systemDisaigen(string nameChange)
        {
            IList<IWebElement> systemD = driver.FindElements(By.XPath("//mat-expansion-panel/mat-expansion-panel-header/span/mat-panel-title"));
            for (int i = 0; i < systemD.Count; i++)
             if (systemD[i].Text.Trim().Contains(nameChange.Trim()))
                    { 
                    systemD[i].Click();
                    break;
                } 
        }
        public  string changeWrote(string typeWrote)
        {
            string typeFont = "";
            try {
                Thread.Sleep(200);
                systemDisaigen(" סוגי כתבים ");
                string[] wores = { "font-Assistant", "font-MiriamLibre", "font-Rubik", "font-Heebo", "font-SegoeUI" };
                for (int i = 0; i < wores.Length; i++) 
                    if (driver.FindElement(By.Id(wores[i])).Text.Trim().Contains(typeWrote.Trim()))
                    { 
                        fp.click(By.Id(wores[i]));
                        typeFont= wores[i];
                        break;
                    }
                //fp.click(By.CssSelector("button[mattooltip='שמירה']"));
                systemDisaigen(" סוגי כתבים ");

                }
                catch { fp.picter("change wrote in private profil"); throw new NotImplementedException(); }
            return typeFont;
        }

        public void changeDisplayTable(bool displayRow, bool displayColumn)
        {
            try
            {
                systemDisaigen(" תצוגת טבלה ");
                clickCheckBox(1, "הצג פסי רוחב בטבלה", displayRow);
                clickCheckBox(0, "הצג פסי אורך בטבלה", displayColumn);
                fp.click(By.CssSelector("button[mattooltip='שמירה']"));
                systemDisaigen(" תצוגת טבלה ");
            }
            catch { fp.picter("change display table in private profil"); throw new NotImplementedException(); }

        }
        public void clickCheckBox(int mikom, string valueClick, bool display)
        {
            IList<IWebElement> highlightT = driver.FindElements(By.XPath("//div[@id='cols']/div[@class='form-field-wrapper']"));
            try
            {
                if (display == true)
                {
                    string valueCheckBox = highlightT[mikom].FindElement(By.TagName("mat-label")).Text;
                    if (valueCheckBox.Trim().Contains(valueClick))
                        highlightT[mikom].FindElement(By.TagName("mat-checkbox")).Click();
                }
            }
            catch { Debug.WriteLine(highlightT[mikom].FindElement(By.TagName("mat-label")).Text); }
        }

        public  void iconUser()
        {
            try { fp.click(By.ClassName("user-icon"));
                    }
            catch { throw new NotImplementedException(); }
        }

        public void checkChange(string userName, string color, string typeWord)
        {
            try { fp.checkElmentText("userName",By.ClassName("user-name"), userName);
                string colorType = driver.FindElement(By.ClassName("wrap-contact-btn")).GetCssValue("background-color"); 
                if (colorType!= color)
                { fp.picter("check change private profile the color not good "+color); }
                Thread.Sleep(400);
                string typeFont = driver.FindElements(By.ClassName("wrap-action-name"))[0].GetCssValue("font-family");
                char[] tav = { ','};
                if(typeFont.Split(tav)[0]!=typeWord)
                { fp.picter("check change private profile the word not good " + typeWord); }
            } catch { throw new NotImplementedException(); }
        }
        //אני אוחזת פה
        public void changeInTable(bool row, bool column,string color)
        {
            try {
                AddTable at = new AddTable();
                at.openTable(student,"הורים");
                IList<IWebElement> rows = driver.FindElements(By.TagName("mat-row"));
                string colorRo= rows[1].GetCssValue("border-bottom-color");
                string colorRow= rows[1].GetCssValue("border-left-color");
                if ((row==true && colorRo!=color)||(column==true && colorRow!= "rgba(0, 0, 0, 0.87)"))
                { Debug.WriteLine("changeInTable"); }
            }
            catch { throw new NotImplementedException(); }
        }
    }
}